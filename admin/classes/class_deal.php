<?php
class Deal extends Common
{
private $table="deal";
private $module="deal";
/////////////////////////////////////////////////////////////
function fun_add()
{
	global $img_obj;
	@extract($_POST);
	$id = '';
	$conid ='';
	if ($_REQUEST['id'] !='')
	{
		$id=$_REQUEST['id'];
		$conid="del_id =".$id;
		$remsg="Data has been successfully updated.";
	}
	else
	{
		$remsg="Data has been successfully submitted.";	
	}
	
	$image =$_FILES["del_image"]["name"];
	if ($image !="") 
	{
		$filename = stripslashes($_FILES['del_image']['name']);
		$ext = explode(".",$filename);
		$extension = strtolower(end($ext));
		if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
		{
			$msg="Unknown Image extension";
			$errors=1;
		}
		else
		{
			$tempname = $_FILES['del_image']['tmp_name'];
			$imgname = "del_".time().".".$extension;				
			//$folder = "../../../uploads/del_image/";
			$folder = "../../../img/";
			if (!file_exists($folder)) 
			{
				mkdir($folder, 0777, true);
			}
			$folder .= "del_image/";
			if (!file_exists($folder)) 
			{
				mkdir($folder, 0777, true);
			}
			$src = $img_obj->fun_insert_img($tempname,$newwidth,$newheight,$imgname,$extension,$folder);
			imagedestroy($src);
		}
	}
	
	
		$data=array();
		$data['del_title'] =$del_title;
		$data['del_price'] =$del_price;
		$data['del_min_passenger'] =$del_min_passenger;
		$data['del_fdcid'] =$del_fdcid;
		$data['del_release_date'] =strtotime($del_release_date);
		$data['del_expiry_date'] =strtotime($del_expiry_date);
		$data['del_short_description'] =mysql_real_escape_string($del_short_description);
		$data['del_description'] =mysql_real_escape_string($del_description);
		$data['del_exclusion'] =mysql_real_escape_string($del_exclusion);
		$data['del_inclusion'] =mysql_real_escape_string($del_inclusion);
		$data['del_custom'] =mysql_real_escape_string($del_custom);
		$data['del_image'] =$imgname;
		$data['del_status'] =$del_status;
		//print_r($data);
		//exit;
		
		$fields = '';
		$cnt=1;
		
		foreach($data as $key=>$values)
		{
			$fields .=$key."='".$values."'";
			if($cnt<count($data))
			{
				$fields .=",";
				$cnt++;
			}
		}
		$lid=$this->fun_save($this->table,$fields,$conid);
		if($lid >0)
		{
			header ("location:index.php?msg=".$remsg);
		}
		else
		{
			if($_REQUEST['id']!='')
			{
				$msg="You have not changed in any fields.Click cancel button and go back.";	
			}
		}
		
		return $msg;
	}
//////////////////////////////////////////////////////////////////////
	function fun_change_status($id,$status)
	{
		if($status=='1')
		{
			$fields="del_status=0";
		}
		elseif($status=='0')
		{
		$fields="del_status=1";	
		}
			$conid="del_id=".$id;
			
			
			$lid = $this->fun_save($this->table,$fields,$conid);
			if($lid >0)
			{
				header ("location:index.php");	
			}
		
	
	}
	
	function fun_delete($id,$ctype)
	{
		$row = $this->fun_save($this->table," is_delete = '1'"," del_id = ".$id);
		if(0<$row)
		{
			$_SESSION["flush"] = "Record Successfully Deleted";
		}
		else
		{
			$_SESSION["flush"] = "Unable to Delete";
		}
		header("location:".ROOT_VIEW.$this->module);
	}
	
///////////////////////////////////////////////////////////////////////////
}
$deal_obj = new Deal();

?>