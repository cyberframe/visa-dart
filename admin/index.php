<?php 
	ob_start();
	session_start();
	require_once "../config/general.php"; 
?>
<?php 
    require_once "../config/class_db.php"; 
    require_once("../class/class_common.php");
?>
<?php 
    require_once "classes/class_login.php"; 
?>
<?php
if(isset($_POST['submit']))
{
	$msg=$login_obj->login();
}
?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Admin</title>

      
        <meta name="viewport" content="width=device-width,initial-scale=1">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo ROOT_ADMIN;?>img/favicon.ico">
        <link rel="apple-touch-icon" href="<?php echo ROOT_ADMIN;?>img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="<?php echo ROOT_ADMIN;?>img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="<?php echo ROOT_ADMIN;?>img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="<?php echo ROOT_ADMIN;?>img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="<?php echo ROOT_ADMIN;?>img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="<?php echo ROOT_ADMIN;?>img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="<?php echo ROOT_ADMIN;?>img/icon152.png" sizes="152x152">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- The roboto font is included from Google Web Fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,400italic,700,700italic">

        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="<?php echo ROOT_ADMIN;?>css/bootstrap.css">

        <!-- Related styles of various javascript plugins -->
        <link rel="stylesheet" href="<?php echo ROOT_ADMIN;?>css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="<?php echo ROOT_ADMIN;?>css/main.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (Browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support them) -->
        <script src="<?php echo ROOT_ADMIN;?>js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>

    <body class="login">
        <!-- Login Container -->
        <div id="login-container">
            <div id="login-logo">
                <a href="">
                    <img src="<?php echo ROOT_ADMIN;?>img/template/uadmin_logo.png" alt="logo">
                </a>
                <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            </div>

          

            <!-- Login Form -->
            <form id="login-form" action="" method="post" style="display: block;" class="form-horizontal">
               
                <div class="form-group">
                    <div class="input-group col-xs-12">
                        <input type="text" id="login-email" required name="userName" placeholder="Email.." class="form-control">
                        <span class="input-group-addon"><i class="icon-envelope-alt icon-fixed-width"></i></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group col-xs-12">
                        <input type="password" id="login-password" required name="userPassword" placeholder="Password.." class="form-control">
                        <span class="input-group-addon"><i class="icon-asterisk icon-fixed-width"></i></span>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="btn-group btn-group-sm pull-right">
                       
                        <button type="submit" name="submit" class="btn btn-success"><i class="icon-arrow-right"></i> Login</button>
                    </div>
                   
                </div>
            </form>
            <!-- END Login Form -->
        </div>
        <!-- END Login Container -->

        <!-- Get Jquery library from Google but if something goes wrong get Jquery from local file - Remove 'http:' if you have SSL -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(unescape('%3Cscript src="js/vendor/jquery-1.9.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js -->
        <script src="<?php echo ROOT_ADMIN;?>js/vendor/bootstrap.min.js"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="<?php echo ROOT_ADMIN;?>js/plugins.js"></script>
        <script src="<?php echo ROOT_ADMIN;?>js/main.js"></script>

       
    </body>
</html>