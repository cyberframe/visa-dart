$(document).ready(function() {
    var benIncome = $.trim($("#ben-income").val());
   //console.log(benIncome.length);
    if (benIncome.length == 0) {
        $('#bpl').hide();
        $("#incomeproof").hide();
    }
    if (benIncome.length != 0) {
        var Income = $("#ben-income").val();
        //console.log(Income);
        if (Income == 'BPL') {
            $("#bpl").show();
            $("#incomeproof").hide();
        }
        if (Income == 'UPTO 15000 P.M.') {
            $("#incomeproof").show();
            $("#bpl").hide();
        }
        if (Income == 'More than 15000 P.M.') {
            $('#bpl').hide();
            $("#incomeproof").hide();
        }
    }
    $('#ben-income').change(function() {

        var Income = $("#ben-income").val();
        if (Income.length == 0) {
            $('#bpl').hide();
            $("#incomeproof").hide();
        }
       // console.log(Income.length);
        if (Income == 'BPL') {
            $("#bpl").show();
            $("#incomeproof").hide();
        }
        if (Income == 'UPTO 15000 P.M.') {
            $("#incomeproof").show();
            $("#bpl").hide();
        }
        if (Income == 'More than 15000 P.M.') {
            $('#bpl').hide();
            $("#incomeproof").hide();
        }
    });

})