/*
 *  Document   : main.js
 *  Author     : pixelcave
 *  Description: Custom scripts and plugin initializations
 */

var webApp = function() {

    /* Initialization UI Code */
    var uiInit = function() {



        // Set min-height to #page-content, so that footer is visible at the bottom if there is not enough content
        var pageContent = $('#page-content');

        pageContent.css('min-height', $(window).height() -
                ($('header').height() + $('footer').outerHeight()) + 'px');

        $(window).resize(function() {
            pageContent.css('min-height', $(window).height() -
                    ($('header').height() + $('footer').outerHeight()) + 'px');
        });

        // If .navbar-fixed class added to <header>, add padding to the page-container
        if ($('header').hasClass('navbar-fixed-top')) {
            $('#page-container').addClass('header-fixed-top');
        } else if ($('header').hasClass('navbar-fixed-bottom')) {
            $('#page-container').addClass('header-fixed-bottom');
        }

        // Select/Deselect all checkboxes in tables
        $('thead input:checkbox').click(function() {
            var checkedStatus = $(this).prop('checked');
            var table = $(this).closest('table');

            $('tbody input:checkbox', table).each(function() {
                $(this).prop('checked', checkedStatus);
            });
        });

        // Initialize tabs
        $('[data-toggle="tabs"] a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });

        // Gallery hover options functionality
        $('[data-toggle="gallery-options"] > li')
                .mouseover(function() {
                    $(this).find('.thumbnails-options').show();
                })
                .mouseout(function() {
                    $(this).find('.thumbnails-options').hide();
                });

        // Initialize Slimscroll
        $('.scrollable').slimScroll({height: '100px', size: '3px', touchScrollStep: 750});
        $('.scrollable-tile').slimScroll({height: '130px', size: '3px', touchScrollStep: 750});
        $('.scrollable-tile-2x').slimScroll({height: '330px', size: '3px', touchScrollStep: 750});

        // Initialize Tooltips
        $('[data-toggle="tooltip"]').tooltip({container: 'body', animation: false});

        // Initialize Popovers
        $('[data-toggle="popover"]').popover({container: 'body', animation: false});

        // Initialize Chosen
        $('.select-chosen').chosen();


       


    };

    /* Primary navigation functionality */
    var primaryNav = function() {

        // Get all primary navigation top links
        var menuLinks = $('#primary-nav > ul > li > a');

        // Initialize submenu number indicators
        menuLinks.filter(function() {
            return $(this).next().is('ul');
        }).each(function(n, e) {
            $(e).append('<span>' + $(e).next('ul').children().length + '</span>');
        });

        // Accordion functionality
        menuLinks.click(function() {

            var link = $(this);

            if (link.next('ul').length > 0) {
                if (link.parent().hasClass('active') !== true) {
                    if (link.hasClass('open')) {
                        link.removeClass('open').next().slideUp(250);
                    }
                    else {
                        $('#primary-nav li > a.open').removeClass('open').next().slideUp(250);
                        link.addClass('open').next().slideDown(250);
                    }
                }

                return false;
            }

            return true;
        });
    };

    /* Scroll to top link */
    var scrollToTop = function() {

        // Get link
        var link = $('#to-top');

        $(window).scroll(function() {

            // If the user scrolled a bit (150 pixels) show the link
            if ($(this).scrollTop() > 150) {
                link.fadeIn(150);
            } else {
                link.fadeOut(150);
            }
        });

        // On click get to top
        link.click(function() {
            $("html, body").animate({scrollTop: 0}, 300);
            return false;
        });
    };

   

    /* Input placeholder for older browsers */
    var oldiePlaceholder = function() {

        // Check if placeholder feature is supported by browser
        if (!Modernizr.input.placeholder) {

            // If not, add the functionality
            $('[placeholder]').focus(function() {
                var input = $(this);
                if (input.val() === input.attr('placeholder')) {
                    input.val('');
                    input.removeClass('ph');
                }
            }).blur(function() {
                var input = $(this);
                if (input.val() === '' || input.val() === input.attr('placeholder')) {
                    input.addClass('ph');
                    input.val(input.attr('placeholder'));
                }
            }).blur().parents('form').submit(function() {
                $(this).find('[placeholder]').each(function() {
                    var input = $(this);
                    if (input.val() === input.attr('placeholder')) {
                        input.val('');
                    }
                });
            });
        }
    };

    /* Datatables Bootstrap integration */
    var dtIntegration = function() {

        // Set the defaults for DataTables initialization
        $.extend(true, $.fn.dataTable.defaults, {
            "sDom": "<'row'<'col-sm-6 col-xs-5'l><'col-sm-6 col-xs-7'f>r>t<'row'<'col-sm-5 hidden-xs'i><'col-sm-7 col-xs-12 clearfix'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_",
                "sSearch": "<div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"icon-search\"></i></span>_INPUT_</div>",
                "sInfo": "<strong>_START_</strong>-<strong>_END_</strong> of <strong>_TOTAL_</strong>",
                "oPaginate": {
                    "sPrevious": "",
                    "sNext": ""
                }
            }
        });

        // Default class modification
        $.extend($.fn.dataTableExt.oStdClasses, {
            "sWrapper": "dataTables_wrapper form-inline"
        });

        // API method to get paging information
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        // Bootstrap style pagination control
        $.extend($.fn.dataTableExt.oPagination, {
            "bootstrap": {
                "fnInit": function(oSettings, nPaging, fnDraw) {
                    var oLang = oSettings.oLanguage.oPaginate;
                    var fnClickHandler = function(e) {
                        e.preventDefault();
                        if (oSettings.oApi._fnPageChange(oSettings, e.data.action)) {
                            fnDraw(oSettings);
                        }
                    };

                    $(nPaging).append(
                            '<ul class="pagination pagination-sm remove-margin">' +
                            '<li class="prev disabled"><a href="javascript:void(0)"><i class="icon-chevron-left"></i> ' + oLang.sPrevious + '</a></li>' +
                            '<li class="next disabled"><a href="javascript:void(0)">' + oLang.sNext + ' <i class="icon-chevron-right"></i></a></li>' +
                            '</ul>'
                            );
                    var els = $('a', nPaging);
                    $(els[0]).bind('click.DT', {action: "previous"}, fnClickHandler);
                    $(els[1]).bind('click.DT', {action: "next"}, fnClickHandler);
                },
                "fnUpdate": function(oSettings, fnDraw) {
                    var iListLength = 5;
                    var oPaging = oSettings.oInstance.fnPagingInfo();
                    var an = oSettings.aanFeatures.p;
                    var i, j, sClass, iStart, iEnd, iHalf = Math.floor(iListLength / 2);

                    if (oPaging.iTotalPages < iListLength) {
                        iStart = 1;
                        iEnd = oPaging.iTotalPages;
                    }
                    else if (oPaging.iPage <= iHalf) {
                        iStart = 1;
                        iEnd = iListLength;
                    } else if (oPaging.iPage >= (oPaging.iTotalPages - iHalf)) {
                        iStart = oPaging.iTotalPages - iListLength + 1;
                        iEnd = oPaging.iTotalPages;
                    } else {
                        iStart = oPaging.iPage - iHalf + 1;
                        iEnd = iStart + iListLength - 1;
                    }

                    for (i = 0, iLen = an.length; i < iLen; i++) {
                        // Remove the middle elements
                        $('li:gt(0)', an[i]).filter(':not(:last)').remove();

                        // Add the new list items and their event handlers
                        for (j = iStart; j <= iEnd; j++) {
                            sClass = (j === oPaging.iPage + 1) ? 'class="active"' : '';
                            $('<li ' + sClass + '><a href="javascript:void(0)">' + j + '</a></li>')
                                    .insertBefore($('li:last', an[i])[0])
                                    .bind('click', function(e) {
                                        e.preventDefault();
                                        oSettings._iDisplayStart = (parseInt($('a', this).text(), 10) - 1) * oPaging.iLength;
                                        fnDraw(oSettings);
                                    });
                        }

                        // Add / remove disabled classes from the static elements
                        if (oPaging.iPage === 0) {
                            $('li:first', an[i]).addClass('disabled');
                        } else {
                            $('li:first', an[i]).removeClass('disabled');
                        }

                        if (oPaging.iPage === oPaging.iTotalPages - 1 || oPaging.iTotalPages === 0) {
                            $('li:last', an[i]).addClass('disabled');
                        } else {
                            $('li:last', an[i]).removeClass('disabled');
                        }
                    }
                }
            }
        });
    };

    return {
        init: function() {
            uiInit(); // Initialize UI Code
            primaryNav(); // Primary Navigation functionality
            scrollToTop(); // Scroll to top functionality
            oldiePlaceholder(); // Make input placeholder work in older browsers
            dtIntegration(); // Datatables Bootstrap integration
        }
    };
}();

/* Initialize WebApp when page loads */
$(function() {
    webApp.init();
});