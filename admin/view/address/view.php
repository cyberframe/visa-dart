<?php require_once("../../applicationtop.php"); $page="address";?>

<?php
	$cond = " add_id='".$_REQUEST['id']."' && is_delete=0";
	$rows= $common_obj->fun_select("address",$cond);
?>
<?php
require '../include/header.php';
?>
    <script>
$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			add_phone:"required",
			add_address:{required:true},
			add_email:{required:true , email:true},
			add_google:{required:true},
			add_facebook:{required:true},
			add_twitter:{required:true},
			},
	
	});
	});
</script>
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
             <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">Address</a></li>
            <li class="active">
            <a href="">View</a>
            </li>
            </ul>
            <h3 class="page-header">
            View <a href="index.php" class="btn btn-info pull-right">Go Back </a>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            <div class="form-box-content">   
            
                       <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Phone No. </label>
                    <div class="col-md-4">
                    <?php echo $rows[0]['add_title']; ?>
                    </div>
                    </div> 
					
					<div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Phone No. </label>
                    <div class="col-md-4">
                    <?php echo $rows[0]['add_phone']; ?>
                    </div>
                    </div> 
                    
                     <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Email</label>
                    <div class="col-md-4">
                   <?php echo $rows[0]['add_email']; ?>
                    </div>
                    </div> 
                    
                     <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Mobile</label>
                    <div class="col-md-4">
                   <?php echo $rows[0]['add_google']; ?>
                    </div>
                    </div> 
                    <?php /*?>
                     <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Facebook Link</label>
                    <div class="col-md-4">
                    <?php echo $rows['add_facebook']; ?>
                    </div>
                    </div> 
                    
                     <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Twitter Link</label>
                    <div class="col-md-4">
                   <?php echo $rows['add_twitter']; ?>
                    </div>
                    </div> 
                    
                     <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Youtube Link </label>
                    <div class="col-md-4">
                    <?php echo $rows['add_youtube']; ?> 
                    </div>
                    </div> 
                    
                    
                     <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Vimeo Link </label>
                    <div class="col-md-4">
                    <?php echo $rows['add_vimeo']; ?> 
                    </div>
                    </div> 
                    
                    
                     <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Instagram Link </label>
                    <div class="col-md-4">
                     <?php echo $rows['add_instagram']; ?> 
                    </div>
                    </div> <?php */?>
                    
                    <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Fax No.<samp style="color:#FF0000;">*</samp></label>
                    <div class="col-md-4">
                <?php echo $rows[0]['add_faxno']; ?>
                    </div>
                    </div> 
                    
                      <?php /*?><div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Flickr Link<samp style="color:#FF0000;">*</samp></label>
                    <div class="col-md-4">
                   <?php echo $rows['add_flickr']; ?> 
                    </div>
                    </div> <?php */?>
                    
                    <?php ?><div id="imagehideidinedit" class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Current Image</label>
                    <div class="col-md-4">
                   <?php 
				if($rows[0]['add_image']!='') { ?>
                <img src="<?php echo ROOT; ?>uploads/address/<?php echo $rows[0]['add_image'];?>" width="150" height="40">
                <?php } ?>
                    
                    </div>
                    </div><?php ?>
                    
                    <div class="form-group">
            
                                <label class="control-label col-md-2" for="example-textarea-large">Address </label>
                                <div class="col-md-6">
                                 <?php echo $rows[0]['add_address']; ?>
                                
                            </div>
                            </div>
							
                            <div class="clearfix"></div>
			 
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->
</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>