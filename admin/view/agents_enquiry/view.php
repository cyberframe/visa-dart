<?php require_once("../../applicationtop.php"); $page="agents_enquiry";?>

<?php
	$cond = " agn_id='".$_REQUEST['id']."' && is_delete=0";
	$rows = $common_obj->fun_select("agents_enquiry",$cond);
	if($_REQUEST['id']!='')
	{
	$_POST=$rows;		
	}
	else
	{
	$_POST=$_POST;
	}
	?>

<?php
require '../include/header.php';
?>

            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">Agents Enquiry</a></li>
            <li class="active">
            <a href="">View</a>
            </li>
            </ul>
            <h3 class="page-header">
          View <a href="index.php" class="btn btn-info pull-right">Go Back </a>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            <div class="form-box-content">   
                    
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Company Name</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_company_name']; ?>
                        </div>
                        </div>
                        
                       
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Type Of Company</label>
                        <div class="col-md-4">
                        
                        <?php
						if($_POST[0]['agn_typeof_company'] == 1){echo "Proprietor"; }else{ echo "Pvt. Ltd"; }
						?>
                        </div>
                        </div>
                        
                        
                         <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Incorporation Date</label>
                        <div class="col-md-4">
                        <?php echo date("d M Y",$_POST[0]['agn_incorporation_date']); ?>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Contact Person Name</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_contact_person_name']; ?>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Designation</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_designation']; ?>
                        </div>
                        </div>
                        
                         
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Landline No.</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_landline_no']; ?>
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Mobile No.</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_mobile_no']; ?>
                        </div>
                        </div>
                        
                        
                         <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Pin Code</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_pin_code']; ?>
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Country</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_country']; ?>
                        </div>
                        </div>
                        
                        
                         <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">State</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_state']; ?>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">City</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_city']; ?>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">TDS Details</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_tds_details']; ?>
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Company Pan</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_company_pan']; ?>
                        </div>
                        </div>
                        
                        
                         <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Service Tax Regn No.</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_service_tax_regn_no']; ?>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Account Dept.</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_account_dept']; ?>
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Other Details</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_other_details']; ?>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Status</label>
                        <div class="col-md-4">
                        <?php if($_POST[0]['agn_status']==0){echo "Inactive";}else{echo "Active";} ?>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Insert Date</label>
                        <div class="col-md-4">
                        <?php echo date("d M Y",$_POST[0]['agn_insert_date']); ?>
                        </div>
                        </div>
                        
                        
                        
                         <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Address</label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['agn_address']; ?>
                        </div>
                        </div>
                        
                        
                        
               
                </div>            
			  <div class="clearfix"></div>
			 
           
         
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->


</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>