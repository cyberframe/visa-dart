<?php require_once("../../applicationtop.php"); $page="applicant_details";?>
<?php
	if (isset($_POST['submit']))
	{
		$msg=$applicant_details_obj->fun_add();
	}
?>
<?php
	if($_REQUEST['id'] != '')
	{
		$sql=" ad_id='".$_REQUEST["id"]."'";
		$resultselect= $common_obj->fun_select("applicant_details",$sql);
		foreach($resultselect as $row)
		{
			$ad_first_name 			=	$row["ad_first_name"];
			$ad_last_name 		    = 	$row["ad_last_name"];
			$ad_userid 				=	$row["ad_userid"];
			$ad_dob 				=	$row["ad_dob"];
			$ad_photo 				=	$row["ad_photo"];
			$ad_religion 			=	$row["ad_religion"];
			$ad_nationality 		=	$row["ad_nationality"];
			$ad_occupation 			=	$row["ad_occupation"];
			$ad_education_qualification =	$row["ad_education_qualification"];
			$ad_passport_type 			=	$row["ad_passport_type"];
			$ad_passport_no 			=	$row["ad_passport_no"];
			$ad_date_of_issue 			=	$row["ad_date_of_issue"];
			$ad_expiry_date 			=	$row["ad_expiry_date"];
			$ad_passport_front_page 	=	$row["ad_passport_front_page"];
			$ad_passport_back_type 		=	$row["ad_passport_back_type"];
			$ad_status 					=	$row["ad_status"];
			$ad_air_ticket 				=	$row["ad_air_ticket"];
			$ad_other_doc 				=	$row["ad_other_doc"];
			$ad_airticket_file 			=	$row["ad_airticket_file"];
			$ad_airticket_second_file 			=	$row["ad_airticket_second_file"];
			$ad_file1 				=	$row["ad_file1"];
			$ad_file2 				=	$row["ad_file2"];
			$ad_file3 				=	$row["ad_file3"];
			$ad_file4 				=	$row["ad_file4"];
			$ad_file5 				=	$row["ad_file5"];
		}
	}
	?>
    

<?php
require '../include/header.php';
?>
<script>
function hideimage(value)
{
if(value==1)
{
	$(".forhide").removeClass("hide");
	//$("#airtct1").removeClass("hide");
}
if(value==0)
{
	$(".forhide").addClass("hide");
	//$("#airtct1").addClass("hide");
}
}

$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			fdc_name:{required:true, maxlength:25},
			ad_passport_type:{required:true},
			},
	});
	});
</script>	  
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php<?php if($_REQUEST['uid']!=''){echo "?uid=".$_REQUEST['uid']."";} ?>">Applicant Details</a></li>
            <li class="active">
            <a href="<?php if($_REQUEST['uid']!=''){echo "?uid=".$_REQUEST['uid']."&id=".$_REQUEST['id']."";} ?>"><?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?></a>
            </li>
            </ul>
            <h3 class="page-header">
           <?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            <div class="form-box-content">   
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">First Name <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter First Name" class="form-control req" id="ad_first_name"  name="ad_first_name" value="<?php echo $ad_first_name; ?>" >
                </div>
                </div>
                
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Last Name <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Last Name " class="form-control req" id="ad_last_name"  name="ad_last_name" value="<?php echo $ad_last_name; ?>" >
                </div>
                </div>
                
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">User <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <select id="ad_userid" name="ad_userid" class="form-control req">
                <option  value="">Select User</option>
                <?php
				$conduser=" is_delete=0";
				$selectuser=$common_obj->fun_select("user_details",$conduser);
				foreach($selectuser as $getuser)
				{
				?>
                <option <?php if($ad_userid==$getuser['u_id']){echo "selected='selected'";} ?> value="<?php echo  $getuser['u_id'];?>"><?php echo  $getuser['u_name'];?></option>
                <?php } ?>
                
                </select>
                </div>
                </div> 
                
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Religion <span style="color:#F00;">*</span></label>
                
                <div class="col-md-4">
                <select id="ad_religion" name="ad_religion" class="form-control req">
                <option  value="">Select Religion</option>
                <?php 
				foreach($religionarray as $relkey => $religionget)
				{
				?>
                <option <?php if($ad_religion==$religionget){echo "selected='selected'";} ?> value="<?php echo  $religionget; ?>"><?php echo  $relkey; ?></option>
                <?php } ?>
                </select>
                </div>
                </div> 
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Passport Type <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <select id="ad_passport_type" name="ad_passport_type" class="form-control reql">
                <option  value="">Type</option>
                 <?php //echo $ad_passport_type; die; ?>
                <option <?php if($ad_passport_type==1 || $ad_passport_type == "Normal"){echo "selected='selected'";} ?> value="1">Normal</option>
				
                
                </select>
                </div>
                </div> 
                <?php 
				if($_REQUEST['id']!='')
				{
				?>
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Date Of Birth <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="dd/mm/yyyy" class="form-control datepicker req" id="ad_dob"  name="ad_dob" value="<?php echo date("d-m-Y",$ad_dob); ?>" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Date Of Issue <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="dd/mm/yyyy" class="form-control datepicker req" id="ad_date_of_issue"  name="ad_date_of_issue" value="<?php echo date("d-m-Y",$ad_date_of_issue); ?>" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Expiry Date <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="dd/mm/yyyy" class="form-control datepicker req" id="ad_expiry_date"  name="ad_expiry_date" value="<?php echo date("d-m-Y",$ad_expiry_date); ?>" >
                </div>
                </div>
                <?php } else{ ?>
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Date Of Birth <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="dd/mm/yyyy" class="form-control datepicker req" id="ad_dob"  name="ad_dob" value="" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Date Of Issue <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="dd/mm/yyyy" class="form-control datepicker req" id="ad_date_of_issue"  name="ad_date_of_issue" value="" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Expiry Date <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="dd/mm/yyyy" class="form-control datepicker req" id="ad_expiry_date"  name="ad_expiry_date" value="" >
                </div>
                </div>
                <?php } ?>
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Nationality <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  readonly class="form-control req" id="ad_nationality"  name="ad_nationality" value="India" >
                </div>
                </div>
                
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Occupation <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Occupation" class="form-control req" id="ad_occupation"  name="ad_occupation" value="<?php echo $ad_occupation; ?>" >
                </div>
                </div>
                
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Education Qualification <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Education Qualification" class="form-control req" id="ad_education_qualification"  name="ad_education_qualification" value="<?php echo $ad_education_qualification; ?>" >
                </div>
                </div>
                
                
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Passport No. <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Passport No." class="form-control req" id="ad_passport_no"  name="ad_passport_no" value="<?php echo $ad_passport_no; ?>" >
                </div>
                </div>
                
                 
                
                 <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Status</label>
                    <div class="col-md-4">
                    <div class="radio radio-inline">
                    <label for="example-radio-inline1">
                    <input type="radio" id="ad_status" <?php if($ad_status==1){echo "checked='checked'";}?> name="ad_status" value="1"> Active
                    </label>
                    </div>
                    <div class="radio radio-inline">
                    <label for="example-radio-inline2">
                    <input type="radio" id="ad_status" <?php if($ad_status==0){echo "checked='checked'";}?> name="ad_status" value="0"> Inactive
                    </label>
                    </div>
                    </div>
                    </div>
                    
                     <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Air Ticket</label>
                    <div class="col-md-4">
                    <div class="radio radio-inline">
                    <label for="example-radio-inline1">
                    <input onChange="hideimage(this.value);" type="radio" id="ad_air_ticket" <?php if($ad_air_ticket==1){echo "checked='checked'";}?> name="ad_air_ticket" value="1"> Yes
                    </label>
                    </div>
                    <div class="radio radio-inline">
                    <label for="example-radio-inline2">
                    <input onChange="hideimage(this.value);" type="radio" id="ad_air_ticket" <?php if($ad_air_ticket==0){echo "checked='checked'";}?> name="ad_air_ticket" value="0"> No
                    </label>
                    </div>
                    </div>
                    </div>
                
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Passport Front Page</label>
				
				<div class="col-md-4">
                    <input id="uploadFile" class="" type="file" name="ad_passport_front_page"  >
                    <input value="<?php echo $ad_passport_front_page;?>" type="hidden" name="oldimage2"  >
				</div>
                	</div>
                     <div id="imagehideid" class="form-group hide">
                    <label class="control-label col-md-2" for="example-input-normal">Selected</label>
                    <div class="col-md-4 " >
                    <div class=" imagePreview " id="imagePreview">
                    </div>
                    </div>
                    </div>
                    <?php if($_REQUEST['id']!='') { ?>
                    <div id="imagehideidinedit" class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Current</label>
                    <div class="col-md-4">
                     <?php //if($_POST['ad_passport_front_page']!=''){
						$e=end(explode(".",$ad_passport_front_page));
						
						if($e=='pdf'){
							?>
							 <img src="<?php echo ROOT;?>/images/pdf.png" width="150" height="40">
                        <?php
						}
						else
						{
						?>
                   <?php 
				//if($_POST['ad_passport_front_page']!='') { ?>
                <img src="../../../uploads/applicant/<?php echo $ad_passport_front_page;?>" width="150" height="40">
                <?php } ?>
                    </div>
                    
                    </div>
                   <?php } ?> 
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Passport Back Type</label>
				
				<div class="col-md-4">
                    <input id="uploadFile" class="" type="file" name="ad_passport_back_type"  >
                    <input value="<?php echo $ad_passport_back_type;?>" type="hidden" name="oldimage3"  >
				</div>
                	</div>
                     <div id="imagehideid" class="form-group hide">
                    <label class="control-label col-md-2" for="example-input-normal">Selected</label>
                    <div class="col-md-4 " >
                    <div class=" imagePreview " id="imagePreview">
                    </div>
                    </div>
                    </div>
                    <?php if($_REQUEST['id']!='') { ?>
                    <div id="imagehideidinedit" class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Current</label>
                    <div class="col-md-4">
                     <?php //if($_POST['ad_passport_front_page']!=''){
						$e=end(explode(".",$ad_passport_back_type));
						
						if($e=='pdf'){
							?>
							 <img src="<?php echo ROOT;?>/images/pdf.png" width="150" height="40">
                        <?php
						}
						else
						{
						?>
                   <?php 
				//if($_POST['ad_passport_front_page']!='') { ?>
                <img src="../../../uploads/applicant/<?php echo $ad_passport_back_type;?>" width="150" height="40">
                <?php } ?>
                    </div>
                    
                    </div>
                   <?php } ?> 
                 
                   <div class="form-group forhide  <?php if($_REQUEST['id']!=''){if($ad_air_ticket==1){echo "";} else{echo "hide";}}else{echo "hide";}?>">
                <label class="control-label col-md-2" for="example-input-normal">Air Ticket File</label>
				
				<div class="col-md-4">
                    <input id="uploadFile" class="" type="file" name="ad_airticket_file"  >
                    <input value="<?php echo $ad_airticket_file;?>" type="hidden" name="oldimage4"  >
				</div>
                	
                     <div id="imagehideid" class="form-group  hide">
                    <label class="control-label col-md-2" for="example-input-normal">Selected</label>
                    <div class="col-md-4 " >
                    <div class=" imagePreview " id="imagePreview">
                    </div>
                    </div>
                    </div>
                    </div>
                    <?php if($_REQUEST['id']!='') { ?>
                    <div id="imagehideidinedit" class="form-group forhide <?php if($_REQUEST['id']!=''){if($_POST['ad_air_ticket']==1){echo "";} else{echo "hide";}}else{echo "hide";}?>">
                    <label class="control-label col-md-2" for="example-input-normal">Current</label>
                    <div class="col-md-4">
                    <?php //if($_POST['ad_passport_front_page']!=''){
						$e=end(explode(".",$ad_airticket_file));
						
						if($e=='pdf'){
							?>
							 <img src="<?php echo ROOT;?>/images/pdf.png" width="150" height="40">
                        <?php
						}
						else
						{
						?>
                   <?php 
				//if($_POST['ad_passport_front_page']!='') { ?>
                <img src="../../../uploads/applicant/<?php echo $ad_airticket_file;?>" width="150" height="40">
                <?php } ?>
                   
                    </div>
                    </div>
                   <?php } ?> 
                    <div class="form-group forhide  <?php if($_REQUEST['id']!=''){if($ad_air_ticket==1){echo "";} else{echo "hide";}}else{echo "hide";}?>">
                <label class="control-label col-md-2" for="example-input-normal">Air Ticket Second File</label>
				
				<div class="col-md-4">
                    <input id="uploadFile" class="" type="file" name="ad_airticket_second_file"  >
                    <input value="<?php echo $ad_airticket_second_file;?>" type="hidden" name="oldimage5"  >
				</div>
                
                	
                     <div id="imagehideid" class="form-group hide">
                    <label class="control-label col-md-2" for="example-input-normal">Selected</label>
                    <div class="col-md-4 " >
                    <div class=" imagePreview " id="imagePreview">
                    </div>
                    </div>
                    </div>
                    </div>
                    <?php if($_REQUEST['id']!='') { ?>
                    <div id="imagehideidinedit" class="form-group forhide <?php if($_REQUEST['id']!=''){if($ad_air_ticket==1){echo "";} else{echo "hide";}}else{echo "hide";}?>">
                    <label class="control-label col-md-2" for="example-input-normal">Current</label>
                    <div class="col-md-4">
                    <?php //if($_POST['ad_passport_front_page']!=''){
						$e=end(explode(".",$ad_airticket_second_file));
						
						if($e=='pdf'){
							?>
							 <img src="<?php echo ROOT;?>/images/pdf.png" width="150" height="40">
                        <?php
						}
						else
						{
						?>
                   <?php 
				//if($_POST['ad_passport_front_page']!='') { ?>
                <img src="../../../uploads/applicant/<?php echo $ad_airticket_second_file;?>" width="150" height="40">
                <?php } ?>
                    </div>
                    </div>
                   <?php } ?> 
                   
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Photo</label>
				
				<div class="col-md-4">
                    <input id="uploadFile" class="" type="file" name="ad_photo"  >
                    <input value="<?php echo $ad_photo;?>" type="hidden" name="oldimage1"  >
				</div>
                	</div>
                     <div id="imagehideid" class="form-group hide">
                    <label class="control-label col-md-2" for="example-input-normal">Selected</label>
                    <div class="col-md-4 " >
                    <div class=" imagePreview " id="imagePreview">
                    </div>
                    </div>
                    </div>
                    <?php if($_REQUEST['id']!='') { ?>
                    <div id="imagehideidinedit" class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Current</label>
                    <div class="col-md-4">
                   <?php 
				if($ad_photo !='') { ?>
                <img src="<?php echo ROOT.'uploads/applicant/'.$ad_photo ; ?>" width="150" height="40">
                <?php } ?>
                    </div>
                    </div>
                   <?php } ?> 
				
                            </div>
			  <div class="clearfix"></div>
			 <div class="form-actions">
                                <div class="col-md-offset-2">
                                <button class="btn btn-success "  name="submit" type="submit"><i class="icon-save"></i> Save</button>
									  <a href="index.php<?php if($_REQUEST['uid']!=''){echo "?uid=".$_REQUEST['uid']."";} ?>" class="btn btn-danger"><i class="icon-remove"></i> Cancel</a>
									
                                   
                                </div>
                            </div>
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->


</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>