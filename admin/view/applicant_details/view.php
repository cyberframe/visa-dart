<?php require_once("../../applicationtop.php"); $page="applicant_details";?>
<?php
	if (isset($_POST['submit']))
	{
		$msg=$applicant_details_obj->fun_add();
	}
?>
<?php
	$cond = " ad_id='".$_REQUEST['id']."' && is_delete=0";
	$rows = $common_obj->fun_select("applicant_details",$cond);
	if($_REQUEST['id']!='')
	{
	$_POST=$rows;		
	}
	else
	{
	$_POST=$_POST;
	}
	?>

<?php
require '../include/header.php';
?>
<script>

$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			fdc_name:{required:true, maxlength:25},
			fdc_type:{required:true},
			},
	});
	});
</script>	  
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php<?php if($_REQUEST['uid']!=''){echo "?uid=".$_REQUEST['uid']."";} ?>">Applicant Details</a></li>
            <li class="active">
            <a href="<?php if($_REQUEST['uid']!=''){echo "?uid=".$_REQUEST['uid']."&id=".$_REQUEST['id']."";} ?>">View</a>
            </li>
            </ul>
            <h3 class="page-header">
           View <a href="index.php<?php if($_REQUEST['uid']!=''){echo "?uid=".$_REQUEST['uid']."";} ?>" class="btn btn-info pull-right">Go Back </a>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            <div class="form-box-content">   
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">First Name </label>
                <div class="col-md-4">
                 <?php echo $_POST[0]['ad_first_name']; ?> 
                </div>
                </div>
                
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Last Name </label>
                <div class="col-md-4">
                <?php echo $_POST[0]['ad_last_name']; ?> 
                </div>
                </div>
                
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">User </label>
                <div class="col-md-4">
                <?php
				$conduser=" is_delete=0";
				$selectuser=$common_obj->fun_select("user_details",$conduser);
				foreach($selectuser as $getuser)
				{
				  if($_POST[0]['ad_userid']==$getuser['u_id']){echo $getuser['u_name'];}  } ?>
                
                </div>
                </div> 
                
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Religion</label>
                
                <div class="col-md-4">
                <?php 
				foreach($religionarray as $relkey => $religionget)
				{
				  if($_POST[0]['ad_religion']==$religionget){echo  $relkey;}  } ?>
                </div>
                </div> 
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Passport Type </label>
                <div class="col-md-4">
                 <?php 
				
				if($_POST[0]['ad_passport_type'] == 1 ||$_POST[0]['ad_passport_type'] == "Normal" ){ echo "Normal";} ?>
                
                </div>
                </div> 
               
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Date Of Birth </label>
                <div class="col-md-4">
                <?php echo date("d-m-Y",$_POST[0]['ad_dob']); ?>
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Date Of Issue </label>
                <div class="col-md-4">
                <?php echo date("d-m-Y",$_POST[0]['ad_date_of_issue']); ?>
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Expiry Date </label>
                <div class="col-md-4">
                <?php echo date("d-m-Y",$_POST[0]['ad_expiry_date']); ?>
                </div>
                </div>
               
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Nationality </label>
                <div class="col-md-4">
                India 
                </div>
                </div>
                
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Occupation </label>
                <div class="col-md-4">
                <?php echo $_POST[0]['ad_occupation']; ?>
                </div>
                </div>
                
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Education Qualification </label>
                <div class="col-md-4">
                <?php echo $_POST[0]['ad_education_qualification']; ?>
                </div>
                </div>
                
                
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Passport No.</label>
                <div class="col-md-4">
                <?php echo $_POST[0]['ad_passport_no']; ?>
                </div>
                </div>
                
                 
                
                 <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Status</label>
                    <div class="col-md-4">
                     <?php if($_POST[0]['ad_status']==1){echo "Active";}else{echo "Inactive";}?> 
                    
                    </label>
                   
                    </div>
                    </div>
                    
                     <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Air Ticket</label>
                    <div class="col-md-4">
                    
                  <?php if($_POST[0]['ad_air_ticket']==1){echo "Yes";}else{echo "No";} ?> 
                    </label>
                    </div>
                    </div>
                    </div>
                
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Passport Front Page</label>
				
				<div class="col-md-4">
                <a href="<?php echo ROOT;?>uploads/applicant/<?php echo $_POST[0]['ad_passport_front_page'];?>" target="_blank">
                 <?php //if($_POST[0]['ad_passport_front_page']!=''){
						$e=end(explode(".",$_POST[0]['ad_passport_front_page']));
						
						if($e=='pdf'){
							?>
							 <img src="<?php echo ROOT;?>/images/pdf.png"  height="40">
                        <?php
						}
						else
						{
						?>
                        
                   <?php 
				//if($_POST['ad_passport_front_page']!='') { ?>
               <img src="<?php echo ROOT;?>uploads/applicant/<?php echo $_POST[0]['ad_passport_front_page'];?>" height="40">
              
                <?php } ?>
                
                 </a>
                    
				</div>
                	</div>
                     <div id="imagehideid" class="form-group hide">
                    <label class="control-label col-md-2" for="example-input-normal">Selected</label>
                    <div class="col-md-4 " >
                    <div class=" imagePreview " id="imagePreview">
                    </div>
                    </div>
                    </div>
                     
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Passport Back Type</label>
				
				<div class="col-md-4">
                <a href="<?php echo ROOT;?>uploads/applicant/<?php echo $_POST[0]['ad_passport_back_type'];?>" target="_blank">
                 <?php //if($_POST['ad_passport_front_page']!=''){
						$e=end(explode(".",$_POST[0]['ad_passport_back_type']));
						
						if($e=='pdf'){
							?>
							 <img src="<?php echo ROOT;?>/images/pdf.png" height="40">
                        <?php
						}
						else
						{
						?>
                         
                   <?php 
				  
				//if($_POST['ad_passport_front_page']!='') { ?>
                <img src="<?php echo ROOT;?>uploads/applicant/<?php echo $_POST[0]['ad_passport_back_type'];?>" height="40">
               
                <?php } ?>
                     </a>
				</div>
                	</div>
                     <div id="imagehideid" class="form-group hide">
                    <label class="control-label col-md-2" for="example-input-normal">Selected</label>
                    <div class="col-md-4 " >
                    <div class=" imagePreview " id="imagePreview">
                    </div>
                    </div>
                    </div>
                   <?php if($_POST[0]['ad_airticket_file'] != ''){ ?>
                   <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Air Ticket File</label>
				
				<div class="col-md-4">
                <a href="<?php echo ROOT;?>uploads/applicant/<?php echo $_POST[0]['ad_airticket_file'];?>" target="_blank">
                 <?php //if($_POST['ad_passport_front_page']!=''){
						$e=end(explode(".",$_POST[0]['ad_airticket_file']));
						
						if($e=='pdf'){
							?>
							 <img src="<?php echo ROOT;?>/images/pdf.png"  height="40">
                        <?php
						}
						else
						{
						?>
                   <?php 
				//if($_POST['ad_passport_front_page']!='') { ?>
                <img src="../../../uploads/applicant/<?php echo $_POST[0]['ad_airticket_file'];?>" height="40">
                <?php } ?>
                   </a>
				</div>
                	</div>
						<?php } ?>
                     <div id="imagehideid" class="form-group hide">
                    <label class="control-label col-md-2" for="example-input-normal">Selected</label>
                    <div class="col-md-4 " >
                    <div class=" imagePreview " id="imagePreview">
                    </div>
                    </div>
                    </div>
                   
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Air Ticket Second File</label>
				
				<div class="col-md-4">
                <?php //if($_POST['ad_passport_front_page']!=''){
						$e=end(explode(".",$_POST[0]['ad_airticket_second_file']));
						
						if($e=='pdf'){
							?>
							 <img src="<?php echo ROOT;?>/images/pdf.png" width="150" height="40">
                        <?php
						}
						else
						{
						?>
                   <?php 
				//if($_POST['ad_passport_front_page']!='') { ?>
               <img src="../../../uploads/applicant/<?php echo $_POST[0]['ad_airticket_second_file'];?>" width="150" height="40">
                <?php } ?>
                
                    
				</div>
                	</div>
                   
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">US/UK/ Schengen VISA</label>
				
				<div class="col-md-4">
                <?php //if($_POST['ad_passport_front_page']!=''){
						$e=end(explode(".",$_POST['ad_file1']));
						
						if($e=='pdf'){
							?>
							 <img src="<?php echo ROOT;?>/images/pdf.png" width="150" height="40">
                        <?php
						}
						else
						{
						?>
                   <?php 
				//if($_POST['ad_passport_front_page']!='') { ?>
               <img src="../../../uploads/applicant/<?php echo $_POST[0]['ad_file1'];?>" width="150" height="40">
                <?php } ?>
                
                    
				</div>
                	</div>
                    
                    
                       <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Parents Photo ID/ Residence VISA</label>
				
				<div class="col-md-4">
                <?php //if($_POST['ad_passport_front_page']!=''){
						$e=end(explode(".",$_POST[0]['ad_file3']));
						
						if($e=='pdf'){
							?>
							 <img src="<?php echo ROOT;?>/images/pdf.png" width="150" height="40">
                        <?php
						}
						else
						{
						?>
                   <?php 
				//if($_POST['ad_passport_front_page']!='') { ?>
               <img src="../../../uploads/applicant/<?php echo $_POST[0]['ad_file3'];?>" width="150" height="40">
                <?php } ?>
                
                    
				</div>
                	</div>
                    
                    
                      <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">NOC from Parents</label>
				
				<div class="col-md-4">
                <?php //if($_POST['ad_passport_front_page']!=''){
						$e=end(explode(".",$_POST[0]['ad_file2']));
						
						if($e=='pdf'){
							?>
							 <img src="<?php echo ROOT;?>/images/pdf.png" width="150" height="40">
                        <?php
						}
						else
						{
						?>
                   <?php 
				//if($_POST['ad_passport_front_page']!='') { ?>
               <img src="../../../uploads/applicant/<?php echo $_POST[0]['ad_file2'];?>" width="150" height="40">
                <?php } ?>
                
                    
				</div>
                	</div>
                    
                    
                    
                        <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Income Tax Return(s)</label>
				
				<div class="col-md-4">
                <?php //if($_POST['ad_passport_front_page']!=''){
						$e=end(explode(".",$_POST['ad_file4']));
						
						if($e=='pdf'){
							?>
							 <img src="<?php echo ROOT;?>/images/pdf.png" width="150" height="40">
                        <?php
						}
						else
						{
						?>
                   <?php 
				//if($_POST['ad_passport_front_page']!='') { ?>
               <img src="../../../uploads/applicant/<?php echo $_POST[0]['ad_file4'];?>" width="150" height="40">
                <?php } ?>
                
                    
				</div>
                	</div>
                    
                      <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Others</label>
				
				<div class="col-md-4">
                <?php //if($_POST['ad_passport_front_page']!=''){
						$e=end(explode(".",$_POST[0]['ad_file5']));
						
						if($e=='pdf'){
							?>
							 <img src="<?php echo ROOT;?>/images/pdf.png" width="150" height="40">
                        <?php
						}
						else
						{
						?>
                   <?php 
				//if($_POST['ad_passport_front_page']!='') { ?>
               <img src="../../../uploads/applicant/<?php echo $_POST[0]['ad_file5'];?>" width="150" height="40">
                <?php } ?>
                
                    
				</div>
                	</div>
                    
                     <div id="imagehideid" class="form-group hide">
                    <label class="control-label col-md-2" for="example-input-normal">Selected</label>
                    <div class="col-md-4 " >
                    <div class=" imagePreview " id="imagePreview">
                    </div>
                    </div>
                    </div>
                   
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Photo</label>
				
				<div class="col-md-4">
                    <img src="../../../uploads/applicant/<?php echo $_POST[0]['ad_photo'];?>" width="150" height="40">
				</div>
                	</div>
                     <div id="imagehideid" class="form-group hide">
                    <label class="control-label col-md-2" for="example-input-normal">Selected</label>
                    <div class="col-md-4 " >
                    <div class=" imagePreview " id="imagePreview">
                    </div>
                    </div>
                    </div>
                   
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->


</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>