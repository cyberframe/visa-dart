<?php require_once("../../applicationtop.php"); $page="dashboard";?>
<?php
if(isset($_POST['submit']))
{
	$msg=$login_obj->adminChangePassword();
}
?>
<?php
require '../include/header.php';
?>
<script>
$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			old_pass:"required",
			new_pass:"required",
			con_pass:
			{required:true, equalTo : "#new_pass",}
			},
	});
	});
</script>
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="dashboard.php">Dashboard</a></li>
            <li class="active">
            <a href="">Change Password</a>
            </li>
            </ul>
            <h3 class="page-header">
            Change Password
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            
            <?php  if($_REQUEST['msg']!='') { ?>
            <div class="alert alert-success fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success! </strong> <?php echo $_REQUEST['msg']; ?>
            </div><?php } ?>
			<?php
			
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action=""  id="data_form" enctype="multipart/form-data" method="post" class="form-horizontal form-box">
            <div class="form-box-content">   
                    <div class="form-group">
                    
                    <label class="control-label col-md-2" for="example-input-normal">Old Password</label>
                    <div class="col-md-4">
                    <input  placeholder="Enter Old Password" class="form-control" type="password" id="old_pass" name="old_pass">
                    </div>
                    </div> 
                    <div class="form-group">
                    
                    <label class="control-label col-md-2" for="example-input-normal">New Password</label>
                    <div class="col-md-4">
                    <input  placeholder="Enter New Password" class="form-control" type="password" id="new_pass" name="new_pass">
                    </div>
                    </div> 
                    
                    <div class="form-group">
                    
                    <label class="control-label col-md-2" for="example-input-normal">Confirm New Password</label>
                    <div class="col-md-4">
                    <input  placeholder="Re Enter New Password" class="form-control" type="password" id="con_pass" name="con_pass">
                    </div>
                    </div> 
                    
                            </div>
			  <div class="clearfix"></div>
			 <div class="form-actions">
                                <div class="col-md-offset-2">
									 
									<button class="btn btn-success " onClick="return confirm('Are you sure  Change Your Old Password.');" name="submit" type="submit"><i class="icon-save"></i> Save</button>
                                     <a href="dashboard.php" onClick="return confirm('Are you sure cancel this and Go back.');" class="btn btn-danger"><i class="icon-remove"></i> Cancel</a>
                                   
                                </div>
                            </div>
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->
</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>