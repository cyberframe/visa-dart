<?php require_once("../../applicationtop.php"); $page="deal";?>
<?php
	if (isset($_POST['submit']))
	{
		$msg=$deal_obj->fun_add();
	}
?>
<?php
if($_REQUEST['id'] != '')
{
	$sql=" del_id='".$_REQUEST["id"]."'";
	$resultselect= $common_obj->fun_select("deal",$sql);
	foreach($resultselect as $row)
	{
		$del_title 			=	$row["del_title"];
		$del_price 		    = 	$row["del_price"];
		$del_min_passenger 			=	$row["del_min_passenger"];
		$del_fdcid 			=	$row["del_fdcid"];
		$del_short_description 			=	$row["del_short_description"];
		$del_description 			=	$row["del_description"];
		$del_exclusion 			=	$row["del_exclusion"];
		$del_inclusion 			=	$row["del_inclusion"];
		$del_custom 			=	$row["del_custom"];
		$del_release_date 			=	$row["del_release_date"];
		$del_expiry_date 			=	$row["del_expiry_date"];
		$del_image 			=	$row["del_image"];
		$del_status 			=	$row["del_status"];
	}
}
require '../include/header.php';
?>
	<script src="<?php echo ROOT_ADMIN;?>tiny_mce/tiny_mce.js" type="text/javascript"></script>
    <script src="<?php echo ROOT_ADMIN;?>tiny_mce/tinymice.js" type="text/javascript"></script>
<script>

$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			fdc_name:{required:true, maxlength:25},
			fdc_type:{required:true},
			},
	});
	});
</script>	  
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">Deal</a></li>
            <li class="active">
            <a href=""><?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?></a>
            </li>
            </ul>
            <h3 class="page-header">
           <?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            <div class="form-box-content">   
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Title <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Title" class="form-control req" id="del_title"  name="del_title" value="<?php echo $del_title; ?>" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Price <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Price" class="form-control req num" id="del_price"  name="del_price" value="<?php echo $del_price; ?>" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Min Passenger <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Min Passenger " class="form-control req num" id="del_min_passenger"  name="del_min_passenger" value="<?php echo $del_min_passenger; ?>" >
                </div>
                </div>
                <?php
                if($_REQUEST['id']!='')
				{
				?>
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Release Date <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="dd/mm/yyyy" class="form-control datepicker req" id="del_release_date"  name="del_release_date" value="<?php echo date("d-m-Y",$del_release_date); ?>" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Expiry Date <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="dd/mm/yyyy" class="form-control datepicker req" id="del_expiry_date"  name="del_expiry_date" value="<?php echo date("d-m-Y",$del_expiry_date); ?>" >
                </div>
                </div>
                <?php } else{ ?>
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Release Date <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="dd/mm/yyyy" class="form-control datepicker req" id="del_release_date"  name="del_release_date" value="" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Expiry Date <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="dd/mm/yyyy" class="form-control datepicker req" id="del_expiry_date"  name="del_expiry_date" value="" >
                </div>
                </div>
                <?php } ?>
                
                <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Status</label>
                    <div class="col-md-4">
                    <div class="radio radio-inline">
                    <label for="example-radio-inline1">
                    <input type="radio" id="del_status" <?php if($del_status==1){echo "checked='checked'";}?> name="del_status" value="1"> Active
                    </label>
                    </div>
                    <div class="radio radio-inline">
                    <label for="example-radio-inline2">
                    <input type="radio" id="del_status" <?php if($del_status==0){echo "checked='checked'";}?> name="del_status" value="0"> Inactive
                    </label>
                    </div>
                    </div>
                    </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Deal Category  <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <select id="del_fdcid" name="del_fdcid" class="form-control req">
                <option  value="">Select Deal Category</option>
                <?php
				$pagecond=" fdc_type=1 && is_delete=0";
				$selectpage=$common_obj->fun_select("faq_deal_category",$pagecond);
				foreach($selectpage as $getpage)
				{
				?>
                <option <?php if($del_fdcid==$getpage['fdc_id']){echo "selected='selected'";} ?> value="<?php echo $getpage['fdc_id']; ?>"><?php echo $getpage['fdc_name']; ?></option>
                <?php } ?>
                
                </select>
                </div>
                </div> 
                
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Upload Image</label>
				<div class="col-md-4">
                    <input id="uploadFile" class="" type="file" name="del_image">
                    <input value="<?php echo $del_image;?>" type="hidden" name="oldimage">
				</div>
                	</div>
                     <div id="imagehideid" class="form-group hide">
                    <label class="control-label col-md-2" for="example-input-normal">Selected Image</label>
                    <div class="col-md-4 " >
                    <div class=" imagePreview " id="imagePreview">
                    </div>
                    </div>
                    </div>
                    <?php if($_REQUEST['id']!='' && $del_image) { ?>
                    <div id="imagehideidinedit" class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Current Image</label>
                    <div class="col-md-4">
                   <?php 
				if($del_image!='') { ?>
                <img src="../../../img/del_image/<?php echo $del_image;?>" width="150" height="40">
                <?php } ?>
                    </div>
                    </div>
                   <?php } ?> 
                
                <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Short Description </label>
                        <div class="col-md-4">
                        <textarea id="del_short_description" name="del_short_description" class="form-control textarea-elastic req" rows="3"><?php echo $del_short_description; ?></textarea>
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Description </label>
                        <div class="col-md-4">
                        <textarea id="del_description" name="del_description" class="form-control textarea-elastic req" rows="3"><?php echo $del_description; ?></textarea>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Inclusion </label>
                        <div class="col-md-4">
                        <textarea id="del_inclusion" name="del_inclusion" class="form-control textarea-elastic req" rows="3"><?php echo $del_inclusion; ?></textarea>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Exclusion</label>
                        <div class="col-md-4">
                        <textarea id="del_exclusion" name="del_exclusion" class="form-control textarea-elastic req" rows="3"><?php echo $del_exclusion; ?></textarea>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Custom </label>
                        <div class="col-md-4">
                        <textarea id="del_custom" name="del_custom" class="form-control textarea-elastic req" rows="3"><?php echo $del_custom; ?></textarea>
                        </div>
                        </div>
				
                            </div>
			  <div class="clearfix"></div>
			 <div class="form-actions">
                                <div class="col-md-offset-2">
                                <button class="btn btn-success "  name="submit" type="submit"><i class="icon-save"></i> Save</button>
									  <a href="index.php" class="btn btn-danger"><i class="icon-remove"></i> Cancel</a>
									
                                   
                                </div>
                            </div>
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->


</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>