<?php require_once("../../applicationtop.php"); $page="deal";?>

<?php
	$cond = " del_id='".$_REQUEST['id']."' && is_delete=0";
	$rows = $common_obj->fun_select("deal",$cond);
	if($_REQUEST['id']!='')
	{
	$_POST=$rows;		
	}
	else
	{
	$_POST=$_POST;
	}
	?>

<?php
require '../include/header.php';
?>
	
<script>

$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			fdc_name:{required:true, maxlength:25},
			fdc_type:{required:true},
			},
	});
	});
</script>	  
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">Deal</a></li>
            <li class="active">
            <a href="">View</a>
            </li>
            </ul>
            <h3 class="page-header">
           View <a href="index.php" class="btn btn-info pull-right">Go Back </a>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            <div class="form-box-content">   
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Title </label>
                <div class="col-md-4">
                <?php echo $_POST[0]['del_title']; ?>
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Price </label>
                <div class="col-md-4">
               <?php echo $_POST[0]['del_price']; ?>
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Min Passenger </label>
                <div class="col-md-4">
               <?php echo $_POST[0]['del_min_passenger']; ?>
                </div>
                </div>
               
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Release Date </label>
                <div class="col-md-4">
               <?php echo date("d-m-Y",$_POST[0]['del_release_date']); ?>
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Expiry Date </label>
                <div class="col-md-4">
                <?php echo date("d-m-Y",$_POST[0]['del_expiry_date']); ?>
                </div>
                </div>
                
                
                <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Status</label>
                    <div class="col-md-4">
                    <?php if($_POST[0]['del_status']==1){echo "Active";}else{echo "Inactive";}?>
                    </label>
                    </div>
                    </div>
                    </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Deal Category </label>
                <div class="col-md-4">
                <?php
				$pagecond=" is_delete=0";
				$selectpage=$common_obj->fun_select("faq_deal_category",$pagecond);
				foreach($selectpage as $getpage)
				{
				 if($_POST[0]['del_fdcid']==$getpage['fdc_id']){echo $getpage['fdc_name'];}   } ?>
                
                </div>
                </div> 
                
                <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Short Description </label>
                        <div class="col-md-4">
                       <?php echo $_POST[0]['del_short_description']; ?>
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Description </label>
                        <div class="col-md-4">
                        <?php echo $_POST[0]['del_description']; ?>
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Inclusion </label>
                        <div class="col-md-4">
                       <?php echo $_POST[0]['del_inclusion']; ?>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Exclusion </label>
                        <div class="col-md-4">
                       <?php echo $_POST[0]['del_exclusion']; ?>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Custom </label>
                        <div class="col-md-4">
                       <?php echo $_POST[0]['del_custom']; ?>
                        </div>
                        </div>
                        
                        <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Image</label>
				<div class="col-md-4">
                    <?php 
				if($_POST[0]['del_image']!='') { ?>
                <img src="../../../uploads/del_image/<?php echo $_POST[0]['del_image'];?>" width="300" height="100">
                <?php } ?>
				</div>
                	</div>
				
                            </div>
			  
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->


</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>