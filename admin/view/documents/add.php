<?php require_once("../../applicationtop.php"); $page="documents";?>
<?php
	if (isset($_POST['submit']))
	{
		$msg=$documents_obj->fun_add();
	}

if($_REQUEST['id'] != '')
{
	$sql=" d_id='".$_REQUEST["id"]."'";
	$resultselect= $common_obj->fun_select("documents",$sql);
	foreach($resultselect as $row)
	{
		$title 			=	$row["d_title"];
		$isrequired 		    = 	$row["d_required"];
		$for 			=	$row["d_for"];
	}
}

require '../include/header.php';
?>
<script>

$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			fdc_name:{required:true, maxlength:25},
			fdc_type:{required:true},
			},
	});
	});
</script>	  
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">FAQ Deal Category</a></li>
            <li class="active">
            <a href=""><?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?></a>
            </li>
            </ul>
            <h3 class="page-header">
           <?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            <div class="form-box-content">   
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Title <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Title" class="form-control req" id="title"  name="title" value="<?php echo $title; ?>" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Required <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <select id="isrequired" name="isrequired" class="form-control req">
                <option <?php if($isrequired==1){echo "selected='selected'";} ?> value="1">Yes</option>
                <option <?php if($isrequired==2){echo "selected='selected'";} ?> value="2">No</option>
                
                </select>
                </div>
                </div> 
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">For <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <select id="for" name="for" class="form-control req">
                <option <?php if($for==1){echo "selected='selected'";} ?> value="1">All</option>
                <option <?php if($for==2){echo "selected='selected'";} ?> value="2">Adult</option>
                <option <?php if($for==2){echo "selected='selected'";} ?> value="2">Child</option>
                
                </select>
				</div>
                	</div>
                
				
                            </div>
			  <div class="clearfix"></div>
			 <div class="form-actions">
                                <div class="col-md-offset-2">
                                <button class="btn btn-success "  name="submit" type="submit"><i class="icon-save"></i> Save</button>
									  <a href="index.php" class="btn btn-danger"><i class="icon-remove"></i> Cancel</a>
									
                                   
                                </div>
                            </div>
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->


</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>