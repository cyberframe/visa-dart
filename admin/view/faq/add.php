<?php require_once("../../applicationtop.php"); $page="faq";?>
<?php
	if (isset($_POST['submit']))
	{
		$msg=$faq_obj->fun_add();
	}
?>
<?php
	if($_REQUEST['id'] != '')
{
	$sql=" faq_id='".$_REQUEST["id"]."'";
	$resultselect= $common_obj->fun_select("faq",$sql);
	foreach($resultselect as $row)
	{
		$faq_question	=	$row["faq_question"];
		$faq_answer		= 	$row["faq_answer"];
		$faq_status		=	$row["faq_status"];
		$faq_fdcid		=	$row["faq_fdcid"];
	}
}
?>

<?php
require '../include/header.php';
?>
 <script src="<?php echo ROOT_ADMIN;?>tiny_mce/tiny_mce.js" type="text/javascript"></script>
    <script src="<?php echo ROOT_ADMIN;?>tiny_mce/tinymice.js" type="text/javascript"></script>
<script>

$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			fdc_name:{required:true, maxlength:25},
			fdc_type:{required:true},
			},
	});
	});
</script>	
 
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">FAQ</a></li>
            <li class="active">
            <a href=""><?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?></a>
            </li>
            </ul>
            <h3 class="page-header">
           <?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            
                   
                 <div class="form-box-content">
                 
                 
                  <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Category <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <select id="faq_fdcid" name="faq_fdcid" class="form-control req">
                <option  value="">Select Category</option>
                <?php
				$pagecond=" fdc_type=2 && is_delete=0";
				$selectpage=$common_obj->fun_select("faq_deal_category",$pagecond);
				foreach($selectpage as $getpage)
				{
				?>
                <option <?php if($faq_fdcid==$getpage['fdc_id']){echo "selected='selected'";} ?> value="<?php echo $getpage['fdc_id']; ?>"><?php echo $getpage['fdc_name']; ?></option>
                <?php } ?>
                
                </select>
                </div>
                </div> 
                    
                 
                
                <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Status</label>
                    <div class="col-md-4">
                    <div class="radio radio-inline">
                    <label for="example-radio-inline1">
                    <input type="radio" id="faq_status" <?php if($faq_status==1){echo "checked='checked'";}?> name="faq_status" value="1"> Active
                    </label>
                    </div>
                    <div class="radio radio-inline">
                    <label for="example-radio-inline2">
                    <input type="radio" id="faq_status" <?php if($faq_status==0){echo "checked='checked'";}?> name="faq_status" value="0"> Inactive
                    </label>
                    </div>
                    </div>
                    </div>
                    
                   
                
                 <div class="form-group">
                                <label class="control-label col-md-2" for="example-textarea-elastic">Question  <span style="color:#F00;">*</span></label>
                                <div class="col-md-4">
                                    <textarea id="faq_question" name="faq_question" class="form-control textarea-elastic" rows="3"><?php echo $faq_question; ?></textarea>
                                </div>
							</div>
                
                <div class="form-group">
                                <label class="control-label col-md-2" for="example-textarea-elastic">Answer  </label>
                                <div class="col-md-4">
                                    <textarea id="faq_answer" name="faq_answer" class="form-control textarea-elastic" rows="3"><?php echo $faq_answer; ?></textarea>
                                </div>
							</div>
                          
                
                            </div>
			  <div class="clearfix"></div>
			 <div class="form-actions">
                                <div class="col-md-offset-2">
                                <button class="btn btn-success "  name="submit" type="submit"><i class="icon-save"></i> Save</button>
									  <a href="index.php" class="btn btn-danger"><i class="icon-remove"></i> Cancel</a>
									
                                   
                                </div>
                            </div>
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->


</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>