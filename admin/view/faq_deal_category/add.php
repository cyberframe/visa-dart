<?php require_once("../../applicationtop.php"); $page="faq_deal_category";?>
<?php
	if (isset($_POST['submit']))
	{
		$msg=$faq_deal_category_obj->fun_add();
	}

if($_REQUEST['id'] != '')
{
	$sql=" fdc_id='".$_REQUEST["id"]."'";
	$resultselect= $common_obj->fun_select("faq_deal_category",$sql);
	foreach($resultselect as $row)
	{
		$fdc_name 			=	$row["fdc_name"];
		$fdc_icon 		    = 	$row["fdc_icon"];
		$fdc_type 			=	$row["fdc_type"];
	}
}

require '../include/header.php';
?>
<script>

$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			fdc_name:{required:true, maxlength:25},
			fdc_type:{required:true},
			},
	});
	});
</script>	  
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">FAQ Deal Category</a></li>
            <li class="active">
            <a href=""><?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?></a>
            </li>
            </ul>
            <h3 class="page-header">
           <?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            <div class="form-box-content">   
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Name <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Name" class="form-control req" id="fdc_name"  name="fdc_name" value="<?php echo $fdc_name; ?>" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Type <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <select id="fdc_type" name="fdc_type" class="form-control req">
                <option  value="">Type</option>
                <option <?php if($fdc_type==1){echo "selected='selected'";} ?> value="1">Deal</option>
                <option <?php if($fdc_type==2){echo "selected='selected'";} ?> value="2">FAQ</option>
                
                </select>
                </div>
                </div> 
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Upload Icon</label>
				
				<div class="col-md-4">
                    <input id="uploadFile" class="" type="file" name="fdc_icon"  >
                    <input value="<?php echo $fdc_icon;?>" type="hidden" name="oldimage"  >
				</div>
                	</div>
                     <div id="imagehideid" class="form-group hide">
                    <label class="control-label col-md-2" for="example-input-normal">Selected Icon</label>
                    <div class="col-md-4 " >
                    <div class=" imagePreview " id="imagePreview">
                    </div>
                    </div>
                    </div>
                    <?php if($_REQUEST['id']!='') { ?>
                    <div id="imagehideidinedit" class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Current Icon</label>
                    <div class="col-md-4">
                   <?php 
				if($fdc_icon!='') { ?>
                <img src="../../../uploads/faqicon/<?php echo $fdc_icon;?>" width="150" height="40">
                <?php } ?>
                    </div>
                    </div>
                   <?php } ?> 
				
                            </div>
			  <div class="clearfix"></div>
			 <div class="form-actions">
                                <div class="col-md-offset-2">
                                <button class="btn btn-success "  name="submit" type="submit"><i class="icon-save"></i> Save</button>
									  <a href="index.php" class="btn btn-danger"><i class="icon-remove"></i> Cancel</a>
									
                                   
                                </div>
                            </div>
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->


</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>