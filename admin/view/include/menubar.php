<aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">
                    <!-- Sidebar search -->
                   <!-- <form id="sidebar-search" action="page_search_results.html" method="post">
                        <div class="input-group">
                            <input type="text" id="sidebar-search-term" name="sidebar-search-term" placeholder="Search..">
                            <button><i class="icon-search"></i></button>
                        </div>
                    </form>-->
                    <!-- END Sidebar search -->

                    <!-- Primary Navigation -->
                    <nav id="primary-nav">
                        <ul>
                            <li>
                                <a href="<?php echo ROOT_VIEW; ?>include/dashboard.php" <?php if($page=='dashboard'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Dashboard</a>
                            </li>
							
                            <li><a href="<?php echo ROOT_VIEW; ?>values/?s=false" target="_parent" <?php if($page=="values"){echo "class='active '";}?>><i class="icon-fire"></i>System Setting</a></li>
							<li>
                            <a href="<?php echo ROOT_VIEW; ?>leads?s=false" <?php if($page=='leads'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Leads</a>
                            </li>
                             <li>
                            <a href="<?php echo ROOT_VIEW; ?>lead_reports/?s=false" <?php if($page=='lead_reports'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Lead Reports</a>
                            </li>
                             <li>
                            <a href="<?php echo ROOT_VIEW; ?>slider?s=false" <?php if($page=='slider'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Slider</a>
                            </li>
                            <li>
                                <a href="<?php echo ROOT_VIEW; ?>faq_deal_category?s=false" <?php if($page=='faq_deal_category'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>FAQ Deal Category</a>
                            </li>
                            
                            <li>
                                <a href="<?php echo ROOT_VIEW; ?>visa_type?s=false" <?php if($page=='visa_type'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Visa Type</a>
                            </li>
                            
                             <li>
                                <a href="<?php echo ROOT_VIEW; ?>user_details?s=false" <?php if($page=='user_details'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>User Details</a>
                            </li>
                            
                            <li>
                                <a href="<?php echo ROOT_VIEW; ?>applicant_details?s=false" <?php if($page=='applicant_details'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Applicant Details</a>
                            </li>
                            
                            <li>
                                <a href="<?php echo ROOT_VIEW; ?>testimonials?s=false" <?php if($page=='testimonials'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Testimonials</a>
                            </li>
                            
                            <li>
                                <a href="<?php echo ROOT_VIEW; ?>faq?s=false" <?php if($page=='faq'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>FAQ</a>
                            </li>
                            
                          <li>
                                <a href="<?php echo ROOT_VIEW; ?>message?s=false" <?php if($page=='message'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Message</a>
                            </li>
                              <li>
                                <a href="<?php echo ROOT_VIEW; ?>news_flash?s=false" <?php if($page=='news_flash'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>News</a>
                            </li>
                            
                            <li>
                                <a href="<?php echo ROOT_VIEW; ?>pages?s=false" <?php if($page=='pages'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Pages</a>
                            </li>
                            
                            <li>
                                <a href="<?php echo ROOT_VIEW; ?>keywords?s=false" <?php if($page=='keywords'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Keywords</a>
                            </li>
                            <li>
                                <a href="<?php echo ROOT_VIEW; ?>deal?s=false" <?php if($page=='deal'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Deal</a>
                            </li>
                            
                             <li>
                                <a href="<?php echo ROOT_VIEW; ?>insurance?s=false" <?php if($page=='insurance'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Insurance</a>
                            </li>
                            
                            <li>
                                <a href="<?php echo ROOT_VIEW; ?>insurance_price?s=false" <?php if($page=='insurance_price'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Insurance Price</a>
                            </li>
                            
                             <li>
                            <a href="<?php echo ROOT_VIEW; ?>otb?s=false" <?php if($page=='otb'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>OTB</a>
                            </li>
                            
                          
                             <li>
                            <a href="<?php echo ROOT_VIEW; ?>sms_credentials?s=false" <?php if($page=='sms_credentials'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>SMS Credentials</a>
                            </li>
                            
                            <li>
                            <a href="<?php echo ROOT_VIEW; ?>contactus?s=false" <?php if($page=='contactus'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Contact Us</a>
                            </li>
                            
                             <li>
                            <a href="<?php echo ROOT_VIEW; ?>agents_enquiry?s=false" <?php if($page=='agents_enquiry'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Agents Enquiry</a>
                            </li>
                            
                           
                            
                             <li>
                            <a href="<?php echo ROOT_VIEW; ?>documents?s=false" <?php if($page=='documents'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Documents</a>
                            </li>
                            
                              <li>
                            <a href="<?php echo ROOT_VIEW; ?>address?s=false" <?php if($page=='address'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Address</a>
                            </li>
                            <li>
                            <a href="<?php echo ROOT_VIEW; ?>payment_log?s=false" <?php if($page=='payment_log'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Payment Log</a>
                            </li>
                           <li>
                            <a href="<?php echo ROOT_VIEW; ?>services?s=false" <?php if($page=='services'){echo  "class='active'";}?> class=""><i class="icon-fire"></i>Services</a>
                            </li>
                            
                           
                        </ul>
                    </nav>
                    <!-- END Primary Navigation -->

                    
                    <!-- END Demo Theme Options -->
                </aside>