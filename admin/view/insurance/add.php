<?php require_once("../../applicationtop.php"); $page="insurance";?>

<?php
if (isset($_POST['submit']))
{
	$msg=$insurance_obj->fun_add();
}


	
if($_REQUEST['id'] != '')
{
	$sql=" ins_id='".$_REQUEST["id"]."'";
	$resultselect= $common_obj->fun_select("insurance",$sql);
	foreach($resultselect as $row)
	{
		$ins_plan_details 	=	$row["ins_plan_details"];
		$ins_inclusion 		    = 	$row["ins_inclusion"];
		$ins_exclusion 			=	$row["ins_exclusion"];
		$ins_policy_terms 			=	$row["ins_policy_terms"];
		$ins_claim_process 			=	$row["ins_claim_process"];
		$ins_status 			=	$row["ins_status"];
		$ins_image 			=	$row["ins_image"];
	}
}
require '../include/header.php';

?>

	<script src="<?php echo ROOT_ADMIN;?>tiny_mce/tiny_mce.js" type="text/javascript"></script>

    <script src="<?php echo ROOT_ADMIN;?>tiny_mce/tinymice.js" type="text/javascript"></script>

<script>



$(document).ready(function() {

	$("#data_form").validate({

	rules:{

			fdc_name:{required:true, maxlength:25},

			fdc_type:{required:true},

			},

	});

	});

</script>	  

            <!-- Page Content -->

            <div id="page-content">

            <!-- Navigation info -->

            <ul id="nav-info" class="clearfix">

            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>

            <li><a href="index.php">Insurance</a></li>

            <li class="active">

            <a href=""><?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?></a>

            </li>

            </ul>

            <h3 class="page-header">

           <?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?>

            </h3>

            <?php  if($msg!='') { ?>

            <div class="alert alert-danger fade in">

            <a href="#" class="close" data-dismiss="alert">&times;</a>

            <strong>Error! </strong> <?php echo $msg; ?>

            </div><?php } ?>

            <?php

            //$common_obj->fun_session_alert();

            ?>

            <!-- Nav Dash -->

            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">

            <div class="form-box-content">   

                    

                

                <div class="form-group">

                    <label class="control-label col-md-2" for="example-input-normal">Status</label>

                    <div class="col-md-4">

                    <div class="radio radio-inline">

                    <label for="example-radio-inline1">

                    <input type="radio" id="ins_status" <?php if($ins_status==1){echo "checked='checked'";}?> name="ins_status" value="1"> Active

                    </label>

                    </div>

                    <div class="radio radio-inline">

                    <label for="example-radio-inline2">

                    <input type="radio" id="ins_status" <?php if($ins_status==0){echo "checked='checked'";}?> name="ins_status" value="0"> Inactive

                    </label>

                    </div>

                    </div>

                    </div>

                

                

                <div class="form-group">

                        <label class="control-label col-md-2" for="example-textarea-elastic">Plan Details </label>

                        <div class="col-md-4">

                        <textarea id="ins_plan_details" name="ins_plan_details" class="form-control textarea-elastic req" rows="3"><?php echo $ins_plan_details; ?></textarea>

                        </div>

                        </div>

                        

                         <div class="form-group">

                        <label class="control-label col-md-2" for="example-textarea-elastic">Inclusion  </label>

                        <div class="col-md-4">

                        <textarea id="ins_inclusion" name="ins_inclusion" class="form-control textarea-elastic req" rows="3"><?php echo $ins_inclusion; ?></textarea>

                        </div>

                        </div>

                        

                         <div class="form-group">

                        <label class="control-label col-md-2" for="example-textarea-elastic">Exclusion  </label>

                        <div class="col-md-4">

                        <textarea id="ins_exclusion" name="ins_exclusion" class="form-control textarea-elastic req" rows="3"><?php echo $ins_exclusion; ?></textarea>

                        </div>

                        </div>

                        

                         <div class="form-group">

                        <label class="control-label col-md-2" for="example-textarea-elastic">Policy Terms  </label>

                        <div class="col-md-4">

                        <textarea id="ins_policy_terms" name="ins_policy_terms" class="form-control textarea-elastic req" rows="3"><?php echo $ins_policy_terms; ?></textarea>

                        </div>

                        </div>

                        

                         <div class="form-group">

                        <label class="control-label col-md-2" for="example-textarea-elastic">Claim Process  </label>

                        <div class="col-md-4">

                        <textarea id="ins_claim_process" name="ins_claim_process" class="form-control textarea-elastic req" rows="3"><?php echo $ins_claim_process; ?></textarea>

                        </div>

                        </div>
                        
                        <div class="form-group">
                        
                        <label class="control-label col-md-2" for="example-input-normal">Upload Image</label>
                        
                        
                        
                        <div class="col-md-4">
                        
                        <input id="uploadFile" class="" type="file" name="ins_image"  >
                        
                        <input value="<?php echo $ins_image;?>" type="hidden" name="oldimage"  >
                        
                        </div>
                        
                        </div>
                        
                        <div id="imagehideid" class="form-group hide">
                        
                        <label class="control-label col-md-2" for="example-input-normal">Selected Image</label>
                        
                        <div class="col-md-4 " >
                        
                        <div class=" imagePreview " id="imagePreview">
                        
                        </div>
                        
                        </div>
                        
                        </div>
                        
                        <?php if($_REQUEST['id']!='') { ?>
                        
                        <div id="imagehideidinedit" class="form-group">
                        
                        <label class="control-label col-md-2" for="example-input-normal">Current Image</label>
                        
                        <div class="col-md-4">
                        
                        <?php 
                        
                        if($ins_image!='') { ?>
                        
                        <img src="../../../uploads/insurance/<?php echo $ins_image ; ?>" width="150" height="40">
                        
                        <?php } ?>
                        
                        </div>
                        
                        </div>
                        
                        <?php } ?> 
				

                            </div>

			  <div class="clearfix"></div>

			 <div class="form-actions">

                                <div class="col-md-offset-2">

                                <button class="btn btn-success "  name="submit" type="submit"><i class="icon-save"></i> Save</button>

									  <a href="index.php" class="btn btn-danger"><i class="icon-remove"></i> Cancel</a>

									

                                   

                                </div>

                            </div>

           

            <div class="clearfix"></div>

      

    </div>

	  </form>

	  <div class="clearfix"></div>

    <!-- END Nav Dash -->





</div>

<!-- END Page Content -->

<?php

require '../include/footer.php';

?>