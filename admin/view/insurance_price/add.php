<?php require_once("../../applicationtop.php"); $page="insurance_price";?>
<?php
if (isset($_POST['submit']))
{
	$msg=$insurance_price_obj->fun_add();
}

if($_REQUEST['id'] != '')
{
	$sql=" insp_id='".$_REQUEST["id"]."'";
	$resultselect= $common_obj->fun_select("insurance_price",$sql);
	foreach($resultselect as $row)
	{
		$insp_start_age 	=	$row["insp_start_age"];
		$insp_end_age 		= 	$row["insp_end_age"];
		$insp_price 		=	$row["insp_price"];
		$insp_day 			=	$row["insp_day"];
	}
}
require '../include/header.php';
?>
	
<script>

$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			fdc_name:{required:true, maxlength:25},
			fdc_type:{required:true},
			},
	});
	});
</script>	  
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">Insurance Price</a></li>
            <li class="active">
            <a href=""><?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?></a>
            </li>
            </ul>
            <h3 class="page-header">
           <?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            <div class="form-box-content">   
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Start Age <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Start Age" class="form-control req num" id="insp_start_age"  name="insp_start_age" value="<?php echo $insp_start_age; ?>" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">End Age <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter End Age" class="form-control req num" id="insp_end_age"  name="insp_end_age" value="<?php echo $insp_end_age; ?>" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Day <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Day" class="form-control req num" id="insp_day"  name="insp_day" value="<?php echo $insp_day; ?>" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Price <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Price " class="form-control req num" id="insp_price"  name="insp_price" value="<?php echo $insp_price; ?>" >
                </div>
                </div>
                
				
                            </div>
			  <div class="clearfix"></div>
			 <div class="form-actions">
                                <div class="col-md-offset-2">
                                <button class="btn btn-success "  name="submit" type="submit"><i class="icon-save"></i> Save</button>
									  <a href="index.php" class="btn btn-danger"><i class="icon-remove"></i> Cancel</a>
									
                                   
                                </div>
                            </div>
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->


</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>