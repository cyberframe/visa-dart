<?php require_once("../../applicationtop.php"); $page="keywords";?>
<?php
	if (isset($_POST['submit']))
	{
		$msg=$keywords_obj->fun_add();
	}
?>
<?php
if($_REQUEST['id'] != '')
{
	$sql=" key_id='".$_REQUEST["id"]."'";
	$resultselect= $common_obj->fun_select("keywords",$sql);
	foreach($resultselect as $row)
	{
		$key_page_id 	=	$row["key_page_id"];
		$key_title 		= 	$row["key_title"];
		$key_keywords 	=	$row["key_keywords"];
		$key_description =	$row["key_description"];
	}
}
require '../include/header.php';
?>
<script>

$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			fdc_name:{required:true, maxlength:25},
			fdc_type:{required:true},
			},
	});
	});
</script>	  
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">Keywords</a></li>
            <li class="active">
            <a href=""><?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?></a>
            </li>
            </ul>
            <h3 class="page-header">
           <?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            <div class="form-box-content">   
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Title <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Title" class="form-control req" id="key_title"  name="key_title" value="<?php echo $key_title; ?>" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Page <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <select id="key_page_id" name="key_page_id" class="form-control req">
                <option  value="">Select Page</option>
                <?php
				$pagecond="";
				$selectpage=$common_obj->fun_select("pages",$pagecond);
				foreach($selectpage as $getpage)
				{
				?>
                <option <?php if($key_page_id==$getpage['pag_id']){echo "selected='selected'";} ?> value="<?php echo $getpage['pag_id']; ?>"><?php echo $getpage['pag_title']; ?></option>
                <?php } ?>
                
                </select>
                </div>
                </div> 
                
                <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Keywords <span style="color:#F00;">*</span></label>
                        <div class="col-md-4">
                        <textarea id="key_keywords" name="key_keywords" class="form-control textarea-elastic req" rows="3"><?php echo $key_keywords; ?></textarea>
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <label class="control-label col-md-2" for="example-textarea-elastic">Description <span style="color:#F00;">*</span></label>
                        <div class="col-md-4">
                        <textarea id="key_description" name="key_description" class="form-control textarea-elastic req" rows="3"><?php echo $key_description; ?></textarea>
                        </div>
                        </div>
				
                            </div>
			  <div class="clearfix"></div>
			 <div class="form-actions">
                                <div class="col-md-offset-2">
                                <button class="btn btn-success "  name="submit" type="submit"><i class="icon-save"></i> Save</button>
									  <a href="index.php" class="btn btn-danger"><i class="icon-remove"></i> Cancel</a>
									
                                   
                                </div>
                            </div>
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->


</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>