<?php require_once("../../applicationtop.php"); $page="leads";?>
<?php
//	$cond = " ord_id='".$_REQUEST['id']."' && is_delete=0";
//	$rows = $db_obj->fun_select_one("orders",$cond);
//	
//	$condpro = " prd_id='".$rows['ord_product_id']."' && is_delete=0";
//	$rowspro = $db_obj->fun_select_one("products",$condpro);
	
	//$cond_category = " ca_id='".$rows['ord_product_id']."' && is_delete=0";
//	$rowspro_category = $db_obj->fun_select_one("category",$condpro);
//	
	?>
    <script>
	$(document).ready(function(){
	$("#disoutid").dblclick(function(){
    $("#disoutid").addClass("hide");
	$("#disinid").removeClass("hide");
});
$("#vatoutid").dblclick(function(){
    $("#vatoutid").addClass("hide");
	$("#vatinid").removeClass("hide");
});
});	
	
//$(document).ready(function(){
//    $("input").blur(function(){
//        alert("This input field has lost its focus.");
//    });
//});
</script>
<style>
.form-horizontal .control-label{ padding-top:0 !important; text-align:center !important;}
</style>
<ul class="breadcrumb">
	<li><a href="index.php" class="glyphicons home"><i></i> Dashboard</a></li>
	<li class="divider"></li>
	<li><a href="index.php">Invoice</a></li>
	
    <li class="divider"></li>
	<li>Invoice</li>
</ul>
<div class="separator"></div>

<form class="form-horizontal"  method="post" action="" autocomplete="off">
	
	<div class="row-fluid">
	<div class="span8">
		<body style="font-family:arial; font-size:14px;font-family:arial;">
			<p style="text-align:center; margin-bottom:2px;">INVOICE</p>
			<div style="margin:0px auto; width: 100%;font-family:arial; border:1px solid #ccc;">
				<table style="width:100%;" cellspacing="0">
					<tr>
						<td style="margin: 0px; padding: 0px;  border-bottom: 1px solid #ccc;">
							<img src="theme/images/logo_invoice.jpg" style="width:100px;"/>
						</td>
						<td style="text-align: left; font-size: 13px; border-right: 1px solid #ccc; width: 250px; margin: 0px; padding: 0px; border-bottom: 1px solid #ccc;">
							<span style="padding:0 10px 0 0; font-size:13px; font-weight:bold; ">TRINETRA</span><br>
							<span>402, Shri Ratnam, 3828 M.S.B Ka Rasta<br>Johari Bazar, JAIPUR<br>Tel. No. 0141-2575752<br>E-Mail:trinetra2008@gmail.com</span>
						</td>
						<td style="width:25%; vertical-align: top;  font-size:13px;">
							<p style="padding-bottom: 0px; margin-bottom: 0px;">Invoice no </p>
							<b>Invoice no </b>
							<p style="padding-bottom: 0px; margin-bottom: 0px;">Order No.</p>
							<b><?php echo $rows['ord_order_no']; ?></b>
						</td>
						<td style="vertical-align: top;  font-size:13px;   width: 19%">
							<p style="padding-bottom: 0px; margin-bottom: 0px;">Dated </p>
							<b><?php echo date("d M Y",$rows['ord_order_date']); ?></b>
						</td>
					</tr>
					<tr>
						<td style="text-align: left; font-size: 13px;">
							<span style="padding:0 10px 0 0; font-size:12px;">Buyer</span><br>
							<span><b><?php echo $rows['ord_name']; ?></b> <br><?php echo $rows['ord_address']; ?> <?php echo $rows['ord_city']; ?> <?php echo $rows['ord_state']; ?> <?php echo $rows['ord_pincode']; ?><br><?php if($rows['ord_mobile']!='') {?><b>Mobile No.</b><?php echo $rows['ord_mobile']; ?></span><?php } ?>
						</td>
						<td style="border-right:1px solid #ccc;"></td>
					</tr>
				</table>
				<table style="width:100%; height:300px;" cellspacing="0">
					<tbody>
						<tr>
					  <th style="border:1px solid #ccc; border-left:0; margin: 0px; padding: 6px; height:35px;">Sr.</th>
					  <th style="border:1px solid #ccc; border-left:0; margin: 0px; padding: 6px;">Product</th>
					  <th style="border:1px solid #ccc; border-left:0; margin: 0px; padding: 6px;">Qty</th>
					  <th style="border:1px solid #ccc; border-left:0; margin: 0px; padding: 6px;">Price/Piece</th>
					  <th style="border:1px solid #ccc; border-left:0; margin: 0px; padding: 6px;">Disc %</th>
					  <th style="border:1px solid #ccc; border-left:0; margin: 0px; padding: 6px;">Disc Amt</th>
					  <th style="border:1px solid #ccc; border-left:0;  border-right:0;margin: 0px; padding: 6px;">Net Amt</th>
					</tr>
						<tr style="vertical-align: top; text-align:right;">
							<td style="margin: 0px; padding: 0px; border-right: 1px solid #ccc;  text-align:center; max">1</td>
							<td style="margin: 0px; padding: 0px;width:250px; text-align:left;  border-right: 1px solid #ccc;"><?php echo $rowspro['prd_name']; ?> <br><span><?php echo $rowspro['prd_product_sku']; ?></span></td>
							<td style="margin: 0px; padding: 0px; border-right: 1px solid #ccc;"><?php echo $rows['ord_quantity']; ?></td>
							<td style="margin: 0px; padding: 0px;width:100px; border-right: 1px solid #ccc;"><?php echo $rows['ord_amount']/$rows['ord_quantity']; ?></td>
							<td style=" margin: 0px; padding: 0px;width:100px; border-right: 1px solid #ccc;">
                            <span id="disinid" class="hide"><input name="ord_discount_percentage" type="text" style="width:80px; margin:5px;" value="<?php echo $rows['ord_discount_percentage'];?>"></input>
                            </span>
                            <span id="disoutid" class="">
                                    <?php echo $rows['ord_vat_percentage'];?>
                                    </span>
                            </td>
							<td style="; margin: 0px; padding: 0px;width:100px; border-right: 1px solid #ccc;">--</td>
							<td style="margin: 0px; padding: 0px;"><?php echo $rows['ord_amount']; ?></td>
                            </tr>
                            <tr style="vertical-align: top; text-align:right;">
                                <td style="border-bottom: 1px solid #ccc;margin: 0px; padding: 0px; border-right: 1px solid #ccc;text-align:center; height:35px;"></td>
                                <td style="border-bottom: 1px solid #ccc; margin: 0px; padding: 0px;width:250px; text-align:left;  border-right: 1px solid #ccc;"></td>
                                <td style="border-bottom: 1px solid #ccc; margin: 0px; padding: 0px; border-right: 1px solid #ccc;"></td>
                                <td style="border-bottom: 1px solid #ccc; margin: 0px; padding: 0px;width:100px; border-right: 1px solid #ccc;"></td>
                                <td style="border-top: 1px solid #ccc; border-bottom: 1px solid #ccc; margin: 0px; padding: 0px;width:100px;vertical-align: middle; text-align:center;">VAT %</td>
                                <td style="border-top: 1px solid #ccc;  border-bottom: 1px solid #ccc; margin: 0px; padding: 0px;width:100px; border-right: 1px solid #ccc; vertical-align: middle;">
                                    <span id="vatinid" class="hide"><input type="text" name="ord_vat_percentage" style="width:80px; margin:5px;" value="<?php echo $rows['ord_vat_percentage'];?>"></input>
                                    </span>
                                    <span id="vatoutid" class="">
                                    <?php echo $rows['ord_vat_percentage'];?>
                                    </span>
                                </td>
                                <td style="border-top: 1px solid #ccc;  border-bottom: 1px solid #ccc; margin: 0px; padding: 0px; vertical-align: middle;">2000</td>
                        	</tr>
                        
						<tr style="vertical-align: top; text-align:right;">
							<td style="margin: 0px; padding: 0px; border-right: 1px solid #ccc;text-align:center; height:35px;"></td>
							<td style="margin: 0px; padding: 0px;width:250px; text-align:left;  border-right: 1px solid #ccc;">Total</td>
							<td style="margin: 0px; padding: 0px; border-right: 1px solid #ccc;"><?php echo $rows['ord_quantity']; ?>pcs</td>
							<td style="margin: 0px; padding: 0px;width:100px; border-right: 1px solid #ccc;"></td>
							<td style="margin: 0px; padding: 0px;width:100px; border-right: 1px solid #ccc;"></td>
							<td style="margin: 0px; padding: 0px;width:100px; border-right: 1px solid #ccc;"></td>
							<td style="margin: 0px; padding: 0px;"><?php echo $rows['ord_amount']; ?></td>
						</tr>
				  </tbody>
				 </table>
				<div style="border:1px solid #ccc; border-left:0; border-right:0; font-size:14px;">
					<span>Amount chargable (in words)</span> <span style="float:right;">E.OE</span>
                   </br>
                   <span> <b>Indian Rupees One Hundred Ninety Nine Only</b></span>
					<div style="padding-top:140px;">
						<table style="width:100%;" cellspacing="0">
							<tr>
								<td style="font-size:14px; padding-right:15px;">
									Company's VAT TIN : <b>08462256374</b></br>
									Company's CST No. : <b>08462256374</b></br>
									Declaration<br>
									We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct.
								</td>
								<td style="font-size:14px; width: 324px;">
									Company's Bank Details:</br>
									Bank Name:<b> Induslnd Bank Ltd. </b></br>
									A/c No.: <b>200001737321</b><br>
									Branch & IFS Codw:<b> Johri Bazar Bramch & INDB0000279</b>
								</td>
							</tr>
							<tr>
								<td style="height:50px; border-top:1px solid #ccc; verticle-align:top;">Customer seal and signature</td>
								<td style="border-top:1px solid #ccc; text-align:right;"><b>for Trinetra</b><br>Authorised Sign</td>
							</tr>
						<table>
					</div>
				</div>
			</div>
	</body>
	</div>
	</div>
    <button type="submit" name="invoice_submit" class="btn btn-icon btn-primary glyphicons circle_ok"><i></i>Save</button>
</form>  