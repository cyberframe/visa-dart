<?php if($_REQUEST['t'] == 'pdf') { 
require_once("../../../vender/pdf/mpdf.php");
require_once("../../applicationtop.php");
 } ?>
<?php 

 ?>
<?php
$ledcond="led_id='".$_REQUEST['id']."' && is_delete=0";
$ledselect=$common_obj->fun_select("leads",$ledcond);


$usercond="u_id='".$ledselect['led_user_id']."' && is_delete=0";
$userselect=$common_obj->fun_select("user_details",$usercond);

$ledappcond="lep_lead_id='".$_REQUEST['id']."' && is_delete=0";
$ledappselect=$common_obj->fun_select("leads_applicant",$ledappcond);

$condvisa=" vt_id='".$ledselect['led_visatype_id']."' && is_delete=0";
$selectvisa=$common_obj->fun_select("visa_type",$condvisa);

$selectaddress=$common_obj->fun_select("address","is_delete=0");


?>
 
		<div style="font-family:arial; font-size:14px;font-family:arial;">
			
			<div style="margin:0px auto; width: 100%;font-family:arial; border:1px solid #ccc;">
				<table style="width:100%;" cellspacing="0" cellpadding="5px">
					<tr>
						
						<td colspan="2" style="text-align: center; font-size: 13px; width: 250px; margin: 0px; padding: 0px; ">
                        <img src="../../img/evisa-logo.png" style="width:100px;"/>
                        <br/><br/>
							<span style="padding:10 10px 0 0; font-size:20px; font-weight:bold; ">DubaiEvisa</span><br>
							<span>Registered Office : G-2/3 Second Floor Sector 16<br/>Rohini Delhi 110085<br>Tel. No. <?= $selectaddress['add_phone']; ?><br>E-Mail: <?= $selectaddress['add_email']; ?>
                            <br/>CIN : U63000DL2016PTC292151<br/>
Service TAX # AAACY7923KSD001
                            </span>
                            <br/><br/>
                         <span style="padding:10 10px 0 0; font-size:20px; font-weight:bold; ">INVOICE</span>
                            
						</td>
						
						
					</tr>
                    <tr>
						
						<td style=" width: 60%; vertical-align: top;  font-size:13px; text-transform:capitalize">
                        <strong>Billed To</strong><br/>
                        <?php echo $userselect[0]['u_name'];?>
                        <br/>
                        <?php echo $userselect[0]['u_emailid'];?>
                        <br/>
                        <?php echo $userselect[0]['u_mobileno'];?>
                        </td>
						<td style=" vertical-align: top;  font-size:13px;">
							<p style="padding-bottom: 0px; margin-bottom: 0px;">Invoice No: <b><?php echo $ledselect[0]['led_invoice_no']; ?></b> </p>
						  <p style="padding-bottom: 0px; margin-bottom: 0px;">Date: <b><?php echo date("d M Y",$ledselect[0]['led_insert_date']);  ?></b> </p>	
							<p style="padding-bottom: 0px; margin-bottom: 0px;">Ref. No.: <b><?php echo $ledselect[0]['led_lead_no']; ?></b></p>
                          
							
                             
						</td>
						
					</tr>
					
				</table>
				<table style="width:100%;" cellspacing="0" cellpadding=" 5px">
					<tbody>
						<tr>
					  <th style="border:1px solid #ccc; border-left:0;">S.No</th>
                      <th style="border:1px solid #ccc; border-left:0; ">Applicant Name</th>
                      
					  <th style="border:1px solid #ccc; border-left:0; ">Services</th>
					  <th style="border:1px solid #ccc; border-left:0; text-align:right"  align="right">Rate</th>
                       <th style="border:1px solid #ccc; border-left:0; text-align:right"  align="right">Amount</th>
					
			
					 
					</tr>
                  <?php 
				  $sn = $gt = 0;
				  foreach($ledappselect as $countins)
{
	
	
			$appcond="ad_id='".$countins['lep_applicant_id']."'";
			$appselect=$common_obj->fun_select("applicant_details",$appcond);
			// $applicant_name = "";
			$applicant_name = $appselect[0]['ad_first_name']." ".$appselect[0]['ad_last_name'];
						$sn++;
	?>
				  
						<tr style="vertical-align: top; text-align:left;">
							<td style=" vertical-align: top;border-right: 1px solid #ccc;  text-align:left;">
								<?php echo $sn;?>
                            </td>
							<td style=" vertical-align: top;border-right: 1px solid #ccc;  text-align:left;">
								<?php echo $applicant_name;?>
                             
                            </td>
                            <td style=" vertical-align: top;border-right: 1px solid #ccc;  text-align:left;">
								<?php echo $selectvisa[0]['vt_title'];?>
                                <?php if($ledselect[0]['led_otb'] != '0')
								{
									echo "</br>OTB Charges";
									
								}
								?>
                                <?php if($ledselect['led_express_visa'] != '0')
								{
									echo "</br>Express Charges";
									
								}
								?>
                                 <?php if($countins['lep_insurance'] != '0')
								{
									echo "</br>Insurance";
									
								}
								?>
                            </td>
                            <td style=" vertical-align: top;border-right: 1px solid #ccc;  text-align:right;"  align="right">
								<?php echo $ledselect['led_visa_amount'];?>
                                <?php if($ledselect['led_otb_charge'] != '0')
								{
									echo "<br/>".$ledselect['led_otb_charge'];
									
								}
								?>
                                <?php if($ledselect['led_express_add_on'] != '0')
								{
									echo "<br/>".$ledselect['led_express_add_on'];
									
								}
								 if($countins['lep_insurance'] != '0')
								{
									echo "<br/>".$countins['lep_insurance_amount'];
									
								}
								
								$total_rate = 0;
								$total_rate = $ledselect['led_visa_amount']+$ledselect['led_otb_charge']+$ledselect['led_express_add_on']+$countins['lep_insurance_amount'];
								$gt += $total_rate;
								?>
                                
                            </td>
                            <td style=" vertical-align: top;border-right: 1px solid #ccc;  text-align:right;"  align="right">
                            <?php echo number_format($total_rate,2,".",",");?>
                            	
                            </td>
                            
                            </tr>
                         
                        <?php } ?>    
                          <tr style="border-top: 1px solid #ccc!important;">
                         	<th colspan="4" align="right" style="border-right: 1px solid #ccc; ">
                            	Grand Total
                            </th>
                            <th colspan="" align="right" style=" text-align:right; border-right: 1px solid #ccc;" >
                            <?php echo number_format($gt,2,".",",");?>
                            </th>
                          </tr>
                          <tr style="border-top: 1px solid #ccc!important;">
                         	<th colspan="5" align="right" style="border-right: 1px solid #ccc; text-transform: capitalize; ">
                            	
                            <?php echo "In Words: ".$leads_obj->convert_number_to_words($gt);?>
                            </th>
                          </tr>
                         <tr style="border-top: 1px solid #ccc;" align="justify" >
                         	<td colspan="5" style="font-size: 8pt;">
                            <span style="text-align:center; margin-bottom: 20px;"><strong>THIS IS SYSTEM GENERATED INVOICE HENCE DOESN'T REQUIRE SIGNATURE</strong></span>
                            <br /><br/>
                           		<strong>Terms and Conditions</strong>
                              <ul>
                       		      <li>Status of Application is normally available in 4-5 working days from the date of submission of your application (excludes Friday &amp; Saturday).</li>
                       		      <li>Request you to refer, read &amp; understand the disclaimer which is available on our website www.dubaievisa.in</li>
                       		      <li>Application is subject to approval from the concerned Government Authorities.</li>
                       		      <li>The decision to grant or refuse or hold an application is the sole prerogative of the concerned Government authorities. DubaieVISA.in does not in any way influence the same.</li>
                       		      <li>Application processing fees are non-refundable, irrespective of the circumstances.</li>
                       		      <li>Advice in your interest to check &quot;OK TO BOARD&quot; status 48 hrs (UAE working days) prior to your scheduled departure.</li>
                       		      <li>Application &amp; its details terms &amp; conditions as governed by concerned authorities including liabilities / penalties, if any, will apply.</li>
                       		      <li>Rates are subject to change without prior notice &amp; rates applicable on the day of issuance of visa will be applicable.</li>
                       		      <li>E. &amp; O. E. Accepted.</li>
                       		      <li>The above mentioned amount is all inclusive Fees and Service Tax.</li>
                       		      <li>Any queries, please call our Call Center phone No 91 782 783 3000, Email: info@dubaievisa.in, Mon to Sat-10.00 am to 7.00 pm.</li>
                       		      <li>If you notice any incorrect information in the invoice please bring to our notice in writing immediately.</li>
                   		      </ul>
               		       </td>
                         </tr>
                        
				  </tbody>
				 </table>
				  
			</div>
           
	</div>
	