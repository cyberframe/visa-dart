
<?php
$ledcond="led_id='".$_REQUEST['id']."' && is_delete=0";
$ledselect=$db_obj->fun_select_one("leads",$ledcond);

$ledappcond="lep_lead_id='".$_REQUEST['id']."' && is_delete=0";
$ledappselect=$db_obj->fun_select("leads_applicant",$ledappcond);
$instotal=0;
foreach($ledappselect as $countins)
{
	$instotal+=$countins['lep_insurance_amount'];
}

$condvisa=" vt_id='".$ledselect['led_visatype_id']."' && is_delete=0";
$selectvisa=$db_obj->fun_select_one("visa_type",$condvisa);

$selectaddress=$db_obj->fun_select_one("address","is_delete=0");


?>
<?php
	if (isset($_POST['submit']))
	{
		$msg=$leads_obj->fun_add_invoice();
	}
?>
<?php
require '../include/header.php';
?>
<script>

$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			fdc_name:{required:true, maxlength:25},
			fdc_type:{required:true},
			},
	});
	});
</script>	  
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">Leads</a></li>
            <li class="active">
            <a href="">Invoice</a>
            </li>
            </ul>
            <h3 class="page-header">
           Invoice  <a href="index.php" class="btn btn-info pull-right">Go Back </a> 
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form class="form-horizontal"  method="post" action="" autocomplete="off">
	
	<div class="form-box-content">
	<div class="col-md-8">
		<body style="font-family:arial; font-size:14px;font-family:arial;">
			<p style="text-align:center; margin-bottom:2px;">INVOICE</p>
			<div style="margin:0px auto; width: 100%;font-family:arial; border:1px solid #ccc;">
				<table style="width:100%;" cellspacing="0">
					<tr>
						<td style="margin: 0px; padding: 0px;  border-bottom: 1px solid #ccc;">
							<img src="../../img/evisa-logo.png" style="width:100px;"/>
						</td>
						<td style="text-align: left; font-size: 13px; border-right: 1px solid #ccc; width: 250px; margin: 0px; padding: 0px; border-bottom: 1px solid #ccc;">
							<span style="padding:0 10px 0 0; font-size:13px; font-weight:bold; ">DubaiEvisa</span><br>
							<span><?= $selectaddress['add_address']; ?><br>Tel. No. <?= $selectaddress['add_phone']; ?><br>E-Mail: <?= $selectaddress['add_email']; ?></span>
						</td>
						<td style="width:25%; vertical-align: top;  font-size:13px;">
							<p style="padding-bottom: 0px; margin-bottom: 0px;">Invoice no </p>
							<b>Invoice no </b>
							<p style="padding-bottom: 0px; margin-bottom: 0px;">Reference No.</p>
							<b><?php echo $ledselect['led_lead_no']; ?></b>
                            <input name="reference_no" id="reference_no" value="<?php echo $ledselect['led_lead_no']; ?>" type="hidden" />
                             <input name="invoice_no" id="invoice_no" value="" type="hidden" />
						</td>
						<td style="vertical-align: top;  font-size:13px;   width: 19%">
							<p style="padding-bottom: 0px; margin-bottom: 0px;">Dated </p>
							<b><?php echo date("d M Y",$ledselect['led_insert_date']);  ?></b>
                             <input name="dated" id="dated" value="<?php echo $ledselect['led_insert_date'];  ?>" type="hidden" />
						</td>
					</tr>
					<tr>
						
						<td style="border-right:1px solid #ccc;"></td>
					</tr>
				</table>
				<table style="width:100%; height:300px;" cellspacing="0">
					<tbody>
						<tr>
					  <th style="border:1px solid #ccc; border-left:0; margin: 0px; padding: 6px; height:35px;">Particular</th>
                      
					  <th style="border:1px solid #ccc; border-left:0; margin: 0px; padding: 6px;">Qty</th>
					  <th style="border:1px solid #ccc; border-left:0; margin: 0px; padding: 6px;">Rate</th>
					
			 <th style="border:1px solid #ccc; border-left:0; margin: 0px; padding: 6px;">Amount</th>
					 
					</tr>
						<tr style="vertical-align: top; text-align:right;">
							<td style="margin: 0px; padding: 0px; border-right: 1px solid #ccc;  text-align:center; max"><?php echo $selectvisa['vt_title'];?>
                            <input name="visa_title" id="visa_title" value="<?php echo $selectvisa['vt_title'];?>" type="hidden" />
                            </td>
							<td style="margin: 0px; padding: 0px;width:250px; text-align:left;  border-right: 1px solid #ccc;"><?php echo count($ledappselect); ?>
                             <input name="visa_qty" id="visa_qty" value="<?php echo count($ledappselect); ?>" type="hidden" />
                            </td>
							<td style="margin: 0px; padding: 0px; border-right: 1px solid #ccc;"><?php echo $ledselect['led_visa_amount']; ?>
                             <input name="visa_amount" id="visa_amount" value="<?php echo $ledselect['led_visa_amount']; ?>" type="hidden" />
                            </td>
							<td style="margin: 0px; padding: 0px;width:100px; border-right: 1px solid #ccc;"><?php echo $ledselect['led_visa_amount']*count($ledappselect); ?>
                             <input name="total_visa_amount" id="total_visa_amount" value="<?php echo $ledselect['led_visa_amount']*count($ledappselect); ?>" type="hidden" />
                            </td>
							
                            
                            </tr>
                            <tr style="vertical-align: top; text-align:right;">
							<td style="margin: 0px; padding: 0px; border-right: 1px solid #ccc;  text-align:center; max">Express Charge</td>
							<td style="margin: 0px; padding: 0px;width:250px; text-align:left;  border-right: 1px solid #ccc;"><?php echo count($ledappselect); ?> 
                             <input name="qty_express_add_on" id="qty_express_add_on" value="<?php echo count($ledappselect); ?>" type="hidden" />
                            </td>
							<td style="margin: 0px; padding: 0px; border-right: 1px solid #ccc;"><?php echo $ledselect['led_express_add_on']; ?>
                             <input name="express_add_on" id="express_add_on" value="<?php echo $ledselect['led_express_add_on']; ?>" type="hidden" />
                            </td>
							<td style="margin: 0px; padding: 0px;width:100px; border-right: 1px solid #ccc;"><?php echo count($ledappselect)*$ledselect['led_express_add_on']; ?>
                             <input name="total_express_add_on" id="total_express_add_on" value="<?php echo count($ledappselect)*$ledselect['led_express_add_on']; ?>" type="hidden" />
                            </td>
							
                            
                            </tr>
                            <tr style="vertical-align: top; text-align:right;">
							<td style="margin: 0px; padding: 0px; border-right: 1px solid #ccc;  text-align:center; max">OTB</td>
							<td style="margin: 0px; padding: 0px;width:250px; text-align:left;  border-right: 1px solid #ccc;"><?php echo count($ledappselect); ?></td>
							<td style="margin: 0px; padding: 0px; border-right: 1px solid #ccc;"><?php echo $ledselect['led_otb_charge']; ?>
                             <input name="otb_charge" id="otb_charge" value="><?php echo $ledselect['led_otb_charge']; ?>" type="hidden" />
                            </td>
							<td style="margin: 0px; padding: 0px;width:100px; border-right: 1px solid #ccc;"><?php echo $ledselect['led_otb_charge']*count($ledappselect); ?>
                            <input name="total_otb_charge" id="total_otb_charge" value="<?php echo $ledselect['led_otb_charge']*count($ledappselect); ?>" type="hidden" />
                            </td>
							
                            
                            </tr>
                            <tr style="vertical-align: top; text-align:right;">
							<td style="margin: 0px; padding: 0px; border-right: 1px solid #ccc;  text-align:center; max">Insurance</td>
							<td style="margin: 0px; padding: 0px;width:250px; text-align:left;  border-right: 1px solid #ccc;">--</td>
							<td style="margin: 0px; padding: 0px; border-right: 1px solid #ccc;">--</td>
							<td style="margin: 0px; padding: 0px;width:100px; border-right: 1px solid #ccc;"><?php echo $instotal; ?>
                            <input name="insurance" id="insurance" value="<?php echo $instotal; ?>" type="hidden" />
                            </td>
							
                            
                            </tr>
                            
                            <tr style="vertical-align: top; text-align:right;">
                                <td style="border-bottom: 1px solid #ccc;margin: 0px; padding: 0px; border-right: 1px solid #ccc;text-align:center; height:35px;"></td>
                                <td style="border-bottom: 1px solid #ccc; margin: 0px; padding: 0px;width:250px; text-align:left;  border-right: 1px solid #ccc;"></td>
                                <td style="border-bottom: 1px solid #ccc; margin: 0px; padding: 0px; border-right: 1px solid #ccc;"></td>
                                <td style="  border-bottom: 1px solid #ccc; margin: 0px; padding: 0px;width:100px; border-right: 1px solid #ccc; vertical-align: middle;">
                                </td>
                                
                        	</tr>
                        
						<tr style="vertical-align: top; text-align:right;">
							<td style="margin: 0px; padding: 0px; border-right: 1px solid #ccc;text-align:center; height:35px;"></td>
							<td colspan="2" style="margin: 0px; padding: 0px;width:250px; text-align:left;  border-right: 1px solid #ccc;">Grand Total</td>
							
							
							<td style="margin: 0px; padding: 0px;width:100px; border-right: 1px solid #ccc;"><?php echo $ledselect['led_visa_amount']*count($ledappselect)+count($ledappselect)*$ledselect['led_express_add_on']+$ledselect['led_otb_charge']*count($ledappselect)+$instotal; ?></td>
                            <input name="grand_total" id="grand_total" value="<?php echo $ledselect['led_visa_amount']*count($ledappselect)+count($ledappselect)*$ledselect['led_express_add_on']+$ledselect['led_otb_charge']*count($ledappselect)+$instotal; ?>" type="hidden" />
						</tr>
				  </tbody>
				 </table>
				  
			</div>
            <button class="btn btn-success pull-right"  name="submit" type="submit"><i class="icon-save"></i> PDF</button>
	</body>
	</div>
   
	</div>
    
    </form>
   
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->


</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>