<?php require_once("../../applicationtop.php"); $page="news_flash";?>
<?php
	$page="pages";
	if (isset($_POST['ps_smt']))
	{
		$msg=$news_obj->fun_add();
	}
?>
<?php
$nf_date = date("d-m-Y");
if($_REQUEST['id'] != '')
{
	$sql=" nf_id='".$_REQUEST["id"]."'";
	$resultselect= $common_obj->fun_select("news_flash",$sql);
	foreach($resultselect as $row)
	{
		$nf_date 		=	date("d-m-Y",$row["nf_date"]);
		$nf_title 		=	$row["nf_title"];
		$nf_content 	=	$row["nf_content"];
		$nf_image		=   $row["nf_image"];
	}
}
?>
<?php
require '../include/header.php';
?>
	<script src="<?php echo ROOT_ADMIN;?>tiny_mce/tiny_mce.js" type="text/javascript"></script>
    <script src="<?php echo ROOT_ADMIN;?>tiny_mce/tinymice.js" type="text/javascript"></script>
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">News</a></li>
            <li class="active">
            <a href=""><?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?></a>
            </li>
            </ul>
            <h3 class="page-header">
            <?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$db_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            <div class="form-box-content"> 
            <div class="form-group">
                    
                    <label class="control-label col-md-2" for="example-input-normal">Date</label>
                    <div class="col-md-4">
                    <input  placeholder="Enter Date" readonly class="form-control req date-picker-all" type="text" id="nf_date" name="nf_date" value="<?php echo $nf_date; ?>" >
                    </div>
                    </div>  
                    <div class="form-group">
                    
                    <label class="control-label col-md-2" for="example-input-normal">Title</label>
                    <div class="col-md-4">
                    <input  placeholder="Enter Title" class="form-control req" type="text" id="nf_title" name="nf_title" value="<?php echo $nf_title; ?>" >
                    </div>
                    </div> 
                        
            <div class="form-group">
            
                                <label class="control-label col-md-2" for="example-textarea-large">Content</label>
                                <div class="col-md-10">
                                    <textarea id="nf_content" name="nf_content" class="form-control" rows="25"><?php echo $nf_content; ?></textarea>
                                
                            </div>
                            </div>
							<div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Upload Image</label>
				
				<div class="col-md-4">
                    <input id="uploadFile" class="" type="file"  name="news_image"  >
                    <input value="<?php echo $news_image;?>" type="hidden" name="oldimage"  >
				</div>
                
                	</div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Status</label>
                    <div class="col-md-4">
                    <div class="radio radio-inline">
                    <label for="example-radio-inline1">
                    <input type="radio" id="nf_status" <?php if($nf_status==1){echo "checked='checked'";}?> name="nf_status" value="1"> Active
                    </label>
                    </div>
                    <div class="radio radio-inline">
                    <label for="example-radio-inline2">
                    <input type="radio" id="nf_status" <?php if($nf_status==0){echo "checked='checked'";}?> name="nf_status" value="0"> Inactive
                    </label>
                    </div>
                    </div>
                    </div>
                    <div class="clearfix"></div>
			 <div class="form-actions">
                                <div class="col-md-offset-2">
                                <button class="btn btn-success " onClick="return confirm('Are you sure Save this Entry.');" name="ps_smt" type="submit"><i class="icon-save"></i> Save</button>
									  <a href="index.php" onClick="return confirm('Are you sure cancel this and Go back.');" class="btn btn-danger"><i class="icon-remove"></i> Cancel</a>
									
                                   
                                </div>
                            </div>
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->
</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>