<?php require_once("../../applicationtop.php"); $page="news_flash";?>
<?php
$page="news_flash";
$con=" nf_id='".$_REQUEST['id']."' && is_delete=0";
$pages=$common_obj->fun_select("news_flash",$con);
?>
<?php
require '../include/header.php';
?>
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">News</a></li>
            <li class="active">
            <a href="">View</a>
            </li>
            </ul>
            <h3 class="page-header">
            View <a href="index.php" class="btn btn-info pull-right">Go Back </a>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="food_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            <div class="form-box-content">   
                    <div class="form-group">
                    
                    <label  class="control-label col-md-2" for="example-input-normal">Date</label>
                    <div class="col-md-4">
                    
                   <?php echo date('d-m-Y',$pages[0]['nf_date']); ?>
                   
                    </div>
                    </div> 
                    
                    <div class="form-group">
                    
                    <label  class="control-label col-md-2" for="example-input-normal">Title</label>
                    <div class="col-md-4">
                    
                   <?php echo $pages[0]['nf_title']; ?>
                   
                    </div>
                    </div> 
                    
                        
            <div class="form-group">
            
                                <label class="control-label col-md-2" for="example-textarea-large">Content</label>
                                <div class="col-md-10">
                                    <?php echo $pages[0]['nf_content']; ?>
                                
                            </div>
                            </div>
                            <div class="clearfix"></div>
			 
                            </div>
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->
</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>