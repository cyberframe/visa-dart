<?php require_once("../../applicationtop.php"); $page="email_template";?>
<?php
    $page="pages";
    if (isset($_POST['ps_smt'])){
        $msg=$pages_obj->fun_add();
    }
?>
<?php
    $page_image = '';
    if($_REQUEST['id'] != ''){
        $sql=" pag_id='".$_REQUEST["id"]."'";
        $resultselect= $common_obj->fun_select("pages",$sql);
        foreach($resultselect as $row){
            $pag_title      =   $row["pag_title"];
            $pag_content    =   $row["pag_content"];
            $page_image      =   $row["page_image"];
        }
    }
?>
<?php
    require '../include/header.php';
?>
<script src="<?php echo ROOT_ADMIN;?>tiny_mce/tiny_mce.js" type="text/javascript"></script>
<script src="<?php echo ROOT_ADMIN;?>tiny_mce/tinymice.js" type="text/javascript"></script>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
        <li><a href="index.php">Pages</a></li>
        <li class="active">
            <a href=""><?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?></a>
        </li>
    </ul>
    <h3 class="page-header">
        <?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?>
    </h3>
    <?php  if($msg!='') { ?>
    <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Error! </strong> <?php echo $msg; ?>
    </div>
    <?php } ?>
    <?php
        //$db_obj->fun_session_alert();
        ?>
    <!-- Nav Dash -->
    <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
        <div class="form-box-content">
            <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Title</label>
                <div class="col-md-4">
                    <input  placeholder="Enter Title" class="form-control req" type="text" id="pag_title" name="pag_title" value="<?php echo $pag_title; ?>" >
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="example-textarea-large">Content</label>
                <div class="col-md-10">
                    <textarea id="pag_content" name="pag_content" class="form-control" rows="25"><?php echo $pag_content; ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Upload Image</label>
                <div class="col-md-4">
                    <input id="uploadFile" class="" type="file" name="page_image"  >
                    <input value="<?php echo $page_image;?>" type="hidden" name="oldimage"  >
                </div>
            </div>
            <?php if($_REQUEST['id']!='') { ?>
            <div id="imagehideidinedit" class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Current Image</label>
                <div class="col-md-4">
                    <?php 
                        if($page_image!='') { ?>
                    <img src="../../../img/page_images/<?php echo $page_image;?>" width="300" height="150">
                    <?php } ?>
                </div>
            </div>
            <?php } ?> 
            <div class="clearfix"></div>
            <div class="form-actions">
                <div class="col-md-offset-2">
                    <button class="btn btn-success " onClick="return confirm('Are you sure Save this Entry.');" name="ps_smt" type="submit"><i class="icon-save"></i> Save</button>
                    <a href="index.php" onClick="return confirm('Are you sure cancel this and Go back.');" class="btn btn-danger"><i class="icon-remove"></i> Cancel</a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
    <div class="clearfix"></div>
    <!-- END Nav Dash -->
</div>
<!-- END Page Content -->
<?php
    require '../include/footer.php';
?>