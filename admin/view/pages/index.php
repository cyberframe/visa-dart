<?php 
require_once("../../applicationtop.php"); 
$page="email_template";
?>
<?php
    if(($_REQUEST["s"] == "false") && (empty($_POST)))
    {
        $_SESSION["con1"] = ""; 
        $_SESSION["pn"] = "";
        if(!isset($_POST['searchbtn'])){ unset($_SESSION[$search]); }
    }
    else
    {
        $con = $_SESSION["con1"];
    }
    
    
    if(isset($_POST['searchbtn']))
    {   
        $con='';
        $search=trim($_POST['newstitle']);
        if($search!='')
        {
            $_SESSION[$search] = $search;
            $con .=" && pag_title LIKE '%".$search."%'";
        }
        else{
            $_SESSION[$search] = '';
        }
        $_SESSION["con1"] = $con;
    }
    
        $page = "pages";
        $order=" order by pag_id DESC ";
        $pageSize =PAGESIZE;
        $resultarray1 = $login_obj->pagno("".DBPREFEX."pages where pag_id !=''  && is_delete='0' ".$con.$order,$pageSize);
        $pageno = $resultarray1[0];
        $resultselect12 = $resultarray1[1];
        $rows=mysql_num_rows($resultselect12);
?>
<?php
    require '../include/header.php';
    ?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
        <li><a href="">Pages</a></li>
    </ul>
    <h3 class="page-header">
        Pages
    </h3>
    <!--<a href="pages_save.php" class="btn btn-success " style="margin-bottom: 2%;"> Add New</a>-->
    <!-- Nav Dash -->
    <?php 
        if($_REQUEST['msg']!='')
        {
        ?>
    <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>success! </strong> <?php echo $_REQUEST['msg']; ?>
    </div>
    <?php } ?>
    <form method="post">
        <div class="form-horizontal form-box">
            <div class="col-md-3">
                <div class="form-group">
                    <input  placeholder="Search By Page Title" class="form-control" type="text" name="newstitle" value="<?php echo $_SESSION[$search] ; ?>" >
                </div>
            </div>
            <div class="col-md-2">
                <button class="btn btn-success " name="searchbtn" type="submit" style="margin-top:9px; "><i class="icon-search"></i> Search</button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
    <div class="clearfix"></div>
    <table class="table table-bordered" id="news-id">
        <thead>
            <tr>
                <th class="hidden-xs hidden-sm">Page Title</th>
                <th class="hidden-xs hidden-sm">Page Image</th>
                <th class="cell-small text-center"><i class="icon-bolt"></i> Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                while($ps_row = mysql_fetch_array($resultselect12))
                {
                ?>
            <tr>
                <td>
                    <?php echo $ps_row['pag_title']; ?>
                </td>
                <td>
                    <?php if($ps_row['page_image']!='') { ?>
                        <img src="../../../img/page_images/<?php echo $ps_row['page_image'];?>" width="300" height="150">
                    <?php } ?>
                </td>
                <td class="text-center">
                    <a href="view.php?id=<?php  echo $ps_row['pag_id'] ?>" data-toggle="tooltip" title="View" class="btn btn-xs btn-info"><i class="icon-info-sign"></i></a>
                    <div class="btn-group">
                        <a href="add.php?id=<?php echo $ps_row['pag_id'];?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-success"><i class="icon-pencil"></i></a>
                    </div>
                </td>
            </tr>
            <?php } ?>
            <?php if($pageno>1){  $login_obj->paging_no("",$pageno,"2");} ?>
        </tbody>
    </table>
</div>
<?php
    require '../include/footer.php';
?>