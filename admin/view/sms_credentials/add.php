<?php require_once("../../applicationtop.php"); $page="sms_credentials";?>
<?php
if(isset($_POST['submit']))
{
	$msg = $sms_credentials_obj->fun_add();
}


if($_REQUEST['id'] != '')
{
	$sql=" sms_id='".$_REQUEST["id"]."'";
	$resultselect= $common_obj->fun_select("sms_credential",$sql);
	foreach($resultselect as $row)
	{
		$sms_name 			=	$row["sms_name"];
		$sms_username 		= 	$row["sms_username"];
		$sms_password 		=	$row["sms_password"];
		$sms_url 			=	$row["sms_url"];
		$sms_status 		=	$row["sms_status"];
	}
}
require '../include/header.php';
?>
 
<script>

$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			fdc_name:{required:true, maxlength:25},
			fdc_type:{required:true},
			},
	});
	});
</script>	
 
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">SMS Credentials</a></li>
            <li class="active">
            <a href=""><?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?></a>
            </li>
            </ul>
            <h3 class="page-header">
           <?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            
                   
                 <div class="form-box-content">
                    
                  <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">User Name <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter User Name" class="form-control req" id="sms_username"  name="sms_username" value="<?php echo $sms_username; ?>" >
                </div>
                </div>
                
                
                    
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Password <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Password" class="form-control req" id="sms_password"  name="sms_password" value="<?php echo $sms_password; ?>" >
                </div>
                </div>
                
                 <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">SMS Name <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter SMS Name" class="form-control req" id="sms_name"  name="sms_name" value="<?php echo $sms_name; ?>" >
                </div>
                </div>
                
                
                    
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">URL <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter URL" class="form-control req" id="sms_url"  name="sms_url" value="<?php echo $sms_url; ?>" >
                </div>
                </div>
                
                 <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Status</label>
                    <div class="col-md-4">
                    <div class="radio radio-inline">
                    <label for="example-radio-inline1">
                    <input type="radio" id="sms_status" <?php if($sms_status==1){echo "checked='checked'";}?> name="sms_status" value="1"> Active
                    </label>
                    </div>
                    <div class="radio radio-inline">
                    <label for="example-radio-inline2">
                    <input type="radio" id="sms_status" <?php if($sms_status==0){echo "checked='checked'";}?> name="sms_status" value="0"> Inactive
                    </label>
                    </div>
                    </div>
                    </div>
                          
                
                            </div>
			  <div class="clearfix"></div>
			 <div class="form-actions">
                                <div class="col-md-offset-2">
                                <button class="btn btn-success "  name="submit" type="submit"><i class="icon-save"></i> Save</button>
									  <a href="index.php" class="btn btn-danger"><i class="icon-remove"></i> Cancel</a>
									
                                   
                                </div>
                            </div>
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->


</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>