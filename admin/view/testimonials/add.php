<?php require_once("../../applicationtop.php"); $page="testimonials";?>
<?php
	if (isset($_POST['submit']))
	{
		$msg=$testimonials_obj->fun_add();
	}
?>
<?php
	if($_REQUEST['id'] != '')
	{
		$sql=" tsm_id='".$_REQUEST["id"]."'";
		$resultselect= $common_obj->fun_select("testimonials",$sql);
		foreach($resultselect as $row)
		{
			$tsm_sessionid 		=	$row["tsm_sessionid"];
			$tsm_message 		= 	$row["tsm_message"];
			$tsm_image 			=	$row["tsm_image"];
			$tsm_insert_date 	=	$row["tsm_insert_date"];
			$tsm_name 			=	$row["tsm_name"];
			$tsm_status         =   $row["tsm_status"];
		}
	}
	?>

<?php
require '../include/header.php';
?>
 
<script>

$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			fdc_name:{required:true, maxlength:25},
			fdc_type:{required:true},
			},
	});
	});
</script>	
 
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">Testimonials</a></li>
            <li class="active">
            <a href=""><?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?></a>
            </li>
            </ul>
            <h3 class="page-header">
           <?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            <div class="form-box-content"> 
            
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Name <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Name" class="form-control req" id="tsm_name"  name="tsm_name" value="<?php echo $tsm_name; ?>" >
                </div>
                </div>  
                   
                 <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Status</label>
                    <div class="col-md-4">
                    <div class="radio radio-inline">
                    <label for="example-radio-inline1">
                    <input type="radio" id="tsm_status" <?php if($tsm_status==1){echo "checked='checked'";}?> name="tsm_status" value="1"> Active
                    </label>
                    </div>
                    <div class="radio radio-inline">
                    <label for="example-radio-inline2">
                    <input type="radio" id="tsm_status" <?php if($tsm_status==0){echo "checked='checked'";}?> name="tsm_status" value="0"> Inactive
                    </label>
                    </div>
                    </div>
                    </div>
                <div class="form-group">
                                <label class="control-label col-md-2" for="example-textarea-elastic">Message : <span style="color:#F00;">*</span> </label>
                                <div class="col-md-4">
                                    <textarea id="tsm_message" name="tsm_message" class="form-control textarea-elastic req" rows="3"><?php echo $tsm_message; ?></textarea>
                                </div>
							</div>
                          <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Image</label>
				
				<div class="col-md-4">
                    <input id="uploadFile" class="" type="file" name="tsm_image"  >
                    <input value="<?php echo $tsm_image;?>" type="hidden" name="oldimage"  >
				</div>
                	</div>
                     <div id="imagehideid" class="form-group hide">
                    <label class="control-label col-md-2" for="example-input-normal">Selected Icon</label>
                    <div class="col-md-4 " >
                    <div class=" imagePreview " id="imagePreview">
                    </div>
                    </div>
                    </div>
                    <?php if($_REQUEST['id']!='') { ?>
                    <div id="imagehideidinedit" class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Image</label>
                    <div class="col-md-4">
                   <?php 
				if($tsm_image!='') { ?>
                <img src="../../../img/testimonials/<?php echo $tsm_image;?>" width="150" height="40">
                <?php } ?>
                    </div>
                    </div>
                   <?php } ?> 
                
                            </div>
			  <div class="clearfix"></div>
			 <div class="form-actions">
                                <div class="col-md-offset-2">
                                <button class="btn btn-success "  name="submit" type="submit"><i class="icon-save"></i> Save</button>
									  <a href="index.php" class="btn btn-danger"><i class="icon-remove"></i> Cancel</a>
									
                                   
                                </div>
                            </div>
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->


</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>