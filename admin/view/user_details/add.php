<?php require_once("../../applicationtop.php"); $page="user_details";?>
<?php
	if (isset($_POST['submit']))
	{
		$msg=$user_details_obj->fun_add();
	}
?>
<?php
	if($_REQUEST['id'] != '')
	{
		$sql=" u_id='".$_REQUEST["id"]."'";
		$resultselect= $common_obj->fun_select("user_details",$sql);
		foreach($resultselect as $row)
		{
			$u_name 		=	$row["u_name"];
			$u_emailid 		= 	$row["u_emailid"];
			$u_mobileno 	=	$row["u_mobileno"];
			$u_password 	=	$row["u_password"];
			$u_dor 			=	$row["u_dor"];
			$u_status 		=	$row["u_status"];
			$u_photo 		=	$row["u_photo"];
			$u_ip 			=	$row["u_ip"];
		}
	}
require '../include/header.php';
?>
 
<script>

$(document).ready(function() {
	$("#data_form").validate({
	rules:{
			fdc_name:{required:true, maxlength:25},
			fdc_type:{required:true},
			},
	});
	});
</script>	
 
            <!-- Page Content -->
            <div id="page-content">
            <!-- Navigation info -->
            <ul id="nav-info" class="clearfix">
            <li><a href="<?php echo ROOT_VIEW; ?>/include/dashboard.php"><i class="icon-home"></i></a></li>
            <li><a href="index.php">User Details</a></li>
            <li class="active">
            <a href=""><?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?></a>
            </li>
            </ul>
            <h3 class="page-header">
           <?php if($_REQUEST['id']!=''){echo "Edit";}else{echo "Add New";} ?>
            </h3>
            <?php  if($msg!='') { ?>
            <div class="alert alert-danger fade in">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error! </strong> <?php echo $msg; ?>
            </div><?php } ?>
            <?php
            //$common_obj->fun_session_alert();
            ?>
            <!-- Nav Dash -->
            <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
            <div class="form-box-content">   
                    <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Name <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Name" class="form-control req" id="u_name"  name="u_name" value="<?php echo $u_name; ?>" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Mobile No. <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Mobile No." class="form-control req num" id="u_mobileno"  name="u_mobileno" value="<?php echo $u_mobileno; ?>" >
                </div>
                </div>
                
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Email ID <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="text"  placeholder="Enter Email ID" class="form-control req eml" id="u_emailid"  name="u_emailid" value="<?php echo $u_emailid; ?>" >
                </div>
                </div>
                
                <?php if($_REQUEST['id']==''){ ?>
                <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Password <span style="color:#F00;">*</span></label>
                <div class="col-md-4">
                <input type="password"  placeholder="Enter Password" class="form-control req" id="u_password"  name="u_password" value="" >
                </div>
                </div>
                <?php } ?>
                
                 <div class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Status</label>
                    <div class="col-md-4">
                    <div class="radio radio-inline">
                    <label for="example-radio-inline1">
                    <input type="radio" id="u_status" <?php if($u_status==1){echo "checked='checked'";}?> name="u_status" value="1"> Active
                    </label>
                    </div>
                    <div class="radio radio-inline">
                    <label for="example-radio-inline2">
                    <input type="radio" id="u_status" <?php if($u_status==0){echo "checked='checked'";}?> name="u_status" value="0"> Inactive
                    </label>
                    </div>
                    </div>
                    </div>
                    
                      <div class="form-group">
                <label class="control-label col-md-2" for="example-input-normal">Photo</label>
				
				<div class="col-md-4">
                    <input id="uploadFile" class="" type="file" name="u_photo"  >
                    <input value="<?php echo $u_photo;?>" type="hidden" name="oldimage"  >
				</div>
                	</div>
                     <div id="imagehideid" class="form-group hide">
                    <label class="control-label col-md-2" for="example-input-normal">Selected Photo</label>
                    <div class="col-md-4 " >
                    <div class=" imagePreview " id="imagePreview">
                    </div>
                    </div>
                    </div>
                    <?php if($_REQUEST['id']!='') { ?>
                    <div id="imagehideidinedit" class="form-group">
                    <label class="control-label col-md-2" for="example-input-normal">Photo</label>
                    <div class="col-md-4">
                   <?php 
				if($u_photo!='') { ?>
                <img src="../../../uploads/userprofile/<?php echo $u_photo;?>" width="150" height="40">
                <?php } ?>
                    </div>
                    </div>
                   <?php } ?> 
                
                          
                
                            </div>
			  <div class="clearfix"></div>
			 <div class="form-actions">
                                <div class="col-md-offset-2">
                                <button class="btn btn-success "  name="submit" type="submit"><i class="icon-save"></i> Save</button>
									  <a href="index.php" class="btn btn-danger"><i class="icon-remove"></i> Cancel</a>
									
                                   
                                </div>
                            </div>
           
            <div class="clearfix"></div>
      
    </div>
	  </form>
	  <div class="clearfix"></div>
    <!-- END Nav Dash -->


</div>
<!-- END Page Content -->
<?php
require '../include/footer.php';
?>