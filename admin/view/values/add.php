<?php require_once("../../applicationtop.php");
$group='values';
$page_type = "Add";
$hd = "values";
$page = "values";
$bklink = "values";
$page = "values";

if($_REQUEST['id'] != '')
{
	$sql=" vl_id='".$_REQUEST["id"]."'";
	$resultselect= $common_obj->fun_select("values",$sql);
	foreach($resultselect as $row)
	{
		$vl_title 			=	$row["vl_title"];
		$vl_value1 		    = 	$row["vl_value1"];
		$vl_value2 			=	$row["vl_value2"];
	}
}

if((isset($_POST["sendbut"])) && ($_POST["act"] == "signup"))
{
	$msg = $obj_values->fun_add();
}
require ('../include/header.php');
?>
 <div id="page-content">
  

<!-- Navigation info -->
<ul id="nav-info" class="clearfix">
	<li><a href="dashboard.php"><i class="icon-home"></i></a></li>
	<li class=""><a href="values.php">Values</a></li>
	<li class="active"><a href="">Update</a></li>
</ul>
<!-- END Navigation info -->
    <h3>Update Values</h3>
	
   <form class="form-horizontal form-box" id="contactfrm" action="" method="post" >
	<div class="form-box-content">
      <div class="form-group">
      <div class="control-label col-md-2">Title:<span class="red">*</span> </div>
      <div class="col-md-8">
        <input name="title" required  type="text" class="form-control" id="title" <?php if($vl_title !=''){ echo 'readonly="readonly"'; } ?> value="<?php echo $vl_title;?>"  >
      </div>
      </div>
	 <div class="form-group">
      <div class="control-label col-md-2">Value 1:<span class="red">*</span> </div>
      <div class="col-md-8">
        <input name="value1" required  type="text" class="form-control" id="value1"  value="<?php echo $vl_value1;?>"  >
      </div>
      </div>
       <div class="form-group">
      <div class="control-label col-md-2">Value 2: </div>
      <div class="col-md-8">
        <input name="value2"   type="text" class="form-control" id="value2"  value="<?php echo $vl_value2;?>" >
      </div>
      </div>
           <div class="form-actions">
       <input id="city_display" type="hidden" name="city_display" value="0" />
        <input id="act" type="hidden" name="act" value="signup" />
     
        <input type="submit"  name="sendbut" value="Add" class="btn btn-success" />
	 </div>
 </div>
    </form>
	
    </div>  
  
  
</div>
<?php
require '../include/footer.php';
?>