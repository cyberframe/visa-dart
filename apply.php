<?php 
include("application.php");
include("includes/head_top.php");
$page = "login";
$hd = "Login";
@extract($_REQUEST);
if (isset($_POST['submit'])) {
    $msg = $login_obj->login();
}
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="css/apply.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="./js/apply.js"></script>
    <div class="topheadbar" id="headtopbg" style="background-image: url(imgs/visaraaivalsbg.jpg);">
        <?php include("includes/header.php"); ?>
        <div class="split"></div>
        <h1>Apply Now</h1>
    </div>
    <div class="breadcum">
        <div class="wrapper">
            <div class="currently"><strong>You are viewing:</strong> &nbsp; <i class="icon fa-home"></i> &nbsp;/&nbsp; Apply Now</div>
        </div>
    </div>
    <div class="ipage">
        <div class="wrapper">
            <div class="ipage" style="background:#f2f2f2;width:100%;margin:auto;">
                <h4 style="text-align:center;font-weight:600;padding-top:20px;">Apply Now</h4>
                <ul class="nav nav-tabs">
                    <li><a href="#">Visa Processing</a></li>
                    <li><a href="#">Login</a></li>
                    <li><a href="#">Passengers</a></li>
                    <li><a href="#">Documents</a></li>
                    <li><a href="#">Overview</a></li>
                    <li class="disabled"><a href="#">Payment</a></li>
                </ul>
                <div class="tab-content">
                    <div id="visa" class="tab-pane fade in active">
                        <section class="login_sec">
                            <div class="row" style="width:100%;margin:auto;padding-bottom:5px;">
                                <div class="alert label-success text-center" id="visa_process_success" style="display:none;"></div>
                                <form id="form-one">
                                    <div class="left-section">
                                        <div class="row">
                                            <h4 class="apply-h41">Visa Processing</h4>
                                            <div style="float:left;width:50%;padding:10px 10px 10px 10px;">
                                                <div class="form-group">
                                                    <label class="apply-label">Visa Type * :</label>
                                                    <select class="form-control input-form-apply" name="visa_type" id="visa_type" onchange="getVisaType()">
                                                    <option value="0">Select One</option>
                                                    <?php 
                                                        $where="vt_status=1";
                                                        $type= $common_obj->fun_select("visa_type",$where);
                                                        if(count($type)>0){
                                                            $i=1;
                                                            foreach($type as $typenew){ 
                                                        ?>
                                                        <option value="<?=$typenew['vt_price'].','.$typenew['vt_express_add_on'].','.$typenew['vt_title'];?>"><?=$typenew['vt_title']?></option>
                                                        <?php $i++;  } } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div style="float:right;width:50%;padding:10px 10px 10px 10px;">
                                                <div class="form-group">
                                                    <label class="apply-label">Processing Priority * :</label>
                                                    <select class="form-control input-form-apply" id="pro_priority" onchange="getProcessing()">
                                                        <option value="1">Normal</option>
                                                        <option value="2">Express</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div style="float:left;width:50%;padding:10px 10px 10px 10px;">
                                                <div class="form-group">
                                                    <label class="apply-label">Date of Travel * :</label>
                                                    <input type="date" name="date_of_travel" id="date_of_travel"  min="<?php echo date("Y-m-d"); ?>" class="form-control input-form-apply" required>
                                                </div>
                                            </div>
                                            <div style="float:right;width:50%;padding:10px 10px 10px 10px;">
                                                <div class="form-group">
                                                    <label class="apply-label">Date of Return * :</label>
                                                    <input type="date" name="date_of_return" id="date_of_return"  min="<?php echo date("Y-m-d"); ?>" class="form-control input-form-apply" required>
                                                </div>
                                            </div>
                                            <div style="float:left;width:50%;padding:10px 10px 10px 10px;">
                                                <div class="form-group">
                                                    <label class="apply-label">Number of Adult * :</label>
                                                    <select class="form-control input-form-apply" name="no_of_adult" id="no_of_adult" onchange="getAdult()">
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                        <option>7</option>
                                                        <option>8</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div style="float:right;width:50%;padding:10px 10px 10px 10px;">
                                                <div class="form-group">
                                                    <label class="apply-label">Number of Child * :</label>
                                                    <select class="form-control input-form-apply" name="no_of_child" id="no_of_child" onchange="getChild()">
                                                        <option>0</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <h4 class="apply-h42">Traveller Information</h4>
                                            <div style="float:left;width:20%;padding:10px 10px 10px 10px;">
                                                <div class="form-group">
                                                    <label class="apply-label">Title * :</label>
                                                    <select class="form-control input-form-apply" name="title" id="title">
                                                        <option value="Mr">Mr</option>
                                                        <option value="Ms">Ms</option>
                                                        <option value="Mrs">Mrs</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div style="float:left;width:40%;padding:10px 10px 10px 10px;">
                                                <div class="form-group">
                                                    <label class="apply-label">First Name * :</label>
                                                    <input type="text" placeholder="First Name" name="first_name" id="first_name" class="form-control input-form-apply" required>
                                                </div>
                                            </div>
                                            <div style="float:right;width:40%;padding:10px 10px 10px 10px;">
                                                <div class="form-group">
                                                    <label class="apply-label">Last Name * :</label>
                                                    <input type="text" placeholder="Last Name" name="last_name" id="last_name" class="form-control input-form-apply" required>
                                                </div>
                                            </div>
                                            <h4 class="apply-h43">Airline Information</h4>
                                            <div style="float:left;width:50%;padding:10px 10px 10px 10px;">
                                                <div class="form-group">
                                                    <label class="apply-label">Airline * :</label>
                                                    <select class="form-control input-form-apply" name="airline" id="airline" onchange="getOTBType()">
                                                        <option value="0">Yet to Book</option>
                                                        <?php 
                                                        $newsrwhere="otb_status=1";
                                                        $otb= $common_obj->fun_select("otb",$newsrwhere);
                                                        if(count($otb)>0){
                                                            $i=1;
                                                            foreach($otb as $otbnew){ 
                                                        ?>
                                                        <option value="<?=$otbnew['otb_charge'].','.$otbnew['otb_airline'].','.$otbnew['otb_image']?>"><?=$otbnew['otb_airline']?></option>
                                                        <?php $i++;  } } ?>

                                                    </select>
                                                </div>
                                            </div>
                                            <div style="float:left;width:50%;padding:10px 10px 10px 10px;">
                                                <div class="form-group">
                                                    <label class="apply-label">OTB * :</label>
                                                    <select class="form-control input-form-apply" name="otb" id="otb" onchange="getOTBCharge()">
                                                        <option value="1">Yes</option>
                                                        <option value="0">No</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <h4 class="apply-h44">Contact Details</h4>
                                            <div style="float:left;width:50%;padding:10px 10px 10px 10px;">
                                                <div class="form-group">
                                                    <label class="apply-label">Email * :</label>
                                                    <input type="text" placeholder="Email" name="email" id="email" class="form-control input-form-apply" required>
                                                </div>
                                            </div>
                                            <div style="float:left;width:15%;padding:10px 10px 10px 10px;">
                                                <div class="form-group">
                                                    <label class="apply-label">Mobile * :</label>
                                                    <input type="text" class="form-control input-form-apply" value="+91" readonly>
                                                </div>
                                            </div>
                                            <div style="float:right;width:35%;padding:10px 10px 10px 10px;margin-top: 18px;">
                                                <div class="form-group">
                                                    <input type="text" placeholder="Mobile Number" name="mobile" id="mobile" class="form-control input-form-apply" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right-section">
                                        <h4 class="visa-fee">Visa Fee</h4>
                                        <div class="visa-type">
                                            <p style="text-align:left;padding-left:10px;margin-bottom:0.5rem;">Visa Type: <span id="user-visa-type"></span></p>
                                            <p class="left-p">Adult <span id="adult-count">(1)</span></p>
                                            <p class="right-p" id="adult-charge">0</p>
                                            <p class="left-p">Child <span id="child-count">(0)</span></p>
                                            <p class="right-p" id="child-charge">0</p>
                                        </div>
                                        <div class="express-service" style="color:black;">
                                            <p style="text-align:left;padding-left:10px;margin-bottom:0.5rem;">Express Service</p>
                                            <p class="left-p">Tax <span id="express-count">(0)</span></p>
                                            <p class="right-p" id="express-charge">0</p>
                                        </div>
                                        <div class="otb-charges">
                                            <p style="text-align:left;padding-left:10px;margin-bottom:0.5rem;">OTB Charges</p>
                                            <p class="left-p"> <span id="otb-count"></span></p>
                                            <p class="right-p" id="otb-charge">0</p>
                                        </div>
                                        <div class="total">
                                            <h6 class="left-p">Total Charges</h6>
                                            <h6 class="right-p" id="total-charge">&#2352; 0</h6>
                                        </div>
                                        
                                        <div class="submit-button">
                                            <div class="form-group">
                                                <ul class="nav nav-tabs">
                                                    <li>
                                                        <a href="#login" onClick="processVisa()" id="process_visa">
                                                            <p style="padding-top:5px;font-size:15px;">Continue</p>
                                                        </a>
                                                    </li>
                                                </ul>  
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <div id="login" class="tab-pane fade">
                        <?php if($_SESSION['uemail']==''){ ?>
                        <section class="login_sec">
                            <div class="row" style="width:100%;margin:auto;    padding-bottom: 5px;">
                                <div class="col-md-offset-3 col-md-6">
                                    <div class="login_box form-horizontal">
                                        <?php if ($msg != '') { ?>
                                            <div class="alert label-success text-center"><?php echo $msg; ?></div>
                                        <?php } ?>
                                        <form style="width:50%;margin:auto;" method="post" name="data_form" action="" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <div class="col-md-12" style="margin-bottom:10px;">
                                                    <label style="text-align:left;font-size:small;">Email ID</label>
                                                    <input type="text" placeholder="Email Id" required name="u_emailid" class="form-control" style="border:0px;border-radius:0px;background:#fff;">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label style="text-align:left;font-size:small;">Password</label>
                                                    <input type="password" required="required" placeholder="Password" required name="u_password" class="form-control" style="border:0px;border-radius:0px;background:#fff;">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <p style="text-align:left;color:red;font-size:12px;margin-bottom:0px;"><a href="<?php echo SITE_URL; ?>forgotPassword.php" style="color:red;">Forgot Password ?</a></p>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12" style="text-align:center;">
                                                    <button type="submit" name="submit" class="btn btn-warning" style="border-radius:0px;color:#fff;background:red;width:100px;height:40px;"><p style="padding-top:5px;font-size:15px;">Login</p></button>
                                                </div> 
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <?php } ?>
                        <?php if($_SESSION['uemail']!=''){ ?>
                        <section class="login_sec">
                            <h4>Already logged In</h4>
                            <ul class="nav nav-tabs">
                                <li>
                                    <a href="#passenger" id="button_two" class="btn btn-warning" style="border:0;border-radius:0px;color:#fff;background:red;width:90%;height:40px;">
                                        <p style="padding-top:5px;font-size:15px;">Continue</p>
                                    </a>
                                </li>
                            </ul>  
                        </section>
                        <?php } ?>
                    </div>
                    <div id="passenger" class="tab-pane fade">
                        <section class="login_sec">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Applicant Details</h4>
                                    <div class="row">
                                        <div style="float:left;width:50%;">
                                            <p><b>Name:</b><span id="passenger-name-1"></span></p>
                                        </div>
                                        <div style="float:right;width:49%;margin-left:5px;">
                                            <p><b>Lead No.:</b><span id="passenger-lead-no-1"></span></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div style="float:left;width:50%;">
                                            <p><b>Visa Type:</b><span id="passenger-visa-type-1"></span></p>
                                        </div>
                                        <div style="float:right;width:49%;margin-left:5px;">
                                            <p><b>Travel Dates :</b><span id="passenger-dates-1"></span></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div style="float:left;width:50%;">
                                            <p><b>Email ID :</b><span id="passenger-email-1"></span></p>
                                        </div>
                                        <div style="float:right;width:49%;margin-left:5px;">
                                            <p><b>Express :</b><span id="passenger-express-1"></span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            </div>
                            <div class="container">
                                <!-- Modal -->
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Applicant Details</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label for="ad_first_name">First Name*</label>
                                                        <input type="text" class="form-control" id="ad_first_name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ad_last_name">Last Name*</label>
                                                        <input type="text" class="form-control" id="ad_last_name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ad_dob">DOB*</label>
                                                        <input type="date" class="form-control" id="ad_dob">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ad_occupation">Occupation*</label>
                                                        <input type="text" class="form-control" id="ad_occupation">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ad_religion">Religion*</label>
                                                        <input type="text" class="form-control" id="ad_religion">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ad_education_qualification">Qualification*</label>
                                                        <input type="text" class="form-control" id="ad_education_qualification">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ad_nationality">Nationality*</label>
                                                        <input type="text" class="form-control" id="ad_nationality">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ad_passport_type">Passport Type*</label>
                                                        <select class="form-control" id="ad_passport_type">
                                                            <option value="normal">Normal</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ad_passport_no">Passport Number*</label>
                                                        <input type="text" class="form-control" id="ad_passport_no">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ad_date_of_issue">Date of Issue*</label>
                                                        <input type="date" class="form-control" id="ad_date_of_issue">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ad_expiry_date">Date of Expiry*</label>
                                                        <input type="date" class="form-control" id="ad_expiry_date">
                                                    </div>
                                                    <div class="row">
                                                        <div style="float:left">
                                                            <p onclick="addApplicant()" id="add_new" class="btn btn-default">Save</p>
                                                        </div>
                                                        <div style="float:right">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div style="float:left;width:50%;">
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="btn btn-warning" style="border:0;border-radius:0px;color:#fff;background:red;width:100%;height:40px;">
                                        <p style="padding-top:5px;font-size:15px;">Add New Applicant</p>
                                    </a>
                                </div>
                                <div style="float:right;width:49%;margin-left:5px;">
                                    <ul class="nav nav-tabs">
                                        <li style="    width: 100%;">
                                            <a href="#document" id="button_two" class="btn btn-warning" style="border:0;border-radius:0px;color:#fff;background:red;width:100%;height:40px;">
                                                <p style="padding-top:5px;font-size:15px;">Save & Continue</p>
                                            </a>
                                        </li>
                                    </ul>  
                                </div>
                            </div>
                        </section>
                    </div>
                    <div id="document" class="tab-pane fade">
                        <section class="login_sec">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Applicant Details</h4>
                                <div class="row">
                                    <div style="float:left;width:50%;">
                                        <p><b>Name:</b><span id="passenger-name-2"></span></p>
                                    </div>
                                    <div style="float:right;width:49%;margin-left:5px;">
                                        <p><b>Lead No.:</b><span id="passenger-lead-no-2"></span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div style="float:left;width:50%;">
                                        <p><b>Visa Type:</b><span id="passenger-visa-type-2"></span></p>
                                    </div>
                                    <div style="float:right;width:49%;margin-left:5px;">
                                        <p><b>Travel Dates :</b><span id="passenger-dates-2"></span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div style="float:left;width:50%;">
                                        <p><b>Email ID :</b><span id="passenger-email-2"></span></p>
                                    </div>
                                    <div style="float:right;width:49%;margin-left:5px;">
                                        <p><b>Express :</b><span id="passenger-express-2"></span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div style="width:50%;float:left;margin:auto;">
                                <h4 style="text-align:center;">Current Local Address :</h4>
                                <form method="post">
                                    <div class="form-group">
                                        <label for="address">Address:</label>
                                        <textarea type="text" class="form-control" name="address" id="address"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="city">City:</label>
                                        <input type="text" class="form-control" name="city" id="city">
                                    </div>
                                    <div class="form-group">
                                        <label for="state">State:</label>
                                        <input type="text" class="form-control" name="state" id="state">
                                    </div>
                                    <div class="form-group">
                                        <label for="pincode">Pincode:</label>
                                        <input type="text" class="form-control" name="pincode" id="pincode">
                                    </div>
                                </form>
                            </div>
                            <div style="width:40%;float:right;">
                                <h4 style="text-align:center;">Upload Documents :</h4>
                                <form id="uploadimage" action="" method="post" enctype="multipart/form-data">
                                <div id="image_preview"><img id="previewing" src="" style="width:100px;height:100px;"/></div>
                                <div id="form-control">
                                    <input type="file" name="file" id="file" required />
                                    <input type="submit" value="Upload" class="btn btn-warning" style="border:0;border-radius:0px;color:#fff;background:red;width:50%;height:40px;margin: 20px auto;" />
                                </div>
                                </form>
                                <div id="message"></div>

                                <script>
                                    $(document).ready(function (e) {
                                    $("#uploadimage").on('submit',(function(e) {
                                    e.preventDefault();
                                    $("#message").empty();
                                    $.ajax({
                                    url: "apply/file_upload.php",
                                    type: "POST",    
                                    data: new FormData(this),
                                    contentType: false, 
                                    cache: false,         
                                    processData:false,   
                                    success: function(data) 
                                    {
                                    alert(data);
                                    $("#message").html(data);
                                    }
                                    });
                                    }));
                                    $(function() {
                                    $("#file").change(function() {
                                    $("#message").empty();
                                    var file = this.files[0];
                                    var imagefile = file.type;
                                    var match= ["image/jpeg","image/png","image/jpg"];
                                    if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
                                    {
                                    $('#previewing').attr('src','noimage.png');
                                    $("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                                    return false;
                                    }
                                    else
                                    {
                                    var reader = new FileReader();
                                    reader.onload = imageIsLoaded;
                                    reader.readAsDataURL(this.files[0]);
                                    }
                                    });
                                    });
                                    function imageIsLoaded(e) {
                                    $("#file").css("color","green");
                                    $('#image_preview').css("display", "block");
                                    $('#previewing').attr('src', e.target.result);
                                    $('#previewing').attr('width', '250px');
                                    $('#previewing').attr('height', '230px');
                                    };
                                    });
                                </script>
                            </div>
                        </div>
                        <ul class="nav nav-tabs">
                            <li>
                                <a href="#overview" onclick="saveAddress()" id="button_three" class="btn btn-warning" style="border:0;border-radius:0px;color:#fff;background:red;width:100%;height:40px;">
                                    <p style="font-size:15px;">Save & Continue</p>
                                </a>
                            </li>
                        </ul>  
                        </section>
                    </div>
                    <div id="overview" class="tab-pane fade">
                        <section class="login_sec">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Applicant Details</h4>
                                <div class="row">
                                    <div style="float:left;width:50%;">
                                        <p><b>Name:</b><span id="passenger-name-3"></span></p>
                                    </div>
                                    <div style="float:right;width:49%;margin-left:5px;">
                                        <p><b>Lead No.:</b><span id="passenger-lead-no-3"></span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div style="float:left;width:50%;">
                                        <p><b>Visa Type:</b><span id="passenger-visa-type-3"></span></p>
                                    </div>
                                    <div style="float:right;width:49%;margin-left:5px;">
                                        <p><b>Travel Dates :</b><span id="passenger-dates-3"></span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div style="float:left;width:50%;">
                                        <p><b>Email ID :</b><span id="passenger-email-3"></span></p>
                                    </div>
                                    <div style="float:right;width:49%;margin-left:5px;">
                                        <p><b>Express :</b><span id="passenger-express-3"></span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div style="width:50%;float:left;margin:auto;">
                                <h4 style="text-align:center;">Overview</h4>
                                <hr>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Insurance</th>
                                        <th>Applicant Name</th>
                                        <th>DOB</th>
                                        <th>Passport No.</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="checkbox">
                                                <label><input type="checkbox" checked></label>
                                            </div>
                                        </td>
                                        <td id="new_applicant_name">Amit</td>
                                        <td id="new_applicant_dob">343/534534/5435</td>
                                        <td id="new_applicant_pn">6576345734</td>
                                    </tr>     
                                    </tbody>
                                </table>
                                <div class="checkbox">
                                    <label><input type="checkbox" checked>Secure your trip with Religare Travel Insurance for selected applicants</label>
                                </div>
                                <div class="row">
                                    <img src="./imgs/1.jpg" style="width:100%;">
                                    <span><img src="./imgs/2.jpg" style="width:40%;"> Read policy details to know more</span>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" checked>Yes, I accept the terms and conditions of the policy (These passengers are between 1 and 70 years of age.)</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" checked>Yes, I accept the Terms & Conditions of the YourDubaiVisa.</label>
                                </div>
                            </div>
                            <div style="width:40%;float:right;">
                                <h4 style="text-align:center;">Payment Summary</h4>
                                <div class="row">
                                    <div style="width:60%;float:left;">No of Applicant:</div>
                                    <div style="width:40%;float:right;"><span id="no_of_applicant"></span></div>
                                    <div style="width:60%;float:left;">Visa Fees:</div>
                                    <div style="width:40%;float:right;"><span id="visa_fee"></span></div>
                                    <div style="width:60%;float:left;">Express Charge:</div>
                                    <div style="width:40%;float:right;"><span id="express_fee"></span></div>
                                    <div style="width:60%;float:left;">Insurance:</div>
                                    <div style="width:40%;float:right;"><span id="insurance_fee"></span></div>
                                    <div style="width:60%;float:left;">Convenience Fee:</div>
                                    <div style="width:40%;float:right;"><span id="convenience_fee"></span></div>
                                    <hr>
                                    <div style="width:60%;float:left;">Total:</div>
                                    <div style="width:40%;float:right;"><span id="total_fee"></span></div>
                                    <div class="checkbox">
                                        <label class="radio-inline"><input type="radio" name="optradio">Credit/Debit Card, Net Banking</label>
                                        <label class="radio-inline"><input type="radio" name="optradio">Paytm</label>
                                    </div>
                                    <div class="form-group">
                                        <a href="#payment" id="button_one" class="btn btn-warning" style="border:0;border-radius:0px;color:#fff;background:red;width:90%;height:40px;">
                                            <p style="padding-top:5px;font-size:15px;">PAY NOW</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </section>
                    </div>
                    <div id="payment" class="tab-pane fade">
                        <section class="login_sec">
                        </section>
                    </div>
                </div>
                <script>
                    var visa_name='';
                    var visa_type=0;
                    var express_fee=0;
                    var pro_priority=1;
                    var date_of_travel='';
                    var date_of_return='';
                    var title='';
                    var first_name='';
                    var last_name='';
                    var no_of_adult=1;
                    var no_of_child=0;
                    var airline=0;
                    var otb=1;
                    var otb_name='';
                    var email='';
                    var mobile='';
                    var total=0,total_adult=0,total_child=0,total_express=0,total_otb=0;

                    function getVisaType(){
                        var temp_visa_type = document.getElementById('visa_type').value;
                        var type_arr = temp_visa_type.split(",");
                        visa_type = parseInt(type_arr[0]);
                        express_fee =parseInt(type_arr[1]);
                        visa_name = type_arr[2];
                        document.getElementById('user-visa-type').innerHTML = type_arr[2];
                        document.getElementById('adult-charge').innerHTML = (visa_type*no_of_adult);
                        document.getElementById('child-charge').innerHTML = (visa_type*no_of_child);
                        total_adult=(visa_type*no_of_adult);
                        no_of_child=(visa_type*no_of_child);
                        if(pro_priority==1){
                            document.getElementById('express-charge').innerHTML = 0;
                            total_express=0;
                        }else{
                            document.getElementById('express-charge').innerHTML = express_fee;
                            total_express=express_fee;
                        }
                        if(otb==0){
                            document.getElementById('otb-count').innerHTML = '';
                            document.getElementById('otb-charge').innerHTML = 0;
                            total=total-total_otb;
                            total_otb = 0;
                        }else{
                            total_otb = (parseInt(airline));
                            document.getElementById('otb-charge').innerHTML = (parseInt(airline));
                        }
                        total=total_adult+total_child+total_express+total_otb;
                        document.getElementById('total-charge').innerHTML = total;
                    }
                    function getProcessing(){
                        pro_priority = document.getElementById('pro_priority').value;
                        if(pro_priority==1){
                            document.getElementById('express-charge').innerHTML = 0;
                            total_express=0;
                            total=total_adult+total_child+total_express+total_otb;
                            document.getElementById('total-charge').innerHTML = total;
                        }else{
                            document.getElementById('express-charge').innerHTML = express_fee;
                            total_express=express_fee;
                            total=total_adult+total_child+total_express+total_otb;
                            document.getElementById('total-charge').innerHTML = total;
                        }
                    }
                    function getAdult(){
                        no_of_adult = document.getElementById('no_of_adult').value;
                        document.getElementById('adult-count').innerHTML = '('+no_of_adult+')';
                        document.getElementById('adult-charge').innerHTML = (parseInt(no_of_adult)*parseInt(visa_type));
                        total_adult=(parseInt(no_of_adult)*parseInt(visa_type));
                        total=total_adult+total_child+total_express+total_otb;
                        document.getElementById('total-charge').innerHTML = total;
                    }
                    function getChild(){
                        no_of_child = document.getElementById('no_of_child').value;
                        document.getElementById('child-count').innerHTML = '('+no_of_child+')';
                        document.getElementById('child-charge').innerHTML = (parseInt(no_of_child)*parseInt(visa_type));
                        total_child=(parseInt(no_of_child)*parseInt(visa_type));
                        total=total_adult+total_child+total_express+total_otb;
                        document.getElementById('total-charge').innerHTML = total;
                    }
                    function getOTBType(){
                        var temp_airline = document.getElementById('airline').value;
                        var airline_arr = temp_airline.split(",");
                        airline = airline_arr[0];
                        otb_name = airline_arr[1];
                        document.getElementById('otb-count').innerHTML = ''+airline_arr[1]+'';
                        document.getElementById('otb-charge').innerHTML = (parseInt(airline));
                        total_otb = (parseInt(airline));
                        total=total_adult+total_child+total_express+total_otb;
                        document.getElementById('total-charge').innerHTML = total;
                    }
                    function getOTBCharge(){
                        otb = parseInt(document.getElementById('otb').value);
                        if(otb==0){
                            document.getElementById('otb-count').innerHTML = '';
                            document.getElementById('otb-charge').innerHTML = 0;
                            total=total-total_otb;
                            total_otb = 0;
                            document.getElementById('total-charge').innerHTML = total;
                        }else{
                            document.getElementById('otb-charge').innerHTML = (parseInt(airline));
                            total_otb = (parseInt(airline));
                            total=total_adult+total_child+total_express+total_otb;
                            document.getElementById('total-charge').innerHTML = total;
                        }
                    }

                    function processVisa(){

                        date_of_travel = document.getElementById('date_of_travel').value;
                        date_of_return = document.getElementById('date_of_return').value;
                        title = document.getElementById('title').value;
                        first_name = document.getElementById('first_name').value;
                        last_name = document.getElementById('last_name').value;
                        email = document.getElementById('email').value;
                        mobile = document.getElementById('mobile').value;

                        localStorage.setItem("total", total);
                        localStorage.setItem("visa_name", visa_name);
                        localStorage.setItem("visa_type", visa_type);
                        localStorage.setItem("no_of_adult", no_of_adult);
                        localStorage.setItem("no_of_child", no_of_child);
                        localStorage.setItem("total_express", total_express);
                        localStorage.setItem("otb_name", otb_name);
                        localStorage.setItem("total_otb", total_otb);
                        localStorage.setItem("date_of_travel", date_of_travel);
                        localStorage.setItem("date_of_return", date_of_return);
                        localStorage.setItem("title", title);
                        localStorage.setItem("first_name", first_name);
                        localStorage.setItem("last_name", last_name);
                        localStorage.setItem("email", email);
                        localStorage.setItem("mobile", mobile);

                        document.getElementById('passenger-name-1').innerHTML = localStorage.getItem("title")+' '+localStorage.getItem("first_name")+' '+localStorage.getItem("last_name");
                        document.getElementById('passenger-visa-type-1').innerHTML = localStorage.getItem("visa_name");
                        document.getElementById('passenger-dates-1').innerHTML = localStorage.getItem("date_of_travel")+' - '+localStorage.getItem("date_of_return");
                        document.getElementById('passenger-email-1').innerHTML = localStorage.getItem("email");
                        document.getElementById('passenger-express-1').innerHTML = localStorage.getItem("otb_name");
                        
                        document.getElementById('passenger-name-2').innerHTML = localStorage.getItem("title")+' '+localStorage.getItem("first_name")+' '+localStorage.getItem("last_name");
                        document.getElementById('passenger-visa-type-2').innerHTML = localStorage.getItem("visa_name");
                        document.getElementById('passenger-dates-2').innerHTML = localStorage.getItem("date_of_travel")+' - '+localStorage.getItem("date_of_return");
                        document.getElementById('passenger-email-2').innerHTML = localStorage.getItem("email");
                        document.getElementById('passenger-express-2').innerHTML = localStorage.getItem("otb_name");
                        
                        document.getElementById('passenger-name-3').innerHTML = localStorage.getItem("title")+' '+localStorage.getItem("first_name")+' '+localStorage.getItem("last_name");
                        document.getElementById('passenger-visa-type-3').innerHTML = localStorage.getItem("visa_name");
                        document.getElementById('passenger-dates-3').innerHTML = localStorage.getItem("date_of_travel")+' - '+localStorage.getItem("date_of_return");
                        document.getElementById('passenger-email-3').innerHTML = localStorage.getItem("email");
                        document.getElementById('passenger-express-3').innerHTML = localStorage.getItem("otb_name");
                        
                        document.getElementById('passenger-name-3').innerHTML = localStorage.getItem("title")+' '+localStorage.getItem("first_name")+' '+localStorage.getItem("last_name");
                        document.getElementById('passenger-visa-type-3').innerHTML = localStorage.getItem("visa_name");
                        document.getElementById('passenger-dates-3').innerHTML = localStorage.getItem("date_of_travel")+' - '+localStorage.getItem("date_of_return");
                        document.getElementById('passenger-email-3').innerHTML = localStorage.getItem("email");
                        document.getElementById('passenger-express-3').innerHTML = localStorage.getItem("otb_name");

                        var dataString = 'visa_type='+ visa_type + '&pro_priority='+ pro_priority + '&date_of_travel='+ date_of_travel + '&date_of_return='+ date_of_return + '&no_of_adult='+ no_of_adult + '&no_of_child='+ no_of_child + '&title='+ title + '&first_name='+ first_name + '&last_name='+ last_name + '&airline='+ airline + '&otb='+ otb + '&email='+ email + '&mobile='+ mobile + '&total='+ total;

                        if(visa_type==''|| pro_priority==''|| date_of_travel==''|| date_of_return=='' || no_of_adult=='' || no_of_child=='' || title=='' || first_name=='' || last_name=='' || airline=='' || otb=='' || email=='' || mobile=='')
                        {
                            alert("Please Fill All Fields");
                        }
                        else
                        {
                            $.ajax({
                                type: "POST",
                                url: "apply/visa_processing.php",
                                data: dataString,
                                cache: false,
                                success: function(data){
                                    var newdata = data.split(",");
                                    console.log(newdata);
                                    localStorage.setItem("leadno", newdata[2]);
                                    localStorage.setItem("leadid", newdata[1]);
                                    document.getElementById('passenger-lead-no-1').innerHTML = newdata[2];
                                    document.getElementById('passenger-lead-no-2').innerHTML = newdata[2];
                                    document.getElementById('passenger-lead-no-3').innerHTML = newdata[2];
                                    document.getElementById('passenger-lead-no-3').innerHTML = newdata[2];
                                    alert(newdata[0]);
                                }
                            });
                        }
                        return false;
                    }

                    function addApplicant(){

                        var ad_first_name = document.getElementById('ad_first_name').value;
                        var ad_last_name = document.getElementById('ad_last_name').value;
                        var ad_dob = document.getElementById('ad_dob').value;
                        var ad_occupation = document.getElementById('ad_occupation').value;
                        var ad_religion = document.getElementById('ad_religion').value;
                        var ad_education_qualification = document.getElementById('ad_education_qualification').value;
                        var ad_nationality = document.getElementById('ad_nationality').value;
                        var ad_passport_type = document.getElementById('ad_passport_type').value;
                        var ad_passport_no = document.getElementById('ad_passport_no').value;
                        var ad_date_of_issue = document.getElementById('ad_date_of_issue').value;
                        var ad_expiry_date = document.getElementById('ad_expiry_date').value;
                        
                        localStorage.setItem("applicant_name", ad_first_name+' '+ad_last_name);
                        localStorage.setItem("applicant_dob", ad_dob);
                        localStorage.setItem("applicant_pn", ad_passport_no);
                        
                        document.getElementById('new_applicant_name').innerHTML = localStorage.getItem("applicant_name");
                        document.getElementById('new_applicant_dob').innerHTML = localStorage.getItem("applicant_dob");
                        document.getElementById('new_applicant_pn').innerHTML = localStorage.getItem("applicant_pn");

                        var adult_count = localStorage.getItem("no_of_adult");
                        var child_count = localStorage.getItem("no_of_child");
                        var visa_type_feee = localStorage.getItem("total");
                        var express_type_fee = localStorage.getItem("total_express");
                        var insurance_type_fee = 1000;
                        var convenience_type_fee = 500;
                        var total_visa_fee =  parseInt(visa_type_feee)+parseInt(express_type_fee)+insurance_type_fee+convenience_type_fee;
                        
                        document.getElementById('no_of_applicant').innerHTML = parseInt(adult_count)+parseInt(child_count);
                        document.getElementById('visa_fee').innerHTML = localStorage.getItem("total");
                        document.getElementById('express_fee').innerHTML = localStorage.getItem("total_express");
                        document.getElementById('insurance_fee').innerHTML = insurance_type_fee;
                        document.getElementById('convenience_fee').innerHTML = convenience_type_fee;
                        document.getElementById('total_fee').innerHTML = total_visa_fee;

                        var dataString = 'ad_first_name='+ ad_first_name + '&ad_last_name='+ ad_last_name + '&ad_dob='+ ad_dob + '&ad_occupation='+ ad_occupation + '&ad_religion='+ ad_religion + '&ad_education_qualification='+ ad_education_qualification + '&ad_nationality='+ ad_nationality + '&ad_passport_type='+ ad_passport_type + '&ad_passport_no='+ ad_passport_no + '&ad_date_of_issue='+ ad_date_of_issue + '&ad_expiry_date='+ ad_expiry_date;

                        if(ad_first_name==''|| ad_last_name==''|| ad_dob==''|| ad_occupation=='' || ad_religion=='' || ad_education_qualification=='' || ad_nationality=='' || ad_passport_type=='' || ad_passport_no=='' || ad_date_of_issue=='' || ad_expiry_date=='')
                        {
                            alert("Please Fill All Fields");
                        }
                        else
                        {
                            $.ajax({
                                type: "POST",
                                url: "apply/add_applicant.php",
                                data: dataString,
                                cache: false,
                                success: function(data){
                                 alert(data);
                                }
                            });
                        }
                        return false;
                    }

                    function saveAddress(){

                        title = document.getElementById('title').value;
                        first_name = document.getElementById('first_name').value;
                        last_name = document.getElementById('last_name').value;
                        email = document.getElementById('email').value;
                        mobile = document.getElementById('mobile').value;

                        var address = document.getElementById('address').value;
                        var city = document.getElementById('city').value;
                        var state = document.getElementById('state').value;
                        var pincode = document.getElementById('pincode').value;

                        var dataString = 'title='+ title + '&first_name='+ first_name + '&last_name='+ last_name + '&email='+ email + '&mobile='+ mobile + '&address='+ address + '&city='+ city + '&state='+ state + '&pincode='+ pincode + '&ad_date_of_issue='+ ad_date_of_issue + '&ad_expiry_date='+ ad_expiry_date;

                        if(address==''|| city==''|| state==''|| pincode=='')
                        {
                            alert("Please Fill All Fields");
                        }
                        else
                        {
                            $.ajax({
                                type: "POST",
                                url: "apply/add_address.php",
                                data: dataString,
                                cache: false,
                                success: function(data){
                                    alert(data);
                                }
                            });
                        }
                        return false;
                    }

                    $(document).ready(function(){
                        $(".nav-tabs a").click(function(){
                            $(this).tab('show');
                        });
                    });

                </script>
            </div>
        </div>
    </div>
    <!-- ipage content div end -->
    <?php include("includes/footer.php"); ?>
    </body>
</html>