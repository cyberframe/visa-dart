<?php
class Images
{
###################################################################################
function fun_insert_img($uploadedfile,$newwidth,$newheight,$name,$extension,$folder)
{ 	
	if($extension=="jpg" || $extension=="jpeg" )
	{
		$src = imagecreatefromjpeg($uploadedfile);
	}
	else if($extension=="png")
	{
		$src = imagecreatefrompng($uploadedfile);
	}
	else 
	{
		$src = imagecreatefromgif($uploadedfile);
	}
	list($width,$height, $type, $attr)=getimagesize($uploadedfile);
	//$newwidth=300;
	//$newheight=300;
	//$newheight=($height/$width)*$newwidth;
	if($newheight == "")
	{
		$newheight=$height;
		$newwidth = $width;
	}
	/*
	else
	{
		if($newwidth == "")
		{
			$newwidth=($width/$height)*$newheight;
		}
	}*/
	$DestImage=imagecreatetruecolor($newwidth,$newheight);
	 // handle transparancy
	if ( ($type == IMAGETYPE_GIF) || ($type == IMAGETYPE_PNG) ) {
        $trnprt_indx = imagecolortransparent($src);
        // If we have a specific transparent color
        if ($trnprt_indx >= 0) {
            // Get the original image's transparent color's RGB values
            $trnprt_color  = imagecolorsforindex($objImage, $trnprt_indx);
            // Allocate the same color in the new image resource
            $trnprt_indx    = imagecolorallocate($DestImage, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);

            // Completely fill the background of the new image with allocated color.
            imagefill($DestImage, 0, 0, $trnprt_indx);

            // Set the background color for new image to transparent
            imagecolortransparent($DestImage, $trnprt_indx);
        } elseif ($type == IMAGETYPE_PNG) {

            // Turn off transparency blending (temporarily)
            imagealphablending($DestImage, false);

            // Create a new transparent color for image
            $color = imagecolorallocatealpha($DestImage, 0, 0, 0, 127);

            // Completely fill the background of the new image with allocated color.
            imagefill($DestImage, 0, 0, $color);

            // Restore transparency blending
            imagesavealpha($DestImage, true);
        }
    }



    imagecopyresampled($DestImage, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height); 
	$imgupload = $folder.$name;
	 switch(strtolower($extension)) {
        case "gif":
            imagegif($DestImage, $imgupload);
            break;
        case "png":
            imagepng($DestImage, $imgupload,0);
            break;
        default:
            imagejpeg($DestImage,$imgupload,100);
            break;
    }
	imagedestroy($DestImage);
	return $src;
	
}
###################################################################################
}
global $img_obj;
$img_obj = new Images;
?>