-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2017 at 11:00 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `visadart`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_address`
--

CREATE TABLE IF NOT EXISTS `tbl_address` (
  `add_id` int(11) NOT NULL AUTO_INCREMENT,
  `add_company` varchar(100) NOT NULL,
  `add_phone` varchar(25) NOT NULL,
  `add_faxno` varchar(255) NOT NULL,
  `add_address` varchar(255) NOT NULL,
  `add_email` varchar(60) NOT NULL,
  `add_h_phone` varchar(100) NOT NULL,
  `add_h_email` varchar(100) NOT NULL,
  `add_google` text NOT NULL,
  `add_facebook` text NOT NULL,
  `add_twitter` text NOT NULL,
  `add_youtube` text NOT NULL,
  `add_vimeo` text NOT NULL,
  `add_flickr` text NOT NULL,
  `add_instagram` text NOT NULL,
  `add_image` varchar(255) NOT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`add_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_address`
--

INSERT INTO `tbl_address` (`add_id`, `add_company`, `add_phone`, `add_faxno`, `add_address`, `add_email`, `add_h_phone`, `add_h_email`, `add_google`, `add_facebook`, `add_twitter`, `add_youtube`, `add_vimeo`, `add_flickr`, `add_instagram`, `add_image`, `is_delete`) VALUES
(1, 'FastwayTrips.com', '+91 8882822822', '', 'YourDubaiVisa, DLF Golf Course road ,\nGurgaon - Haryana , India', 'info@yourdubaivisa.com', '+91 782-783-3000', 'hotel@dubaievisa.in', '7696324223', 'https://www.facebook.com/', 'https://twitter.com/?lang=en', 'https://youtube.com/?lang=en', 'https://vimeo.com/?lang=en', 'https://flickr.com/?lang=en', 'https://instagram.com/?lang=en', 'logovd.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `adm_id` int(11) NOT NULL AUTO_INCREMENT,
  `adm_uname` varchar(255) NOT NULL,
  `adm_email` varchar(255) NOT NULL,
  `adm_pass` varchar(255) NOT NULL,
  `adm_type` int(11) NOT NULL DEFAULT '1',
  `adm_mobile` varchar(255) NOT NULL,
  `adm_insert_date` varchar(255) NOT NULL,
  `adm_name` varchar(255) NOT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`adm_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`adm_id`, `adm_uname`, `adm_email`, `adm_pass`, `adm_type`, `adm_mobile`, `adm_insert_date`, `adm_name`, `is_delete`) VALUES
(1, 'admin', 'fastwaytrips@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, '', '1456354800', 'admin', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_agents_enquiry`
--

CREATE TABLE IF NOT EXISTS `tbl_agents_enquiry` (
  `agn_id` int(11) NOT NULL AUTO_INCREMENT,
  `agn_company_name` varchar(255) NOT NULL,
  `agn_email` varchar(255) NOT NULL,
  `agn_typeof_company` int(11) NOT NULL,
  `agn_incorporation_date` varchar(255) NOT NULL,
  `agn_contact_person_name` varchar(255) NOT NULL,
  `agn_designation` varchar(255) NOT NULL,
  `agn_landline_no` bigint(255) NOT NULL,
  `agn_mobile_no` bigint(10) NOT NULL,
  `agn_address` varchar(255) NOT NULL,
  `agn_pin_code` int(6) NOT NULL,
  `agn_city` varchar(255) NOT NULL,
  `agn_state` varchar(255) NOT NULL,
  `agn_country` varchar(255) NOT NULL,
  `agn_tds_details` varchar(255) NOT NULL,
  `agn_company_pan` varchar(255) NOT NULL,
  `agn_service_tax_regn_no` varchar(100) NOT NULL,
  `agn_account_dept` varchar(255) NOT NULL,
  `agn_other_details` varchar(255) NOT NULL,
  `agn_status` tinyint(2) NOT NULL DEFAULT '0',
  `agn_insert_date` int(11) NOT NULL,
  `agn_ip` varchar(255) NOT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`agn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_applicant_details`
--

CREATE TABLE IF NOT EXISTS `tbl_applicant_details` (
  `ad_id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_first_name` varchar(255) NOT NULL,
  `ad_last_name` varchar(255) NOT NULL,
  `ad_userid` varchar(255) NOT NULL,
  `ad_dob` bigint(11) NOT NULL,
  `ad_photo` varchar(255) NOT NULL,
  `ad_religion` varchar(255) NOT NULL,
  `ad_nationality` varchar(255) NOT NULL,
  `ad_occupation` varchar(255) NOT NULL,
  `ad_education_qualification` varchar(255) NOT NULL,
  `ad_passport_type` varchar(255) NOT NULL,
  `ad_passport_no` varchar(50) NOT NULL,
  `ad_date_of_issue` int(11) NOT NULL,
  `ad_expiry_date` int(11) NOT NULL,
  `ad_passport_front_page` varchar(255) NOT NULL,
  `ad_passport_back_type` varchar(255) NOT NULL,
  `ad_status` tinyint(2) NOT NULL DEFAULT '0',
  `ad_air_ticket` tinyint(2) NOT NULL DEFAULT '0',
  `ad_other_doc` tinyint(2) NOT NULL DEFAULT '0',
  `ad_airticket_file` varchar(255) NOT NULL,
  `ad_airticket_second_file` varchar(255) NOT NULL,
  `ad_file1` varchar(255) NOT NULL,
  `ad_file2` varchar(255) NOT NULL,
  `ad_file3` varchar(255) NOT NULL,
  `ad_file4` varchar(255) NOT NULL,
  `ad_file5` varchar(255) NOT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ad_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- Dumping data for table `tbl_applicant_details`
--

INSERT INTO `tbl_applicant_details` (`ad_id`, `ad_first_name`, `ad_last_name`, `ad_userid`, `ad_dob`, `ad_photo`, `ad_religion`, `ad_nationality`, `ad_occupation`, `ad_education_qualification`, `ad_passport_type`, `ad_passport_no`, `ad_date_of_issue`, `ad_expiry_date`, `ad_passport_front_page`, `ad_passport_back_type`, `ad_status`, `ad_air_ticket`, `ad_other_doc`, `ad_airticket_file`, `ad_airticket_second_file`, `ad_file1`, `ad_file2`, `ad_file3`, `ad_file4`, `ad_file5`, `is_delete`) VALUES
(1, 'Shobhit', 'Jhalani', '1', 1507141800, '1507548954260.jpg', 'India', 'Indian', 'Business', 'MBA', 'Normal', 'M1234567', 1507055400, 1508265000, '1507548954118.jpg', '1507548954992.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(2, 'Samarth', 'KOhli', '41', 57781800, '1507552937423.jpg', 'hbb', 'Indian', 'hjhk', '', 'Normal', 'vfevefvew', 1510338600, 1529519400, '1507552937447.jpg', '1507552937353.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(3, 'Shobhit', 'Jhalani', '45', 1507573800, '1507734529156.png', 'Hindu', 'Indian', 'Business', 'MBA', 'Normal', 'M1234567', 1507487400, 1509042600, '1507734529907.jpg', '1507734529633.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(4, 'shrishti', 'kumawat', '47', 1252089000, '1507809433407.jpg', 'hindu', 'Indian', 'Accountant', 'B.Com', 'Normal', '154646874566', 1267554600, 1575570600, '1507809433453.jpg', '1507809433763.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(5, 'Samarth', 'Kohli', '43', 339186600, '1507809640101.jpeg', 'hindu', 'Indian', 'hindu', 'hindu', 'Normal', 'ffadfad', 1507746600, 1518028200, '1507809640131.pdf', '150780964029.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(6, 'Samarth', 'Kohli', '43', 339186600, '1507809642837.jpeg', 'hindu', 'Indian', 'hindu', 'hindu', 'Normal', 'ffadfad', 1507746600, 1518028200, '1507809642715.pdf', '1507809642663.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(7, 'SFGS', 'udg', '50', 1509474600, '1509532925299.png', 'DFDSF', 'Indian', 'FWF', 'ADFSFDA', 'Normal', 'RFSDFSD', 1509474600, 1522261800, '1509532925243.png', '1509532925126.png', 1, 0, 0, '', '', '', '', '', '', '', 0),
(8, 'Ashish', 'Kumar', '64', 676492200, '1509954054150.jpg', 'Hindu', 'Indian', 'Business', 'Higher Secondry ', 'Normal', 'P7971084', 1497810600, 1813257000, '1509954054469.jpg', '1509954054840.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(9, 'Ashish', 'Kumar', '64', 676492200, '1509954086631.jpg', 'Hindu', 'Indian', 'Business', 'Higher Secondry ', 'Normal', 'P7971084', 1497810600, 1813257000, '1509954086983.jpg', '1509954086466.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(10, 'NEELAM', 'CHOWDHARY', '71', -299136600, '1510469940961.jpg', 'Hindu', 'Indian', 'Teacher', 'Post Graduate', 'Normal', 'J3207162', 1290969000, 1606501800, '1510469940501.pdf', '1510469940162.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(11, 'VIJAY KUNAR', 'CHOWDHARY', '71', -356506200, '1510470571519.jpg', 'Hindu', 'Indian', 'Engineer', 'Graduate', 'Normal', 'H7417212', 1262025000, 1577471400, '1510470571920.pdf', '1510470571518.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(12, 'Reitu', 'Chandak', '62', 268338600, '1510485607361.jpg', 'Hindu', 'Indian', 'House wife', 'Graduation', 'Normal', 'R5142226', 1508092200, 1823538600, '1510485607438.jpg', '151048560742.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(13, 'Ravi', 'Chandak', '62', 161980200, '1510485820304.jpg', 'Hindu', 'Indian', 'Business', 'Graduate', 'Normal', 'K1370024', 1367260200, 1682706600, '1510485820956.jpg', '1510485820340.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(14, 'Ravindra', 'Chandak', '62', 1231180200, '1510486065543.JPG', 'Hindu', 'Indian', 'Student', 'Studying', 'Normal', 'R5137668', 1504809000, 1662489000, '1510486065319.jpg', '1510486065283.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(15, 'Rachit', 'Chandak', '62', 1064341800, '1510486486673.jpg', 'Hindu', 'Indian', 'Student', 'Studying', 'Normal', 'R5142340', 1508178600, 1632335400, '1510486486599.jpg', '1510486486266.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(16, 'Rishita', 'Chandak', '62', 991938600, '1510486758402.jpg', 'Hindu', 'Indian', 'Student', 'Studying', 'Normal', 'R5142358', 1508178600, 1823625000, '1510486758529.jpg', '1510486758483.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(17, 'Aayushi', 'Tayal', '89', 520972200, '1510569415756.jpg', 'HINDU', 'Indian', 'House wife', 'Bachelor in Physiotherapist', 'Normal', 'M7944922', 1429641000, 1745173800, '1510569415321.jpg', '1510569415610.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(18, 'Riyansh', 'Tayal', '89', 1418841000, '1510569626254.jpg', 'HINDU', 'Indian', 'Student', '', 'Normal', 'N5435857', 1453055400, 1610821800, '1510569626633.jpg', '1510569626184.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(19, 'ANSHUL', 'SACHAR', '99', 544818600, '1510744378611.jpg', 'HINDU', 'Indian', 'PROFESSION', 'CHARTERED ACCOUNTANT', 'Normal', 'J5411190', 1297189800, 1612722600, '1510744378359.jpg', '1510744378715.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(20, 'Gaurav', 'Shukla', '101', 382041000, '151083192581.jpg', 'Hindu', 'Indian', 'Self Employed', 'Chartered Accountant', 'Normal', 'N6745121', 1453660200, 1769193000, '1510831925782.jpg', '1510831925345.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(21, 'DEEPAK', 'SHUKLA', '105', 428005800, '1510921302683.jpg', 'HINDU', 'Indian', 'BUSINESS', 'GRADUATE', 'Normal', 'H8264507', 1261333800, 1576780200, '1510921302745.pdf', '1510921302630.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(22, 'NEETI', 'SHUKLA', '105', 475612200, '1510921511658.jpg', 'HINDU', 'Indian', 'housewife', 'post graduate', 'Normal', 'R5662157', 1506450600, 1821983400, '1510921511925.pdf', '1510921511309.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(23, 'PRAKET', 'SHUKLA', '105', 1225045800, '1510921770908.jpg', 'HINDU', 'Indian', 'NA', '3RD', 'Normal', 'R2677495', 1505068200, 1662748200, '1510921770214.pdf', '1510921770656.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(24, 'PRATYUSH', 'SHUKLA', '105', 1405189800, '1510921951898.jpg', 'HINDU', 'Indian', 'NA', 'PRE-SCHOOL', 'Normal', 'R2677247', 1505068200, 1662748200, '1510921951467.pdf', '1510921951804.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(25, 'ISHU', 'SHARMA', '107', 660681000, '1510924397609.jpg', 'HINDU', 'Indian', 'TEACHER', 'M.A B.ED', 'Normal', 'R3443503', 1502217000, 1817663400, '1510924397995.pdf', '1510924397546.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(26, 'Sargam', 'Choudhary', '106', 538597800, '1510925127102.JPG', 'Hinduism', 'Indian', 'Business Manager', 'B.Tech', 'Normal', 'J4897523', 1291833000, 1607365800, '1510925127464.JPG', '1510925127102.JPG', 1, 0, 0, '', '', '', '', '', '', '', 0),
(27, 'Sandip', 'De', '109', 58041000, '151093144946.JPG', 'Hinduism', 'Indian', 'Doctor', 'MBBS,MD', 'Normal', 'L6281340', 1388601000, 1704047400, '1510931449627.JPG', '151093144927.JPG', 1, 0, 0, '', '', '', '', '', '', '', 0),
(28, 'Priyanka', 'De', '109', 230754600, '1510932167895.JPG', 'Hinduism', 'Indian', 'Home Maker', 'MSC,MBA', 'Normal', 'L6281515', 1388601000, 1704047400, '1510932167921.JPG', '1510932167294.JPG', 1, 0, 0, '', '', '', '', '', '', '', 0),
(29, 'Aaratrika', 'De', '109', 1251225000, '1510932518663.jpg', 'Hinduism', 'Indian', 'Student', 'Class 3', 'Normal', 'L6281780', 1388601000, 1546281000, '1510932518999.JPG', '1510932518883.JPG', 1, 0, 0, '', '', '', '', '', '', '', 0),
(30, 'Dulcine Henry', 'Mathias', '125', -558855000, '1511324968460.pdf', 'Catholic', 'Indian', 'Housewife', '9th', 'Normal', 'P6790394', 1485196200, 1800642600, '1511324968790.pdf', '1511324968955.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(31, 'Ryan Vijay Henry', 'Mathias', '125', 558124200, '1511321016115.pdf', 'Catholic', 'Indian', 'IT Professional', 'Master of Science in MIS', 'Normal', 'H5206069', 1247509800, 1562956200, '1511321016608.pdf', '1511321016976.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(32, 'Manish Kishor', 'Ambre', '125', 419797800, '1511322011330.pdf', 'Hindu', 'Indian', 'Businessman', 'B.Com', 'Normal', 'P9059567', 1490121000, 1805567400, '1511322011801.pdf', '151132201180.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(33, 'Iman', 'Das', '121', 400357800, '1511531933410.jpg', 'Hindu', 'Indian', 'Musician', 'M.A.1ST Class', 'Normal', 'RO339401', 1494959400, 1810405800, '1511531933421.JPG', '1511531933520.JPG', 1, 0, 0, '', '', '', '', '', '', '', 0),
(34, 'Dhwani', 'Shah', '131', 753561000, '1511430368480.pdf', 'jain', 'Indian', 'student', '', 'Normal', 'P6136113', 1482690600, 1798137000, '1511430368936.pdf', '1511430368589.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(35, 'Prashant', 'Shah', '131', -146554200, '1511430709992.pdf', 'jain', 'Indian', 'Businessman', '', 'Normal', 'Z2334881', 1335119400, 1650565800, '1511430709216.pdf', '1511430709323.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(36, 'Rupal', 'Shah', '131', -91949400, '1511430853762.pdf', 'Jain', 'Indian', 'Homemaker', '', 'Normal', 'K4066348', 1334687400, 1650133800, '1511430853993.pdf', '1511430853257.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(37, 'MADHUBEN', 'SHAH', '131', -826525800, '1511431004931.pdf', 'JAIN', 'Indian', 'HOUSEWIFE', '', 'Normal', 'H9757568', 1264530600, 1579977000, '1511431004317.pdf', '1511431004902.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(38, 'NIRALI', 'SHAH', '131', -13930200, '1511431180872.pdf', 'JAIN', 'Indian', 'FASHION DESIGNER', '', 'Normal', 'L3088396', 1395167400, 1710700200, '1511431180865.pdf', '1511431180465.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(39, 'SUNDERJI', 'SHAH', '131', -976339800, '1511431367718.pdf', 'JAIN', 'Indian', 'RETIRED', '', 'Normal', 'H9270624', 1261333800, 1576780200, '1511431367168.pdf', '1511431367977.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(40, 'Samarth', 'Kohli', '41', 520540200, '1511581320413.pdf', 'Hindu', 'Indian', 'faf', 'faa', 'Normal', 'P4361120', 1510684200, 1511893800, '151158132044.pdf', '151158132073.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(41, 'Dipali', 'Jain', '132', 719433000, '1511608013649.jpg', 'Jain', 'Indian', 'Self Employed', 'MBA', 'Normal', 'J6902988', 1301941800, 1617474600, '1511608013223.JPG', '1511608013102.JPG', 1, 0, 0, '', '', '', '', '', '', '', 0),
(42, 'Kumar ', 'Akshay', '', 599855400, '151161305570.png', 'Hindu', 'Indian', 'Self Employed', 'MBA', 'Normal', 'Z3514924', 1456079400, 1771612200, '1511613055618.JPG', '1511613056554.JPG', 1, 0, 0, '', '', '', '', '', '', '', 0),
(43, 'Kumar', 'Akshay', '132', 599855400, '1511613304780.png', 'Hindu', 'Indian', 'Self Employed', 'MBA', 'Normal', 'Z3514924', 1456079400, 1771612200, '1511613304278.JPG', '1511613304252.JPG', 1, 0, 0, '', '', '', '', '', '', '', 0),
(44, 'Nalin', 'Swami', '139', 570220200, '1511613799438.jpg', 'Hindu', 'Indian', 'Manager', '', 'Normal', 'L3980710', 1374777000, 1690223400, '1511613799948.jpg', '151161379945.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(45, 'Gaurav', 'Sherkhane', '133', 492460200, '1511638064761.JPG', 'Hindu', 'Indian', 'Service', '', 'Normal', 'H2462480', 1231785000, 1547231400, '1511638064844.jpg', '1511638064202.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(46, 'Surekha', 'Sethuram', '133', 536869800, '1511638492130.jpg', 'Hindu', 'Indian', 'Service', '', 'Normal', 'R6239620', 1511202600, 1826649000, '1511638492354.jpg', '1511638492348.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(47, 'Surekha', 'Sethuram', '133', 536869800, '1511638498826.jpg', 'Hindu', 'Indian', 'Service', '', 'Normal', 'R6239620', 1511202600, 1826649000, '1511638498325.jpg', '1511638498112.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(48, 'Chandra ', 'Iyer', '133', -300346200, '1511639491441.jpg', 'Hindu', 'Indian', 'Service', '', 'Normal', 'R0595795', 1495564200, 1811010600, '1511639491521.jpg', '151163949161.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(49, 'Gaurav', 'Kumar', '', 557865000, '1511778258556.png', 'Hindu', 'Indian', 'Private Job', '', 'Normal', 'P8300678', 1491157800, 1806604200, '1511778258609.jpg', '1511778258313.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(50, 'Gaurav', 'Kumar', '144', 557865000, '1511778543375.png', 'Hindu', 'Indian', 'Private Job', '', 'Normal', 'P8300678', 1491157800, 1806604200, '1511778543200.jpg', '1511778543554.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(51, 'Sushila', 'Bisht', '144', 506111400, '1511780875363.png', 'Hindu', 'Indian', 'House Wife', '', 'Normal', 'P9082703', 1491157800, 1806604200, '1511780875831.jpg', '1511780875908.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(52, 'Bhavika', 'Keenra', '144', 1478716200, '1511781022181.png', 'Hindu', 'Indian', 'Not Working', '', 'Normal', 'P9098174', 1491849000, 1649529000, '1511781022593.jpg', '1511781022246.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(53, 'Boris', 'Kenneth', '161', 652041000, '1512038013643.jpeg', 'Christian', 'Indian', 'Film Producer', '2nd Puc', 'Normal', 'N0433556', 1434479400, 1750012200, '1512038013858.jpeg', '1512038013995.jpeg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(54, 'Dorine', 'Mcguire', '161', 663100200, '1512038225819.jpeg', 'Christian', 'Indian', 'Financial Analyst', 'MBA', 'Normal', 'N8464842', 1457289000, 1772735400, '1512038225624.jpeg', '1512038225991.jpeg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(55, 'Umar', 'Farooq', '164', 576959400, '151212638821.png', 'islam', 'Indian', 'Employee', 'BA', 'Normal', 'J3911949', 1285007400, 1600540200, '1512126388589.jpg', '1512126388841.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(56, 'Justin', 'Castelino', '166', 648498600, '15122002025.jpg', 'ROMAN CATHOLIC', 'Indian', 'PRIVATE', 'GRADUATE', 'Normal', 'L7123581', 1392575400, 1708021800, '1512200202854.pdf', '1512200202596.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(57, 'Martina', 'Dmello', '166', 665173800, '1512200564865.JPG', 'ROMAN CATHOLIC', 'Indian', 'PRIVATE', 'GRADUATE', 'Normal', 'K3722987', 1334860200, 1650306600, '1512200564108.pdf', '1512200564250.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(58, 'RITESHKUMAR RADHESHYAM', 'KAMALIA', '168', 274473000, '1512202612137.jpg', 'HINDU', 'Indian', 'BUSSINESS', 'GRADUATE', 'Normal', 'Z2339983', 1334169000, 1649615400, '1512202612565.jpg', '1512202612757.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(59, 'Achint', 'Arora', '', 560889000, '1512291028802.jpg', 'Hindu', 'Indian', 'Senior Analyst', 'PGDM (MBA)', 'Normal', 'N3084369', 1441823400, 1757356200, '1512291028983.pdf', '1512291028393.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(60, 'Achint', 'Arora', '112', 560889000, '1512291339514.jpg', 'Hindu', 'Indian', 'Senior Analyst', 'MBA', 'Normal', 'N3084369', 1441823400, 1757356200, '1512291339975.pdf', '1512291339710.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(61, 'SONIA', 'OBEROI', '112', 626034600, '151229163049.jpg', 'Hindu', 'Indian', 'Associate Team lead', 'BTECH', 'Normal', 'M9202360', 1434306600, 1749839400, '1512291630369.jpg', '1512291630590.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(62, 'Amit Sudheer', 'Deshpande', '173', 208636200, '1512559878915.jpg', 'Hindu', 'Indian', 'Service', 'Post Graduate', 'Normal', 'P9657192', 1491330600, 1806777000, '151255987836.jpg', '1512559878264.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(63, 'Prachi Amit', 'Deshpande', '173', 234729000, '1512560082452.jpg', 'Hindu', 'Indian', 'Service', 'Post Graduate', 'Normal', 'P9656924', 1491330600, 1806777000, '1512560082527.jpg', '1512560082266.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(64, 'Ishaan', 'Deshpande', '173', 1122316200, '1512560337558.jpg', 'Hindu', 'Indian', 'Student', '7th Grade', 'Normal', 'P9656932', 1491330600, 1649010600, '1512560337358.jpg', '1512560337430.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(65, 'Syed Shahbaz ', 'Mohammad ', '184', 566764200, '1513144752471.JPG', 'Islam', 'Indian', 'Unemployed ', 'B. Com', 'Normal', 'M2415449', 1416335400, 1731868200, '1513144755939.jpg', '1513144756523.jpg', 1, 0, 0, '', '', '', '', '', '', '', 0),
(66, 'Kunal', 'Kiran Shah', '188', 351887400, '151316402695.pdf', 'Hindu', 'Indian', 'Business', '', 'Normal', 'Z3328399', 1437589800, 1753122600, '151316402684.pdf', '1513164026793.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(67, 'SIDDHARTH', 'ARORA', '188', 278965800, '1513164394146.jpg', 'Hindu', 'Indian', 'Business', '', 'Normal', 'Z3323028', 1435775400, 1751308200, '1513164394226.pdf', '1513164394500.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(68, 'Kahan', 'Taraporevala', '', 937074600, '1513164549591.pdf', 'Parsi Zoroastrian', 'Indian', 'Student', 'High School', 'Normal', 'L4968893', 1392316200, 1549996200, '1513164549752.pdf', '1513164549615.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0),
(69, 'Kahan', 'Taraporevala', '186', 937074600, '1513164995628.pdf', 'Parsi Zoroastrian', 'Indian', 'Student', 'High School', 'Normal', 'L4968893', 1392316200, 1549996200, '1513164995733.pdf', '1513164995508.pdf', 1, 0, 0, '', '', '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blocked_ip`
--

CREATE TABLE IF NOT EXISTS `tbl_blocked_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) NOT NULL,
  `dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_blocked_ip`
--

INSERT INTO `tbl_blocked_ip` (`id`, `ip`, `dt`) VALUES
(1, '110.225.211.128', '2017-11-03 12:20:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contactus`
--

CREATE TABLE IF NOT EXISTS `tbl_contactus` (
  `cont_id` int(11) NOT NULL AUTO_INCREMENT,
  `cont_name` varchar(255) NOT NULL,
  `cont_emailid` varchar(255) NOT NULL,
  `cont_subject` varchar(255) NOT NULL,
  `cont_mobileno` bigint(10) NOT NULL,
  `cont_department` varchar(255) NOT NULL,
  `cont_message` varchar(255) NOT NULL,
  `cont_status` tinyint(2) NOT NULL DEFAULT '0',
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  `cont_ip` varchar(255) NOT NULL,
  PRIMARY KEY (`cont_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_contactus`
--

INSERT INTO `tbl_contactus` (`cont_id`, `cont_name`, `cont_emailid`, `cont_subject`, `cont_mobileno`, `cont_department`, `cont_message`, `cont_status`, `is_delete`, `cont_ip`) VALUES
(1, 'saji singh', 'sajisingh@gmail.com', '', 9893029394, '', 'wil be travelling  Bhopal-Mumbai-Abudhabi next week by Air India flight,  i am having a valid 10 years USA visit B1/B2 visa so i havenâ€™t applied for the UAE visa. please let me know the procedure to get the UAE visa on arrival at Abudhabi airport.  Hope', 0, 0, ''),
(2, 'MAHENDRA KUMAR SUGREEV CHAUHAN', 'mahendrachauhan02196@gmail.com', '', 9721374810, '', 'I want a visa', 0, 0, ''),
(3, 'h', 'dh@gmail.com', '', 0, '', 'dh', 0, 0, ''),
(4, 'Chitresh chandrayan', 'chitreshc@yahoo.com', '', 7674817007, '', 'I need tourist visa for dubai', 0, 0, ''),
(5, 'Nekkanti Murali mohan ', 'nekkantimm@gmail.com', '', 0, '', 'I am an Indian passport holder with a valid American B1/B2 visa. Can I get visa on arrival in Emirates ', 0, 0, ''),
(6, 'shivam ', 'shivamshukla871@gmail.com', '', 8795850155, '', 'call me', 0, 0, ''),
(7, 'surjeet singh sandhu', 'judgesandhu@gmail.com', '', 8528880047, '', 'visa fee dubai', 0, 0, ''),
(8, 'Gurleen Kaur', 'gurleen@cyberframe.in', '', 1234567898, '', 'Testing Message', 0, 0, ''),
(9, 'Gurleen Kaur', 'gurleen@cyberframe.in', '', 1234567898, '', 'Testing Message', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deal`
--

CREATE TABLE IF NOT EXISTS `tbl_deal` (
  `del_id` int(11) NOT NULL AUTO_INCREMENT,
  `del_title` varchar(255) NOT NULL,
  `del_price` decimal(10,2) NOT NULL,
  `del_min_passenger` int(11) NOT NULL,
  `del_fdcid` int(11) NOT NULL,
  `del_short_description` varchar(255) NOT NULL,
  `del_description` text NOT NULL,
  `del_exclusion` text NOT NULL,
  `del_inclusion` text NOT NULL,
  `del_custom` text NOT NULL,
  `del_release_date` int(11) NOT NULL,
  `del_expiry_date` int(11) NOT NULL,
  `del_image` varchar(255) NOT NULL,
  `del_status` tinyint(2) NOT NULL DEFAULT '0',
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`del_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_deal`
--

INSERT INTO `tbl_deal` (`del_id`, `del_title`, `del_price`, `del_min_passenger`, `del_fdcid`, `del_short_description`, `del_description`, `del_exclusion`, `del_inclusion`, `del_custom`, `del_release_date`, `del_expiry_date`, `del_image`, `del_status`, `is_delete`) VALUES
(1, 'Desert Safari', 7.00, 123, 3, '20% off on Dubai Marina Dhow Cruise Dinner with Transfer', '', '', '', '', 1505845800, 1505932200, 'del_1505905522.jpg', 1, 1),
(2, 'Desert Safari2', 7.00, 123, 3, '<p>20% off on Dubai Marina Dhow Cruise Dinner with Transfer</p>', '', '', '', '', 1505932200, 1505845800, 'del_1505907575.jpg', 1, 1),
(3, 'Dubai Standard Trio Package', 3799.00, 2, 6, '<p>Dubai Standard Trio Package</p>\r\n<p>Tour Combination</p>\r\n<p><span>Dubai City Tour + Desert Safari + Dhow Cruise Dinner - Creek</span></p>', '<p><span>This epic adventure includes three of our top tours: a four-hour Dubai City tour, a desert safari with dinner and bellydancing and an evening dinner cruise on a traditional dhow. Customize your tour to meet your needs and schedule by hitting all the hot spots in a single day, or spreading them out over two or three afternoons. Whatever you choose, this economical option is a perfect way to see the sites.</span><br /><br /><br /></p>', '<p>Anything Not mentioned in inclusions.</p>', '<p><strong>DUBAI CITY TOUR</strong></p>\r\n<p><strong>FACILITIES</strong></p>\r\n<p>&bull; Pickup from your hotel or residence in Dubai.<br />&bull; Dubai Creek.<br />&bull; Dubai Museum.<br />&bull; Jumeirah Mosque.<br />&bull; Burj Al Arab.<br />&bull; The Palm Island.<br />&bull; Atlantis Hotel.<br />&bull; Mall Of Emirates.<br />&bull; Dubai Mall.<br />&bull; Burj Khalifa.<br />&bull; Drop back to your hotel or residence in Dubai.</p>\r\n<p><strong>DHOW CRUISE INCLUDES</strong></p>\r\n<p>&bull; 2 Hrs. Cruising.<br />&bull; International Buffet Dinner with Veg &amp; Non Veg dishes.<br />&bull; Access to fully air conditioned lower deck and open air upper deck.<br />&bull; Unlimited Soft drinks, Water, Tea &amp; Coffee.<br />&bull; Tanura show and Funny Horse on traditional music.<br />&bull; Separate Toilet facilities for both Men &amp; Women.<br />&bull; Pick up &amp; Drop back to your hotel or residence in Dubai.</p>\r\n<p><strong>DESERT SAFARI with DINNER</strong></p>\r\n<p>&bull; Pickup from your hotel or residence in Dubai.<br />&bull; Desert Dune Bashing / Adventurous dune driving.<br />&bull; Sunset Photographic Opportunity.<br />&bull; Camel Riding.<br />&bull; Henna Designing.<br />&bull; Shisha Facility / &lsquo;Hubbllee Bubblee&rsquo; Smoking facility.<br />&bull; Arabic Dress photograph opportunity.<br />&bull; Unlimited Soft drinks, Water, Tea &amp; Coffee.<br />&bull; Sand Boarding (Optional).<br />&bull; Quad biking (Optional &ndash; Upon request &amp; availability).<br />&bull; BBQ Dinner with Veg &amp; Non Veg dishes.<br />&bull; Belly Dance Show/ Tanura show/Horse dance show.<br />&bull; Drop back to your hotel or residence in Dubai.<br />&bull; Pick up from other emirates is available at an extra charge.</p>', '<div class="pnltitlesub"><span>Booking Policy&nbsp;</span></div>\r\n<div class="font-14 font-b pl20">Cancellation Policy</div>\r\n<div class="removeInline checkcircle p10 xs-accordian">\r\n<div class="removeInline">\r\n<ul>\r\n<li>In case Tours or Tickets cancelled after Booking 100 % charges will be applicable</li>\r\n<li>Ticket Policy - Once your tickets are issued system will not allow you to cancel any order</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class="font-14 font-b pl20">Child Policy</div>\r\n<div class="removeInline checkcircle p10 xs-accordian">\r\n<div class="removeInline">\r\n<ul>\r\n<li>Children under 3 years will be considered as infant and entry will be free of cost.</li>\r\n<li>Children between age 3 to 10 will be considered as child and charged child rate.</li>\r\n<li>Children above age 10 will be considered as an adult and charged adult rate.</li>\r\n</ul>\r\n</div>\r\n</div>', 1506623400, 1522348200, 'del_1509437839.jpeg', 1, 0),
(4, 'Dubai Burj Khalifa Tour', 2399.00, 2, 6, '<div>\r\n<div>\r\n<div>\r\n<p>Burj Khalifa Tour</p>\r\n</div>\r\n</div>\r\n</div>', '<div>\r\n<div>\r\n<div>\r\n<p>Dubai''s Burj Khalifa, the tallest of all the buildings in the world, stands tall and proud amongst the other skyscrapers of the city. One of the major landmarks of the city, the interiors of the Burj is as majestic as the steely exterior. Take a look at the marvellous building from the inside by joining the Dubai Burj Khalifa Tour with us.</p>\r\n<p>&nbsp;</p>\r\n<p>During the tour, you are shown a multimedia presentation about the history of Dubai as well as the story of the construction of the building. Once you have gathered information about this tall structure, you are taken to the 125th observatory deck (At the Top) on the fastest elevator. Here you get a chance to have a 360-degree panoramic view of the entire city of Dubai.</p>\r\n<p>&nbsp;</p>\r\n</div>\r\n<p>This is one tour that you should definitely not miss when in Dubai. So book your tickets today for a tour of the Burj Khalifa.</p>\r\n</div>\r\n</div>', '', '<ul>\r\n<li>Hotel Pick up (If transfer option selected)</li>\r\n<li>Entry ticket and 30 minutes tour of the observatory deck</li>\r\n<li>Hotel Drop off (If transfer option selected)</li>\r\n</ul>', '<div class="desctitle pl10 pt10 accordianico">IMPORTANT INFORMATION</div>\r\n<div class="xs-accordian p10">\r\n<ul class="ml0 removeInline">\r\n<li> \r\n<ul>\r\n<li>Burj Khalifa attraction Tickets come with different time slots.Our website allows you a selection to choose the current available time slots for the Burj Khailfa Ticket.</li>\r\n<li>Bring the Ticket Voucher along with your Passport Copy or Emirates ID and present it to the At the Top ''Will Call'' counter.</li>\r\n<li>The Booking Confirmation is valid only for the specific date and time shown thereon and shall automatically expire upon the lapse of this specific date and time.</li>\r\n<li>Tickets can only be used once and is not subject to refund or exchange of any kind whatsoever.</li>\r\n<li>Lost, damaged and / or stolen Tickets cannot be replaced, refunded or exchanged in any manner.</li>\r\n<li>This product must not be individually sold to an end consumer at rates lower than the attraction''s gate prices. It can only be discounted if in combination with other products and under no circumstances should its standalone price be featured less than the original gate price.</li>\r\n<li>Location Address - Burj Khalifa - 1 Sheikh Mohammed bin Rashid Blvd - Dubai&nbsp;</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n</div>', 1506709800, 1531938600, 'del_1509437820.jpeg', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_documents`
--

CREATE TABLE IF NOT EXISTS `tbl_documents` (
  `d_id` int(11) NOT NULL AUTO_INCREMENT,
  `d_title` varchar(255) NOT NULL,
  `d_for` int(11) NOT NULL COMMENT '1: Both 2 Adult 3 Child',
  `d_required` int(11) NOT NULL,
  `d_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`d_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_documents`
--

INSERT INTO `tbl_documents` (`d_id`, `d_title`, `d_for`, `d_required`, `d_status`) VALUES
(1, 'Air Ticket', 1, 2, 1),
(2, 'Air TIcket Last Page', 1, 2, 1),
(3, 'Address Proof (Aadhar Card, Driving License)', 1, 1, 1),
(4, 'US VISIT VISA OR GREEN CARD COPY', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email_template`
--

CREATE TABLE IF NOT EXISTS `tbl_email_template` (
  `etmp_id` int(11) NOT NULL AUTO_INCREMENT,
  `etmp_title` varchar(255) NOT NULL,
  `etmp_message` longtext NOT NULL,
  `etmp_image` varchar(255) NOT NULL,
  `etmp_status` tinyint(2) NOT NULL DEFAULT '0',
  `etmp_insert_date` int(11) NOT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`etmp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_error_log`
--

CREATE TABLE IF NOT EXISTS `tbl_error_log` (
  `el_id` int(11) NOT NULL AUTO_INCREMENT,
  `el_query` text NOT NULL,
  `el_error` text NOT NULL,
  `el_user_id` int(11) NOT NULL,
  `el_time` varchar(255) NOT NULL,
  `el_url` varchar(255) NOT NULL,
  PRIMARY KEY (`el_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=334 ;

--
-- Dumping data for table `tbl_error_log`
--

INSERT INTO `tbl_error_log` (`el_id`, `el_query`, `el_error`, `el_user_id`, `el_time`, `el_url`) VALUES
(1, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/10/13 16:21', '/admin/view/leads/add.php'),
(2, 'update tbl_news_flash SET  is_delete = &#39;1&#39; where  nf_id = ', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;&#39; at line 1', 0, '2017/10/27 13:05', '/admin/view/news_flash/delete.php?id='),
(3, 'update tbl_news_flash SET  is_delete = &#39;1&#39; where  nf_id = ', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;&#39; at line 1', 0, '2017/10/27 13:06', '/admin/view/news_flash/delete.php?id='),
(4, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;from tbl_leads_applicant where  lep_lead_id = &#39;&#39;&#39; at line 1', 0, '2017/10/30 15:39', '/pay_success.php'),
(5, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(6, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(7, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(8, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(9, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(10, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(11, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(12, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(13, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(14, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(15, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(16, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(17, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(18, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(19, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(20, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(21, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/06 14:58', '/admin/view/lead_reports/?s=false'),
(22, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;&#34;&#39;&#39; at line 1', 0, '2017/11/07 00:41', '/page?id=4&#39;&#34;'),
(23, '', 'The used SELECT statements have a different number of columns', 0, '2017/11/07 21:42', '/page?id=41111111111111&#39;%20UNION%20SELECT%20CHAR(45,120,49,45,81,45)--%20%20'),
(24, '', 'The used SELECT statements have a different number of columns', 0, '2017/11/07 21:42', '/page?id=41111111111111&#39;%20UNION%20SELECT%20CHAR(45,120,49,45,81,45),CHAR(45,120,50,45,81,45)--%20%20'),
(25, '', 'The used SELECT statements have a different number of columns', 0, '2017/11/07 21:42', '/page?id=41111111111111&#39;%20UNION%20SELECT%20CHAR(45,120,49,45,81,45),CHAR(45,120,50,45,81,45),CHAR(45,120,51,45,81,45)--%20%20'),
(26, '', 'SELECT command denied to user &#39;yourduba_d_user&#39;@&#39;localhost&#39; for table &#39;user&#39;', 0, '2017/11/07 21:42', '/page?id=411111111111111111111111111&#39;%20UNION%20SELECT%201,(select%20CONCAT(0x5b6464645d,IFNULL(unhex(Hex(cast(file_priv%20as%20char))),0x20),0x5b6464645d)%20FROM%20mysql.user%20limit%200,1),3,4--%20%20'),
(27, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/11 18:12', '/admin/view/leads/add.php'),
(28, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/11 18:34', '/admin/view/leads/add.php'),
(29, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(30, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(31, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(32, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(33, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(34, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(35, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(36, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(37, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(38, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(39, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(40, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(41, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(42, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(43, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(44, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(45, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(46, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(47, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(48, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(49, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(50, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(51, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(52, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(53, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(54, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(55, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(56, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(57, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(58, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(59, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(60, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(61, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(62, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(63, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(64, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(65, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(66, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(67, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(68, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(69, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/12 21:13', '/admin/view/lead_reports/?s=false'),
(70, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(71, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(72, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(73, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(74, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(75, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(76, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(77, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(78, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(79, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(80, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(81, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(82, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(83, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(84, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(85, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(86, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(87, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(88, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(89, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(90, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(91, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(92, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(93, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(94, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(95, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(96, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(97, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(98, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(99, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(100, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(101, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(102, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(103, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(104, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(105, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:28', '/admin/view/lead_reports/?s=false'),
(106, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(107, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(108, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(109, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(110, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(111, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(112, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(113, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(114, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(115, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(116, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(117, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(118, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(119, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(120, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(121, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(122, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(123, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/lead_reports/?s=false'),
(124, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 11:41', '/admin/view/leads/add.php'),
(125, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/14 14:28', '/admin/view/leads/add.php?id=147'),
(126, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(127, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(128, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(129, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(130, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(131, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(132, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(133, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(134, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(135, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(136, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(137, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(138, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(139, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(140, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(141, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(142, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(143, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/20 12:13', '/admin/view/lead_reports/?s=false'),
(144, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(145, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(146, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(147, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(148, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(149, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(150, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(151, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(152, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(153, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(154, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(155, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(156, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(157, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(158, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(159, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(160, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(161, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(162, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(163, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(164, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(165, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(166, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(167, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(168, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(169, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(170, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(171, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(172, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(173, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(174, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(175, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(176, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(177, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(178, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(179, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:27', '/admin/view/lead_reports/?s=false'),
(180, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(181, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(182, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(183, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(184, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(185, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(186, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(187, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(188, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(189, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(190, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(191, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(192, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(193, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(194, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(195, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(196, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(197, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(198, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(199, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(200, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(201, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(202, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(203, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(204, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(205, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(206, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(207, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(208, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(209, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(210, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(211, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(212, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(213, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(214, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(215, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/22 15:28', '/admin/view/lead_reports/?s=false'),
(216, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(217, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(218, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(219, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(220, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(221, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(222, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(223, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(224, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(225, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(226, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(227, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(228, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(229, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(230, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(231, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(232, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(233, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(234, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(235, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(236, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(237, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(238, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(239, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(240, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(241, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(242, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(243, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(244, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(245, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(246, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(247, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(248, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(249, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/23 20:55', '/admin/view/lead_reports/?s=false'),
(250, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(251, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(252, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(253, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(254, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(255, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(256, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(257, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(258, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(259, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(260, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(261, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(262, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(263, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(264, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(265, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/11/29 13:13', '/admin/view/lead_reports/?s=false'),
(266, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;&#39;, &#39;+/&#39;), strlen($d)%4,&#39;=&#39;,STR_PAD_RIGHT)) . &#34;) && sleep(4);&#34;);{/php}&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%7Bphp%7D%24d%3D%22VHJ1ZQ%3D%3D%22%3Beval%28%22return+%28%22+.+base64_decode%28str_pad%28strtr%28%24d%2C+%27-_%27%2C+%27%2B%2F%27%29%2C+strlen%28%24d%29%254%2C%27%3D%27%2CSTR_PAD_RIGHT%29%29+.+%22%29+%26%26+sleep%284%29%3B%22%29%3B%7B%2Fphp%7D'),
(267, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;aZ&#39;.join(&#39;2r&#39;)}${4911774046}&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%24%7B2742329293%7D%24%7B%27aZ%27.join%28%272r%27%29%7D%24%7B4911774046%7D'),
(268, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;aZ&#39;.join(&#39;2r&#39;)}&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%24%7B%27aZ%27.join%28%272r%27%29%7D'),
(269, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;base64&#39;).urlsafe_b64decode(&#39;J2EnLmpvaW4oJ2FiJykgPT0gJ2FhYic=&#39;)) and __import__(&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%24%7B8395780651%7D%24%7Beval%28__import__%28%27base64%27%29.urlsafe_b64decode%28%27J2EnLmpvaW4oJ2FiJykgPT0gJ2FhYic%3D%27%29%29+and+__import__%28%27time%27%29.sleep%284%29%7D%24%7B4096533676%7D'),
(270, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;6870423406&#39;+str(&#39;aZ&#39;.join(&#39;2r&#39;))+&#39;6004382503&#39;&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%276870423406%27%2Bstr%28%27aZ%27.join%28%272r%27%29%29%2B%276004382503%27'),
(271, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;aZ&#39;.join(&#39;2r&#39;))&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=str%28%27aZ%27.join%28%272r%27%29%29'),
(272, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;9938925254&#39;+str(eval(__import__(&#39;base64&#39;).urlsafe_b64decode(&#39;J2EnLmpvaW4oJ2FiJyk&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%279938925254%27%2Bstr%28eval%28__import__%28%27base64%27%29.urlsafe_b64decode%28%27J2EnLmpvaW4oJ2FiJykgPT0gJ2FhYic%3D%27%29%29+and+__import__%28%27time%27%29.sleep%284%29%29%2B%273842122044%27'),
(273, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;aZ&#39;}}{% raw &#39;aZ&#39;.join(&#39;2r&#39;) %}{{&#39;2r&#39;}}{{7892394709}}&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%7B%7B6338997201%7D%7D%7B%7B%27aZ%27%7D%7D%7B%25+raw+%27aZ%27.join%28%272r%27%29+%25%7D%7B%7B%272r%27%7D%7D%7B%7B7892394709%7D%7D'),
(274, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;aZ&#39;}}{% raw &#39;aZ&#39;.join(&#39;2r&#39;) %}{{&#39;2r&#39;}}&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%7B%7B%27aZ%27%7D%7D%7B%25+raw+%27aZ%27.join%28%272r%27%29+%25%7D%7B%7B%272r%27%7D%7D'),
(275, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;base64&#39;).urlsafe_b64decode(&#39;J2EnLmpvaW4oJ2FiJykgPT0gJ2FhYic=&#39;)) and __import__(&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%7B%7B3065305759%7D%7D%7B%7Beval%28__import__%28%27base64%27%29.urlsafe_b64decode%28%27J2EnLmpvaW4oJ2FiJykgPT0gJ2FhYic%3D%27%29%29+and+__import__%28%27time%27%29.sleep%284%29%7D%7D%7B%7B7414013670%7D%7D'),
(276, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;aZ&#39;.join(&#39;2r&#39;)}}{{4617487964}}&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%7B%7B2522432967%7D%7D%7B%7B%27aZ%27.join%28%272r%27%29%7D%7D%7B%7B4617487964%7D%7D'),
(277, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;aZ&#39;.join(&#39;2r&#39;)}}&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%7B%7B%27aZ%27.join%28%272r%27%29%7D%7D'),
(278, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;base64&#39;).urlsafe_b64decode(&#39;ZXZhbChfX2ltcG9ydF9fKCdiYXNlNjQnKS51cmxzYWZlX2I2NGRl&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%7B%7B8484049675%7D%7D%7B%7B%27%27%7D%7D%7B%25+set+d+%3D+%22eval%28__import__%28%27base64%27%29.urlsafe_b64decode%28%27ZXZhbChfX2ltcG9ydF9fKCdiYXNlNjQnKS51cmxzYWZlX2I2NGRlY29kZSgnSjJFbkxtcHZhVzRvSjJGaUp5a2dQVDBnSjJGaFlpYz0nKSkgYW5kIF9faW1wb3J0X18'),
(279, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;6991801170&#39;+&#34;#{81*14}&#34;+&#39;6421809579&#39;)&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%3D%28%276991801170%27%2B%22%23%7B81%2A14%7D%22%2B%276421809579%27%29'),
(280, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;base64&#39;;eval(Base64.urlsafe_decode64(&#39;MS50b19zPT0nMSc=&#39;))&&sleep(4))&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%3D%28require%27base64%27%3Beval%28Base64.urlsafe_decode64%28%27MS50b19zPT0nMSc%3D%27%29%29%26%26sleep%284%29%29'),
(281, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;4474275035&#39;+&#34;#{81*14}&#34;+&#39;3248862270&#39; %>&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%3C%25%3D+%274474275035%27%2B%22%23%7B81%2A14%7D%22%2B%273248862270%27+%25%3E'),
(282, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;base64&#39;;eval(Base64.urlsafe_decode64(&#39;MS50b19zPT0nMSc=&#39;))&&sleep(4) %>&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%3C%25%3D+require%27base64%27%3Beval%28Base64.urlsafe_decode64%28%27MS50b19zPT0nMSc%3D%27%29%29%26%26sleep%284%29+%25%3E'),
(283, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;child_process&#39;).execSync(Buffer(&#39;dHJ1ZQ==&#39;, &#39;base64&#39;).toString() + &#39; && sleep 4&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%0A-+x+%3D+global.process.mainModule.require%0A-+x%28%27child_process%27%29.execSync%28Buffer%28%27dHJ1ZQ%3D%3D%27%2C+%27base64%27%29.toString%28%29+%2B+%27+%26%26+sleep+4%27%29%2F%2F'),
(284, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;child_process&#39;).execSync(Buffer(&#39;dHJ1ZQ==&#39;, &#39;base64&#39;).toString() + &#39; && sleep 4&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%7B%7Brange.constructor%28%22global.process.mainModule.require%28%27child_process%27%29.execSync%28Buffer%28%27dHJ1ZQ%3D%3D%27%2C+%27base64%27%29.toString%28%29+%2B+%27+%26%26+sleep+4%27%29%22%29%28%29%7D%7D'),
(285, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;child_process&#39;).execSync(Buffer(&#39;dHJ1ZQ==&#39;, &#39;base64&#39;).toString() + &#39; && sleep 4&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%7B%7B%3D%27%27%7D%7D%7B%7Bglobal.process.mainModule.require%28%27child_process%27%29.execSync%28Buffer%28%27dHJ1ZQ%3D%3D%27%2C+%27base64%27%29.toString%28%29+%2B+%27+%26%26+sleep+4%27%29%3B%7D%7D'),
(286, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;cmVxdWlyZSgnY2hpbGRfcHJvY2VzcycpLmV4ZWNTeW5jKEJ1ZmZlcignZEhKMVpRPT0nLCAnYmFzZTY0&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%7B%40if+cond%3D%22eval%28Buffer%28%27cmVxdWlyZSgnY2hpbGRfcHJvY2VzcycpLmV4ZWNTeW5jKEJ1ZmZlcignZEhKMVpRPT0nLCAnYmFzZTY0JykudG9TdHJpbmcoKSArICcgJiYgc2xlZXAgNCcpOw%3D%3D%27%2C+%27base64%27%29.toString%28%29%29%22%7D%7B%2Fif%7D'),
(287, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;child_process&#39;).execSync(Buffer(&#39;dHJ1ZQ==&#39;, &#39;base64&#39;).toString() + &#39; && sleep 4&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%24%7Brequire%28%27child_process%27%29.execSync%28Buffer%28%27dHJ1ZQ%3D%3D%27%2C+%27base64%27%29.toString%28%29+%2B+%27+%26%26+sleep+4%27%29%7D'),
(288, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;1905789180&#39;+typeof(81)+14+&#39;3681881900&#39;&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%271905789180%27%2Btypeof%2881%29%2B14%2B%273681881900%27'),
(289, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;child_process&#39;).execSync(Buffer(&#39;dHJ1ZQ==&#39;, &#39;base64&#39;).toString() + &#39; && sleep 4&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=require%28%27child_process%27%29.execSync%28Buffer%28%27dHJ1ZQ%3D%3D%27%2C+%27base64%27%29.toString%28%29+%2B+%27+%26%26+sleep+4%27%29%2F%2F'),
(290, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;4424242433&#39;);print(81);print_r(&#39;2080340835&#39;);&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=print_r%28%274424242433%27%29%3Bprint%2881%29%3Bprint_r%28%272080340835%27%29%3B'),
(291, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;&#39;, &#39;+/&#39;), strlen($d)%4,&#39;=&#39;,STR_PAD_RIGHT)) . &#34;) && sleep(4);&#34;);&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%24d%3D%22VHJ1ZQ%3D%3D%22%3Beval%28%22return+%28%22+.+base64_decode%28str_pad%28strtr%28%24d%2C+%27-_%27%2C+%27%2B%2F%27%29%2C+strlen%28%24d%29%254%2C%27%3D%27%2CSTR_PAD_RIGHT%29%29+.+%22%29+%26%26+sleep%284%29%3B%22%29%3B'),
(292, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;8843040238&#39;+&#34;#{81*14}&#34;+&#39;5017713700&#39;&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%278843040238%27%2B%22%23%7B81%2A14%7D%22%2B%275017713700%27'),
(293, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;base64&#39;;eval(Base64.urlsafe_decode64(&#39;MS50b19zPT0nMSc=&#39;))&&sleep(4)&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=require%27base64%27%3Beval%28Base64.urlsafe_decode64%28%27MS50b19zPT0nMSc%3D%27%29%29%26%26sleep%284%29'),
(294, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;9516694535&#39;+typeof(81)+14+&#39;5249535169&#39; %>&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%3C%25-+%279516694535%27%2Btypeof%2881%29%2B14%2B%275249535169%27+%25%3E'),
(295, '', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near &#39;child_process&#39;).execSync(Buffer(&#39;dHJ1ZQ==&#39;, &#39;base64&#39;).toString() + &#39; && sleep 4&#39;&#39; at line 1', 0, '2017/12/01 20:47', '/page?id=%3C%25global.process.mainModule.require%28%27child_process%27%29.execSync%28Buffer%28%27dHJ1ZQ%3D%3D%27%2C+%27base64%27%29.toString%28%29+%2B+%27+%26%26+sleep+4%27%29%25%3E'),
(296, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(297, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(298, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(299, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(300, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(301, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(302, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(303, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(304, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(305, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(306, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false');
INSERT INTO `tbl_error_log` (`el_id`, `el_query`, `el_error`, `el_user_id`, `el_time`, `el_url`) VALUES
(307, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(308, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(309, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(310, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(311, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(312, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(313, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(314, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/04 19:01', '/admin/view/lead_reports/?s=false'),
(315, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/leads/add.php'),
(316, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(317, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(318, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(319, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(320, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(321, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(322, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(323, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(324, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(325, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(326, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(327, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(328, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(329, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(330, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(331, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(332, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false'),
(333, '', 'Unknown column &#39;is_delete&#39; in &#39;where clause&#39;', 0, '2017/12/14 15:44', '/visadart/admin/view/lead_reports/?s=false');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faq`
--

CREATE TABLE IF NOT EXISTS `tbl_faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_question` varchar(255) NOT NULL,
  `faq_answer` text NOT NULL,
  `faq_status` tinyint(2) NOT NULL DEFAULT '0',
  `faq_fdcid` int(11) NOT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tbl_faq`
--

INSERT INTO `tbl_faq` (`faq_id`, `faq_question`, `faq_answer`, `faq_status`, `faq_fdcid`, `is_delete`) VALUES
(1, '<p><span class="nodeLabelBox repTarget "><span class="nodeText editable "><span class="  ">Lorem  Ipsum is simply dummy text of the printing and typesetting industry</span></span></span></p>', '<p><span class="nodeLabelBox repTarget "><span class="nodeText editable "><span class="  ">Lorem  Ipsum is simply dummy text of the printing and typesetting industry.  Lorem Ipsum has been the industry''s standard dummy text ever since the  1500s, when an unknown printer took a galley of type and scrambled it to  make a type specimen book. It has survived not only five centuries, but  also the leap into electronic typesetting, remaining essentially  unchanged.</span></span></span></p>', 1, 2, 1),
(2, '<p><span class="nodeLabelBox repTarget "><span class="nodeText editable "><span class="  ">Lorem  Ipsum is simply dummy text of the printing and typesetting industry</span></span></span></p>', '<p><span class="nodeLabelBox repTarget "><span class="nodeText editable "><span class="  ">Lorem  Ipsum is simply dummy text of the printing and typesetting industry</span></span></span></p>', 1, 2, 1),
(3, '<p><span>Why should I use the online application system?</span></p>', '<p><span>It is easy to use and is convenient. You can submit all the required documents and information and pay for the service online without having to send documents by post or fax or visiting the office personally. If your visa application is approved, we will also automatically upload your visa copy which you can download and print as per your convenience.</span></p>', 1, 4, 0),
(4, '<p><span>When do I need to apply for a visa?</span></p>', '<p><span>You can apply for a visa a maximum of 58 days before you travel, and a minimum of seven working days before you travel. We recommend you apply for your visa well in advance of travel.</span></p>', 1, 4, 0),
(5, '<p><span>Will I definitely get a visa if I apply online?</span></p>', '<p>The decision to grant U.A.E visa is entirely at the discretion of the country&rsquo;s immigration authorities &ndash; YourDubaiVisa cannot guarantee that your visa application will be accepted.</p>', 1, 4, 0),
(6, '<p><span>If I get a visa, am I guaranteed entry into the U.A.E?</span></p>', '<p>The granting of a visa does not guarantee entry into the U.A.E. The final decision is made by the immigration officer at the point of entry.</p>', 1, 4, 0),
(7, '<h3><span style="font-size: 10px;">If I Have A Valid Dubai Visa, Will I Be Able To Travel To The Other Emirates?</span></h3>', '<p>Yes, once you have a valid UAE visa, you can travel anywhere within the country.</p>', 1, 4, 0),
(8, '<p><span>How long does it take to get a visa?</span><br /><span style="white-space: pre;"> </span></p>', '<p>If all required documents are submitted correctly, the visa is normally issued within 5 working days.</p>', 1, 4, 0),
(9, '<p><span>My passport is expiring soon &ndash; can I still get a visa?</span></p>', '<p>The U.A.E Immigration office requires all visitors to have a passport that is valid for at least six months from the date of arrival. If your passport is valid for less than six months, you will need to renew it before you can apply for a visa and travel to the U.A.E.</p>', 1, 4, 0),
(10, '<p><span>Do children or infants need a visa?</span></p>', '<p>Yes, all passengers, including children and infants on a parent&rsquo;s passport, must have a visa to enter the U.A.E. This also includes infants traveling on their parents&rsquo; lap.</p>', 1, 4, 0),
(11, '<p><span>Why was my visa application rejected?</span></p>', '<p>Unfortunately, the U.A.E Immigration authorities do not give reasons for rejecting visa applications and hence we cannot provide specific reasons why your application was rejected.</p>', 1, 4, 0),
(12, '<p><span>Will my visa fees be refunded if my application is rejected?</span></p>', '<p>No</p>', 1, 4, 0),
(13, '<h3>Do I Have To Submit My Passport For Stamping Before I Travel?</h3>', '<p>No: you will receive an electronic visa, which does not require anything to be stamped in your passport ahead of travel. All visas are sent by email. You should print out a copy of your visa before you travel, and bring it with you.</p>', 1, 4, 0),
(14, '<p>What is An OK TO BOARD ?<span style="white-space: pre;"> </span></p>', '<p><span>Once you receive a valid UAE visa after applying for a UAE visa, you have to send your visa&nbsp;</span><span>copy to the airline you have booked your UAE flight ticket with. After rechecking your UAE visa, the airline approves your ticket and marks your flight PNR as OK to Board. Please note that you cannot board a flight if your PNR is not reflecting OK to Board. Thus, YourDubaiVisa strongly recommends all the passengers travelling to UAE to check their OK to Board status with the respective airline at least 48 hours prior to departure.</span></p>', 1, 5, 0),
(15, '<p>Why is Ok To Board Mandatory ?<span style="white-space: pre;"> </span></p>', '<p><span>Due to an increase in fake visas to UAE, many Indian passengers were deported back to India in the past. To minimize the fake visa issues, airlines flying to UAE and the sponsors started re-checking the traveller&rsquo;s UAE visa and the return airline ticket before the departure to UAE.</span></p>', 1, 5, 0),
(16, '<p><a class="toggleVisibility toggleShown" tabindex="1" name="faq2">When should I apply for a Dubai visa?</a></p>', '<p>You can apply for a Dubai Visa to 5 to 58 Days Of Travel.</p>', 1, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faq_deal_category`
--

CREATE TABLE IF NOT EXISTS `tbl_faq_deal_category` (
  `fdc_id` int(11) NOT NULL AUTO_INCREMENT,
  `fdc_name` varchar(255) NOT NULL,
  `fdc_icon` varchar(255) NOT NULL,
  `fdc_type` tinyint(3) NOT NULL DEFAULT '0' COMMENT '1=deal 2=faq',
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fdc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_faq_deal_category`
--

INSERT INTO `tbl_faq_deal_category` (`fdc_id`, `fdc_name`, `fdc_icon`, `fdc_type`, `is_delete`) VALUES
(1, 'Faq category', 'faq_1505821615.jpg', 2, 1),
(2, 'Faq category', 'faq_1505821628.jpg', 2, 1),
(3, 'test', 'faq_1505895974.jpg', 1, 1),
(4, 'FAQ', 'faq_1506846305.png', 2, 0),
(5, 'OTB', '', 2, 0),
(6, 'Deals', '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_insurance`
--

CREATE TABLE IF NOT EXISTS `tbl_insurance` (
  `ins_id` int(11) NOT NULL AUTO_INCREMENT,
  `ins_plan_details` text NOT NULL,
  `ins_inclusion` text NOT NULL,
  `ins_exclusion` text NOT NULL,
  `ins_policy_terms` text NOT NULL,
  `ins_claim_process` text NOT NULL,
  `ins_status` text NOT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  `ins_image` varchar(255) NOT NULL,
  PRIMARY KEY (`ins_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_insurance`
--

INSERT INTO `tbl_insurance` (`ins_id`, `ins_plan_details`, `ins_inclusion`, `ins_exclusion`, `ins_policy_terms`, `ins_claim_process`, `ins_status`, `is_delete`, `ins_image`) VALUES
(1, '<table class="table table-bordered table-condensed custom-table" border="0" cellspacing="0" cellpadding="0" width="100%">\r\n<thead> \r\n<tr>\r\n<th colspan="2" valign="top">Plan Detail</th><th width="13%" valign="top">Explore Asia</th><th width="13%" valign="top">Explore Africa</th><th width="13%" valign="top">Explore Europe</th><th width="13%" valign="top">Explore Canada+</th><th width="12%" valign="top">Explore Gold</th><th width="12%" valign="top">Explore Platinum</th>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Geographical Scope</td>\r\n<td valign="top">Asia</td>\r\n<td valign="top">Africa</td>\r\n<td valign="top">Europe</td>\r\n<td valign="top">Worldwide Exclusing US</td>\r\n<td colspan="2" valign="top">Worldwide / Worldwide excluding US and Canada</td>\r\n</tr>\r\n</thead> \r\n<tbody>\r\n<tr>\r\n<td colspan="2" valign="top">Sum Insured (in ''000)</td>\r\n<td valign="top">US $ 25, 50 &amp; 100</td>\r\n<td valign="top">US $ 25, 50 &amp; 100</td>\r\n<td valign="top">&euro; 30 &amp; 100</td>\r\n<td valign="top">US $ 50 &amp; 100</td>\r\n<td colspan="2" valign="top">$ 50, 100, 300 &amp; 500</td>\r\n</tr>\r\n<tr>\r\n<td width="12%" valign="top">Benefit</td>\r\n<td width="12%" valign="top">Deductible</td>\r\n<td colspan="6" valign="top">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Hospitalization Expenses</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">In-patient Care</td>\r\n<td valign="top">US $ 100/&euro; 75</td>\r\n<td valign="top">Up to SI</td>\r\n<td valign="top">Up to SI</td>\r\n<td valign="top">Up to SI</td>\r\n<td valign="top">Up to SI</td>\r\n<td valign="top">Up to SI</td>\r\n<td valign="top">Up to SI</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">Life Threatening Condition for PED</td>\r\n<td valign="top">US $ 100/&euro; 75</td>\r\n<td valign="top">10% of SI</td>\r\n<td valign="top">10% of SI</td>\r\n<td valign="top">10% of SI</td>\r\n<td valign="top">10% of SI</td>\r\n<td valign="top">10% of SI</td>\r\n<td valign="top">10% of SI</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">Additional SI for Accidental Hospitalization</td>\r\n<td valign="top">US $ 100/&euro; 75</td>\r\n<td valign="top">Yes, up to 100% SI</td>\r\n<td valign="top">Yes, up to 100% SI</td>\r\n<td valign="top">Yes, up to 100% SI</td>\r\n<td valign="top">Yes, up to 100% SI</td>\r\n<td valign="top">Yes, up to 100% SI</td>\r\n<td valign="top">Yes, up to 100% SI</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">Out-patient Care</td>\r\n<td valign="top">US $ 100/&euro; 75</td>\r\n<td valign="top">20% of SI</td>\r\n<td valign="top">20% of SI</td>\r\n<td valign="top">&euro; 30,000</td>\r\n<td valign="top">US $ 50,000</td>\r\n<td valign="top">US $ 50,000</td>\r\n<td valign="top">US $ 50,000</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">\r\n<div class="fl">Daily Allowance</div>\r\n</td>\r\n<td valign="top">2 days</td>\r\n<td valign="top">US $ 25 per day, max 5 days</td>\r\n<td valign="top">US $ 25 per day, max 5 days</td>\r\n<td valign="top">&euro; 25 per day, max 5 days</td>\r\n<td valign="top">US $ 25 per day, max 5 days</td>\r\n<td valign="top">US $ 25 per day, max 5 days</td>\r\n<td valign="top">US $ 25 per day, max 5 days</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">\r\n<div class="fl">Compassionate Visit</div>\r\n</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">US $ 5,000</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">Return of Minor Child</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">US $ 2,000</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">\r\n<div class="fl">Up-gradation to Business Class</div>\r\n</td>\r\n<td valign="top">-</td>\r\n<td valign="top">US $ 1,000</td>\r\n<td valign="top">US $ 1,000</td>\r\n<td valign="top">&euro; 750</td>\r\n<td valign="top">US $ 1,000</td>\r\n<td valign="top">US $ 1,000</td>\r\n<td valign="top">US $ 1,000</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">Dental Expenses</td>\r\n<td valign="top">US $ 100/&euro; 75</td>\r\n<td valign="top">US $ 300</td>\r\n<td valign="top">US $ 300</td>\r\n<td valign="top">US $ 300</td>\r\n<td valign="top">US $ 300</td>\r\n<td valign="top">US $ 300</td>\r\n<td valign="top">US $ 300</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">Personal Accident</td>\r\n<td valign="top">-</td>\r\n<td valign="top">US $ 15,000</td>\r\n<td valign="top">US $ 15,000</td>\r\n<td valign="top">&euro; 10,000</td>\r\n<td valign="top">US $ 15,000</td>\r\n<td valign="top">US $ 15,000</td>\r\n<td valign="top">US $ 15,000</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">Common Carrier Accidental Death</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">US $ 5,000</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">\r\n<div class="fl">Medical Evacuation</div>\r\n</td>\r\n<td valign="top">-</td>\r\n<td valign="top">US $ 10,000</td>\r\n<td valign="top">US $ 10,000</td>\r\n<td valign="top">&euro; 30,000</td>\r\n<td valign="top">US $ 50,000</td>\r\n<td valign="top">US $ 50,000</td>\r\n<td valign="top">US $ 50,000</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">Repatriation of Mortal Remains</td>\r\n<td valign="top">-</td>\r\n<td valign="top">US $ 10,000</td>\r\n<td valign="top">US $ 10,000</td>\r\n<td valign="top">&euro; 30,000</td>\r\n<td valign="top">US $ 50,000</td>\r\n<td valign="top">US $ 50,000</td>\r\n<td valign="top">US $ 50,000</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">\r\n<div class="fl">Trip Cancellation &amp; Interruption</div>\r\n</td>\r\n<td valign="top">-</td>\r\n<td valign="top">US $ 1,000</td>\r\n<td valign="top">US $ 1,000</td>\r\n<td valign="top">&euro; 750</td>\r\n<td valign="top">US $ 1,000</td>\r\n<td valign="top">US $ 1,000</td>\r\n<td valign="top">US $ 1,000</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">\r\n<div class="fl">Trip Delay</div>\r\n</td>\r\n<td valign="top">12 hours</td>\r\n<td valign="top">US $ 500</td>\r\n<td valign="top">US $ 500</td>\r\n<td valign="top">&euro; 300</td>\r\n<td valign="top">US $ 500</td>\r\n<td valign="top">US $ 500</td>\r\n<td valign="top">US $ 500</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">\r\n<div class="fl">Loss of Checked-in Baggage</div>\r\n</td>\r\n<td valign="top">-</td>\r\n<td valign="top">US $ 100</td>\r\n<td valign="top">US $ 100</td>\r\n<td valign="top">&euro; 100</td>\r\n<td valign="top">US $ 100</td>\r\n<td valign="top">US $ 100</td>\r\n<td valign="top">US $ 100</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">\r\n<div class="fl">Delay of Checked-in Baggage</div>\r\n</td>\r\n<td valign="top">12 hours</td>\r\n<td valign="top">US $ 100</td>\r\n<td valign="top">US $ 100</td>\r\n<td valign="top">&euro; 100</td>\r\n<td valign="top">US $ 100</td>\r\n<td valign="top">US $ 100</td>\r\n<td valign="top">US $ 100</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">\r\n<div class="fl">Loss of Passport</div>\r\n</td>\r\n<td valign="top">-</td>\r\n<td valign="top">US $ 300</td>\r\n<td valign="top">US $ 300</td>\r\n<td valign="top">&euro; 250</td>\r\n<td valign="top">US $ 300</td>\r\n<td valign="top">US $ 300</td>\r\n<td valign="top">US $ 300</td>\r\n</tr>\r\n<tr>\r\n<td valign="top">Personal Liability</td>\r\n<td valign="top">US $ 100/&euro; 75</td>\r\n<td valign="top">US $ 100,000</td>\r\n<td valign="top">US $ 100,000</td>\r\n<td valign="top">&euro; 75,000</td>\r\n<td valign="top">US $ 100,000</td>\r\n<td valign="top">US $ 100,000</td>\r\n<td valign="top">US $ 100,000</td>\r\n</tr>\r\n<tr>\r\n<th valign="top">Trip Options</th><th colspan="7" valign="top">&nbsp;</th>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Single Trip</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Multi Trip (Policy will be on annual basis)</td>\r\n<td valign="top">No</td>\r\n<td valign="top">No</td>\r\n<td valign="top">No</td>\r\n<td valign="top">No</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Trip Duration (days)</td>\r\n<td colspan="6" valign="top">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Minimum</td>\r\n<td valign="top">2</td>\r\n<td valign="top">2</td>\r\n<td valign="top">2</td>\r\n<td valign="top">2</td>\r\n<td valign="top">2</td>\r\n<td valign="top">2</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Maximum (Single Trip)</td>\r\n<td valign="top">365</td>\r\n<td valign="top">365</td>\r\n<td valign="top">365</td>\r\n<td valign="top">365</td>\r\n<td valign="top">365</td>\r\n<td valign="top">365</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Maximum (Multi Trip)</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">45 or 60 days</td>\r\n<td valign="top">45 or 60 days</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Entry Age - Single Trip</td>\r\n<td colspan="6" valign="top">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Minimum</td>\r\n<td valign="top">1 day</td>\r\n<td valign="top">1 day</td>\r\n<td valign="top">1 day</td>\r\n<td valign="top">1 day</td>\r\n<td valign="top">1 day</td>\r\n<td valign="top">1 day</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Maximum</td>\r\n<td valign="top">No Age Bar</td>\r\n<td valign="top">No Age Bar</td>\r\n<td valign="top">No Age Bar</td>\r\n<td valign="top">No Age Bar</td>\r\n<td valign="top">No Age Bar</td>\r\n<td valign="top">No Age Bar</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Entry Age - Multi Trip</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Minimum</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">1 day</td>\r\n<td valign="top">1 day</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Maximum</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">-</td>\r\n<td valign="top">70 years</td>\r\n<td valign="top">70 years</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Sub-limits applicable (For age 61 years and above) As per Appendix</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">No</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">No</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" valign="top">Family Option*</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n<td valign="top">Yes</td>\r\n</tr>\r\n</tbody>\r\n</table>', '<p><strong>Inclusion</strong></p>\r\n<p><strong></strong>To be Removed</p>', '<p><strong>Exclusion</strong></p>\r\n<p><strong>What is not covered?<br /> </strong></p>\r\n<ul>\r\n<li>Expenses arising out of or attributable to alcohol or drug use/misuse/abuse.</li>\r\n<li>War and Nuclear perils or consequences thereof</li>\r\n<li>Ionising Radiation or contamination arising out of the same</li>\r\n<li>Any intentional self-injury, suicide or attempted suicide</li>\r\n<li>Any claim relating to hazardous activities</li>\r\n<li>The insured being involved in Breach of taw</li>\r\n<li>HIV/AIDS</li>\r\n</ul>', '<p><strong>Policy Terms</strong></p>\r\n<p><strong></strong>Please click here to see the detailed policy&nbsp; <a href="http://www.religarehealthinsurance.com/userfiles/file/PolicyTermsandConditions.pdf">Terms &amp; Conditions.</a></p>', '<p><strong>Claim Process</strong></p>\r\n<table class="table table-bordered table-condensed" border="0" cellspacing="0" cellpadding="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td><strong>No matter which part of the world you are, we''re just a call away!</strong><br /> <br /> In case of Claim, notify us immediately on any of the below  touch-points for hassle free processing and speedy settlements.<br /> <span>Falck Global Assistance (24 hours assistance centre)</span></td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<table border="0" cellspacing="0" cellpadding="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td height="30" align="right" valign="middle">US and Canada</td>\r\n<td align="center" valign="middle">:</td>\r\n<td height="30" align="left" valign="middle">18443013135 /18443013146 ( Toll Free)</td>\r\n<td height="30" align="right" valign="middle">&nbsp;</td>\r\n<td align="center" valign="middle">&nbsp;</td>\r\n<td height="30" align="left" valign="middle">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td height="30" align="right" valign="middle">Other countries</td>\r\n<td align="center" valign="middle">:</td>\r\n<td height="30" align="left" valign="middle">+91-124-4498760  (Call Back Facility)</td>\r\n<td height="30" align="right" valign="middle">Fax</td>\r\n<td align="center" valign="middle">:</td>\r\n<td height="30" align="left" valign="middle">+91- 124- 4006674</td>\r\n</tr>\r\n<tr>\r\n<td width="16%" height="30" align="right" valign="middle"><img src="http://www.religaretravelinsurance.com/images/icon2.jpg" alt="" /></td>\r\n<td width="3%" align="center" valign="middle">:</td>\r\n<td width="34%" height="30" align="left" valign="middle">travelassistance@religare.com</td>\r\n<td width="10%" height="30" align="right" valign="middle">&nbsp;</td>\r\n<td width="3%" align="center" valign="middle">&nbsp;</td>\r\n<td width="34%" height="30" align="left" valign="middle">&nbsp;</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>In case of reimbursement of treatment expenses, reach us at the below touch points<br /> <span>Religare Health Insurance Company Limited</span> GYS Global, Plot No. A3, A4, A5, Sector - 125, Noida, U.P. - 201301</td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<table border="0" cellspacing="0" cellpadding="0" width="100%">\r\n<tbody>\r\n<tr>\r\n<td height="30" align="right" valign="middle">India</td>\r\n<td align="center" valign="middle">:</td>\r\n<td height="30" align="left" valign="middle">1800 200 4488 (Toll Free &amp; Accessible in India only)</td>\r\n<td height="30" align="right" valign="middle">Fax (RHICL)</td>\r\n<td align="center" valign="middle">:</td>\r\n<td height="30" align="left" valign="middle">1800 200 6677</td>\r\n</tr>\r\n<tr>\r\n<td width="11%" height="30" align="right" valign="middle"><img src="http://www.religaretravelinsurance.com/images/icon2.jpg" alt="" /></td>\r\n<td width="3%" align="center" valign="middle">:</td>\r\n<td width="45%" height="30" align="left" valign="middle">travelclaims@religare.com</td>\r\n<td width="11%" height="30" align="right" valign="middle">&nbsp;</td>\r\n<td width="3%" align="center" valign="middle">&nbsp;</td>\r\n<td width="27%" height="30" align="left" valign="middle">&nbsp;</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>Be it Cashless settlement or reimbursement of medical expenses, we deliver on our promise of worry free experience!</td>\r\n</tr>\r\n</tbody>\r\n</table>', '1', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_insurance_price`
--

CREATE TABLE IF NOT EXISTS `tbl_insurance_price` (
  `insp_id` int(11) NOT NULL AUTO_INCREMENT,
  `insp_start_age` int(11) NOT NULL,
  `insp_end_age` int(11) NOT NULL,
  `insp_price` decimal(7,2) NOT NULL,
  `insp_day` int(11) NOT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`insp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=365 ;

--
-- Dumping data for table `tbl_insurance_price`
--

INSERT INTO `tbl_insurance_price` (`insp_id`, `insp_start_age`, `insp_end_age`, `insp_price`, `insp_day`, `is_delete`) VALUES
(9, 0, 40, 207.00, 2, 0),
(10, 0, 40, 244.00, 3, 0),
(11, 0, 40, 281.00, 4, 0),
(12, 0, 40, 318.00, 5, 0),
(13, 0, 40, 355.00, 6, 0),
(14, 0, 40, 362.00, 7, 0),
(15, 0, 40, 369.00, 8, 0),
(16, 0, 40, 373.00, 9, 0),
(17, 0, 40, 400.00, 10, 0),
(18, 0, 40, 428.00, 11, 0),
(19, 0, 40, 456.00, 12, 0),
(20, 0, 40, 483.00, 13, 0),
(21, 0, 40, 488.00, 14, 0),
(22, 0, 40, 493.00, 15, 0),
(23, 0, 40, 498.00, 16, 0),
(24, 0, 40, 503.00, 17, 0),
(25, 0, 40, 508.00, 18, 0),
(26, 0, 40, 513.00, 19, 0),
(27, 0, 40, 516.00, 20, 0),
(28, 0, 40, 536.00, 21, 0),
(29, 0, 40, 539.00, 22, 0),
(30, 0, 40, 558.00, 23, 0),
(31, 0, 40, 577.00, 24, 0),
(32, 0, 40, 596.00, 25, 0),
(33, 0, 40, 616.00, 26, 0),
(34, 0, 40, 635.00, 27, 0),
(35, 0, 40, 641.00, 28, 0),
(36, 0, 40, 648.00, 29, 0),
(37, 0, 40, 666.00, 30, 0),
(38, 0, 40, 685.00, 31, 0),
(39, 0, 40, 703.00, 32, 0),
(40, 0, 40, 722.00, 33, 0),
(41, 0, 40, 740.00, 34, 0),
(42, 0, 40, 759.00, 35, 0),
(43, 0, 40, 773.00, 36, 0),
(44, 0, 40, 791.00, 37, 0),
(45, 0, 40, 810.00, 38, 0),
(46, 0, 40, 828.00, 39, 0),
(47, 0, 40, 847.00, 40, 0),
(48, 0, 40, 865.00, 41, 0),
(49, 0, 40, 884.00, 42, 0),
(50, 0, 40, 902.00, 43, 0),
(51, 0, 40, 921.00, 44, 0),
(52, 0, 40, 939.00, 45, 0),
(53, 0, 40, 958.00, 46, 0),
(54, 0, 40, 976.00, 47, 0),
(55, 0, 40, 995.00, 48, 0),
(56, 0, 40, 1013.00, 49, 0),
(57, 0, 40, 1032.00, 50, 0),
(58, 0, 40, 1050.00, 51, 0),
(59, 0, 40, 1069.00, 52, 0),
(60, 0, 40, 1087.00, 53, 0),
(61, 0, 40, 1106.00, 54, 0),
(62, 0, 40, 1124.00, 55, 0),
(63, 0, 40, 1143.00, 56, 0),
(64, 0, 40, 1161.00, 57, 0),
(65, 0, 40, 1180.00, 58, 0),
(66, 0, 40, 1198.00, 59, 0),
(67, 0, 40, 1217.00, 60, 0),
(68, 0, 40, 1227.00, 61, 0),
(69, 0, 40, 1246.00, 62, 0),
(70, 0, 40, 1264.00, 63, 0),
(71, 0, 40, 1283.00, 64, 0),
(72, 0, 40, 1301.00, 65, 0),
(73, 0, 40, 1320.00, 66, 0),
(74, 0, 40, 1338.00, 67, 0),
(75, 0, 40, 1356.00, 68, 0),
(76, 0, 40, 1375.00, 69, 0),
(77, 0, 40, 1393.00, 70, 0),
(78, 0, 40, 1412.00, 71, 0),
(79, 0, 40, 1430.00, 72, 0),
(80, 0, 40, 1449.00, 73, 0),
(81, 0, 40, 1467.00, 74, 0),
(82, 0, 40, 1486.00, 75, 0),
(83, 0, 40, 1504.00, 76, 0),
(84, 0, 40, 1523.00, 77, 0),
(85, 0, 40, 1541.00, 78, 0),
(86, 0, 40, 1559.00, 79, 0),
(87, 0, 40, 1578.00, 80, 0),
(88, 0, 40, 1596.00, 81, 0),
(89, 0, 40, 1615.00, 82, 0),
(90, 0, 40, 1633.00, 83, 0),
(91, 0, 40, 1652.00, 84, 0),
(92, 0, 40, 1670.00, 85, 0),
(93, 0, 40, 1689.00, 86, 0),
(94, 0, 40, 1707.00, 87, 0),
(95, 0, 40, 1726.00, 88, 0),
(96, 0, 40, 1744.00, 89, 0),
(97, 0, 40, 1762.00, 90, 0),
(98, 41, 60, 223.00, 2, 0),
(99, 41, 60, 268.00, 3, 0),
(100, 41, 60, 314.00, 4, 0),
(101, 41, 60, 359.00, 5, 0),
(102, 41, 60, 404.00, 6, 0),
(103, 41, 60, 412.00, 7, 0),
(104, 41, 60, 421.00, 8, 0),
(105, 41, 60, 428.00, 9, 0),
(106, 41, 60, 462.00, 10, 0),
(107, 41, 60, 495.00, 11, 0),
(108, 41, 60, 529.00, 12, 0),
(109, 41, 60, 563.00, 13, 0),
(110, 41, 60, 569.00, 14, 0),
(111, 41, 60, 574.00, 15, 0),
(112, 41, 60, 580.00, 16, 0),
(113, 41, 60, 586.00, 17, 0),
(114, 41, 60, 592.00, 18, 0),
(115, 41, 60, 598.00, 19, 0),
(116, 41, 60, 605.00, 20, 0),
(117, 41, 60, 630.00, 21, 0),
(118, 41, 60, 633.00, 22, 0),
(119, 41, 60, 656.00, 23, 0),
(120, 41, 60, 680.00, 24, 0),
(121, 41, 60, 703.00, 25, 0),
(122, 41, 60, 727.00, 26, 0),
(123, 41, 60, 750.00, 27, 0),
(124, 41, 60, 758.00, 28, 0),
(125, 41, 60, 767.00, 29, 0),
(126, 41, 60, 789.00, 30, 0),
(127, 41, 60, 812.00, 31, 0),
(128, 41, 60, 835.00, 32, 0),
(129, 41, 60, 857.00, 33, 0),
(130, 41, 60, 880.00, 34, 0),
(131, 41, 60, 902.00, 35, 0),
(132, 41, 60, 921.00, 36, 0),
(133, 41, 60, 943.00, 37, 0),
(134, 41, 60, 966.00, 38, 0),
(135, 41, 60, 988.00, 39, 0),
(136, 41, 60, 1011.00, 40, 0),
(137, 41, 60, 1034.00, 41, 0),
(138, 41, 60, 1056.00, 42, 0),
(139, 41, 60, 1079.00, 43, 0),
(140, 41, 60, 1102.00, 44, 0),
(141, 41, 60, 1124.00, 45, 0),
(142, 41, 60, 1147.00, 46, 0),
(143, 41, 60, 1169.00, 47, 0),
(144, 41, 60, 1192.00, 48, 0),
(145, 41, 60, 1215.00, 49, 0),
(146, 41, 60, 1237.00, 50, 0),
(147, 41, 60, 1260.00, 51, 0),
(148, 41, 60, 1283.00, 52, 0),
(149, 41, 60, 1305.00, 53, 0),
(150, 41, 60, 1328.00, 54, 0),
(151, 41, 60, 1350.00, 55, 0),
(152, 41, 60, 1373.00, 56, 0),
(153, 41, 60, 1396.00, 57, 0),
(154, 41, 60, 1418.00, 58, 0),
(155, 41, 60, 1441.00, 59, 0),
(156, 41, 60, 1463.00, 60, 0),
(157, 41, 60, 1477.00, 61, 0),
(158, 41, 60, 1500.00, 62, 0),
(159, 41, 60, 1522.00, 63, 0),
(160, 41, 60, 1545.00, 64, 0),
(161, 41, 60, 1567.00, 65, 0),
(162, 41, 60, 1590.00, 66, 0),
(163, 41, 60, 1612.00, 67, 0),
(164, 41, 60, 1635.00, 68, 0),
(165, 41, 60, 1657.00, 69, 0),
(166, 41, 60, 1680.00, 70, 0),
(167, 41, 60, 1703.00, 71, 0),
(168, 41, 60, 1725.00, 72, 0),
(169, 41, 60, 1748.00, 73, 0),
(170, 41, 60, 1770.00, 74, 0),
(171, 41, 60, 1793.00, 75, 0),
(172, 41, 60, 1815.00, 76, 0),
(173, 41, 60, 1838.00, 77, 0),
(174, 41, 60, 1860.00, 78, 0),
(175, 41, 60, 1883.00, 79, 0),
(176, 41, 60, 1905.00, 80, 0),
(177, 41, 60, 1928.00, 81, 0),
(178, 41, 60, 1951.00, 82, 0),
(179, 41, 60, 1973.00, 83, 0),
(180, 41, 60, 1996.00, 84, 0),
(181, 41, 60, 2018.00, 85, 0),
(182, 41, 60, 2041.00, 86, 0),
(183, 41, 60, 2063.00, 87, 0),
(184, 41, 60, 2086.00, 88, 0),
(185, 41, 60, 2108.00, 89, 0),
(186, 41, 60, 2131.00, 90, 0),
(187, 61, 70, 264.00, 2, 0),
(188, 61, 70, 329.00, 3, 0),
(189, 61, 70, 394.00, 4, 0),
(190, 61, 70, 460.00, 5, 0),
(191, 61, 70, 525.00, 6, 0),
(192, 61, 70, 536.00, 7, 0),
(193, 61, 70, 546.00, 8, 0),
(194, 61, 70, 564.00, 9, 0),
(195, 61, 70, 612.00, 10, 0),
(196, 61, 70, 661.00, 11, 0),
(197, 61, 70, 710.00, 12, 0),
(198, 61, 70, 759.00, 13, 0),
(199, 61, 70, 766.00, 14, 0),
(200, 61, 70, 774.00, 15, 0),
(201, 61, 70, 782.00, 16, 0),
(202, 61, 70, 790.00, 17, 0),
(203, 61, 70, 798.00, 18, 0),
(204, 61, 70, 806.00, 19, 0),
(205, 61, 70, 824.00, 20, 0),
(206, 61, 70, 859.00, 21, 0),
(207, 61, 70, 863.00, 22, 0),
(208, 61, 70, 897.00, 23, 0),
(209, 61, 70, 931.00, 24, 0),
(210, 61, 70, 965.00, 25, 0),
(211, 61, 70, 1000.00, 26, 0),
(212, 61, 70, 1034.00, 27, 0),
(213, 61, 70, 1046.00, 28, 0),
(214, 61, 70, 1059.00, 29, 0),
(215, 61, 70, 1092.00, 30, 0),
(216, 61, 70, 1125.00, 31, 0),
(217, 61, 70, 1157.00, 32, 0),
(218, 61, 70, 1190.00, 33, 0),
(219, 61, 70, 1223.00, 34, 0),
(220, 61, 70, 1255.00, 35, 0),
(221, 61, 70, 1284.00, 36, 0),
(222, 61, 70, 1316.00, 37, 0),
(223, 61, 70, 1349.00, 38, 0),
(224, 61, 70, 1382.00, 39, 0),
(225, 61, 70, 1415.00, 40, 0),
(226, 61, 70, 1447.00, 41, 0),
(227, 61, 70, 1480.00, 42, 0),
(228, 61, 70, 1513.00, 43, 0),
(229, 61, 70, 1545.00, 44, 0),
(230, 61, 70, 1578.00, 45, 0),
(231, 61, 70, 1611.00, 46, 0),
(232, 61, 70, 1644.00, 47, 0),
(233, 61, 70, 1676.00, 48, 0),
(234, 61, 70, 1709.00, 49, 0),
(235, 61, 70, 1742.00, 50, 0),
(236, 61, 70, 1774.00, 51, 0),
(237, 61, 70, 1807.00, 52, 0),
(238, 61, 70, 1840.00, 53, 0),
(239, 61, 70, 1873.00, 54, 0),
(240, 61, 70, 1905.00, 55, 0),
(241, 61, 70, 1938.00, 56, 0),
(242, 61, 70, 1971.00, 57, 0),
(243, 61, 70, 2003.00, 58, 0),
(244, 61, 70, 2036.00, 59, 0),
(245, 61, 70, 2069.00, 60, 0),
(246, 61, 70, 2090.00, 61, 0),
(247, 61, 70, 2123.00, 62, 0),
(248, 61, 70, 2156.00, 63, 0),
(249, 61, 70, 2188.00, 64, 0),
(250, 61, 70, 2221.00, 65, 0),
(251, 61, 70, 2253.00, 66, 0),
(252, 61, 70, 2286.00, 67, 0),
(253, 61, 70, 2319.00, 68, 0),
(254, 61, 70, 2351.00, 69, 0),
(255, 61, 70, 2384.00, 70, 0),
(256, 61, 70, 2416.00, 71, 0),
(257, 61, 70, 2449.00, 72, 0),
(258, 61, 70, 2482.00, 73, 0),
(259, 61, 70, 2514.00, 74, 0),
(260, 61, 70, 2547.00, 75, 0),
(261, 61, 70, 2579.00, 76, 0),
(262, 61, 70, 2612.00, 77, 0),
(263, 61, 70, 2645.00, 78, 0),
(264, 61, 70, 2677.00, 79, 0),
(265, 61, 70, 2710.00, 80, 0),
(266, 61, 70, 2742.00, 81, 0),
(267, 61, 70, 2775.00, 82, 0),
(268, 61, 70, 2808.00, 83, 0),
(269, 61, 70, 2840.00, 84, 0),
(270, 61, 70, 2873.00, 85, 0),
(271, 61, 70, 2905.00, 86, 0),
(272, 61, 70, 2938.00, 87, 0),
(273, 61, 70, 2971.00, 88, 0),
(274, 61, 70, 3003.00, 89, 0),
(275, 61, 70, 3036.00, 90, 0),
(276, 71, 100, 416.00, 2, 0),
(277, 71, 100, 558.00, 3, 0),
(278, 71, 100, 700.00, 4, 0),
(279, 71, 100, 842.00, 5, 0),
(280, 71, 100, 983.00, 6, 0),
(281, 71, 100, 1003.00, 7, 0),
(282, 71, 100, 1023.00, 8, 0),
(283, 71, 100, 1077.00, 9, 0),
(284, 71, 100, 1183.00, 10, 0),
(285, 71, 100, 1288.00, 11, 0),
(286, 71, 100, 1394.00, 12, 0),
(287, 71, 100, 1500.00, 13, 0),
(288, 71, 100, 1515.00, 14, 0),
(289, 71, 100, 1530.00, 15, 0),
(290, 71, 100, 1546.00, 16, 0),
(291, 71, 100, 1561.00, 17, 0),
(292, 71, 100, 1577.00, 18, 0),
(293, 71, 100, 1592.00, 19, 0),
(294, 71, 100, 1651.00, 20, 0),
(295, 71, 100, 1728.00, 21, 0),
(296, 71, 100, 1737.00, 22, 0),
(297, 71, 100, 1811.00, 23, 0),
(298, 71, 100, 1884.00, 24, 0),
(299, 71, 100, 1958.00, 25, 0),
(300, 71, 100, 2032.00, 26, 0),
(301, 71, 100, 2106.00, 27, 0),
(302, 71, 100, 2136.00, 28, 0),
(303, 71, 100, 2166.00, 29, 0),
(304, 71, 100, 2237.00, 30, 0),
(305, 71, 100, 2308.00, 31, 0),
(306, 71, 100, 2379.00, 32, 0),
(307, 71, 100, 2450.00, 33, 0),
(308, 71, 100, 2521.00, 34, 0),
(309, 71, 100, 2592.00, 35, 0),
(310, 71, 100, 2658.00, 36, 0),
(311, 71, 100, 2729.00, 37, 0),
(312, 71, 100, 2800.00, 38, 0),
(313, 71, 100, 2871.00, 39, 0),
(314, 71, 100, 2942.00, 40, 0),
(315, 71, 100, 3013.00, 41, 0),
(316, 71, 100, 3084.00, 42, 0),
(317, 71, 100, 3155.00, 43, 0),
(318, 71, 100, 3225.00, 44, 0),
(319, 71, 100, 3296.00, 45, 0),
(320, 71, 100, 3367.00, 46, 0),
(321, 71, 100, 3438.00, 47, 0),
(322, 71, 100, 3509.00, 48, 0),
(323, 71, 100, 3580.00, 49, 0),
(324, 71, 100, 3651.00, 50, 0),
(325, 71, 100, 3722.00, 51, 0),
(326, 71, 100, 3793.00, 52, 0),
(327, 71, 100, 3863.00, 53, 0),
(328, 71, 100, 3934.00, 54, 0),
(329, 71, 100, 4005.00, 55, 0),
(330, 71, 100, 4076.00, 56, 0),
(331, 71, 100, 4147.00, 57, 0),
(332, 71, 100, 4218.00, 58, 0),
(333, 71, 100, 4289.00, 59, 0),
(334, 71, 100, 4360.00, 60, 0),
(335, 71, 100, 4412.00, 61, 0),
(336, 71, 100, 4482.00, 62, 0),
(337, 71, 100, 4553.00, 63, 0),
(338, 71, 100, 4624.00, 64, 0),
(339, 71, 100, 4694.00, 65, 0),
(340, 71, 100, 4765.00, 66, 0),
(341, 71, 100, 4836.00, 67, 0),
(342, 71, 100, 4906.00, 68, 0),
(343, 71, 100, 4977.00, 69, 0),
(344, 71, 100, 5048.00, 70, 0),
(345, 71, 100, 5118.00, 71, 0),
(346, 71, 100, 5189.00, 72, 0),
(347, 71, 100, 5260.00, 73, 0),
(348, 71, 100, 5330.00, 74, 0),
(349, 71, 100, 5401.00, 75, 0),
(350, 71, 100, 5472.00, 76, 0),
(351, 71, 100, 5542.00, 77, 0),
(352, 71, 100, 5613.00, 78, 0),
(353, 71, 100, 5683.00, 79, 0),
(354, 71, 100, 5754.00, 80, 0),
(355, 71, 100, 5825.00, 81, 0),
(356, 71, 100, 5895.00, 82, 0),
(357, 71, 100, 5966.00, 83, 0),
(358, 71, 100, 6037.00, 84, 0),
(359, 71, 100, 6107.00, 85, 0),
(360, 71, 100, 6178.00, 86, 0),
(361, 71, 100, 6249.00, 87, 0),
(362, 71, 100, 6319.00, 88, 0),
(363, 71, 100, 6390.00, 89, 0),
(364, 71, 100, 6461.00, 90, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_keywords`
--

CREATE TABLE IF NOT EXISTS `tbl_keywords` (
  `key_id` int(11) NOT NULL AUTO_INCREMENT,
  `key_page_id` int(11) NOT NULL,
  `key_title` varchar(255) NOT NULL,
  `key_keywords` varchar(255) NOT NULL,
  `key_description` varchar(255) NOT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_leads`
--

CREATE TABLE IF NOT EXISTS `tbl_leads` (
  `led_id` int(11) NOT NULL AUTO_INCREMENT,
  `led_lead_no` varchar(15) NOT NULL,
  `led_invoice_no` varchar(100) NOT NULL,
  `led_visatype_id` int(11) NOT NULL,
  `led_user_id` int(11) NOT NULL,
  `led_travel_date_from` int(11) NOT NULL,
  `led_travel_date_to` int(11) NOT NULL,
  `led_airline_otb_id` int(11) NOT NULL,
  `led_travel_from` varchar(255) NOT NULL,
  `led_otb` tinyint(4) NOT NULL DEFAULT '0',
  `led_express_visa` tinyint(4) NOT NULL DEFAULT '0',
  `led_apply_date` int(11) NOT NULL,
  `led_visa_amount` decimal(10,2) NOT NULL,
  `led_otb_charge` decimal(10,2) NOT NULL,
  `led_visa_cost` decimal(10,2) NOT NULL,
  `led_otb_cost` decimal(10,2) NOT NULL,
  `led_express_add_on` decimal(10,2) NOT NULL,
  `led_payment_status` tinyint(2) NOT NULL DEFAULT '0',
  `led_totalamount` int(11) NOT NULL,
  `led_insert_date` int(11) NOT NULL,
  `led_status` tinyint(2) NOT NULL DEFAULT '0',
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  `led_no_of_applicant` int(11) NOT NULL,
  `led_no_of_child` int(11) NOT NULL,
  PRIMARY KEY (`led_id`),
  KEY `led_user_id` (`led_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=300 ;

--
-- Dumping data for table `tbl_leads`
--

INSERT INTO `tbl_leads` (`led_id`, `led_lead_no`, `led_invoice_no`, `led_visatype_id`, `led_user_id`, `led_travel_date_from`, `led_travel_date_to`, `led_airline_otb_id`, `led_travel_from`, `led_otb`, `led_express_visa`, `led_apply_date`, `led_visa_amount`, `led_otb_charge`, `led_visa_cost`, `led_otb_cost`, `led_express_add_on`, `led_payment_status`, `led_totalamount`, `led_insert_date`, `led_status`, `is_delete`, `led_no_of_applicant`, `led_no_of_child`) VALUES
(1, 'YDV17104353', '', 1, 41, 1507833000, 1507919400, 9, '', 1, 0, 1507487400, 5700.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1507487400, 3, 1, 1, 0),
(2, 'YDV17108148', '', 1, 1, 1509042600, 1509129000, 10, '', 1, 0, 1507487400, 5700.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1507487400, 3, 1, 1, 0),
(3, 'YDV17104441', '', 1, 1, 1509042600, 1509129000, 5, '', 1, 0, 1507487400, 5700.00, 300.00, 5250.00, 0.00, 0.00, 1, 0, 1507487400, 5, 1, 1, 0),
(4, 'YDV17102792', '', 2, 1, 1509042600, 1509129000, 0, '', 1, 0, 1507487400, 6299.00, 0.00, 5150.00, 0.00, 0.00, 1, 6299, 1507487400, 5, 1, 1, 0),
(5, 'YDV17107855', '', 1, 41, 1507660200, 1507746600, 13, '', 1, 0, 1507487400, 5700.00, 600.00, 5250.00, 0.00, 0.00, 0, 6300, 1507487400, 1, 1, 1, 0),
(6, 'YDV17103486', '', 1, 42, 1507660200, 1507746600, 0, '', 0, 0, 1507573800, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 57, 1507573800, 1, 1, 2, 0),
(7, 'YDV17104257', '', 1, 43, 1508783400, 1508869800, 0, '', 1, 0, 1507573800, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1507573800, 3, 1, 1, 0),
(8, 'YDV17104243', '', 1, 1, 1507660200, 1508437800, 0, '', 1, 0, 1507573800, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1507573800, 3, 1, 1, 0),
(9, 'YDV17107790', '', 1, 44, 1507660200, 1508437800, 2, '', 0, 0, 1507573800, 5700.00, 500.00, 5250.00, 0.00, 0.00, 0, 0, 1507573800, 3, 1, 2, 0),
(10, 'YDV17108081', '', 2, 45, 1508956200, 1509042600, 10, '', 1, 0, 1507660200, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1507660200, 3, 1, 1, 0),
(11, 'YDV17109292', '', 2, 45, 1508956200, 1509042600, 10, '', 1, 0, 1507660200, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1507660200, 3, 1, 1, 0),
(12, 'YDV17104282', '', 2, 45, 1508956200, 1509042600, 10, '', 1, 0, 1507660200, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1507660200, 3, 1, 1, 0),
(13, 'YDV17106481', '', 2, 45, 1508956200, 1509042600, 10, '', 1, 0, 1507660200, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1507660200, 3, 1, 1, 0),
(14, 'YDV17104651', '', 2, 45, 1508956200, 1509042600, 10, '', 1, 0, 1507660200, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1507660200, 3, 1, 1, 0),
(15, 'YDV17103248', '', 2, 45, 1508956200, 1509042600, 10, '', 1, 0, 1507660200, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1507660200, 3, 1, 1, 0),
(16, 'YDV17104823', '', 2, 45, 1508956200, 1509042600, 10, '', 1, 0, 1507660200, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1507660200, 3, 1, 1, 0),
(17, 'YDV17105529', '', 2, 45, 1508956200, 1509042600, 10, '', 1, 0, 1507660200, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1507660200, 3, 1, 1, 0),
(18, 'YDV17101471', '', 2, 45, 1508956200, 1509042600, 10, '', 1, 0, 1507660200, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1507660200, 3, 1, 1, 0),
(19, 'YDV17101562', '', 2, 45, 1508956200, 1509042600, 10, '', 1, 0, 1507660200, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1507660200, 3, 1, 1, 0),
(20, 'YDV17108309', '', 2, 45, 1508956200, 1509042600, 10, '', 1, 0, 1507660200, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1507660200, 3, 1, 1, 0),
(21, 'YDV17104528', '', 2, 45, 1508956200, 1509042600, 10, '', 1, 0, 1507660200, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 6899, 1507660200, 1, 1, 1, 0),
(22, 'YDV17103113', '', 1, 46, 1507833000, 1507919400, 0, '', 1, 0, 1507746600, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 5700, 1507746600, 1, 1, 2, 0),
(23, 'YDV17102938', '', 1, 41, -19800, -19800, 0, '', 1, 0, 1507746600, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 5700, 1507746600, 1, 1, 4, 0),
(24, 'YDV17104885', '', 1, 41, -19800, -19800, 0, '', 1, 0, 1507746600, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 5700, 1507746600, 1, 1, 1, 0),
(25, 'YDV17106767', '', 1, 47, 1509388200, 1510252200, 5, '', 1, 0, 1507746600, 5700.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1507746600, 1, 1, 2, 1),
(26, 'YDV17103849', '', 1, 47, -19800, -19800, 5, '', 1, 0, 1507746600, 5700.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1507746600, 3, 1, 1, 0),
(27, 'YDV17104317', '', 1, 43, 1508351400, 1508437800, 0, '', 1, 0, 1507746600, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 5700, 1507746600, 1, 1, 1, 0),
(28, 'YDV17104433', '', 1, 41, 0, 0, 0, '', 1, 0, 1507746600, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 5700, 1507746600, 1, 1, 1, 0),
(29, 'YDV17105139', '', 2, 41, -19800, -19800, 0, '', 1, 0, 1507746600, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 6299, 1507746600, 1, 1, 1, 0),
(30, 'YDV17104814', '', 2, 41, -19800, -19800, 0, '', 1, 0, 1507746600, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 6299, 1507746600, 1, 1, 1, 0),
(31, 'YDV17105878', '', 1, 48, 0, 0, 0, '', 1, 0, 1507833000, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 5700, 1507833000, 1, 1, 1, 0),
(32, 'YDV17102612', '', 1, 41, -19800, -19800, 0, '', 1, 0, 1507833000, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 5700, 1507833000, 1, 1, 1, 0),
(33, 'YDV17103924', '', 2, 41, -19800, -19800, 0, '', 1, 0, 1507833000, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 6299, 1507833000, 1, 1, 1, 0),
(34, 'YDV17104480', '', 1, 41, 0, 0, 0, '', 1, 0, 1507919400, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1507919400, 3, 1, 1, 0),
(35, 'YDV17103995', '', 1, 41, -19800, -19800, 0, '', 1, 0, 1507919400, 5700.00, 0.00, 5250.00, 0.00, 0.00, 1, 5700, 1507919400, 5, 1, 1, 0),
(36, 'YDV17108985', '', 1, 41, -19800, -19800, 0, '', 1, 0, 1507919400, 5700.00, 0.00, 5250.00, 0.00, 0.00, 1, 5700, 1507919400, 5, 1, 1, 0),
(37, 'YDV17105745', '', 1, 43, 1508697000, 1508783400, 10, '', 1, 0, 1508697000, 5700.00, 600.00, 5250.00, 0.00, 0.00, 0, 6300, 1508697000, 1, 1, 1, 0),
(38, 'YDV17104306', '', 1, 43, 1508697000, 1508783400, 13, '', 1, 0, 1508697000, 5700.00, 600.00, 5250.00, 0.00, 0.00, 1, 6300, 1508697000, 5, 1, 1, 0),
(39, 'YDV17106025', '', 1, 43, 1509042600, 1509129000, 10, '', 0, 0, 1508869800, 5700.00, 600.00, 5250.00, 0.00, 0.00, 0, 5700, 1508869800, 1, 1, 1, 0),
(40, 'YDV17104255', '', 1, 43, 1509042600, 1509129000, 0, '', 1, 0, 1508956200, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 5700, 1508956200, 1, 1, 1, 0),
(41, 'YDV17107161', '', 1, 41, 1509042600, 1509129000, 0, '', 1, 0, 1508956200, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 5700, 1508956200, 1, 1, 1, 0),
(42, 'YDV17103414', '', 1, 1, 1511375400, 1511461800, 0, '', 1, 0, 1508956200, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 5700, 1508956200, 1, 1, 1, 0),
(43, 'YDV17104642', '', 1, 1, 1511980200, 1512066600, 0, '', 1, 0, 1508956200, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 5700, 1508956200, 1, 1, 1, 0),
(44, 'YDV17104767', '', 1, 1, 1509388200, 1509474600, 0, '', 1, 0, 1508956200, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 5700, 1508956200, 1, 1, 1, 0),
(45, 'YDV17105169', '', 1, 43, 1509042600, 1509129000, 0, '', 1, 0, 1508956200, 5700.00, 0.00, 5250.00, 0.00, 0.00, 0, 5700, 1508956200, 1, 1, 1, 0),
(46, 'YDV17105494', '', 1, 43, 1509129000, 1509215400, 0, '', 1, 0, 1508956200, 100.00, 0.00, 5250.00, 0.00, 0.00, 0, 100, 1508956200, 1, 1, 1, 0),
(47, 'YDV17109974', '', 1, 43, 1509042600, 1509129000, 0, '', 1, 0, 1508956200, 100.00, 0.00, 5250.00, 0.00, 0.00, 0, 100, 1508956200, 1, 1, 1, 0),
(48, 'YDV17106382', '', 1, 49, 1511461800, 1511980200, 10, '', 0, 0, 1508956200, 100.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1508956200, 3, 1, 1, 0),
(49, 'YDV17105502', '', 1, 43, 1509129000, 1509215400, 0, '', 1, 0, 1509042600, 100.00, 0.00, 5250.00, 0.00, 0.00, 0, 100, 1509042600, 1, 1, 1, 0),
(50, 'YDV17108365', '', 1, 43, 1509042600, 1509215400, 0, '', 1, 0, 1509042600, 100.00, 0.00, 5250.00, 0.00, 0.00, 0, 307, 1509042600, 1, 1, 1, 0),
(51, 'YDV17101861', '', 1, 43, 1509042600, 1509215400, 0, '', 1, 0, 1509042600, 100.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1509042600, 3, 1, 1, 0),
(52, 'YDV17106800', '', 1, 43, 1509129000, 1509215400, 0, '', 1, 0, 1509042600, 100.00, 0.00, 5250.00, 0.00, 0.00, 0, 100, 1509042600, 1, 1, 1, 0),
(53, 'YDV17109327', '', 2, 50, 0, 0, 9, '', 0, 0, 1509042600, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1509042600, 1, 1, 1, 0),
(54, 'YDV17108690', '', 1, 43, 1509129000, 1509215400, 0, '', 1, 0, 1509042600, 100.00, 0.00, 5250.00, 0.00, 0.00, 0, 100, 1509042600, 1, 1, 1, 0),
(55, 'YDV17109390', '', 1, 43, 1509129000, 1509215400, 0, '', 1, 0, 1509042600, 100.00, 0.00, 5250.00, 0.00, 0.00, 0, 100, 1509042600, 1, 1, 1, 0),
(56, 'YDV17108224', '', 1, 43, 1509129000, 1509215400, 0, '', 1, 0, 1509042600, 100.00, 0.00, 5250.00, 0.00, 0.00, 0, 100, 1509042600, 1, 1, 1, 0),
(57, 'YDV17106361', '', 1, 43, 1509388200, 1509474600, 0, '', 1, 0, 1509042600, 100.00, 0.00, 5250.00, 0.00, 0.00, 0, 100, 1509042600, 1, 1, 1, 0),
(58, 'YDV17104010', '', 1, 43, 1509388200, 1509474600, 0, '', 1, 0, 1509042600, 100.00, 0.00, 5250.00, 0.00, 0.00, 0, 100, 1509042600, 1, 1, 1, 0),
(59, 'YDV17104142', '', 2, 45, 1509388200, 1509474600, 1, '', 1, 0, 1509301800, 6299.00, 500.00, 5150.00, 0.00, 0.00, 0, 6799, 1509301800, 1, 1, 1, 0),
(60, 'YDV17106462', '', 2, 45, 1509388200, 1509474600, 0, '', 1, 0, 1509301800, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 6299, 1509301800, 1, 1, 1, 0),
(61, 'YDV17106593', '', 2, 45, 1509388200, 1509474600, 0, '', 1, 0, 1509301800, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 6299, 1509301800, 1, 1, 1, 0),
(62, 'YDV17104647', '', 2, 45, 1509388200, 1509474600, 0, '', 1, 0, 1509301800, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 6299, 1509301800, 1, 1, 1, 0),
(63, 'YDV17107499', '', 2, 45, 1509388200, 1509474600, 0, '', 1, 0, 1509301800, 6299.00, 0.00, 5150.00, 0.00, 0.00, 1, 6299, 1509301800, 5, 1, 1, 0),
(64, 'YDV17106055', '', 2, 45, 1509388200, 1509474600, 0, '', 1, 0, 1509301800, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 6299, 1509301800, 1, 1, 1, 0),
(65, 'YDV17109540', '', 2, 45, 1509388200, 1509474600, 0, '', 1, 0, 1509301800, 6299.00, 0.00, 5150.00, 0.00, 0.00, 1, 6299, 1509301800, 5, 1, 1, 0),
(66, 'YDV17102596', '', 2, 45, 1509388200, 1509474600, 0, '', 1, 0, 1509301800, 6299.00, 0.00, 5150.00, 0.00, 0.00, 1, 6299, 1509301800, 5, 1, 1, 0),
(67, 'YDV17109861', '', 2, 45, 1509388200, 1509474600, 0, '', 1, 0, 1509301800, 6299.00, 0.00, 5150.00, 0.00, 0.00, 1, 6299, 1509301800, 5, 1, 1, 0),
(68, 'YDV17104751', '', 2, 45, 1511980200, 1512066600, 0, '', 1, 0, 1509301800, 6299.00, 0.00, 5150.00, 0.00, 0.00, 1, 6299, 1509301800, 5, 1, 1, 0),
(69, 'YDV17103572', '', 2, 45, 1511980200, 1512066600, 0, '', 1, 0, 1509301800, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 6299, 1509301800, 1, 1, 1, 0),
(70, 'YDV17107115', '', 2, 45, 1511980200, 1512066600, 0, '', 1, 0, 1509301800, 6299.00, 0.00, 5150.00, 0.00, 0.00, 1, 6299, 1509301800, 5, 1, 1, 0),
(71, 'YDV17103271', '', 1, 41, 1509301800, 1509388200, 0, '', 1, 0, 1509301800, 100.00, 0.00, 5250.00, 0.00, 0.00, 1, 100, 1509301800, 5, 1, 1, 0),
(72, 'YDV17108149', '', 1, 43, 1509388200, 1509474600, 0, '', 1, 0, 1509388200, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 5699, 1509388200, 1, 1, 1, 0),
(73, 'YDV17105526', '', 1, 43, 1509388200, 1509474600, 0, '', 1, 0, 1509388200, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1509388200, 3, 1, 1, 0),
(74, 'YDV17109960', '', 1, 43, 1510165800, 1510252200, 0, '', 1, 0, 1509388200, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 5699, 1509388200, 1, 1, 1, 0),
(75, 'YDV17103853', '', 1, 43, 1509388200, 1510165800, 0, '', 1, 0, 1509388200, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1509388200, 3, 1, 1, 0),
(76, 'YDV17106847', '', 1, 51, 1510770600, 1510857000, 0, '', 1, 0, 1509388200, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1509388200, 3, 1, 1, 0),
(77, 'YDV17111891', '', 1, 52, 1509906600, 1510165800, 12, '', 1, 0, 1509474600, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1509474600, 3, 0, 1, 0),
(78, 'YDV17112671', '', 1, 50, 1510770600, 1510857000, 0, '', 1, 0, 1509474600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1509474600, 1, 1, 1, 0),
(79, 'YDV17112958', '', 1, 52, 1509906600, 1510165800, 12, '', 1, 0, 1509474600, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1509474600, 3, 0, 1, 0),
(80, 'YDV17119023', '', 1, 50, 1510770600, 1510857000, 0, '', 1, 0, 1509474600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1509474600, 3, 1, 1, 0),
(81, 'YDV17113910', '', 3, 53, 1510511400, 1515868200, 10, '', 1, 0, 1509474600, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1509474600, 3, 0, 1, 0),
(82, 'YDV17118151', '', 1, 54, 1518287400, 1518892200, 0, '', 1, 0, 1509474600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1509474600, 3, 0, 4, 0),
(83, 'YDV17114763', '', 1, 55, 1510165800, 1510252200, 5, '', 1, 0, 1509474600, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1509474600, 3, 1, 1, 0),
(84, 'YDV17112538', '', 1, 55, 1509647400, 1509733800, 0, '', 1, 0, 1509474600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1509474600, 3, 1, 1, 0),
(85, 'YDV17114206', '', 1, 55, 1509647400, 1509733800, 0, '', 1, 0, 1509474600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1509474600, 3, 1, 1, 0),
(86, 'YDV17118286', '', 1, 41, 1510684200, 1510770600, 13, '', 1, 0, 1509474600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1509474600, 3, 1, 1, 0),
(87, 'YDV17117446', '', 5, 45, 1511980200, 1512066600, 10, '', 1, 0, 1509647400, 18500.00, 0.00, 16500.00, 0.00, 0.00, 0, 0, 1509647400, 3, 1, 1, 0),
(88, 'YDV17119538', '', 5, 45, 1511980200, 1512066600, 10, '', 1, 0, 1509647400, 18500.00, 0.00, 16500.00, 0.00, 0.00, 0, 0, 1509647400, 3, 1, 1, 0),
(89, 'YDV17116099', '', 5, 45, 1511980200, 1512066600, 10, '', 1, 1, 1509647400, 18500.00, 0.00, 16500.00, 0.00, 0.00, 0, 0, 1509647400, 3, 1, 1, 0),
(90, 'YDV17119912', '', 2, 45, 1511980200, 1512066600, 0, '', 1, 1, 1509647400, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 8829, 1509647400, 1, 1, 1, 0),
(91, 'YDV17118229', '', 2, 45, 1511980200, 1512066600, 0, '', 1, 1, 1509647400, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 8829, 1509647400, 1, 1, 1, 0),
(92, 'YDV17117143', '', 1, 45, 1511980200, 1512066600, 0, '', 1, 1, 1509647400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 8322, 1509647400, 1, 1, 1, 0),
(93, 'YDV17118405', '', 1, 41, 1510770600, 1510857000, 9, '', 1, 1, 1509647400, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 8931, 1509647400, 1, 1, 1, 0),
(94, 'YDV17111979', '', 1, 43, 1510165800, 1510425000, 10, '', 1, 1, 1509647400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 8566, 1509647400, 1, 1, 1, 0),
(95, 'YDV17113076', '', 1, 45, 1511980200, 1512066600, 10, '', 1, 0, 1509647400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 5784, 1509647400, 1, 1, 1, 0),
(96, 'YDV17116780', '', 1, 45, 1511893800, 1511980200, 1, '', 1, 0, 1509647400, 5699.00, 500.00, 5250.00, 0.00, 0.00, 0, 6292, 1509647400, 1, 1, 1, 0),
(97, 'YDV17111369', '', 2, 56, 1511289000, 1511375400, 8, '', 1, 1, 1509647400, 6299.00, 300.00, 5150.00, 0.00, 0.00, 0, 0, 1509647400, 3, 1, 1, 0),
(98, 'YDV17119230', '', 1, 57, 1511980200, 1512066600, 8, '', 1, 0, 1509647400, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1509647400, 3, 1, 1, 0),
(99, 'YDV17119991', '', 1, 58, 1511980200, 1512153000, 0, '', 1, 0, 1509733800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1509733800, 3, 0, 1, 0),
(100, 'YDV17116302', '', 2, 55, 1511375400, 1511461800, 13, '', 1, 0, 1509820200, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 6393, 1509820200, 1, 1, 1, 0),
(101, 'YDV17119132', '', 3, 59, 1514140200, 1521484200, 0, '', 1, 0, 1509820200, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1509820200, 3, 1, 1, 0),
(102, 'YDV17119696', '', 1, 60, 1519756200, 1520274600, 12, '', 0, 0, 1509906600, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1509906600, 3, 0, 2, 0),
(103, 'YDV17119923', '', 6, 61, 1513276200, 0, 10, '', 1, 1, 1509906600, 37000.00, 0.00, 32000.00, 0.00, 0.00, 0, 0, 1509906600, 3, 0, 1, 0),
(104, 'YDV17111602', '', 1, 62, 1513708200, 1513794600, 12, '', 1, 0, 1509906600, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1509906600, 3, 0, 4, 1),
(105, 'YDV17114346', '', 2, 63, 1509993000, 1510079400, 10, '', 1, 1, 1509906600, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1509906600, 3, 1, 1, 0),
(106, 'YDV17119949', '', 2, 64, 1510252200, 1512412200, 2, '', 0, 1, 1509906600, 6299.00, 500.00, 5150.00, 0.00, 0.00, 0, 0, 1509906600, 3, 0, 1, 0),
(107, 'YDV17118317', '', 2, 64, 1510252200, 1512412200, 0, '', 0, 1, 1509906600, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1509906600, 3, 0, 1, 0),
(108, 'YDV17112059', '', 2, 64, 1510252200, 1510338600, 0, '', 1, 1, 1509906600, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 17659, 1509906600, 1, 1, 1, 0),
(109, 'YDV17118084', '', 1, 57, 1511980200, 1512066600, 9, '', 1, 0, 1509906600, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1509906600, 3, 1, 1, 0),
(110, 'YDV17113186', '', 1, 45, 1511980200, 1512066600, 0, '', 1, 0, 1509906600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1509906600, 3, 1, 1, 0),
(111, 'YDV17119326', '', 2, 45, 1511893800, 1511980200, 12, '', 1, 0, 1509906600, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 7002, 1509906600, 1, 1, 1, 0),
(112, 'YDV17116244', '', 1, 41, 1510857000, 1511721000, 0, '', 1, 0, 1509906600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 6246, 1509906600, 1, 1, 1, 0),
(113, 'YDV17116374', '', 1, 41, -19800, -19800, 0, '', 1, 0, 1509906600, 100.00, 0.00, 5250.00, 0.00, 0.00, 1, 102, 1509906600, 5, 1, 1, 0),
(114, 'YDV17114432', '', 2, 65, 1510943400, 1511375400, 0, '', 0, 0, 1509906600, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1509906600, 3, 0, 2, 0),
(115, 'YDV17115117', '', 2, 65, 1510943400, 1511375400, 0, '', 0, 0, 1509906600, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1509906600, 3, 0, 2, 0),
(116, 'YDV17113440', '', 3, 66, 1511116200, 1518892200, 9, '', 1, 0, 1509993000, 20000.00, 600.00, 18000.00, 0.00, 0.00, 0, 0, 1509993000, 3, 1, 3, 0),
(117, 'YDV17112772', '', 1, 67, 0, 0, 11, '', 1, 0, 1509993000, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1509993000, 3, 0, 2, 1),
(118, 'YDV17117383', '', 2, 68, 1510165800, 1510252200, 0, '', 1, 0, 1509993000, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1509993000, 3, 1, 1, 0),
(119, 'YDV17112916', '', 1, 67, 1512325800, 1512757800, 11, '', 1, 0, 1509993000, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1509993000, 3, 0, 2, 1),
(120, 'YDV17111608', '', 1, 69, 1513708200, 1514831400, 0, '', 1, 0, 1509993000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1509993000, 3, 0, 2, 1),
(121, 'YDV17118999', '', 3, 45, 1511980200, 1512066600, 0, '', 1, 0, 1510079400, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1510079400, 3, 1, 1, 0),
(122, 'YDV17117950', '', 3, 45, 1511980200, 1512066600, 0, '', 1, 0, 1510079400, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1510079400, 3, 1, 1, 0),
(123, 'YDV17113758', '', 3, 45, 1511980200, 1512066600, 0, '', 1, 0, 1510079400, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1510079400, 3, 1, 1, 0),
(124, 'YDV17111431', '', 3, 45, 1511980200, 1512066600, 0, '', 1, 0, 1510079400, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1510079400, 3, 1, 1, 0),
(125, 'YDV17113027', '', 1, 70, 1513103400, 1514140200, 7, '', 1, 0, 1510165800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1510165800, 3, 0, 1, 0),
(126, 'YDV17113189', '', 1, 71, 1513967400, 1514831400, 6, '', 1, 0, 1510165800, 5699.00, 500.00, 5250.00, 0.00, 0.00, 0, 0, 1510165800, 3, 0, 2, 0),
(127, 'YDV17119963', '', 1, 72, 1511893800, 1512844200, 10, '', 1, 0, 1510165800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1510165800, 3, 0, 1, 0),
(128, 'YDV17116140', '', 5, 73, 1510770600, 1510857000, 10, '', 1, 0, 1510165800, 18500.00, 0.00, 16500.00, 0.00, 0.00, 0, 0, 1510165800, 3, 1, 1, 0),
(129, 'YDV17111872', '', 1, 74, 1514140200, 1514917800, 7, '', 0, 0, 1510165800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1510165800, 3, 0, 2, 1),
(130, 'YDV17117630', '', 2, 75, 1511202600, 1513017000, 12, '', 1, 0, 1510252200, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1510252200, 3, 0, 1, 0),
(131, 'YDV17116427', '', 2, 76, 1510684200, 1510770600, 10, '', 1, 0, 1510252200, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1510252200, 3, 0, 1, 0),
(132, 'YDV17115840', '', 1, 77, 1514831400, 1514917800, 2, '', 1, 0, 1510252200, 5699.00, 500.00, 5250.00, 0.00, 0.00, 0, 0, 1510252200, 3, 0, 1, 0),
(133, 'YDV17116988', '', 1, 78, 1512153000, 1512412200, 0, '', 1, 0, 1510252200, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1510252200, 3, 0, 2, 1),
(134, 'YDV17115879', '', 5, 79, 1511375400, 1511893800, 9, '', 1, 1, 1510338600, 18500.00, 600.00, 16500.00, 0.00, 0.00, 0, 0, 1510338600, 3, 0, 1, 1),
(135, 'YDV17115190', '', 2, 80, 1511116200, 1513708200, 0, '', 1, 0, 1510338600, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1510338600, 3, 0, 2, 0),
(136, 'YDV17114171', '', 2, 81, 1517423400, 1519842600, 10, '', 1, 0, 1510338600, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1510338600, 3, 0, 1, 0),
(137, 'YDV17113279', '', 1, 41, 1510943400, 1511029800, 0, '', 1, 0, 1510338600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 5784, 1510338600, 1, 1, 1, 0),
(138, 'YDV17113089', '', 2, 82, 1510857000, 1513189800, 0, '', 0, 0, 1510338600, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1510338600, 3, 0, 1, 0),
(139, 'YDV17117633', '', 1, 83, 1516818600, 1517337000, 0, '', 1, 0, 1510338600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1510338600, 3, 0, 2, 0),
(140, 'YDV17113684', '', 1, 84, 1516818600, 1517337000, 0, '', 1, 0, 1510338600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1510338600, 3, 0, 2, 0),
(141, 'YDV17119918', '', 1, 71, 1513967400, 1514831400, 6, '', 1, 0, 1510425000, 5699.00, 500.00, 5250.00, 0.00, 0.00, 1, 12584, 1510425000, 5, 0, 2, 0),
(142, 'YDV17116751', '', 1, 62, 1513708200, 1514745000, 0, '', 1, 0, 1510425000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 1, 36054, 1510425000, 5, 0, 4, 1),
(143, 'YDV17112507', '', 2, 85, 1512066600, 1512844200, 0, '', 1, 0, 1510511400, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1510511400, 3, 0, 1, 1),
(144, 'YDV17117004', '', 1, 86, 1512498600, 1513017000, 12, '', 1, 1, 1510511400, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1510511400, 3, 0, 2, 0),
(145, 'YDV17111544', '', 6, 87, 1510511400, 1510770600, 10, '', 1, 1, 1510511400, 37000.00, 0.00, 32000.00, 0.00, 0.00, 0, 0, 1510511400, 3, 1, 1, 0),
(146, 'YDV17111673', '', 2, 88, 0, 0, 0, '', 1, 0, 1510511400, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1510511400, 3, 0, 1, 0),
(147, 'YDV17112990', '', 1, 89, 1511634600, 1512153000, 9, '', 1, 0, 1510511400, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1510511400, 3, 1, 1, 1),
(148, 'YDV17112472', '', 1, 90, 1512153000, 1512412200, 13, '', 1, 0, 1510511400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1510511400, 3, 0, 1, 0),
(149, 'YDV17117293', '', 1, 91, 1515177000, 1516386600, 12, '', 1, 0, 1510511400, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1510511400, 3, 0, 1, 0),
(150, 'YDV17116462', '', 3, 92, 1512066600, 1519065000, 10, '', 1, 0, 1510511400, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1510511400, 3, 1, 1, 0),
(151, 'YDV17111669', '', 1, 93, 1512066600, 1512498600, 0, '', 1, 0, 1510597800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1510597800, 3, 0, 3, 0),
(152, 'YDV17113568', '', 1, 94, 1512066600, 1512153000, 0, '', 1, 0, 1510597800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1510597800, 3, 0, 2, 1),
(153, 'YDV17114628', '', 1, 95, 1514313000, 1514831400, 2, '', 0, 0, 1510597800, 5699.00, 500.00, 5250.00, 0.00, 0.00, 0, 0, 1510597800, 3, 0, 2, 0),
(154, 'YDV17117350', '', 1, 96, 1517682600, 1518201000, 11, '', 1, 0, 1510597800, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1510597800, 3, 0, 2, 0),
(155, 'YDV17118008', '', 3, 97, 1537381800, 1543948200, 10, '', 1, 0, 1510597800, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1510597800, 3, 1, 1, 0),
(156, 'YDV17116353', '', 2, 98, 1511375400, 1511461800, 10, '', 1, 0, 1510684200, 6299.00, 300.00, 5150.00, 0.00, 0.00, 0, 0, 1510684200, 3, 1, 1, 0),
(157, 'YDV17118002', '', 1, 99, 1512066600, 1512498600, 12, '', 1, 0, 1510684200, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1510684200, 3, 1, 1, 0),
(158, 'YDV17119901', '', 1, 99, 1512066600, 1512498600, 12, '', 1, 0, 1510684200, 5699.00, 600.00, 5250.00, 0.00, 0.00, 1, 6393, 1510684200, 5, 0, 1, 0),
(159, 'YDV17116337', '', 2, 100, 1512066600, 1514572200, 12, '', 0, 0, 1510684200, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1510684200, 3, 0, 1, 0),
(160, 'YDV17117967', '', 4, 41, 0, 0, 0, '', 1, 0, 1510770600, 4799.00, 0.00, 4200.00, 0.00, 0.00, 0, 0, 1510770600, 3, 1, 5, 2),
(161, 'YDV17117124', '', 6, 101, 0, 0, 0, '', 1, 0, 1510770600, 37000.00, 0.00, 32000.00, 0.00, 0.00, 0, 0, 1510770600, 3, 0, 1, 0),
(162, 'YDV17117104', '', 4, 41, -19800, -19800, 0, '', 1, 0, 1510770600, 4799.00, 0.00, 4200.00, 0.00, 0.00, 0, 0, 1510770600, 3, 1, 5, 2),
(163, 'YDV17117647', '', 3, 102, 1513103400, 1520620200, 0, '', 0, 0, 1510770600, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1510770600, 3, 0, 1, 0),
(164, 'YDV17116353', '', 2, 103, 1512066600, 0, 7, '', 1, 0, 1510770600, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1510770600, 3, 0, 1, 0),
(165, 'YDV17117225', '', 6, 104, 1511289000, 0, 0, '', 1, 0, 1510770600, 37000.00, 0.00, 32000.00, 0.00, 0.00, 0, 0, 1510770600, 3, 0, 1, 0),
(166, 'YDV17119161', '', 6, 101, 0, 0, 0, '', 1, 0, 1510857000, 37000.00, 0.00, 32000.00, 0.00, 0.00, 0, 0, 1510857000, 3, 0, 1, 0),
(167, 'YDV17114887', '', 1, 105, 1511461800, 1511807400, 0, '', 1, 0, 1510857000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1510857000, 3, 0, 2, 2),
(168, 'YDV17116701', '', 1, 106, 1511202600, 1511289000, 0, '', 1, 1, 1510857000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 8322, 1510857000, 1, 0, 1, 0),
(169, 'YDV17119651', '', 1, 107, 1513276200, 1514399400, 5, '', 1, 0, 1510857000, 5699.00, 300.00, 5250.00, 0.00, 0.00, 1, 6572, 1510857000, 5, 0, 1, 0),
(170, 'YDV17119653', '', 2, 108, 1511980200, 1514485800, 10, '', 1, 0, 1510857000, 6299.00, 300.00, 5150.00, 0.00, 0.00, 0, 0, 1510857000, 3, 0, 1, 0),
(171, 'YDV17117799', '', 1, 109, 1513362600, 1514313000, 0, '', 1, 0, 1510857000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1510857000, 3, 0, 2, 1),
(172, 'YDV17119033', '', 6, 110, 0, 0, 5, '', 1, 0, 1510857000, 37000.00, 300.00, 32000.00, 0.00, 0.00, 0, 0, 1510857000, 3, 1, 1, 0),
(173, 'YDV17118438', '', 3, 111, 0, 0, 10, '', 1, 0, 1510857000, 20000.00, 300.00, 18000.00, 0.00, 0.00, 0, 0, 1510857000, 3, 1, 1, 0),
(174, 'YDV17114457', '', 3, 111, 0, 0, 10, '', 1, 0, 1510857000, 20000.00, 300.00, 18000.00, 0.00, 0.00, 0, 0, 1510857000, 3, 1, 1, 0),
(175, 'YDV17111499', '', 1, 112, 1515781800, 1516213800, 5, '', 1, 0, 1510943400, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1510943400, 3, 0, 2, 0),
(176, 'YDV17117133', '', 2, 103, 1514745000, 1517250600, 7, '', 1, 0, 1510943400, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1510943400, 3, 0, 1, 0),
(177, 'YDV17112390', '', 2, 113, 1513103400, 1513535400, 11, '', 1, 0, 1510943400, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1510943400, 3, 0, 4, 0),
(178, 'YDV17114392', '', 3, 114, 1511461800, 1518719400, 0, '', 0, 0, 1511029800, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1511029800, 3, 0, 1, 0),
(179, 'YDV17115504', '', 1, 115, 1518287400, 1518805800, 12, '', 1, 0, 1511029800, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1511029800, 3, 0, 1, 0),
(180, 'YDV17115802', '', 3, 116, 1515522600, 1523212200, 0, '', 1, 0, 1511029800, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1511029800, 3, 0, 1, 0),
(181, 'YDV17113114', '', 6, 117, 1511980200, 1519756200, 10, '', 1, 0, 1511029800, 37000.00, 300.00, 32000.00, 0.00, 0.00, 0, 0, 1511029800, 3, 0, 1, 0),
(182, 'YDV17119572', '', 1, 118, 1516386600, 1516732200, 0, '', 1, 0, 1511029800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511029800, 3, 0, 1, 0),
(183, 'YDV17115634', '', 1, 119, 1513881000, 1515090600, 7, '', 1, 0, 1511029800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511029800, 3, 0, 2, 1),
(184, 'YDV17114507', '', 3, 120, 1512930600, 1520447400, 0, '', 1, 1, 1511029800, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1511029800, 3, 0, 1, 0),
(185, 'YDV17119630', '', 1, 121, 1512153000, 1512498600, 7, '', 1, 0, 1511116200, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511116200, 3, 0, 1, 0),
(186, 'YDV17117184', '', 2, 122, 1512153000, 1512844200, 0, '', 1, 0, 1511116200, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1511116200, 3, 0, 1, 0),
(187, 'YDV17114368', '', 2, 123, 1511375400, 1512066600, 0, '', 0, 0, 1511116200, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1511116200, 3, 0, 2, 1),
(188, 'YDV17117682', '', 1, 124, 1515954600, 1516386600, 0, '', 1, 0, 1511202600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511202600, 3, 0, 2, 0),
(189, 'YDV17114260', '', 1, 125, 1512498600, 1512844200, 5, '', 1, 0, 1511289000, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1511289000, 3, 0, 3, 0),
(190, 'YDV17113156', '', 1, 125, 1512498600, 1512844200, 5, '', 1, 0, 1511289000, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1511289000, 3, 0, 3, 0),
(191, 'YDV17115764', '', 1, 125, 1512498600, 1512757800, 5, '', 1, 0, 1511289000, 5699.00, 300.00, 5250.00, 0.00, 0.00, 1, 19986, 1511289000, 5, 0, 3, 0),
(192, 'YDV17111489', '', 3, 126, 1522521000, 1527791400, 0, '', 1, 0, 1511289000, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1511289000, 3, 1, 1, 1),
(193, 'YDV17119336', '', 3, 127, 0, 0, 8, '', 1, 0, 1511289000, 20000.00, 300.00, 18000.00, 0.00, 0.00, 0, 0, 1511289000, 3, 0, 1, 0),
(194, 'YDV17114518', '', 3, 128, 1512412200, 1520015400, 0, '', 1, 0, 1511289000, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1511289000, 3, 0, 1, 0),
(195, 'YDV17113570', '', 3, 128, 1512412200, 1520015400, 0, '', 1, 0, 1511289000, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1511289000, 3, 0, 1, 0),
(196, 'YDV17116518', '', 2, 129, 1512498600, 1512757800, 0, '', 1, 0, 1511375400, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1511375400, 3, 0, 1, 0),
(197, 'YDV17112754', '', 2, 130, 1513967400, 1514399400, 7, '', 1, 0, 1511375400, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1511375400, 3, 0, 4, 0),
(198, 'YDV17111236', '', 1, 131, 1514313000, 1515090600, 5, '', 1, 0, 1511375400, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1511375400, 3, 0, 6, 0),
(199, 'YDV17113204', '', 1, 121, 1512153000, 1512498600, 7, '', 1, 0, 1511375400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511375400, 3, 0, 1, 0),
(200, 'YDV17117917', '', 1, 131, 1514313000, 1515090600, 5, '', 0, 0, 1511375400, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1511375400, 3, 0, 6, 0),
(201, 'YDV17115516', '', 1, 43, 0, 0, 5, '', 0, 0, 1511375400, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1511375400, 3, 1, 6, 0),
(202, 'YDV17114331', '', 1, 43, 1511893800, 1511980200, 5, '', 0, 0, 1511375400, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1511375400, 3, 1, 6, 0),
(203, 'YDV17112578', '', 1, 131, 1514313000, 1515090600, 5, '', 1, 0, 1511375400, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1511375400, 3, 0, 6, 0),
(204, 'YDV17113610', '', 1, 131, 1514313000, 1515090600, 5, '', 0, 0, 1511375400, 5699.00, 300.00, 5250.00, 0.00, 0.00, 1, 34707, 1511375400, 5, 0, 6, 0),
(205, 'YDV17118218', '', 1, 132, 0, 0, 12, '', 1, 0, 1511375400, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1511375400, 3, 0, 2, 0),
(206, 'YDV17112689', '', 1, 133, 1512585000, 1513017000, 7, '', 1, 0, 1511461800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511461800, 3, 0, 3, 0),
(207, 'YDV17111039', '', 1, 134, 1511980200, 1512153000, 0, '', 1, 0, 1511461800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511461800, 3, 0, 1, 0),
(208, 'YDV17116978', '', 1, 135, 0, 0, 0, '', 1, 0, 1511461800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511461800, 3, 0, 1, 0),
(209, 'YDV17116400', '', 1, 135, 1513621800, 1514831400, 0, '', 1, 0, 1511461800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511461800, 3, 0, 1, 0),
(210, 'YDV17119599', '', 1, 135, 1513621800, 1514831400, 0, '', 1, 0, 1511461800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511461800, 3, 0, 1, 0),
(211, 'YDV17116414', '', 1, 121, 1512153000, 1512412200, 0, '', 1, 0, 1511461800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 1, 6028, 1511461800, 5, 0, 1, 0),
(212, 'YDV17117521', '', 1, 41, 1511980200, 1512066600, 9, '', 1, 0, 1511548200, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1511548200, 3, 0, 1, 0),
(213, 'YDV17111260', '', 1, 41, 1511721000, 1511807400, 0, '', 1, 0, 1511548200, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511548200, 3, 0, 1, 0),
(214, 'YDV17111948', '', 1, 41, 1511548200, 1511980200, 0, '', 1, 0, 1511548200, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511548200, 3, 0, 1, 0),
(215, 'YDV17112977', '', 1, 41, 1512585000, 1513189800, 9, '', 1, 0, 1511548200, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1511548200, 3, 1, 1, 0),
(216, 'YDV17114296', '', 1, 132, 0, 0, 12, '', 1, 0, 1511548200, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1511548200, 3, 0, 2, 0),
(217, 'YDV17111656', '', 1, 136, 1517941800, 1518546600, 11, '', 1, 0, 1511548200, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1511548200, 3, 0, 6, 0),
(218, 'YDV17115720', '', 1, 137, 1512671400, 1513535400, 12, '', 1, 0, 1511548200, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1511548200, 3, 0, 1, 0),
(219, 'YDV17112995', '', 2, 138, 1511980200, 1512066600, 0, '', 1, 1, 1511548200, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1511548200, 3, 0, 1, 0),
(220, 'YDV17119243', '', 1, 132, 1513017000, 1513535400, 12, '', 1, 0, 1511548200, 5699.00, 600.00, 5250.00, 0.00, 0.00, 1, 13852, 1511548200, 1, 0, 2, 0),
(221, 'YDV17111454', '', 2, 139, 0, 0, 0, '', 1, 1, 1511548200, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1511548200, 3, 1, 1, 0),
(222, 'YDV17119482', '', 2, 139, 1512498600, 1512930600, 0, '', 0, 1, 1511548200, 6299.00, 0.00, 5150.00, 0.00, 0.00, 1, 8829, 1511548200, 5, 0, 1, 0),
(223, 'YDV17111817', '', 1, 133, 1512585000, 1513017000, 7, '', 1, 0, 1511634600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511634600, 3, 0, 3, 0),
(224, 'YDV17114415', '', 1, 133, 1512585000, 1513017000, 7, '', 1, 0, 1511634600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511634600, 3, 0, 3, 0),
(225, 'YDV17111943', '', 1, 133, 1512585000, 1513017000, 7, '', 1, 0, 1511634600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 1, 17353, 1511634600, 5, 0, 3, 0),
(226, 'YDV17111466', '', 2, 140, 0, 0, 0, '', 1, 0, 1511634600, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1511634600, 3, 1, 1, 0),
(227, 'YDV17112178', '', 1, 141, 1522089000, 0, 0, '', 1, 0, 1511634600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511634600, 3, 0, 2, 0),
(228, 'YDV17113102', '', 1, 142, 1513967400, 1512844200, 2, '', 1, 0, 1511721000, 5699.00, 500.00, 5250.00, 0.00, 0.00, 0, 0, 1511721000, 3, 0, 1, 0),
(229, 'YDV17118042', '', 1, 143, 1513967400, 1512844200, 2, '', 1, 0, 1511721000, 5699.00, 500.00, 5250.00, 0.00, 0.00, 0, 0, 1511721000, 3, 0, 1, 0),
(230, 'YDV17117980', '', 1, 144, 1512844200, 1513276200, 0, '', 1, 0, 1511721000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511721000, 3, 0, 2, 1),
(231, 'YDV17111567', '', 1, 145, 0, 0, 5, '', 1, 0, 1511721000, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1511721000, 3, 0, 1, 0),
(232, 'YDV17117888', '', 1, 143, 1514053800, 1515263400, 5, '', 1, 0, 1511721000, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1511721000, 3, 0, 1, 0),
(233, 'YDV17113293', '', 1, 144, 0, 0, 0, '', 1, 0, 1511721000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 17353, 1511721000, 1, 1, 2, 1),
(234, 'YDV17113823', '', 1, 146, 0, 0, 7, '', 1, 0, 1511721000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511721000, 3, 1, 3, 0),
(235, 'YDV17114148', '', 2, 147, 1514399400, 1514485800, 10, '', 1, 0, 1511721000, 6299.00, 300.00, 5150.00, 0.00, 0.00, 0, 0, 1511721000, 3, 1, 1, 0),
(236, 'YDV17117589', '', 2, 147, 1514399400, 1514485800, 10, '', 1, 0, 1511721000, 6299.00, 300.00, 5150.00, 0.00, 0.00, 0, 0, 1511721000, 3, 1, 1, 0),
(237, 'YDV17114898', '', 6, 148, 1512412200, 1512498600, 9, '', 0, 0, 1511721000, 37000.00, 600.00, 32000.00, 0.00, 0.00, 0, 0, 1511721000, 3, 0, 1, 0),
(238, 'YDV17111456', '', 1, 144, 1512844200, 1513276200, 0, '', 1, 0, 1511721000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 1, 17353, 1511721000, 5, 0, 2, 1),
(239, 'YDV17112617', '', 1, 149, 1512239400, 1512671400, 0, '', 0, 1, 1511807400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 1, 0, 1511807400, 3, 0, 1, 0),
(240, 'YDV17112307', '', 2, 150, 0, 0, 6, '', 1, 1, 1511807400, 6299.00, 500.00, 5150.00, 0.00, 0.00, 0, 0, 1511807400, 3, 0, 2, 0),
(241, 'YDV17112575', '', 6, 151, 1514745000, 1521570600, 10, '', 1, 0, 1511807400, 37000.00, 300.00, 32000.00, 0.00, 0.00, 0, 0, 1511807400, 3, 0, 1, 0),
(242, 'YDV17119423', '', 1, 152, 1512153000, 1512325800, 0, '', 1, 0, 1511807400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511807400, 3, 0, 1, 0),
(243, 'YDV17111896', '', 1, 153, 1512412200, 1513017000, 0, '', 1, 0, 1511807400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 1, 0, 1511807400, 3, 0, 2, 0),
(244, 'YDV17115037', '', 1, 154, 1515004200, 1515522600, 1, '', 0, 0, 1511893800, 5699.00, 500.00, 5250.00, 0.00, 0.00, 0, 0, 1511893800, 3, 0, 1, 0),
(245, 'YDV17118909', '', 1, 155, 1514226600, 1514831400, 6, '', 1, 0, 1511893800, 5699.00, 500.00, 5250.00, 0.00, 0.00, 0, 0, 1511893800, 3, 0, 2, 0),
(246, 'YDV17114857', '', 1, 154, 1515004200, 1515522600, 1, '', 1, 0, 1511893800, 5699.00, 500.00, 5250.00, 0.00, 0.00, 0, 0, 1511893800, 3, 0, 2, 0),
(247, 'YDV17116912', '', 2, 156, 1513708200, 1514140200, 0, '', 1, 0, 1511893800, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1511893800, 3, 0, 1, 0),
(248, 'YDV17111870', '', 6, 157, 0, 0, 13, '', 1, 0, 1511893800, 37000.00, 300.00, 32000.00, 300.00, 0.00, 0, 0, 1511893800, 3, 0, 1, 0),
(249, 'YDV17115349', '', 1, 158, 1513967400, 1514226600, 0, '', 1, 0, 1511893800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511893800, 3, 0, 3, 0),
(250, 'YDV17111970', '', 1, 159, 1513967400, 1514226600, 0, '', 1, 0, 1511893800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511893800, 3, 0, 3, 0),
(251, 'YDV17111876', '', 1, 159, 1513967400, 1514226600, 0, '', 1, 0, 1511893800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1511893800, 3, 0, 3, 0),
(252, 'YDV17118880', '', 1, 160, 1512585000, 1512930600, 5, '', 1, 1, 1511980200, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1511980200, 3, 0, 1, 0),
(253, 'YDV17111022', '', 1, 161, 1513449000, 1514053800, 12, '', 0, 0, 1511980200, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1511980200, 3, 0, 2, 0),
(254, 'YDV17114958', '', 1, 161, 1513449000, 1514053800, 12, '', 1, 0, 1511980200, 5699.00, 600.00, 5250.00, 0.00, 0.00, 1, 12787, 1511980200, 5, 0, 2, 0),
(255, 'YDV17125513', '', 1, 162, 0, 0, 12, '', 1, 1, 1512066600, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 0, 1512066600, 3, 0, 2, 2),
(256, 'YDV17122003', '', 1, 163, 1512671400, 1512844200, 0, '', 1, 0, 1512066600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1512066600, 3, 0, 2, 0),
(257, 'YDV17124543', '', 2, 164, 0, 0, 6, '', 1, 0, 1512066600, 6299.00, 500.00, 5150.00, 0.00, 0.00, 0, 0, 1512066600, 3, 1, 1, 0),
(258, 'YDV17126500', '', 1, 165, 1515522600, 1516127400, 0, '', 1, 0, 1512153000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1512153000, 3, 0, 1, 0),
(259, 'YDV17126201', '', 1, 166, 1513362600, 1513794600, 7, '', 1, 0, 1512153000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1512153000, 3, 0, 4, 0),
(260, 'YDV17128807', '', 3, 167, 1515954600, 1516041000, 0, '', 1, 0, 1512153000, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1512153000, 3, 0, 1, 0),
(261, 'YDV17122457', '', 1, 166, 1513362600, 1513794600, 0, '', 1, 0, 1512153000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1512153000, 3, 0, 4, 0),
(262, 'YDV17123436', '', 1, 168, 1512844200, 1513276200, 12, '', 1, 0, 1512153000, 5699.00, 600.00, 5250.00, 0.00, 0.00, 0, 6393, 1512153000, 1, 0, 1, 0),
(263, 'YDV17125439', '', 1, 166, 1513362600, 1513794600, 0, '', 1, 0, 1512153000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 1, 12523, 1512153000, 5, 0, 2, 0),
(264, 'YDV17124839', '', 1, 166, 1513362600, 1513794600, 0, '', 1, 0, 1512153000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 12523, 1512153000, 1, 0, 2, 0),
(265, 'YDV17128336', '', 1, 169, 1512930600, 1513449000, 10, '', 1, 0, 1512153000, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1512153000, 3, 0, 1, 0),
(266, 'YDV17124635', '', 1, 165, 1515522600, 1516127400, 5, '', 1, 0, 1512239400, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1512239400, 3, 0, 2, 0),
(267, 'YDV17128785', '', 1, 112, 1515781800, 1516213800, 5, '', 1, 0, 1512239400, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1512239400, 3, 0, 2, 0),
(268, 'YDV17123980', '', 1, 112, 1515781800, 1516213800, 5, '', 1, 0, 1512239400, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 13132, 1512239400, 1, 0, 2, 0),
(269, 'YDV17125464', '', 1, 166, 1513362600, 1513794600, 0, '', 1, 0, 1512325800, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1512325800, 3, 0, 2, 0),
(270, 'YDV17124431', '', 1, 170, 1513621800, 1514226600, 5, '', 1, 0, 1512498600, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1512498600, 3, 0, 3, 0),
(271, 'YDV17125570', '', 1, 171, 1516559400, 1516991400, 0, '', 1, 0, 1512498600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1512498600, 3, 0, 2, 0),
(272, 'YDV17126688', '', 1, 172, 1514745000, 1515954600, 5, '', 1, 0, 1512498600, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1512498600, 3, 0, 1, 0),
(273, 'YDV17121840', '', 1, 173, 1513881000, 1514572200, 8, '', 1, 0, 1512498600, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1512498600, 3, 0, 2, 1),
(274, 'YDV17125922', '', 3, 174, 1516905000, 1524681000, 0, '', 1, 0, 1512498600, 20000.00, 0.00, 18000.00, 0.00, 0.00, 0, 0, 1512498600, 3, 0, 1, 0),
(275, 'YDV17128613', '', 1, 173, 1513881000, 1514572200, 8, '', 1, 0, 1512498600, 5699.00, 300.00, 5250.00, 0.00, 0.00, 1, 18267, 1512498600, 5, 0, 3, 0),
(276, 'YDV17124371', '', 1, 175, 1516213800, 1516645800, 5, '', 0, 0, 1512585000, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1512585000, 3, 0, 1, 0),
(277, 'YDV17126706', '', 4, 176, 1512844200, 1513103400, 0, '', 1, 1, 1512585000, 4799.00, 0.00, 4200.00, 0.00, 0.00, 0, 0, 1512585000, 3, 0, 1, 0),
(278, 'YDV17128887', '', 2, 177, 1515522600, 1518028200, 9, '', 1, 0, 1512585000, 6299.00, 600.00, 5150.00, 0.00, 0.00, 0, 0, 1512585000, 3, 0, 1, 0),
(279, 'YDV17128214', '', 1, 178, 1514053800, 1514658600, 0, '', 1, 0, 1512585000, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1512585000, 3, 0, 3, 0),
(280, 'YDV17123447', '', 1, 166, 1513362600, 1513794600, 7, '', 1, 0, 1512671400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1512671400, 3, 0, 2, 0),
(281, 'YDV17125868', '', 1, 179, 1514053800, 1514485800, 0, '', 1, 0, 1512671400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1512671400, 3, 0, 3, 0),
(282, 'YDV17126498', '', 1, 180, 1514399400, 1514745000, 13, '', 0, 0, 1512671400, 5699.00, 300.00, 5250.00, 300.00, 0.00, 0, 0, 1512671400, 3, 0, 3, 0),
(283, 'YDV17122312', '', 2, 181, 1513362600, 1513794600, 13, '', 1, 0, 1512671400, 6299.00, 300.00, 5150.00, 300.00, 0.00, 0, 0, 1512671400, 3, 0, 1, 0),
(284, 'YDV17126606', '', 1, 182, 1516041000, 1516818600, 0, '', 1, 0, 1512671400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1512671400, 3, 0, 1, 0),
(285, 'YDV17122600', '', 2, 181, 1513362600, 1513967400, 13, '', 1, 0, 1512671400, 6299.00, 300.00, 5150.00, 300.00, 0.00, 0, 0, 1512671400, 3, 0, 1, 0),
(286, 'YDV17124542', '', 2, 181, 1513362600, 1514572200, 13, '', 1, 0, 1512757800, 6299.00, 300.00, 5150.00, 300.00, 0.00, 0, 0, 1512757800, 3, 0, 1, 0),
(287, 'YDV17128703', '', 1, 171, 1516559400, 1516991400, 0, '', 1, 0, 1512930600, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1512930600, 3, 0, 2, 0),
(288, 'YDV17125920', '', 6, 183, 1514313000, 1522002600, 7, '', 1, 1, 1512930600, 37000.00, 0.00, 32000.00, 0.00, 0.00, 0, 0, 1512930600, 3, 0, 1, 0),
(289, 'YDV17129380', '', 2, 100, 1514399400, 1521829800, 0, '', 0, 0, 1513017000, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1513017000, 3, 0, 1, 0),
(290, 'YDV17122637', '', 2, 184, 1515177000, 1517769000, 0, '', 1, 0, 1513103400, 6299.00, 0.00, 5150.00, 0.00, 0.00, 0, 0, 1513103400, 3, 0, 1, 0),
(291, 'YDV17127459', '', 1, 185, 1515781800, 1516818600, 0, '', 1, 0, 1513103400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1513103400, 3, 0, 1, 0),
(292, 'YDV17126442', '', 1, 186, 1513276200, 1513535400, 7, '', 1, 0, 1513103400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1513103400, 3, 0, 1, 0),
(293, 'YDV17128278', '', 1, 187, 1513708200, 1514226600, 10, '', 1, 0, 1513103400, 5699.00, 300.00, 5250.00, 0.00, 0.00, 0, 0, 1513103400, 3, 0, 1, 0),
(294, 'YDV17123824', '', 1, 188, 1514140200, 1514399400, 7, '', 1, 0, 1513103400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1513103400, 3, 0, 2, 0),
(295, 'YDV17127971', '', 1, 189, 1514658600, 1515263400, 0, '', 1, 0, 1513103400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1513103400, 3, 0, 1, 0),
(296, 'YDV17123700', '', 1, 186, 1513276200, 1513535400, 0, '', 1, 0, 1513103400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1513103400, 3, 0, 1, 0),
(297, 'YDV17122240', '', 1, 186, 1513276200, 1513535400, 0, '', 1, 0, 1513103400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 0, 0, 1513103400, 3, 0, 1, 0),
(298, 'YDV17124568', '', 1, 186, 0, 0, 0, '', 1, 0, 1513103400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 1, 5784, 1513103400, 5, 0, 1, 0),
(299, 'YDV17122432', '', 1, 186, 0, 0, 0, '', 1, 1, 1513103400, 5699.00, 0.00, 5250.00, 0.00, 0.00, 1, 8322, 1513103400, 5, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_leads_applicant`
--

CREATE TABLE IF NOT EXISTS `tbl_leads_applicant` (
  `lep_id` int(11) NOT NULL AUTO_INCREMENT,
  `lep_lead_id` int(11) NOT NULL,
  `lep_applicant_id` int(11) NOT NULL,
  `lep_insurance` int(11) NOT NULL DEFAULT '0',
  `lep_insurance_amount` decimal(10,2) NOT NULL,
  `lep_status` tinyint(2) NOT NULL DEFAULT '0',
  `lep_visa_file` varchar(255) NOT NULL,
  `lep_remark` varchar(255) NOT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lep_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=169 ;

--
-- Dumping data for table `tbl_leads_applicant`
--

INSERT INTO `tbl_leads_applicant` (`lep_id`, `lep_lead_id`, `lep_applicant_id`, `lep_insurance`, `lep_insurance_amount`, `lep_status`, `lep_visa_file`, `lep_remark`, `is_delete`) VALUES
(1, 3, 1, 0, 0.00, 5, '', '', 0),
(4, 4, 1, 0, 0.00, 5, '', '', 0),
(5, 5, 2, 0, 0.00, 0, '', '', 0),
(6, 6, 2, 0, 0.00, 0, '', '', 0),
(7, 21, 3, 0, 0.00, 5, '', '', 0),
(8, 22, 2, 0, 0.00, 0, '', '', 0),
(9, 23, 2, 0, 0.00, 0, '', '', 0),
(10, 24, 2, 0, 0.00, 0, '', '', 0),
(11, 27, 5, 0, 0.00, 0, '', '', 0),
(13, 28, 2, 0, 0.00, 0, '', '', 0),
(14, 29, 2, 0, 0.00, 0, '', '', 0),
(15, 30, 2, 0, 0.00, 0, '', '', 0),
(16, 31, 2, 0, 0.00, 0, '', '', 0),
(17, 32, 2, 0, 0.00, 0, '', '', 0),
(18, 33, 2, 0, 0.00, 0, '', '', 0),
(19, 35, 2, 0, 0.00, 0, '', '', 0),
(20, 36, 2, 0, 0.00, 0, '', '', 0),
(21, 37, 5, 0, 0.00, 0, '', '', 0),
(22, 38, 5, 0, 0.00, 0, '', '', 0),
(23, 39, 5, 0, 0.00, 0, '', '', 0),
(24, 40, 5, 0, 0.00, 0, '', '', 0),
(25, 41, 2, 0, 0.00, 0, '', '', 0),
(26, 42, 1, 0, 0.00, 0, '', '', 0),
(27, 43, 1, 0, 0.00, 0, '', '', 0),
(28, 44, 1, 0, 0.00, 0, '', '', 0),
(29, 45, 5, 0, 0.00, 0, '', '', 0),
(30, 46, 5, 0, 0.00, 0, '', '', 0),
(32, 47, 5, 0, 0.00, 0, '', '', 0),
(33, 49, 5, 0, 0.00, 0, '', '', 0),
(34, 50, 5, 207, 207.00, 0, '', '', 0),
(35, 51, 5, 0, 0.00, 1, '', '', 0),
(36, 52, 5, 0, 0.00, 0, '', '', 0),
(37, 54, 5, 0, 0.00, 0, '', '', 0),
(38, 55, 5, 0, 0.00, 0, '', '', 0),
(40, 56, 5, 0, 0.00, 0, '', '', 0),
(41, 57, 5, 0, 0.00, 0, '', '', 0),
(42, 58, 5, 0, 0.00, 0, '', '', 0),
(43, 59, 3, 0, 0.00, 0, '', '', 0),
(44, 60, 3, 0, 0.00, 0, '', '', 0),
(45, 61, 3, 0, 0.00, 0, '', '', 0),
(46, 62, 3, 0, 0.00, 0, '', '', 0),
(47, 63, 3, 0, 0.00, 0, '', '', 0),
(48, 64, 3, 0, 0.00, 0, '', '', 0),
(49, 65, 3, 0, 0.00, 0, '', '', 0),
(50, 66, 3, 0, 0.00, 0, '', '', 0),
(51, 67, 3, 0, 0.00, 0, '', '', 0),
(52, 68, 3, 0, 0.00, 0, '', '', 0),
(53, 69, 3, 0, 0.00, 0, '', '', 0),
(54, 70, 3, 0, 0.00, 0, '', '', 0),
(55, 71, 2, 0, 0.00, 1, '', '', 0),
(56, 72, 5, 0, 0.00, 0, '', '', 0),
(57, 73, 5, 0, 0.00, 0, '', '', 0),
(58, 74, 5, 0, 0.00, 0, '', '', 0),
(59, 75, 5, 0, 0.00, 0, '', '', 0),
(60, 80, 7, 0, 0.00, 0, '', '', 0),
(61, 86, 2, 0, 0.00, 0, '', '', 0),
(62, 89, 3, 0, 0.00, 0, '', '', 0),
(63, 90, 3, 0, 0.00, 0, '', '', 0),
(64, 91, 3, 0, 0.00, 0, '', '', 0),
(65, 92, 3, 0, 0.00, 0, '', '', 0),
(66, 93, 2, 0, 0.00, 0, '', '', 0),
(67, 94, 5, 244, 244.00, 0, '', '', 0),
(68, 95, 3, 0, 0.00, 0, '', '', 0),
(69, 96, 3, 0, 0.00, 0, '', '', 0),
(70, 100, 2, 0, 0.00, 0, '', '', 0),
(71, 108, 8, 0, 0.00, 0, '', '', 0),
(72, 108, 9, 0, 0.00, 0, '', '', 0),
(73, 110, 3, 0, 0.00, 0, '', '', 0),
(74, 111, 3, 0, 0.00, 0, '', '', 0),
(75, 112, 2, 462, 462.00, 0, '', '', 0),
(76, 113, 2, 0, 0.00, 0, '', '', 0),
(78, 137, 2, 0, 0.00, 0, '', '', 0),
(79, 141, 10, 0, 0.00, 0, '', '', 0),
(80, 141, 11, 0, 0.00, 0, '', '', 0),
(86, 142, 12, 456, 456.00, 0, '', '', 0),
(87, 142, 13, 985, 985.00, 0, '', '', 0),
(88, 142, 14, 1441, 1441.00, 0, '', '', 0),
(89, 142, 15, 1897, 1897.00, 0, '', '', 0),
(90, 142, 16, 2353, 2353.00, 0, '', '', 0),
(91, 147, 17, 0, 0.00, 0, '', '', 0),
(92, 147, 18, 0, 0.00, 0, '', '', 0),
(93, 158, 19, 0, 0.00, 0, '', '', 0),
(94, 161, 20, 0, 0.00, 0, '', '', 0),
(95, 167, 21, 0, 0.00, 0, '', '', 0),
(96, 167, 22, 0, 0.00, 0, '', '', 0),
(97, 167, 23, 0, 0.00, 0, '', '', 0),
(98, 167, 24, 0, 0.00, 0, '', '', 0),
(99, 169, 25, 483, 483.00, 0, '', '', 0),
(100, 168, 26, 0, 0.00, 0, '', '', 0),
(101, 189, 30, 0, 0.00, 0, '', '', 0),
(102, 189, 31, 0, 0.00, 0, '', '', 0),
(103, 189, 32, 0, 0.00, 0, '', '', 0),
(104, 190, 30, 0, 0.00, 0, '', '', 0),
(105, 190, 31, 0, 0.00, 0, '', '', 0),
(106, 190, 32, 0, 0.00, 0, '', '', 0),
(107, 191, 30, 329, 329.00, 0, '', '', 0),
(108, 191, 31, 573, 573.00, 0, '', '', 0),
(109, 191, 32, 817, 817.00, 0, '', '', 0),
(110, 199, 33, 0, 0.00, 0, '', '', 0),
(111, 202, 5, 0, 0.00, 0, '', '', 0),
(112, 203, 34, 0, 0.00, 0, '', '', 0),
(113, 203, 35, 0, 0.00, 0, '', '', 0),
(114, 203, 36, 0, 0.00, 0, '', '', 0),
(115, 203, 37, 0, 0.00, 0, '', '', 0),
(116, 203, 38, 0, 0.00, 0, '', '', 0),
(117, 203, 39, 0, 0.00, 0, '', '', 0),
(118, 204, 34, 0, 0.00, 0, '', '', 0),
(119, 204, 35, 0, 0.00, 0, '', '', 0),
(120, 204, 36, 0, 0.00, 0, '', '', 0),
(121, 204, 37, 0, 0.00, 0, '', '', 0),
(122, 204, 38, 0, 0.00, 0, '', '', 0),
(123, 204, 39, 0, 0.00, 0, '', '', 0),
(124, 211, 33, 244, 244.00, 0, '', '', 0),
(127, 212, 40, 0, 0.00, 0, '', '', 0),
(128, 213, 2, 0, 0.00, 0, '', '', 0),
(129, 214, 2, 0, 0.00, 0, '', '', 0),
(130, 215, 2, 0, 0.00, 0, '', '', 0),
(131, 216, 41, 0, 0.00, 0, '', '', 0),
(132, 220, 41, 355, 355.00, 0, '', '', 0),
(133, 220, 43, 710, 710.00, 0, '', '', 0),
(134, 222, 44, 0, 0.00, 0, '', '', 0),
(135, 224, 45, 0, 0.00, 0, '', '', 0),
(136, 224, 47, 0, 0.00, 0, '', '', 0),
(137, 224, 48, 0, 0.00, 0, '', '', 0),
(138, 225, 45, 0, 0.00, 0, '', '', 0),
(139, 225, 46, 0, 0.00, 0, '', '', 0),
(140, 225, 48, 0, 0.00, 0, '', '', 0),
(141, 233, 50, 0, 0.00, 0, '', '', 0),
(142, 233, 51, 0, 0.00, 0, '', '', 0),
(143, 233, 52, 0, 0.00, 0, '', '', 0),
(144, 238, 50, 0, 0.00, 0, '', '', 0),
(145, 238, 51, 0, 0.00, 0, '', '', 0),
(146, 238, 52, 0, 0.00, 0, '', '', 0),
(147, 254, 53, 0, 0.00, 0, '', '', 0),
(148, 254, 54, 0, 0.00, 0, '', '', 0),
(149, 257, 55, 0, 0.00, 0, '', '', 0),
(150, 261, 56, 0, 0.00, 0, '', '', 0),
(151, 261, 57, 0, 0.00, 0, '', '', 0),
(152, 262, 58, 0, 0.00, 0, '', '', 0),
(153, 263, 56, 318, 318.00, 0, '', '', 0),
(154, 263, 57, 636, 636.00, 0, '', '', 0),
(155, 264, 56, 318, 318.00, 0, '', '', 0),
(156, 264, 57, 636, 636.00, 0, '', '', 0),
(157, 268, 60, 318, 318.00, 0, '', '', 0),
(158, 268, 61, 636, 636.00, 0, '', '', 0),
(159, 275, 62, 0, 0.00, 0, '', '', 0),
(160, 275, 63, 0, 0.00, 0, '', '', 0),
(161, 275, 64, 0, 0.00, 0, '', '', 0),
(162, 280, 56, 0, 0.00, 0, '', '', 0),
(163, 280, 57, 0, 0.00, 0, '', '', 0),
(164, 290, 65, 0, 0.00, 0, '', '', 0),
(165, 294, 66, 0, 0.00, 0, '', '', 0),
(166, 294, 67, 0, 0.00, 0, '', '', 0),
(167, 298, 69, 0, 0.00, 0, '', '', 0),
(168, 299, 69, 0, 0.00, 0, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lead_documents`
--

CREATE TABLE IF NOT EXISTS `tbl_lead_documents` (
  `ld_id` int(11) NOT NULL AUTO_INCREMENT,
  `ld_lead_id` int(11) NOT NULL,
  `ld_document_id` int(11) NOT NULL,
  `ld_document_name` varchar(255) NOT NULL,
  PRIMARY KEY (`ld_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=294 ;

--
-- Dumping data for table `tbl_lead_documents`
--

INSERT INTO `tbl_lead_documents` (`ld_id`, `ld_lead_id`, `ld_document_id`, `ld_document_name`) VALUES
(1, 3, 1, '1_0_1507550501424.jpg'),
(2, 3, 2, '2_0_1507550501199.jpg'),
(3, 3, 3, '3_0_1507550501238.jpg'),
(12, 4, 1, '1_0_1507552649962.jpg'),
(13, 4, 2, '2_0_1507552649591.jpg'),
(14, 5, 1, '1_0_1507552977502.pdf'),
(15, 5, 2, '2_0_1507552977481.pdf'),
(16, 5, 3, '3_0_1507552977853.jpg'),
(17, 6, 1, '1_0_1507609474357.jpg'),
(18, 6, 2, '2_0_1507609474291.jpg'),
(19, 6, 3, '3_0_150760947446.jpg'),
(20, 21, 1, '1_0_1507734565398.jpg'),
(21, 21, 2, '2_0_1507734565544.jpg'),
(22, 22, 1, '1_0_1507785944875.jpg'),
(23, 22, 2, '2_0_1507785944922.jpg'),
(24, 22, 3, '3_0_1507785944198.jpg'),
(25, 23, 1, '1_0_1507786192914.jpg'),
(26, 23, 2, '2_0_1507786192498.jpg'),
(27, 23, 3, '3_0_1507786192252.jpg'),
(28, 24, 1, '1_0_1507786292955.jpg'),
(29, 24, 2, '2_0_1507786292580.jpg'),
(30, 24, 3, '3_0_150778629241.jpg'),
(31, 25, 1, '1_0_1507808733400.jpg'),
(32, 25, 2, '2_0_1507808733254.jpg'),
(33, 25, 3, '3_0_1507808733843.jpg'),
(34, 27, 1, '1_0_1507809708339.jpeg'),
(35, 27, 2, '2_0_1507809708657.jpeg'),
(36, 27, 3, '3_0_1507809708588.jpeg'),
(37, 26, 1, '1_0_1507809725389.jpg'),
(38, 26, 2, '2_0_1507809725954.jpg'),
(39, 26, 3, '3_0_1507809725720.jpg'),
(43, 28, 1, '1_0_1507815844759.pdf'),
(44, 28, 2, '2_0_1507815844938.pdf'),
(45, 28, 3, '3_0_1507815844351.pdf'),
(46, 29, 1, '1_0_1507818894417.pdf'),
(47, 29, 2, '2_0_1507818894483.pdf'),
(48, 30, 1, '1_0_1507819385835.pdf'),
(49, 30, 2, '2_0_1507819385253.pdf'),
(50, 31, 1, '1_0_1507885501229.pdf'),
(51, 31, 2, '2_0_150788550130.pdf'),
(52, 31, 3, '3_0_1507885501812.pdf'),
(53, 32, 1, '1_0_1507885789982.pdf'),
(54, 32, 2, '2_0_1507885789826.pdf'),
(55, 32, 3, '3_0_1507885789644.pdf'),
(56, 33, 1, '1_0_1507885959775.pdf'),
(57, 33, 2, '2_0_1507885959322.pdf'),
(58, 34, 1, '1_0_1507954607539.jpg'),
(59, 34, 2, '2_0_1507954607157.jpg'),
(60, 34, 3, '3_0_1507954607276.jpg'),
(61, 35, 1, '1_0_1507954683816.jpg'),
(62, 35, 2, '2_0_1507954683587.jpg'),
(63, 35, 3, '3_0_1507954683238.jpg'),
(64, 36, 1, '1_0_1507955250949.jpg'),
(65, 36, 2, '2_0_1507955250658.jpg'),
(66, 36, 3, '3_0_1507955250302.jpg'),
(67, 37, 1, '1_0_1508767981472.jpeg'),
(68, 37, 2, '2_0_1508767981280.jpg'),
(69, 37, 3, '3_0_1508767981199.jpg'),
(70, 38, 1, '1_0_150876812520.jpg'),
(71, 38, 2, '2_0_1508768125763.jpg'),
(72, 38, 3, '3_0_1508768125900.jpg'),
(73, 39, 1, '1_0_150893948922.jpeg'),
(74, 39, 2, '2_0_1508939489857.jpeg'),
(75, 39, 3, '3_0_1508939489422.jpeg'),
(76, 40, 1, '1_0_1509005706967.jpeg'),
(77, 40, 2, '2_0_1509005706309.jpeg'),
(78, 40, 3, '3_0_1509005706261.jpeg'),
(79, 41, 1, '1_0_1509005921227.jpg'),
(80, 41, 2, '2_0_150900592171.jpg'),
(81, 41, 3, '3_0_1509005921507.jpg'),
(82, 42, 1, '1_0_1509008722229.jpg'),
(83, 42, 2, '2_0_1509008722230.jpg'),
(84, 42, 3, '3_0_1509008722748.jpg'),
(85, 43, 1, '1_0_1509008831780.jpg'),
(86, 43, 2, '2_0_1509008831459.jpg'),
(87, 43, 3, '3_0_150900883123.jpg'),
(88, 44, 1, '1_0_1509008989904.jpg'),
(89, 44, 2, '2_0_1509008989905.jpg'),
(90, 44, 3, '3_0_1509008989301.jpg'),
(91, 45, 1, '1_0_1509010390225.pdf'),
(92, 45, 2, '2_0_1509010390545.pdf'),
(93, 45, 3, '3_0_1509010390420.pdf'),
(94, 46, 1, '1_0_1509010644841.pdf'),
(95, 46, 2, '2_0_1509010644260.pdf'),
(96, 46, 3, '3_0_1509010644342.pdf'),
(100, 47, 1, '1_0_1509012038138.pdf'),
(101, 47, 2, '2_0_1509012038477.pdf'),
(102, 47, 3, '3_0_1509012038174.pdf'),
(103, 49, 1, '1_0_1509086622668.pdf'),
(104, 49, 2, '2_0_1509086622780.pdf'),
(105, 49, 3, '3_0_1509086622406.pdf'),
(106, 50, 1, '1_0_1509094267587.jpeg'),
(107, 50, 2, '2_0_1509094267918.jpeg'),
(108, 50, 3, '3_0_1509094267627.jpeg'),
(109, 51, 1, '1_0_1509094548827.jpeg'),
(110, 51, 2, '2_0_1509094548667.jpeg'),
(111, 51, 3, '3_0_1509094548363.jpeg'),
(112, 52, 1, '1_0_1509100500319.png'),
(113, 52, 2, '2_0_1509100500195.png'),
(114, 52, 3, '3_0_1509100500388.png'),
(115, 53, 1, '1_0_1509101513995.png'),
(116, 53, 2, '2_0_150910151314.png'),
(117, 54, 1, '1_0_1509102986780.jpeg'),
(118, 54, 2, '2_0_1509102986517.jpeg'),
(119, 54, 3, '3_0_1509102986572.jpeg'),
(120, 55, 1, '1_0_1509103615313.pdf'),
(121, 55, 2, '2_0_1509103615722.pdf'),
(122, 55, 3, '3_0_1509103615231.pdf'),
(126, 56, 1, '1_0_1509104908208.pdf'),
(127, 56, 2, '2_0_1509104908228.pdf'),
(128, 56, 3, '3_0_1509104908562.pdf'),
(131, 57, 1, '1_0_1509112587429.pdf'),
(132, 57, 2, '2_0_1509112587311.pdf'),
(133, 57, 3, '3_0_1509112587699.pdf'),
(134, 58, 1, '1_0_1509113407781.pdf'),
(135, 58, 2, '2_0_1509113407233.pdf'),
(136, 58, 3, '3_0_1509113407779.pdf'),
(137, 59, 1, '1_0_1509357487502.jpg'),
(138, 59, 2, '2_0_1509357487988.png'),
(139, 60, 1, '1_0_1509358011645.png'),
(140, 60, 2, '2_0_1509358011672.png'),
(141, 61, 1, '1_0_1509358375234.jpg'),
(142, 61, 2, '2_0_1509358375326.png'),
(145, 62, 1, '1_0_1509358799766.jpg'),
(146, 62, 2, '2_0_1509358799221.jpg'),
(147, 63, 1, '1_0_1509358915633.jpg'),
(148, 63, 2, '2_0_1509358915269.png'),
(149, 64, 1, '1_0_1509360474829.png'),
(150, 64, 2, '2_0_1509360474484.png'),
(151, 65, 1, '1_0_1509360738673.png'),
(152, 65, 2, '2_0_1509360738701.png'),
(153, 66, 1, '1_0_1509361116505.jpg'),
(154, 66, 2, '2_0_1509361116329.jpg'),
(155, 67, 1, '1_0_1509361685516.png'),
(156, 67, 2, '2_0_1509361685316.png'),
(157, 68, 1, '1_0_150936382572.png'),
(158, 68, 2, '2_0_1509363825748.png'),
(159, 69, 1, '1_0_1509364523260.png'),
(160, 69, 2, '2_0_1509364523647.png'),
(161, 70, 1, '1_0_1509364623437.png'),
(162, 70, 2, '2_0_1509364623986.png'),
(163, 71, 1, '1_0_1509377234361.jpg'),
(164, 71, 2, '2_0_1509377234815.jpg'),
(165, 71, 3, '3_0_1509377234390.jpg'),
(166, 72, 0, ''),
(167, 74, 1, ''),
(168, 74, 2, ''),
(169, 75, 1, ''),
(170, 75, 2, ''),
(171, 79, 1, '1_0_150953276018.pdf'),
(172, 79, 2, ''),
(173, 80, 1, ''),
(174, 80, 2, ''),
(175, 85, 1, ''),
(176, 85, 2, ''),
(177, 86, 1, ''),
(178, 86, 2, ''),
(179, 90, 1, ''),
(180, 90, 2, ''),
(181, 91, 1, '1_0_1509702478842.png'),
(182, 91, 2, '2_0_1509702478125.png'),
(183, 92, 1, '1_0_1509702875868.png'),
(184, 92, 2, '2_0_1509702875658.png'),
(185, 93, 1, ''),
(186, 93, 2, ''),
(187, 94, 1, ''),
(188, 94, 2, ''),
(189, 95, 1, '1_0_1509710122598.png'),
(190, 95, 2, '2_0_1509710122278.png'),
(191, 96, 1, '1_0_1509711755369.jpg'),
(192, 96, 2, '2_0_1509711755291.jpg'),
(193, 100, 1, ''),
(194, 100, 2, ''),
(195, 108, 1, ''),
(196, 108, 2, ''),
(197, 110, 1, ''),
(198, 110, 2, ''),
(201, 111, 1, ''),
(202, 111, 2, ''),
(203, 112, 1, ''),
(204, 112, 2, ''),
(205, 113, 1, ''),
(206, 113, 2, ''),
(207, 137, 1, ''),
(208, 137, 2, ''),
(209, 141, 1, '1_0_1510470719271.pdf'),
(210, 141, 2, ''),
(213, 142, 1, '1_0_1510487165657.pdf'),
(214, 142, 2, ''),
(215, 147, 1, '1_0_1510570134859.jpg'),
(216, 147, 2, '2_0_151057013445.jpg'),
(217, 158, 1, ''),
(218, 158, 2, ''),
(219, 161, 0, ''),
(220, 167, 1, ''),
(221, 167, 2, ''),
(222, 169, 1, '1_0_151092480788.pdf'),
(223, 169, 2, '2_0_1510924807168.pdf'),
(224, 168, 1, ''),
(225, 168, 2, ''),
(226, 189, 1, ''),
(227, 189, 2, ''),
(228, 190, 1, '1_0_1511326786138.pdf'),
(229, 190, 2, '2_0_1511326786440.pdf'),
(230, 191, 1, '1_0_1511327178430.pdf'),
(231, 191, 2, '2_0_1511327178373.pdf'),
(232, 202, 1, ''),
(233, 202, 2, ''),
(236, 203, 1, ''),
(237, 203, 2, ''),
(240, 204, 1, ''),
(241, 204, 2, ''),
(242, 211, 1, '1_0_151153209811.pdf'),
(243, 211, 2, '2_0_1511532098130.pdf'),
(250, 212, 1, ''),
(251, 212, 2, ''),
(252, 213, 1, ''),
(253, 213, 2, ''),
(254, 214, 1, ''),
(255, 214, 2, ''),
(256, 215, 1, ''),
(257, 215, 2, ''),
(258, 222, 1, ''),
(259, 222, 2, ''),
(260, 220, 1, '1_0_1511615042261.pdf'),
(261, 220, 2, '2_0_1511615042422.png'),
(262, 224, 1, '1_0_1511640541814.pdf,1_1_1511640541986.pdf'),
(263, 224, 2, '2_0_151164054157.pdf,2_1_1511640541981.pdf'),
(264, 225, 1, '1_0_1511674642317.pdf,1_1_1511674642123.pdf'),
(265, 225, 2, '2_0_1511674642729.pdf,2_1_151167464277.pdf'),
(266, 233, 1, ''),
(267, 233, 2, ''),
(268, 238, 1, ''),
(269, 238, 2, ''),
(270, 254, 1, '1_0_1512038468201.pdf'),
(271, 254, 2, '2_0_151203846836.pdf'),
(272, 257, 1, '1_0_1512127022128.pdf'),
(273, 257, 2, '2_0_1512127022870.pdf'),
(274, 261, 1, ''),
(275, 261, 2, ''),
(276, 263, 1, '1_0_1512202147608.pdf,1_1_1512202147248.pdf'),
(277, 263, 2, '2_0_1512202147410.pdf,2_1_1512202147562.pdf'),
(278, 262, 1, '1_0_1512202323850.pdf'),
(279, 262, 2, '2_0_1512202323880.pdf'),
(280, 264, 1, ''),
(281, 264, 2, ''),
(282, 268, 1, '1_0_1512292517129.pdf,1_1_1512292517159.pdf,1_2_151229251764.pdf'),
(283, 268, 2, ''),
(284, 275, 1, '1_0_151256075476.pdf'),
(285, 275, 2, '2_0_1512560754779.pdf'),
(286, 290, 1, ''),
(287, 290, 2, ''),
(288, 294, 1, '1_0_1513164832818.pdf'),
(289, 294, 2, '2_0_1513164832467.pdf'),
(290, 298, 1, ''),
(291, 298, 2, ''),
(292, 299, 1, ''),
(293, 299, 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message`
--

CREATE TABLE IF NOT EXISTS `tbl_message` (
  `mess_id` int(11) NOT NULL AUTO_INCREMENT,
  `mess_message` text NOT NULL,
  `mess_status` tinyint(4) NOT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mess_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_message`
--

INSERT INTO `tbl_message` (`mess_id`, `mess_message`, `mess_status`, `is_delete`) VALUES
(1, 'u', 1, 1),
(2, 'u', 1, 1),
(3, 'gdfg', 1, 1),
(4, 'u.kjfgjgkhkljmhm,jkn.m', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news_flash`
--

CREATE TABLE IF NOT EXISTS `tbl_news_flash` (
  `nf_id` int(11) NOT NULL AUTO_INCREMENT,
  `nf_title` varchar(255) NOT NULL,
  `nf_date` int(11) NOT NULL,
  `nf_content` text NOT NULL,
  `nf_image` varchar(200) NOT NULL,
  `nf_status` tinyint(2) NOT NULL DEFAULT '0',
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`nf_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_news_flash`
--

INSERT INTO `tbl_news_flash` (`nf_id`, `nf_title`, `nf_date`, `nf_content`, `nf_image`, `nf_status`, `is_delete`) VALUES
(1, 'Dubai is building a mock Martian city', 1506537000, '<div>\r\n<div>\r\n<div>\r\n<div>\r\n<p>Someday, you could take a&nbsp;flying taxi&nbsp;from Dubai to a Martian city in the middle of the desert right here on Earth. In preparation for its plans to establish a settlement on the red planet, the United Arab Emirates has&nbsp;announced&nbsp;that it''s building a 1.9 million square feet simulated Mars settlement. It will be called Mars Science City and will serve as home to interconnected domes housing various laboratories simulating the planet''s terrain. The team building the structure plans to use advanced 3D printing techniques and heat and radiation insulation to mimic the harsh environment of our neighbor.&nbsp;</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<div>\r\n<div>\r\n<div>\r\n<div>\r\n<div>\r\n<p>Scientists will use those labs to develop technologies that can provide future Martian colonies with food, water and energy. That way, settlers wouldn''t have to spend years eating only potatoes in their new home. In addition to laboratories, the man-made city will house a museum showcasing humanity''s greatest space achievements, which will boast 3D-printed walls made of sand from the country''s desert. There will be areas meant to engage kids and ignite their interest in space, as well. Once the city''s up, the UAE intends to conduct an experiment involving a group of people living within its confines&nbsp;for a year.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n<p>According to the Government of Dubai, which is heading the project, it will cost around $140 million to build the artificial city. UAE''s Vice President and Prime Minister Sheikh Mohammed bin Rashid Al Maktoum said:</p>\r\n<blockquote>\r\n<p><strong>"The UAE seeks to establish international efforts to develop technologies that benefit humankind, and that establish the foundation of a better future for more generations to come. We also want to consolidate the passion for leadership in science in the UAE, contributing to improving life on earth and to developing innovative solutions to many of our global challenges."</strong></p>\r\n</blockquote>\r\n<p>The country hasn''t revealed a timeline for the project yet, but we''d sure love to see Matt Damon grace the city''s ribbon-cutting ceremony.</p>\r\n<p>\r\n<div class="mt-5">Via:&nbsp;<span class="th-meta"><a class="th-meta" href="http://bgr.com/2017/09/28/mars-city-project-dubai-settlement/" target="_blank">BGR</a></span></div>\r\n<div class="mt-5">Source:&nbsp;<span class="th-meta"><a class="th-meta" href="http://mediaoffice.ae/en/media-center/news/26/9/2017/mars.aspx" target="_blank">Government of Dubai</a></span></div>\r\n</p>\r\n<p>\r\n<div class="mt-5"><span class="th-meta">&nbsp;</span></div>\r\n</p>', 'news_1509090168.jpeg', 1, 0),
(2, 'Visa on arrival in UAE for Indian passport holders with UK and EU residence visa', 1506882600, '<p>Abu Dhabi: UAE Cabinet has approved visa on arrival for Indian passport holders with UK and EU residency visa.</p>\r\n<p>Already,&nbsp;Indian passport holders with a valid American visa or a Green Card are granted UAE visa on arrival at all ports, starting from May 1, 2017.</p>\r\n<div id="_forkInArticleAdContainer"><ins></ins></div>\r\n<p>&nbsp;</p>\r\n<p>The decision to have a simplified visa process is aimed at advancing the UAE-India relations in economic, politics, and trade, said officials.</p>\r\n<p>It also contributes to achieving UAE&rsquo;s vision to be the leading country in attracting global tourism.</p>\r\n<p>&nbsp;</p>\r\n<p>Courtesy -&nbsp;http://gulfnews.com/news/uae/government/visa-on-arrival-in-uae-for-indian-passport-holders-with-uk-and-eu-residence-visa-1.2089403</p>', 'news_1506848230.jpg', 1, 0),
(3, 'Emirates and flydubai could operate from single terminal at Dubai Airport', 1508956200, '<div class="embed-container image">\r\n<div class="attribution">\r\n<p>DUBAI: Emirates and flydubai could use the same terminal at Dubai International Airport, the world&rsquo;s busiest for international travellers, after expanding their commercial relationship earlier this year, the head of the airport operator said.</p>\r\n<p>Emirates, the Middle East&rsquo;s largest airline, exclusively uses Terminal 3 with the exception of some Qantas flights, which will end next year. Budget airline flydubai shares Terminal 2, on the other side of the airport, with other carriers.</p>\r\n<div id="_forkInArticleAdContainer"><ins></ins></div>\r\n<p>&nbsp;</p>\r\n<p>Proposals to improve connections for passengers between the two airlines are under consideration, including whether it&rsquo;s feasible for them to operate out of Terminal 3.</p>\r\n<p>&ldquo;We need to make a strategic decision about how the traffic distribution will work,&rdquo; Dubai Airports Chief Executive Paul Griffiths told Reuters on Thursday in an interview in Dubai.</p>\r\n<p>&ldquo;Then we need to move quickly into a design and construction phase to build whatever facilities are necessary for that, or adapt existing facilities around the new business model.&rdquo; Emirates and flydubai agreed in July to coordinate on network planning, schedules and frequent flyer programmes, and said they would align airport systems and operations, but did not say how that would happen.</p>\r\n<p>&ldquo;What we are trying to do is see if we can get some sort of operating model established with them that will mean the infrastructure is shared on a more operational basis,&rdquo; Griffiths said.</p>\r\n<p>A flydubai spokeswoman confirmed talks on how to improve passenger connectivity were underway. Emirates did not respond to a request for comment.</p>\r\n<p>Emirates, flydubai and Dubai Airports, are owned by the government of Dubai.</p>\r\n<p>&ldquo;We&rsquo;ve got a series of proposals which hopefully before the end of the month will be evaluated and agreed,&rdquo; Griffiths said.</p>\r\n<p>Emirates and flydubai are managed independently despite the shared ownership and, as of July, operated a fleet of 317 aircraft to 216 destinations between them.</p>\r\n<p>The airlines operate different aircraft. Emirates only flies wide-body Airbus A380s and Boeing 777s, while flydubai has an exclusive fleet of narrowbody Boeing 737s.</p>\r\n<p>The combined fleet will expand to 380 aircraft flying to 240 destinations by 2022, the airlines said in July.</p>\r\n<p>Moving flydubai&rsquo;s operations to Terminal 1, located alongside Terminal 3, is also being looked at, and the final solution could be a hybrid of the proposals under consideration, Griffiths said.</p>\r\n<p>Terminal 1 is used by foreign airlines, including British Airways, Lufthansa, and Singapore Airlines.</p>\r\nCourtesy-&nbsp;http://gulfnews.com/business/aviation/emirates-and-flydubai-could-operate-from-single-terminal-at-dubai-airport-1.2113742</div>\r\n</div>\r\n<div class="group"></div>', 'news_1509089873.jpg', 1, 0),
(4, 'Hi', 1506796200, '<p>HI</p>', 'news_1506847838.png', 0, 0),
(5, 'Dubai looks at autonomous vehicles for its future', 1509388200, '<p>Dubai is looking at autonomous vehicles for shuttle buses to be used in certain communities as early as next year, officials said on Monday.</p>\r\n<p>But although autonomous vehicles are expected to start testing for safety on some of the emirate&rsquo;s roads in January and experts discuss built-in charging plates to power the vehicles of the future, they said transitioning to the phase will prove challenging.</p>\r\n<p>&ldquo;Autonomous vehicles are the next revolution in transportation,&rdquo; said Ahmed Bahrozyan, chief executive of the Licencing Agency at the Roads and Transport Authority (RTA). &ldquo;It started with being very apprehensive but in Dubai, we proved we prefer to take action, be positive and be ahead. If we want to become one of smartest cities in world, we definitely need to be at the forefront of cities ready to accept autonomous vehicles.&rdquo;</p>\r\n<p>He said the technology was evolving, with billions of dollars being invested by manufacturers and operators. &ldquo;The question is which city will keep up with the technology to have operations,&rdquo; he said. &ldquo;We need to be ready as Dubai if we want to reach our targets.&rdquo;</p>\r\n<p>The global smart city market is growing, projected from 622 billion dollars this year to 3.4 trillion dollars in a decade.</p>\r\n<p>A new concept called Transit X has created the world&rsquo;s first 100 per cent-solar paneled, fully autonomous, on demand, point-to-point, non-stop public transportation system.</p>\r\n<p>According to Paul Copping, chief innovation officer at Digital Greenwich in the UK, autonomous vehicles are a technological curve, similar to those we have already lived with. &ldquo;It&rsquo;s an exciting time to be in this industry,&rdquo; he said. &ldquo;We should change our spatial planning and rethink how we centre our communities to give us fast transit network, reduce people&rsquo;s commute and improve resilience and agility of the last mile community.&rdquo;</p>\r\n<p>Greenwich, one of London&rsquo;s 32 boroughs, operates as a smart city testbed.</p>\r\n<p>The company has also done some regulatory work to support the RTA&rsquo;s strategy.</p>\r\n<p>&ldquo;Dubai is the world leader in smart city and distribution of technology with some tight timelines,&rdquo; Mr Copping said.</p>\r\n<p>&ldquo;A big decision is how much to contain the private transport network. It&rsquo;s challenging but having 20 per cent of all transport to be public in Dubai seems sensible. Within 20 years, driving your car will be as comfortable as riding your horse and as sensible as doing it in the city.&rdquo;</p>\r\n<p>Experts said they hoped to one day see autonomous public transit and freight in Dubai.</p>\r\n<p>&ldquo;There&rsquo;s going to be a move towards a more sustainable and integrated planning,&rdquo; said Martin Tilman, director of transport planning at Aecom.</p>\r\n<p>&ldquo;We&rsquo;re currently working on autonomous public transit in Dubai in segregation, away from highways. But the challenge will be when you start mixing autonomous with standard vehicles.&rdquo;</p>\r\n<p>He said there was a transition phase where they might have them running separately to other vehicles.</p>\r\n<p>&ldquo;There&rsquo;s a whole set of planning exercises that needs to run around that,&rdquo; he said.</p>\r\n<p>&ldquo;We need to think about powering those vehicles, charging station locations and highways with built-in charging plates. We&rsquo;re all thinking of 20 to 30 years in the future and, at this stage, that transition stage is going to be the most problematic because you have to separate vehicles.&rdquo;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Courtesy/Credits- &nbsp;</strong>https://www.thenational.ae/uae/government/dubai-looks-at-autonomous-vehicles-for-its-future-1.671636</p>', 'news_1509433387.jpg', 1, 0),
(6, '2 days to go: Dubai''s Global Village opens on November 1', 1509215400, '<p>Dubai: If you''re ready for some exciting outdoor fun in Dubai this winter, the Global Village is set to reopen its doors in two days &mdash; on November 1, 2017.</p>\r\n<p>It will be the 22nd season for the festive&nbsp;destination, marked by fun treats for children and retail extravanganzas for adults, and loads of family-oriented entertainment.</p>\r\n<p>In the runup to the November 1, and as workers are putting the finishing touches to the Global Village, upcoming events had been posted on its Facebook page.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>Courtesy-&nbsp;</strong>http://gulfnews.com/news/uae/tourism/2-days-to-go-dubai-s-global-village-opens-on-november-1-1.2106198</p>\r\n<p>&nbsp;</p>', 'news_1509433498.jpg', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_otb`
--

CREATE TABLE IF NOT EXISTS `tbl_otb` (
  `otb_id` int(11) NOT NULL AUTO_INCREMENT,
  `otb_airline` varchar(255) NOT NULL,
  `otb_charge` decimal(7,2) NOT NULL,
  `otb_cost` decimal(7,2) NOT NULL,
  `otb_status` tinyint(2) NOT NULL DEFAULT '0',
  `otb_dstatus` tinyint(2) NOT NULL DEFAULT '0',
  `otb_image` varchar(255) NOT NULL,
  PRIMARY KEY (`otb_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tbl_otb`
--

INSERT INTO `tbl_otb` (`otb_id`, `otb_airline`, `otb_charge`, `otb_cost`, `otb_status`, `otb_dstatus`, `otb_image`) VALUES
(1, 'Gulf Air', 500.00, 0.00, 1, 0, 'otb1507393302.png'),
(2, 'Fly Dubai', 500.00, 0.00, 1, 0, 'otb1507393290.png'),
(3, 'Kuwait Airways', 500.00, 0.00, 1, 0, 'otb1507393277.jpg'),
(4, 'Qatar', 500.00, 0.00, 1, 0, 'otb1507393264.png'),
(5, 'Emirates', 300.00, 0.00, 1, 0, 'otb1507393240.png'),
(6, 'Oman Air', 500.00, 0.00, 1, 0, 'otb1507393225.png'),
(7, 'Jet Airways', 0.00, 0.00, 1, 0, 'otb1507393212.png'),
(8, 'Etihad', 300.00, 0.00, 1, 0, 'otb1507393197.jpg'),
(9, 'Air Arabia', 600.00, 0.00, 1, 0, 'otb1507393178.png'),
(10, 'Air India', 300.00, 0.00, 1, 0, ''),
(11, 'Spicejet', 600.00, 0.00, 1, 0, 'otb1507393140.png'),
(12, 'Indigo', 600.00, 0.00, 1, 0, 'otb1507393344.png'),
(13, 'Air India Express', 300.00, 300.00, 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_otp`
--

CREATE TABLE IF NOT EXISTS `tbl_otp` (
  `otp_id` int(11) NOT NULL AUTO_INCREMENT,
  `otp_leadid` int(11) NOT NULL,
  `otp_uid` int(11) NOT NULL,
  `otp_otp` int(11) NOT NULL,
  `otp_email` varchar(100) NOT NULL,
  `otp_session_id` varchar(255) NOT NULL,
  PRIMARY KEY (`otp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pages`
--

CREATE TABLE IF NOT EXISTS `tbl_pages` (
  `pag_id` int(11) NOT NULL AUTO_INCREMENT,
  `pag_title` varchar(255) NOT NULL,
  `pag_content` text NOT NULL,
  `page_image` varchar(500) DEFAULT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pag_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `tbl_pages`
--

INSERT INTO `tbl_pages` (`pag_id`, `pag_title`, `pag_content`, `page_image`, `is_delete`) VALUES
(1, 'About us', '<p>YourDubaiVisa is an expert at providing speedy, efficient and expedient processing services for Dubai tourist visas and for our clientele traveling to Dubai.</p>\r\n<p>YourDubaiVisa is a part of the company FastwayTrips.com. YourDubaiVisa was incorporated in 2017 and is a specialist business process outsource agency which focuses on serving Consular sections of diplomatic missions by providing administrative support and managing non-judgmental tasks related to the entire lifecycle of a Visa, Passport &amp; Consular Service application process, thereby enabling missions to focus on the key tasks of adjudication. As a core business, the organization is extremely well placed to address the need of the Diplomatic Missions Worldwide and focuses on addressing the concerns regarding security, integrity, speed and efficacy on the outsourced solution of the business.&nbsp;</p>', 'page_1513326917.jpg', 0),
(2, 'Privacy Policy', '<p><strong>Privacy Policy</strong></p>\r\n<p><strong>Your website may use the Privacy Policy given below:</strong></p>\r\n<p>The terms "We" / "Us" / "Our"/&rdquo;Company&rdquo; individually and collectively refer to <strong>YourDubaiVisa or its parent company FastwayTrips.com</strong> and the terms "You" /"Your" / "Yourself" refer to the users.</p>\r\n<p>This Privacy Policy is an electronic record in the form of an electronic contract formed under the information Technology Act, 2000 and the rules made thereunder and the amended provisions pertaining to electronic documents / records in various statutes as amended by the information Technology Act, 2000. This Privacy Policy does not require any physical, electronic or digital signature.</p>\r\n<p>This Privacy Policy is a legally binding document between you and <strong>YourDubaiVisa or its parent company FastwayTrips.com</strong> (both terms defined below). The terms of this Privacy Policy will be effective upon your acceptance of the same (directly or indirectly in electronic form, by clicking on the I accept tab or by use of the website or by other means) and will govern the relationship between you and <strong>YourDubaiVisa or its parent company FastwayTrips.com</strong> for your use of the website &ldquo;<strong>Website</strong>&rdquo; (defined below).</p>\r\n<p>This document is published and shall be construed in accordance with the provisions of the Information Technology (reasonable security practices and procedures and sensitive personal data of information) rules, 2011 under Information Technology Act, 2000; that require publishing of the Privacy Policy for collection, use, storage and transfer of sensitive personal data or information.</p>\r\n<p>Please read this Privacy Policy carefully by using the Website, you indicate that you understand, agree and consent to this Privacy Policy. If you do not agree with the terms of this Privacy Policy, please do not use this Website.</p>\r\n<p>By providing us your Information or by making use of the facilities provided by the Website, You hereby consent to the collection, storage, processing and transfer of any or all of Your Personal Information and Non-Personal Information by us as specified under this Privacy Policy. You further agree that such collection, use, storage and transfer of Your Information shall not cause any loss or wrongful gain to you or any other person.</p>\r\n<p><strong>USER INFORMATION</strong><strong>&nbsp;</strong></p>\r\n<p>To avail certain services on our Websites, users are required to provide certain information for the registration process namely: - a) your name, b) email address, c) sex, d) age, e) PIN code, f) credit card or debit card details g) medical records and history h) sexual orientation, i) biometric information, j) password etc., and / or your occupation, interests, and the like. The Information as supplied by the users enables us to improve our sites and provide you the most user-friendly experience.<br /> <br /> All required information is service dependent and we may use the above said user information to, maintain, protect, and improve its services (including advertising services) and for developing new services<br /> <br /> Such information will not be considered as sensitive if it is freely available and accessible in the public domain or is furnished under the Right to Information Act, 2005 or any other law for the time being in force.<br /> <br /> <strong>COOKIES</strong><br /> To improve the responsiveness of the sites for our users, we may use "cookies", or similar electronic tools to collect information to assign each visitor a unique, random number as a User Identification (User ID) to understand the user''s individual interests using the Identified Computer. Unless you voluntarily identify yourself (through registration, for example), we will have no way of knowing who you are, even if we assign a cookie to your computer. The only personal information a cookie can contain is information you supply (an example of this is when you ask for our Personalised Horoscope). A cookie cannot read data off your hard drive. Our advertisers may also assign their own cookies to your browser (if you click on their ads), a process that we do not control.&nbsp;<br /> <br /> Our web servers automatically collect limited information about your computer''s connection to the Internet, including your IP address, when you visit our site. (Your IP address is a number that lets computers attached to the Internet know where to send you data -- such as the web pages you view.) Your IP address does not identify you personally. We use this information to deliver our web pages to you upon request, to tailor our site to the interests of our users, to measure traffic within our site and let advertisers know the geographic locations from where our visitors come.&nbsp;<strong></strong></p>\r\n<p><strong>LINKS TO THE OTHER SITES</strong><br /> Our policy discloses the privacy practices for our own web site only. Our site provides links to other websites also that are beyond our control. We shall in no way be responsible in way for your use of such sites.<strong>5. </strong></p>\r\n<p><strong>INFORMATION SHARING</strong><br /> We shares the sensitive personal information to any third party without obtaining the prior consent of the user in the following limited circumstances:<br /> <br /> <strong>(a)</strong>&nbsp;When it is requested or required by law or by any court or governmental agency or authority to disclose, for the purpose of verification of identity, or for the prevention, detection, investigation including cyber incidents, or for prosecution and punishment of offences. These disclosures are made in good faith and belief that such disclosure is reasonably necessary for enforcing these Terms; for complying with the applicable laws and regulations.&nbsp;<br /> <br /> <strong>(b)</strong>&nbsp;We proposes to share such information within its group companies and officers and employees of such group companies for the purpose of processing personal information on its behalf. We also ensure that these recipients of such information agree to process such information based on our instructions and in compliance with this Privacy Policy and any other appropriate confidentiality and security measures.</p>\r\n<p><strong>How do we use your information?&nbsp;</strong></p>\r\n<p>We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;To personalize your experience and to allow us to deliver the type of content and product offerings in &nbsp;&nbsp;which you are most interested.</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;To improve our website in order to better serve you.</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;To allow us to better service you in responding to your customer service requests.</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;To quickly process your transactions.</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;To ask for ratings and reviews of services or products</p>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&bull;</strong>&nbsp;To follow up with them after correspondence (live chat, email or phone inquiries)</p>\r\n<p><strong>Google</strong></p>\r\n<p>Google''s Advertising Principles can sum up Google&rsquo;s advertising requirements. They are put in place to provide a positive experience for users.</p>\r\n<p><strong>Changes to this Privacy Policy</strong></p>\r\n<p>We reserve the right to change this policy should we deem it advisable to do so. If we make material changes that will affect personal information we have already collected from you, we will make reasonable efforts to notify you of the changes and to give you the opportunity to amend or cancel your registration.<strong>&nbsp;</strong></p>\r\n<p><strong>How do we protect your information?</strong></p>\r\n<p>We do not use vulnerability scanning and/or scanning to PCI standards.</p>\r\n<p>An external PCI compliant payment gateway handles all CC transactions.</p>\r\n<p>We use regular Malware Scanning.</p>\r\n<p>Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.&nbsp;</p>\r\n<p>We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information.</p>\r\n<p>All transactions are processed through a gateway provider and are not stored or processed on our servers.</p>\r\n<p><strong>INFORMATION SECURITY</strong><br /> We take appropriate security measures to protect against unauthorized access to or unauthorized alteration, disclosure or destruction of data. These include internal reviews of our data collection, storage and processing practices and security measures, including appropriate encryption and physical security measures to guard against unauthorized access to systems where we store personal data.<br /> <br /> All information gathered on our Website is securely stored within our controlled database. The database is stored on servers secured behind a firewall; access to the servers is password-protected and is strictly limited. However, as effective as our security measures are, no security system is impenetrable. We cannot guarantee the security of our database, nor can we guarantee that information you supply will not be intercepted while being transmitted to us over the Internet. And, of course, any information you include in a posting to the discussion areas is available to anyone with Internet access.&nbsp;<br /> <br /> However the internet is an ever evolving medium. We may change our Privacy Policy from time to time to incorporate necessary future changes. Of course, our use of any information we gather will always be consistent with the policy under which the information was collected, regardless of what the new policy may be.&nbsp;<br /> <br /> <strong>Grievance Redressal</strong><br /> Redressal Mechanism: Any complaints, abuse or concerns with regards to content and or comment or breach of these terms shall be immediately informed to the designated Grievance Officer as mentioned below via in writing or through email.</p>\r\n<p>Samarth Bawa Kohli</p>\r\n<p>samarth@yourdubaivisa.com</p>\r\n<p>+91-93577-00050.</p>', 'page_1513326972.jpg', 0),
(4, 'Test Page123123', '<p>Lorem ipsum is simply a dummy text. Lorem ipsum is simply a dummy text. Lorem ipsum is simply a dummy text. Lorem ipsum is simply a dummy text. Lorem ipsum is simply a dummy text. Lorem ipsum is simply a dummy text. Lorem ipsum is simply a dummy text. Lorem ipsum is simply a dummy text. Lorem ipsum is simply a dummy text. Lorem ipsum is simply a dummy text. Lorem ipsum is simply a dummy text.</p>', 'page_1513324948.jpg', 0),
(22, 'Visa on Arrivals', '<h2><span style="font-size: 10px;">The type of visa needed to enter Dubai or other emirates within the UAE depends on several factors such as your nationality, the purpose of your planned visit and its duration. GCC nationals are granted entry upon arrival to the UAE with passports or national IDs. Non-GCC passport holders travelling with GCC nationals should check visa requirements for their respective countries.</span></h2>\n<p>Citizens of the following countries do not require advance visa arrangements to enter the UAE and can obtain a visa upon arrival for 90 days:</p>\n<table border="0" cellspacing="0" cellpadding="0" width="100%">\n<tbody>\n<tr>\n<td valign="top">\n<p>France</p>\n</td>\n<td valign="top">\n<p>Iceland</p>\n</td>\n<td valign="top">\n<p>Italy</p>\n</td>\n<td valign="top">\n<p>Norway</p>\n</td>\n<td valign="top">\n<p>Holland</p>\n</td>\n</tr>\n<tr>\n<td valign="top">\n<p>Germany</p>\n</td>\n<td valign="top">\n<p>Sweden</p>\n</td>\n<td valign="top">\n<p>Austria</p>\n</td>\n<td valign="top">\n<p>Finland</p>\n</td>\n<td valign="top">\n<p>Liechtenstein</p>\n</td>\n</tr>\n<tr>\n<td valign="top">\n<p>Belgium</p>\n</td>\n<td valign="top">\n<p>Greece</p>\n</td>\n<td valign="top">\n<p>Luxembourg</p>\n</td>\n<td valign="top">\n<p>Poland</p>\n</td>\n<td valign="top">\n<p>Switzerland</p>\n</td>\n</tr>\n<tr>\n<td valign="top">\n<p>Estonia</p>\n</td>\n<td valign="top">\n<p>Portugal</p>\n</td>\n<td valign="top">\n<p>Malta</p>\n</td>\n<td valign="top">\n<p>Spain</p>\n</td>\n<td valign="top">\n<p>Cyprus</p>\n</td>\n</tr>\n<tr>\n<td valign="top">\n<p>Denmark</p>\n</td>\n<td valign="top">\n<p>Croatia</p>\n</td>\n<td valign="top">\n<p>Slovenia</p>\n</td>\n<td valign="top">\n<p>Romania</p>\n</td>\n<td valign="top">\n<p>Slovakia</p>\n</td>\n</tr>\n<tr>\n<td valign="top">\n<p>Bulgaria</p>\n</td>\n<td valign="top">\n<p>Czech Republic</p>\n</td>\n<td valign="top">\n<p>Hungary</p>\n</td>\n<td valign="top">\n<p>Lithuania</p>\n</td>\n<td valign="top">\n<p>Latvia</p>\n</td>\n</tr>\n</tbody>\n</table>\n<p>Indian citizens in possession of a valid US visa (green card or student visa) can apply for 14-day visa on arrival at the airport.</p>\n<p>Please note:&nbsp;</p>\n<ul>\n<li>Citizens of the countries listed above can obtain a visa upon arrival for 90 days from the date of entry with their normal passports.</li>\n<li>This visa type is not renewable&nbsp;</li>\n<li>Passports should be normal and valid for more than six months.</li>\n<li>The visa holder can use the 90-day visa upon arrival to be consumed within 180 days from first entry.</li>\n</ul>\n<p>Citizens of the following countries do not require advance visa arrangements to enter the UAE and can obtain a visa upon arrival for 30 days with a 10-day grace period:</p>\n<p>&nbsp;</p>\n<table cellspacing="0" cellpadding="0" width="100%">\n<tbody>\n<tr>\n<td valign="top">Andorra</td>\n<td valign="top">China</td>\n<td valign="top">Malaysia</td>\n<td valign="top">Singapore</td>\n</tr>\n<tr>\n<td valign="top">Australia</td>\n<td valign="top">Hong Kong</td>\n<td valign="top">Monaco</td>\n<td valign="top">South Korea</td>\n</tr>\n<tr>\n<td valign="top">Brunei</td>\n<td valign="top">Ireland</td>\n<td valign="top">New Zealand</td>\n<td valign="top">The Vatican</td>\n</tr>\n<tr>\n<td valign="top">Canada</td>\n<td valign="top">Japan</td>\n<td valign="top">San Marino</td>\n<td valign="top">United States</td>\n</tr>\n<tr>\n<td valign="top">United Kingdom</td>\n<td valign="top">Russia&nbsp;</td>\n<td valign="top">&nbsp;</td>\n</tr>\n</tbody>\n</table>', 'page_1513327428.jpg', 0),
(23, 'Test Add Page Change', '<p>Test Add Page1231212</p>', 'page_1513327428.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE IF NOT EXISTS `tbl_payment` (
  `p_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `reference_no` varchar(255) NOT NULL,
  `lid` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pincode` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL,
  `payment_mode` tinyint(4) NOT NULL,
  `payment_option` tinyint(4) NOT NULL,
  `order_no` varchar(255) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `payment_method` varchar(120) NOT NULL,
  `user_id` int(11) NOT NULL,
  `insertdate` int(11) NOT NULL,
  `doi` int(11) NOT NULL,
  `ptype` varchar(10) NOT NULL DEFAULT 'visa',
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=112 ;

--
-- Dumping data for table `tbl_payment`
--

INSERT INTO `tbl_payment` (`p_id`, `name`, `address`, `city`, `reference_no`, `lid`, `email`, `pincode`, `state`, `phone`, `status`, `amount`, `payment_mode`, `payment_option`, `order_no`, `payment_id`, `payment_method`, `user_id`, `insertdate`, `doi`, `ptype`) VALUES
(1, 'Test_User ltest', 'Jaipur', 'Jaipur', 'YDV17104441', 3, 'test@gmail.com', '', 'Rajasthan', '9024699937', 0, 6000, 0, 0, '', 0, '0', 1, 1507549603, 1507487400, 'visa'),
(2, 'Test_User ltest', 'Jaipur', 'Jaipur', 'YDV17104441', 3, 'test@gmail.com', '', 'Rajasthan', '9024699937', 2, 6000, 0, 0, '', 0, '0', 1, 1507550509, 1507487400, 'visa'),
(3, 'Test_User ltest', 'Jaipur', 'Jaipur', 'YDV17102792', 4, 'test@gmail.com', '', 'Rajasthan', '9024699937', 0, 6299, 0, 0, '', 0, '', 1, 1507551386, 1507487400, 'visa'),
(4, 'Test_User ltest', 'Jaipur', 'Jaipur', 'YDV17102792', 4, 'test@gmail.com', '', 'Rajasthan', '9024699937', 2, 6299, 0, 0, '306003368745', 2147483647, 'Credit Card - Visa', 1, 1507551462, 1507487400, 'visa'),
(5, 'Samarth KOHLI', 'test', 'test', '', 35, 'fastwaytrips@gmail.com', '', 'test', '7696324223', 2, 0, 0, 0, '306003376566', 2147483647, 'Credit Card - Visa', 41, 1507954760, 1507919400, 'visa'),
(6, 'Samarth KOHLI', 'test', 'test', '', 36, 'fastwaytrips@gmail.com', '', 'test', '7696324223', 2, 0, 0, 0, '306003376569', 2147483647, 'Credit Card - Visa', 41, 1507955355, 1507919400, 'visa'),
(7, 'Samarth Kohli', '', '', '', 37, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 0, 0, 0, '', 0, '', 43, 1508767989, 1508697000, 'visa'),
(8, 'Samarth Kohli', 'd', '', '', 38, 'samarth@yourdubaivisa.com', '', '', '7696324223', 2, 0, 0, 0, '306003384445', 2147483647, 'Credit Card - Visa', 43, 1508768130, 1508697000, 'visa'),
(9, 'Samarth ko', '', '', '', 39, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 0, 0, 0, '', 0, '', 43, 1508939497, 1508869800, 'visa'),
(10, 'Samarth ko', '', '', '', 40, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 0, 0, 0, '', 0, '', 43, 1509005721, 1508956200, 'visa'),
(11, 'Samarth ko', '', '', '', 40, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 0, 0, 0, '', 0, '', 43, 1509005728, 1508956200, 'visa'),
(12, 'Samarth ko', '', '', '', 40, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 0, 0, 0, '', 0, '', 43, 1509005749, 1508956200, 'visa'),
(13, 'Samarth KOHLI', '', '', '', 41, 'fastwaytrips@gmail.com', '', '', '7696324223', 0, 0, 0, 0, '', 0, '', 41, 1509005926, 1508956200, 'visa'),
(14, 'Samarth KOHLI', '', '', '', 41, 'fastwaytrips@gmail.com', '', '', '7696324223', 0, 0, 0, 0, '', 0, '', 41, 1509006077, 1508956200, 'visa'),
(15, 'Samarth KOHLI', '', '', '', 41, 'fastwaytrips@gmail.com', '', '', '7696324223', 0, 0, 0, 0, '', 0, '', 41, 1509006079, 1508956200, 'visa'),
(16, 'Test_User ltest', 'Jaipur', 'Jaipur', '', 42, 'test@gmail.com', '', 'Rajasthan', '9024699937', 0, 0, 0, 0, '', 0, '', 1, 1509008769, 1508956200, 'visa'),
(17, 'Test_User ltest', 'Jaipur', 'Jaipur', '', 43, 'test@gmail.com', '', 'Rajasthan', '9024699937', 0, 0, 0, 0, '', 0, '', 1, 1509008835, 1508956200, 'visa'),
(18, 'Test_User ltest', 'Jaipur', 'Jaipur', '', 44, 'test@gmail.com', '', 'Rajasthan', '9024699937', 0, 0, 0, 0, '', 0, '', 1, 1509008995, 1508956200, 'visa'),
(19, 'Test_User ltest', 'Jaipur', 'Jaipur', '', 44, 'test@gmail.com', '', 'Rajasthan', '9024699937', 0, 0, 0, 0, '', 0, '', 1, 1509009068, 1508956200, 'visa'),
(20, 'Test_User ltest', 'Jaipur', 'Jaipur', '', 44, 'test@gmail.com', '', 'Rajasthan', '9024699937', 0, 5700, 0, 0, '', 0, '', 1, 1509009143, 1508956200, 'visa'),
(21, 'Test_User ltest', 'Jaipur', 'Jaipur', '', 44, 'test@gmail.com', '', 'Rajasthan', '9024699937', 0, 5700, 0, 0, '', 0, '', 1, 1509009206, 1508956200, 'visa'),
(22, 'Samarth ko', '', '', '', 45, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 5700, 0, 0, '', 0, '', 43, 1509010410, 1508956200, 'visa'),
(23, 'Samarth ko', '', '', '', 46, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 100, 0, 0, '', 0, '', 43, 1509010649, 1508956200, 'visa'),
(24, 'Samarth ko', '', '', '', 46, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 100, 0, 0, '', 0, '', 43, 1509010795, 1508956200, 'visa'),
(25, 'Samarth ko', '', '', '', 47, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 100, 0, 0, '', 0, '', 43, 1509010875, 1508956200, 'visa'),
(26, 'Samarth ko', '', '', '', 47, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 100, 0, 0, '', 0, '', 43, 1509012065, 1508956200, 'visa'),
(27, 'Samarth ko', '', '', '', 49, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 100, 0, 0, '', 0, '', 43, 1509086627, 1509042600, 'visa'),
(28, 'Samarth ko', '', '', '', 50, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 307, 0, 0, '', 0, '', 43, 1509094276, 1509042600, 'visa'),
(29, ' ', '', '', '', 0, '', '', '', '', 0, 207, 0, 0, '', 0, '', 0, 1509094563, 1509042600, 'visa'),
(30, 'Samarth ko', '', '', '', 52, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 100, 0, 0, '', 0, '', 43, 1509100504, 1509042600, 'visa'),
(31, 'Samarth ko', 'f-1 sector-6', 'noida', '', 53, 'samarth@yourdubaivisa.com', '', 'Uttar Pradesh', '7696324223', 0, 0, 0, 0, '', 0, '', 43, 1509101539, 1509042600, 'visa'),
(32, 'Samarth ko', '', '', '', 54, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 100, 0, 0, '', 0, '', 43, 1509103001, 1509042600, 'visa'),
(33, 'Samarth ko', '', '', '', 55, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 100, 0, 0, '', 0, '', 43, 1509103699, 1509042600, 'visa'),
(34, 'Samarth ko', '', '', '', 56, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 100, 0, 0, '', 0, '', 43, 1509104773, 1509042600, 'visa'),
(35, 'Samarth ko', '', '', '', 56, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 100, 0, 0, '', 0, '', 43, 1509105018, 1509042600, 'visa'),
(36, 'Samarth ko', '', '', '', 57, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 100, 0, 0, '', 0, '', 43, 1509112592, 1509042600, 'visa'),
(37, 'Samarth ko', '', '', '', 58, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 100, 0, 0, '', 0, '', 43, 1509113413, 1509042600, 'visa'),
(38, 'Samarth ko', '', '', '', 58, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 100, 0, 0, '', 0, '', 43, 1509113551, 1509042600, 'visa'),
(39, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 59, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6799, 0, 0, '', 0, '', 45, 1509357493, 1509301800, 'visa'),
(40, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 59, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6799, 0, 0, '', 0, '', 45, 1509357730, 1509301800, 'visa'),
(41, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 59, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6799, 0, 0, '', 0, '', 45, 1509357762, 1509301800, 'visa'),
(42, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 60, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6299, 0, 0, '', 0, '', 45, 1509358023, 1509301800, 'visa'),
(43, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 60, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6299, 0, 0, '', 0, '', 45, 1509358062, 1509301800, 'visa'),
(44, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 61, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6299, 0, 0, '', 0, '', 45, 1509358384, 1509301800, 'visa'),
(45, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 61, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6299, 0, 0, '', 0, '', 45, 1509358426, 1509301800, 'visa'),
(46, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 62, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 0, 0, 0, '', 0, '', 45, 1509358772, 1509301800, 'visa'),
(47, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 62, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6299, 0, 0, '', 0, '', 45, 1509358803, 1509301800, 'visa'),
(48, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 63, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6299, 0, 0, '', 0, '', 45, 1509358920, 1509301800, 'visa'),
(49, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 63, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6299, 0, 0, '', 0, '', 45, 1509358970, 1509301800, 'visa'),
(50, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 64, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6299, 0, 0, '', 0, '', 45, 1509360499, 1509301800, 'visa'),
(51, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 65, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6299, 0, 0, '', 0, '', 45, 1509360743, 1509301800, 'visa'),
(52, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 66, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6299, 0, 0, '', 0, '', 45, 1509361121, 1509301800, 'visa'),
(53, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 67, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6299, 0, 0, '', 0, '', 45, 1509361690, 1509301800, 'visa'),
(54, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 68, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6299, 0, 0, '', 0, '', 45, 1509363835, 1509301800, 'visa'),
(55, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 69, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6299, 0, 0, '', 0, '', 45, 1509364530, 1509301800, 'visa'),
(56, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 70, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6299, 0, 0, '', 0, '', 45, 1509364628, 1509301800, 'visa'),
(57, 'Samarth KOHLI', '', '', '', 71, 'fastwaytrips@gmail.com', '', '', '7696324223', 2, 100, 0, 0, '7301960832', 2147483647, 'PPI', 41, 1509377247, 1509301800, 'visa'),
(58, 'Samarth ko', '', '', '', 72, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 5699, 0, 0, '', 0, '', 43, 1509424346, 1509388200, 'visa'),
(59, 'Samarth ko', '', '', '', 74, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 5699, 0, 0, '', 0, '', 43, 1509424615, 1509388200, 'visa'),
(60, 'dvfg fasf', '', '', '', 78, 'abc@gmail.com', '', '', '7696324423', 0, 0, 0, 0, '', 0, '', 50, 1509532793, 1509474600, 'visa'),
(61, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 90, 'shobhit.jhalani@gmail.com', '', 'Jaipur', '9784550999', 0, 8829, 0, 0, '', 0, '', 45, 1509702310, 1509647400, 'visa'),
(62, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 91, 'shobhit.jhalani@gmail.com', '', 'Jaipur', '9784550999', 0, 8829, 0, 0, '', 0, '', 45, 1509702593, 1509647400, 'visa'),
(63, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 91, 'shobhit.jhalani@gmail.com', '', 'Jaipur', '9784550999', 0, 8829, 0, 0, '', 0, '', 45, 1509702743, 1509647400, 'visa'),
(64, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 91, 'shobhit.jhalani@gmail.com', '', 'Jaipur', '9784550999', 0, 8829, 0, 0, '', 0, '', 45, 1509702809, 1509647400, 'visa'),
(65, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 91, 'shobhit.jhalani@gmail.com', '', 'Jaipur', '9784550999', 0, 8829, 0, 0, '', 0, '', 45, 1509702833, 1509647400, 'visa'),
(66, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 92, 'shobhit.jhalani@gmail.com', '', 'Jaipur', '9784550999', 0, 8322, 0, 0, '', 0, '', 45, 1509702880, 1509647400, 'visa'),
(67, 'Samarth Kohli', '', '', '', 93, 'fastwaytrips@gmail.com', '', '', '7696324223', 0, 8931, 0, 0, '', 0, '', 41, 1509705701, 1509647400, 'visa'),
(68, 'Samarth Kohli', '', '', '', 94, 'samarth@yourdubaivisa.com', '', '', '7696324223', 0, 8566, 0, 0, '', 0, '', 43, 1509707205, 1509647400, 'visa'),
(69, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 95, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 5784, 0, 0, '', 0, '', 45, 1509710126, 1509647400, 'visa'),
(70, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 95, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 5784, 0, 0, '', 0, '', 45, 1509710151, 1509647400, 'visa'),
(71, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 96, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 6292, 0, 0, '', 0, '', 45, 1509711760, 1509647400, 'visa'),
(72, 'Samarth Kohli', '', '', '', 100, 'fastwaytrips@gmail.com', '', '', '7696324223', 0, 6393, 0, 0, '', 0, '', 41, 1509853087, 1509820200, 'visa'),
(73, 'Ashish Kumar', 'Vi9ll- Kothar\r\nPO-Piplidhar Dagar,  tehri Garwal\r\nPin-249161, uttarakhand,      india.', 'Uttarakhand', '', 108, 'anix.niraula@gmail.com', '', ' Uttarakhand', '9911654498', 0, 17659, 0, 0, '', 0, '', 64, 1509954371, 1509906600, 'visa'),
(74, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 111, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 7002, 0, 0, '', 0, '', 45, 1509969421, 1509906600, 'visa'),
(75, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 111, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 7002, 0, 0, '', 0, '', 45, 1509969495, 1509906600, 'visa'),
(76, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 111, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 7002, 0, 0, '', 0, '', 45, 1509969511, 1509906600, 'visa'),
(77, 'Shobhit Jhalani', 'Jaipur', 'Jaipur', '', 111, 'shobhit.jhalani@gmail.com', '', 'Rajasthan', '9784550999', 0, 7002, 0, 0, '', 0, '', 45, 1509969565, 1509906600, 'visa'),
(78, 'Samarth Kohli', '', '', '', 112, 'fastwaytrips@gmail.com', '', '', '7696324223', 0, 6246, 0, 0, '', 0, '', 41, 1509969920, 1509906600, 'visa'),
(79, 'Samarth Kohli', '', '', '', 113, 'fastwaytrips@gmail.com', '', '', '7696324223', 2, 102, 0, 0, '106296355758', 749, 'Debit Card - Visa Debit Card', 41, 1509970177, 1509906600, 'visa'),
(80, 'Samarth Kohli', '', '', '', 137, 'fastwaytrips@gmail.com', '', '', '7696324223', 0, 5784, 0, 0, '', 0, '', 41, 1510384575, 1510338600, 'visa'),
(81, 'VIJAY KUMAR CHOWDHARY', '123 CBI Colony\r\nJagatpura', 'JAIPUR', '', 141, 'chowdharyvijay@yahoo.co.in', '', 'Rajasthan', '9414056762', 2, 12584, 0, 0, '106298496757', 43974, 'Credit Card - Visa', 71, 1510470781, 1510425000, 'visa'),
(82, 'Ravi Chandak', 'Plot No. 48, Ward No. 12, Sunder Nagar, Supela\r\nVaishali Nagar, Bhilai', 'Durg', '', 142, 'engser.in@gmail.com', '', 'Chhattisgarh', '9425293353', 2, 36054, 0, 0, '106298576688', 54183014, 'Net Banking - Bank of India', 62, 1510487172, 1510425000, 'visa'),
(83, 'ANSHUL SACHAR', 'C-20 FF SOAMI NAGAR NORTH', 'NEW DELHI', '', 158, 'sacharanshul@gmail.com', '', 'DELHI', '9891073250', 2, 6393, 0, 0, '106299639893', 292290, 'Credit Card - Amex', 99, 1510744752, 1510684200, 'visa'),
(84, 'ISHU ', 'VILLAGE NAGAR POST OFFICE TALWARA TEHSIL MUKERIAN DISTT HOSHIARPUR PUNJAB -144216', 'HOSHIARPUR', '', 169, 'amit.bhardwaj35@gmail.com', '', 'PUNJAB', '9417416353', 2, 6572, 0, 0, '106300358493', 0, 'Net Banking - State Bank of India', 107, 1510924840, 1510857000, 'visa'),
(85, 'Sargam ', '86/97, Risaldar Park Near Budh Mandir,', 'Lucknow', '', 168, 'sargamc7@gmail.com', '', 'Uttar Pradesh', '9999276675', 0, 8322, 0, 0, '', 0, '', 106, 1510926279, 1510857000, 'visa'),
(86, 'Sargam ', '86/97, Risaldar Park Near Budh Mandir,', 'Lucknow', '', 168, 'sargamc7@gmail.com', '', 'Uttar Pradesh', '9999276675', 0, 8322, 0, 0, '', 0, '', 106, 1510926359, 1510857000, 'visa'),
(87, 'Ryan Vijay Henry Mathias', 'Arun Niwas, Room no 8, Godavari Mhatre Road, \r\nOpposite Jai Ganga Apt, Dahisar - West \r\n', 'Mumbai', '', 191, 'mathiashryan@gmail.com', '', 'Maharashtra', '8149702227', 0, 19986, 0, 0, '', 0, '', 125, 1511327201, 1511289000, 'visa'),
(88, 'Ryan Vijay Henry Mathias', 'Arun Niwas, Room no 8, Godavari Mhatre Road, \r\nOpposite Jai Ganga Apt, Dahisar - West \r\n', 'Mumbai', '', 191, 'mathiashryan@gmail.com', '', 'Maharashtra', '8149702227', 0, 19986, 0, 0, '', 0, '', 125, 1511327288, 1511289000, 'visa'),
(89, 'Ryan Vijay Henry Mathias', 'Arun Niwas, Room no 8, Godavari Mhatre Road, \r\nOpposite Jai Ganga Apt, Dahisar - West \r\n', 'Mumbai', '', 191, 'mathiashryan@gmail.com', '', 'Maharashtra', '8149702227', 0, 19986, 0, 0, '', 0, '', 125, 1511327300, 1511289000, 'visa'),
(90, 'Ryan Vijay Henry Mathias', 'Arun Niwas, Room no 8, Godavari Mhatre Road, \r\nOpposite Jai Ganga Apt, Dahisar - West \r\n', 'Mumbai', '', 191, 'mathiashryan@gmail.com', '', 'Maharashtra', '8149702227', 2, 19986, 0, 0, '106301833447', 2147483647, 'Net Banking - HDFC Bank', 125, 1511327417, 1511289000, 'visa'),
(91, 'Prashant Shah', '', '', '', 204, 'prashant@ator.in', '', '', '9004844440', 2, 34707, 0, 0, '106302330108', 23666, 'Credit Card - Visa', 131, 1511433292, 1511375400, 'visa'),
(92, 'Iman Das', '103 Block A anu apartments Opp Ujjwal School Hope Farm, whitefield ', 'bengaluru', '', 211, 'iman.das1@gmail.com', '', 'Karnataka', '9900302306', 2, 6028, 0, 0, '106302784947', 25535, 'Credit Card - MasterCard', 121, 1511532168, 1511461800, 'visa'),
(93, 'Nalin  Swami', '301 Ashajeevan Malwani no. 8 Malad west', 'Mumbai', '', 222, 'swami.nalin01@gmail.com', '', 'Maharashtra', '9167515049', 2, 8829, 0, 0, '106303122848', 536008385, 'Net Banking - Axis Bank', 139, 1511614654, 1511548200, 'visa'),
(94, 'Kumar Akshay', 'Flat G304, Saraswati Apartment, Madhu Vihar, IP Extension ', 'New Delhi', '', 220, 'kumarakshay2003@gmail.com', '', 'Delhi', '7755054958', 0, 13852, 0, 0, '', 0, '', 132, 1511615470, 1511548200, 'visa'),
(95, 'Gaurav Sherkhane', 'C208, Kalyan Nagari, Kalyan west', 'Thane', '', 225, 'gaurav.sherkhane@gmail.com', '', 'Maharashtra', '8108087499', 2, 17353, 0, 0, '106303294588', 18959, 'Credit Card - Visa', 133, 1511674676, 1511634600, 'visa'),
(96, 'Gaurav Kumar', 'B-1, Bhoti Devi Apartment, Near Pandit Mohalla, Aya Nagar', 'New Delhi', '', 233, 'keenra4u@gmail.com', '', 'Delhi', '9654441545', 0, 17353, 0, 0, '', 0, '', 144, 1511781234, 1511721000, 'visa'),
(97, ' ', '', '', '', 0, '', '', '', '', 0, 0, 0, 0, '', 0, '', 0, 1511784011, 1511721000, 'visa'),
(98, ' ', '', '', '', 0, '', '', '', '', 0, 0, 0, 0, '', 0, '', 0, 1511784012, 1511721000, 'visa'),
(99, ' ', '', '', '', 0, '', '', '', '', 0, 0, 0, 0, '', 0, '', 0, 1511784016, 1511721000, 'visa'),
(100, ' ', '', '', '', 0, '', '', '', '', 0, 0, 0, 0, '', 0, '', 0, 1511784018, 1511721000, 'visa'),
(101, 'Gaurav Kumar', 'B-1, Bhoti Devi Apartment, Near Pandit Mohalla, Aya Nagar', 'New Delhi', '', 238, 'keenra4u@gmail.com', '', 'Delhi', '9654441545', 2, 17353, 0, 0, '106303777072', 910392, 'Credit Card - MasterCard', 144, 1511784308, 1511721000, 'visa'),
(102, 'Boris  Kenneth', '#1020/b 2nd Floor 17th D Cross, Indira Nagar\r\nEshwara Layout', 'Bangalore', '', 254, 'boris414.m@gmail.com', '', 'Karnataka', '9980181588', 0, 13873, 0, 0, '', 0, '', 161, 1512038800, 1511980200, 'visa'),
(103, 'Boris  Kenneth', '#1020/b 2nd Floor 17th D Cross, Indira Nagar\r\nEshwara Layout', 'Bangalore', '', 254, 'boris414.m@gmail.com', '', 'Karnataka', '9980181588', 0, 12787, 0, 0, '', 0, '', 161, 1512038990, 1511980200, 'visa'),
(104, 'Boris  Kenneth', '#1020/b 2nd Floor 17th D Cross, Indira Nagar\r\nEshwara Layout', 'Bangalore', '', 254, 'boris414.m@gmail.com', '', 'Karnataka', '9980181588', 2, 12787, 0, 0, '7361203483', 2147483647, 'NB', 161, 1512039027, 1511980200, 'visa'),
(105, 'Justin Castelino', 'A703 Shantidoot CHS, Charkop Kandivali West, Mumbai 400067', 'Mumbai', '', 263, 'justin.castelino@timesgroup.com', '', 'Maharashtra', '8291036580', 2, 12523, 0, 0, '106305677705', 8024, 'Credit Card - Visa', 166, 1512202216, 1512153000, 'visa'),
(106, 'RITESHKUMAR RADHESHYAM ', 'P2/9, DEEP SADAN CHS, SUNDER NAGAR S V ROAD MALAD WEST', 'MUMBAI', '', 262, 'mailtoalienterprises@yahoo.com', '', 'Maharashtra', '8108502239', 0, 6393, 0, 0, '', 0, '', 168, 1512202361, 1512153000, 'visa'),
(107, 'Justin Castelino', '', '', '', 264, 'justin.castelino@timesgroup.com', '', '', '8291036580', 0, 12523, 0, 0, '', 0, '', 166, 1512203090, 1512153000, 'visa'),
(108, 'Achint  Arora', 'B-504, DELHI STATE C.G.H.S, PLOT NO.1, SEC-19, DWARKA', 'NEW DELHI', '', 268, 'pgp12.achint@spjimr.org', '', 'DELHI', '9930940943', 0, 13132, 0, 0, '', 0, '', 112, 1512292763, 1512239400, 'visa'),
(109, 'Amit Sudheer Deshpande', '1802, BEAUTY PALMS CHS, KOLBAD, KHOPAT', 'THANE', '', 275, 'amitsd@gmail.com', '', 'MAHARASHTRA', '9820527250', 2, 18267, 0, 0, '106307350771', 8103, 'Debit Card - Visa Debit Card', 173, 1512561270, 1512498600, 'visa'),
(110, 'Kahan Taraporevala', 'B3, Amalfi 15, L D Ruparel RD\r\nMalabar Hill', 'Mumbai', '', 298, 'kahantara11@gmail.com', '', 'Maharashtra', '9619424392', 2, 5784, 0, 0, '106310217835', 699352, 'Debit Card - MasterCard Debit Card', 186, 1513165136, 1513103400, 'visa'),
(111, 'Kahan Taraporevala', 'B3, Amalfi 15, L D Ruparel RD\r\nMalabar Hill', 'Mumbai', '', 299, 'kahantara11@gmail.com', '', 'Maharashtra', '9619424392', 2, 8322, 0, 0, '106310223078', 710991, 'Debit Card - MasterCard Debit Card', 186, 1513165879, 1513103400, 'visa');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service`
--

CREATE TABLE IF NOT EXISTS `tbl_service` (
  `ser_id` int(11) NOT NULL AUTO_INCREMENT,
  `ser_title` varchar(255) NOT NULL,
  `ser_sac_code` varchar(255) NOT NULL,
  `ser_gst` varchar(255) NOT NULL,
  `ser_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ser_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_service`
--

INSERT INTO `tbl_service` (`ser_id`, `ser_title`, `ser_sac_code`, `ser_gst`, `ser_status`) VALUES
(5, 'VISA & OTB', '0', '0', 1),
(6, 'Visa', '0', '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_slider` (
  `sldr_id` int(11) NOT NULL AUTO_INCREMENT,
  `sldr_title` varchar(255) NOT NULL,
  `sldr_status` tinyint(2) NOT NULL DEFAULT '0',
  `sldr_image` varchar(255) NOT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sldr_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`sldr_id`, `sldr_title`, `sldr_status`, `sldr_image`, `is_delete`) VALUES
(1, '2', 1, 's1_1507393821.jpg', 1),
(2, '1', 1, 's1_1506235925.jpg', 1),
(3, 'Get your visa in your hands Just in 24hrs', 1, 's1_1506151588.jpg', 1),
(4, 'd', 1, 's1_1507294530.jpg', 1),
(5, 'a', 1, 's1_1507294585.jpg', 1),
(6, '3', 1, 's1_1507393937.jpg', 1),
(7, '4', 1, 's1_1507394610.jpg', 1),
(8, '5', 1, 's1_1508309094.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sms_credential`
--

CREATE TABLE IF NOT EXISTS `tbl_sms_credential` (
  `sms_id` int(11) NOT NULL AUTO_INCREMENT,
  `sms_name` varchar(255) NOT NULL,
  `sms_username` varchar(255) NOT NULL,
  `sms_password` varchar(255) NOT NULL,
  `sms_url` text NOT NULL,
  `sms_status` tinyint(4) NOT NULL DEFAULT '0',
  `sms_delete_status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sms_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_sms_credential`
--

INSERT INTO `tbl_sms_credential` (`sms_id`, `sms_name`, `sms_username`, `sms_password`, `sms_url`, `sms_status`, `sms_delete_status`) VALUES
(2, 'YDVISA', 'nikhil.sood@live.com', 'nickarc1120', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sms_record`
--

CREATE TABLE IF NOT EXISTS `tbl_sms_record` (
  `tsr_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsr_type` tinyint(4) NOT NULL,
  `tsr_msg` varchar(255) NOT NULL,
  `tsr_status` varchar(255) NOT NULL,
  `tsr_name` varchar(255) NOT NULL,
  `tsr_no` varchar(20) NOT NULL,
  `tsr_url` text NOT NULL,
  PRIMARY KEY (`tsr_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `tbl_sms_record`
--

INSERT INTO `tbl_sms_record` (`tsr_id`, `tsr_type`, `tsr_msg`, `tsr_status`, `tsr_name`, `tsr_no`, `tsr_url`) VALUES
(1, 3, 'Dear  Your visa application YDV17104528 is  InProcess.It would take 4-5 business days to get the approval. Regards DubaieVISA.in', '', '', '', 'URL?receipientno=&msgtxt=Dear++Your+visa+application+YDV17104528+is++InProcess.It+would+take+4-5+business+days+to+get+the+approval.+Regards+DubaieVISA.in&user=username%3Apassword&senderID=YDVISA'),
(2, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17101861 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17101861+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(3, 1, 'Dear Samarth Congrats! You visa with reference no YDV17101861 has been approved and visa copy has been sent to your registered email id. Regards Team YourDubaiVisa', 'Status=1,Template not matched\r\n', 'Samarth', '7696324223', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=7696324223&msgtxt=Dear+Samarth+Congrats%21+You+visa+with+reference+no+YDV17101861+has+been+approved+and+visa+copy+has+been+sent+to+your+registered+email+id.+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(4, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17107499 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17107499+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(5, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17109540 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17109540+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(6, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17109540 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17109540+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(7, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17109540 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17109540+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(8, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17109540 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17109540+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(9, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17109540 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17109540+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(10, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17102596 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17102596+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(11, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17109861 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17109861+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(12, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17109861 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17109861+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(13, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17109861 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17109861+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(14, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17104751 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17104751+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(15, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17104751 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17104751+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(16, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17104751 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17104751+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(17, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17104751 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17104751+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(18, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17104751 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17104751+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(19, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17104751 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17104751+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(20, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17104751 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17104751+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(21, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17104751 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17104751+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(22, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17104751 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17104751+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(23, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17107115 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17107115+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(24, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17107115 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17107115+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(25, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17107115 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17107115+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(26, 3, 'Dear Samarth We have received your visa application and it is successfully submitted vide reference No YDV17103271 Regards Team YourDubaiVisa', 'Status=1,Template not matched\r\n', 'Samarth', '7696324223', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=7696324223&msgtxt=Dear+Samarth+We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17103271+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(27, 3, 'Dear  We have received your visa application and it is successfully submitted vide reference No YDV17103271 Regards Team YourDubaiVisa', 'Status=1, Receipient Number does not exist\r\n', '', '', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=&msgtxt=Dear++We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17103271+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(28, 1, 'Dear Samarth Congrats! You visa with reference no YDV17103271 has been approved and visa copy has been sent to your registered email id. Regards Team YourDubaiVisa', 'Status=1,Template not matched\r\n', 'Samarth', '7696324223', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=7696324223&msgtxt=Dear+Samarth+Congrats%21+You+visa+with+reference+no+YDV17103271+has+been+approved+and+visa+copy+has+been+sent+to+your+registered+email+id.+Regards+Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA'),
(29, 3, 'Dear Boris  , We have received your visa application and it is successfully submitted vide reference No YDV17114958 .  Regards  Team YourDubaiVisa', 'Status=0,ins361_15120391014385\r\n', 'Boris ', '9980181588', 'http://api.mVaayoo.com/mvaayooapi/MessageCompose?receipientno=9980181588&msgtxt=Dear+Boris++%2C+We+have+received+your+visa+application+and+it+is+successfully+submitted+vide+reference+No+YDV17114958+.++Regards++Team+YourDubaiVisa&user=nikhil.sood%40live.com%3Anickarc1120&senderID=YDVISA');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_state`
--

CREATE TABLE IF NOT EXISTS `tbl_state` (
  `StateID` int(11) NOT NULL AUTO_INCREMENT,
  `CountryID` varchar(11) NOT NULL,
  `StateName` varchar(50) NOT NULL,
  PRIMARY KEY (`StateID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `tbl_state`
--

INSERT INTO `tbl_state` (`StateID`, `CountryID`, `StateName`) VALUES
(1, 'India', 'Andhra Pradesh'),
(2, 'India', 'Assam'),
(3, 'India', 'Arunachal Pradesh'),
(4, 'India', 'Gujarat'),
(5, 'India', 'Bihar'),
(6, 'India', 'Haryana'),
(7, 'India', 'Himachal Pradesh'),
(8, 'India', 'Jammu & Kashmir'),
(9, 'India', 'Karnataka'),
(10, 'India', 'Kerala'),
(11, 'India', 'Madhya Pradesh'),
(12, 'India', 'Maharashtra'),
(13, 'India', 'Manipur'),
(14, 'India', 'Meghalaya'),
(15, 'India', 'Mizoram'),
(16, 'India', 'Nagaland'),
(17, 'India', 'Orissa'),
(18, 'India', 'Punjab'),
(19, 'India', 'Rajasthan'),
(20, 'India', 'Sikkim'),
(21, 'India', 'Tamil Nadu'),
(22, 'India', 'Tripura'),
(23, 'India', 'Uttar Pradesh'),
(24, 'India', 'West Bengal'),
(25, 'India', 'Delhi'),
(26, 'India', 'Goa'),
(27, 'India', 'Pondichery'),
(28, 'India', 'Lakshdweep'),
(29, 'India', 'Daman & Diu'),
(30, 'India', 'Dadra & Nagar'),
(31, 'India', 'Chandigarh'),
(32, 'India', 'Andaman & Nicobar'),
(33, 'India', 'Uttaranchal'),
(34, 'India', 'Jharkhand'),
(35, 'India', 'Chattisgarh');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subscribe`
--

CREATE TABLE IF NOT EXISTS `tbl_subscribe` (
  `subr_id` int(11) NOT NULL AUTO_INCREMENT,
  `subr_email` varchar(255) NOT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`subr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_takedeal`
--

CREATE TABLE IF NOT EXISTS `tbl_takedeal` (
  `take_id` int(11) NOT NULL AUTO_INCREMENT,
  `take_name` varchar(100) NOT NULL,
  `take_email` varchar(60) NOT NULL,
  `take_mobile` bigint(10) NOT NULL,
  `take_message` varchar(255) NOT NULL,
  `take_ip` varchar(255) NOT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`take_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testimonials`
--

CREATE TABLE IF NOT EXISTS `tbl_testimonials` (
  `tsm_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsm_sessionid` int(11) NOT NULL,
  `tsm_message` text NOT NULL,
  `tsm_image` varchar(255) NOT NULL DEFAULT 'avtar.png',
  `tsm_insert_date` int(11) NOT NULL,
  `tsm_status` tinyint(2) NOT NULL DEFAULT '0',
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  `tsm_name` varchar(255) NOT NULL,
  PRIMARY KEY (`tsm_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_testimonials`
--

INSERT INTO `tbl_testimonials` (`tsm_id`, `tsm_sessionid`, `tsm_message`, `tsm_image`, `tsm_insert_date`, `tsm_status`, `is_delete`, `tsm_name`) VALUES
(1, 0, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever.', 'tms_1507551757.png', 1507487400, 1, 1, 'Venna'),
(2, 0, 'Very Nice work done by team apperciate the work done ', 'tms_1507393483.jpg', 1507314600, 1, 0, 'Palwi'),
(3, 0, 'Thanks for the hassle free issue of visa.\r\nI really appreciate the ease by which this visa was issued.\r\n', '', 1511893800, 1, 0, 'GAURAV');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_details`
--

CREATE TABLE IF NOT EXISTS `tbl_user_details` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_title` varchar(15) NOT NULL,
  `u_name` varchar(255) NOT NULL,
  `u_lname` varchar(255) NOT NULL,
  `u_emailid` varchar(255) NOT NULL,
  `u_country_code` varchar(5) NOT NULL DEFAULT 'IN',
  `u_mobileno` bigint(10) NOT NULL,
  `u_password` varchar(255) NOT NULL,
  `u_dor` int(11) NOT NULL,
  `u_wallet_bal` decimal(20,2) NOT NULL,
  `u_dob` int(11) NOT NULL,
  `u_status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0=inactive 1=imactive',
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  `u_state` varchar(100) NOT NULL,
  `u_city` varchar(100) NOT NULL,
  `u_address` varchar(255) NOT NULL,
  `u_pincode` varchar(10) NOT NULL,
  `u_photo` varchar(255) NOT NULL,
  `u_fbid` varchar(50) DEFAULT '0',
  `u_ip` varchar(255) NOT NULL,
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `u_emailid` (`u_emailid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=190 ;

--
-- Dumping data for table `tbl_user_details`
--

INSERT INTO `tbl_user_details` (`u_id`, `u_title`, `u_name`, `u_lname`, `u_emailid`, `u_country_code`, `u_mobileno`, `u_password`, `u_dor`, `u_wallet_bal`, `u_dob`, `u_status`, `is_delete`, `u_state`, `u_city`, `u_address`, `u_pincode`, `u_photo`, `u_fbid`, `u_ip`) VALUES
(1, 'test', 'Test_User', 'ltest', 'test@gmail.com', 'IN', 9024699937, 'e10adc3949ba59abbe56e057f20f883e', 123456, 122345.00, 0, 1, 0, 'Rajasthan', 'Jaipur', 'Jaipur', '302016', '', '0', '122.161.24.239'),
(41, '', 'Samarth', 'Kohli', 'fastwaytrips@gmail.com', '+91', 7696324223, 'b576146513b012e039cad3f1d6aebaba', 1507540016, 0.00, 0, 1, 0, '', '', '', '', '', '0', '110.225.199.14'),
(42, '', 'test', '', 'test@tets.com', '+91', 9876543211, '51a9d10d55cb5d299a65288e2d2fdbef', 1507609379, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(43, '', 'gh', 'gf', 'samarth@yourdubaivisa.com', '+91', 7696324223, 'b29fcbaa87b5e6283fed53e1576a23d3', 1507633695, 0.00, 0, 1, 0, '', '', '', '', '', '0', '110.225.199.14'),
(44, '', 'asd', '', 'asdasd@gmail.com', '+91', 4564567891, 'b4e91dc04065bce1fd9b2f3b7741c890', 1507639143, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(45, '', 'Shobhit', 'Jhalani', 'shobhit.jhalani@gmail.com', '+91', 9784550999, 'dfedcf7022af55bcb7c54d002ecf1410', 1507733707, 0.00, 0, 1, 0, 'Rajasthan', 'Jaipur', 'Jaipur', '302016', '', '0', '171.50.131.218'),
(46, '', 'test', '', 'wertyu@rtyu.com', '+91', 9876543211, 'd6f1aa906bac56ad79f84a2f5275771d', 1507785886, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(47, '', 'Srishti', 'Kumawat', 'support@myisync.com', '+91', 8290550999, '07f2674b97cb30d33e753b14c53cc08e', 1507807972, 0.00, 0, 1, 0, 'Rajasthan', 'Jaipur', 'Bani Park', '302006', '', '0', '122.161.68.191'),
(48, '', 'Samarth', '', 'kumar.kachhadiya@paladion.net', '+91', 7696324223, 'd1465fcce63e50cb7330af4e93e60e23', 1507885465, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(49, '', 'Ru', '', 'ru@gmail.com', '+91', 9999999999, 'aac596e616d339a902ad67284ee4e958', 1509016126, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(50, '', 'dvfg', 'fasf', 'abc@gmail.com', '+91', 7696324423, 'd9c44e6d0fe4b0db87dd9f8de25d520b', 1509101391, 0.00, 0, 1, 0, '', '', '', '', '', '0', '110.225.211.128'),
(51, '', 'gyjg', '', 'ygg@gmail.com', '+91', 123456789098, '2e5918ba543e05ad3b88ae478fc6d726', 1509447747, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(52, '', 'Khalil ', 'Ansari', 'najeeb271@hotmail.com', '+91', 8879534724, '6e40f955280457eb998727b51e7e774c', 1509530981, 0.00, 0, 1, 0, 'maharashtra', 'Mumbai', '503/ B Silver Arch, Dr. P.L.Deshpande Rd., Oshiwara, Andheri (W)', '400053', '', '0', '202.134.152.101'),
(53, '', 'FAIYAZ MAJID ', '', 'fmmodi@yahoo.com', '+91', 9722840899, 'f22272db4d330bc6bf112ace489c66f7', 1509533038, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(54, '', 'SUHAS', '', 'adith_power@yahoo.com', '+91', 9820054128, '5660681d7c65e9239d3088f4af4ad627', 1509533199, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(55, '', 'Samarth', 'Kohli', 'sbkpgw@gmail.com', '+91', 7696324223, '25ffa8c3790a8325313aa4a3681dc0b7', 1509534967, 0.00, 0, 1, 0, '', '', '', '', '', '0', '110.225.209.193'),
(56, '', 'Shobhit', '', 'shobhit.jhala.ni@gmail.com', '+91', 9784550999, '38d76544746584b2b1949ca1ab27c844', 1509711835, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(57, '', 'Shobhit', 'Jhalani', 'shobhitjhalani@gmail.com', '+91', 9784550999, '44db502e483e0229bc5943bb10ce203a', 1509722264, 0.00, 0, 1, 0, '', '', '', '', '', '0', '122.161.67.40'),
(58, '', 'Rahul', '', 'brat.naukri@gmail.com', '+91', 9878786712, 'b65b185572d82ad143b924bbeb6ebbcf', 1509804656, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(59, '', 'zia', '', 'ziamehmood502@gmail.com', '+91', 7051206916, '5c5555fa092840ec8c790d3759d243b3', 1509876093, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(60, '', 'Sahil', '', 'sahilmangwani17@gmail.com', '+91', 9826355005, 'a1812fbad38ff431b440bf8880ea3862', 1509947549, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(61, '', 'CHANDRA SHEKHAR', '', 'chandramishra67@gmail.com', '+91', 8209553765, '1e0c45e0738f54c0e82a74493bf35a6d', 1509947705, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(62, '', 'Ravi', 'Chandak', 'engser.in@gmail.com', '+91', 9425293353, '7b0be1782eae5d26c310e41776b1cfac', 1509948683, 0.00, 0, 1, 0, 'Chhattisgarh', 'Durg', 'Plot No. 48, Ward No. 12, Sunder Nagar, Supela\r\nVaishali Nagar, Bhilai', '490023', '', '0', '171.49.157.218'),
(63, '', 'x', '', 'x', '+91', 0, 'c81ea2e62d6ccfe2ee0a98fbb49e1827', 1509951516, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(64, '', 'Ashish', 'Kumar', 'anix.niraula@gmail.com', '+91', 9911654498, '63eccbf5a2d97e3e5d343e9220248f6c', 1509952314, 0.00, 0, 1, 0, ' Uttarakhand', 'Uttarakhand', 'Vi9ll- Kothar\r\nPO-Piplidhar Dagar,  tehri Garwal\r\nPin-249161, uttarakhand,      india.', '249161', '', '0', '103.69.6.46'),
(65, '', 'Amardeep sharma ', 'Sharma', 'bali.rama1990@gmail.com', '+91', 9557614011, '12617075ec3401eed70076b8c8d450d3', 1509975310, 0.00, 0, 1, 0, '', '', '', '', '', '0', '27.60.68.23'),
(66, '', 'Aqib', '', 'alhamadoverseas@gmail.com', '+91', 9419375226, '1465e2b543ad78b918336e50c26d14e6', 1510045875, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(67, '', 'Ashish', 'Agarwal', 'ash.csc.07@gmail.com', '+91', 8146160985, '389871ce7c14bbf494e2679566253399', 1510052460, 0.00, 0, 1, 0, '', '', '', '', '', '0', '165.225.104.85'),
(68, '', 'fsdadsfasd', '', 'adsasdada', '+91', 0, '72a7e7ad4d71bbc9185975eefa2cc7a7', 1510054538, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(69, '', 'PARAG', '', 'parag_jain21@rediffmail.com', '+91', 9826053803, '6b1337f91d6b23093bb3231ce5a7deab', 1510058351, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(70, '', 'dsfadsfa', '', 'rohit@gmail.com', '+91', 9769382292, '766cfa7d4ec62e15ed41b90c306e116a', 1510206199, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(71, '', 'VIJAY KUMAR', 'CHOWDHARY', 'chowdharyvijay@yahoo.co.in', '+91', 9414056762, '20da3267f7541b1b705a702bc77bdb68', 1510206447, 0.00, 0, 1, 0, 'Rajasthan', 'JAIPUR', '123 CBI Colony\r\nJagatpura', '302017', '', '0', '223.188.155.4'),
(72, '', 'SUCHA SINGH ', '', 'harman.multani@ymail.com', '+91', 9417744038, '395df7849c30b8f5000b669c8c28720e', 1510207206, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(73, '', 'dfzxcdZF', '', 'sadd', '+91', 0, '2e90a68d5e4598973e076f683b4781a5', 1510211660, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(74, '', 'Chintan', '', 'chintan@lumens.co.in', '+91', 9821074684, '0deedd41deed5163d4579c5f8469f016', 1510214145, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(75, '', 'suddu kumar', '', 'kumarvikash52750@gmail.com', '+91', 9576551093, '9d5513ca5890c61b3f9a724d71d4e8d8', 1510288556, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(76, '', 'Durgesh ', '', 'dk499385@gmail.com ', '+91', 9838347605, '118463a1faec28ebdc81c7509599a060', 1510290896, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(77, '', 'mohd', '', 'mdsaif8315@gmail.com', '+91', 9966488315, '4616a2e9357c2f12c8f41a39407bea59', 1510291515, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(78, '', 'harpal', '', 'cute.harpal@gmail.com', '+91', 9254413013, '43b68e61bbe97d0dd54e251013119d2d', 1510292596, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(79, '', 'Rahul', '', 'jal.rag@gmail.com', '+91', 9876512345, '5015e50f4a9ea385c7f5da516b2f9593', 1510373100, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(80, '', 'Raman Senthil', '', 'zarna-india@zoomlion.com', '+91', 9769108708, '73f8409b2e1353dbf75b2f66bbfb1243', 1510382435, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(81, '', 'Sibu', '', 'sibu1992raipur@gmail.com', '+91', 8602064560, '68c4791690985bd20836c26e5d6c3f4d', 1510383736, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(82, '', 'HUSAIN', '', 'hasaify52@gmail.com', '+91', 8347047126, '9c37c1697b3f0f544ccd17e301da938a', 1510399206, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(83, '', 'Jaimin G.', '', 'jaimin.95@gmail.com', '+91', 9898730842, 'f8c0c14cb91db709b784cf2bbd8400b8', 1510404219, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(84, '', 'Jaimin G.', '', 'jaimin.', '+91', 9898730842, '35fe3aaf7880f249462a78aa44b95e86', 1510404401, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(85, '', 'pooja', '', 'poojata74@gmail.com', '+91', 8826406303, '779f466d84cf724c9f4ea4cb171c23f1', 1510565730, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(86, '', 'lalit', '', 'mansi.sachar24sep@gmail.com', '+91', 9999829887, 'b37755bd533e34cdc0ee544d98277b33', 1510565905, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(87, '', 'Guna', '', 'gunamuthu95@gmail.com ', '+91', 8344741406, '3035fdc5452f478cb6101df781b13a22', 1510566231, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(88, '', 'Murtaza ', '', 'murtazatiger52@gmailcom', '+91', 8305271672, '2bccf8c7ccec0c59fcb90bca93c6f3d3', 1510566718, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(89, '', 'Aayushi', '', 'ankittayal86@gmail.com', '+91', 9752545890, '97fb8d0caf248123f99caf9fc9b303c1', 1510567932, 0.00, 0, 1, 0, 'Goa', 'GOA', 'FLAT NO 2/T-1, models mystique apart\r\ncaranzalem, panjim', '403002', '', '0', ''),
(90, '', 'happy', '', 'happychotu25@gmail.com', '+91', 9992648726, 'd25d58810227f7d21831040458e08a70', 1510569195, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(91, '', 'suresh', '', 'pentamsuresh@gmail.com', '+91', 9849726739, '8cf289f16f33b359f65dca76f5380b04', 1510569930, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(92, '', 'Ravivarman', '', 'trravi04@gmail.com', '+91', 9688474555, '4eda6c825d359b3b46216aa5c5223d1b', 1510576528, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(93, '', 'sharath', '', 'csr8296443391@gmail.co', '+91', 8296443391, '4ef1b49bc85e3a01105eb615f912c0a7', 1510641907, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(94, '', 'naga', '', 'csr8296443391@gmail.com', '+91', 8296443391, '9cf728b0e22c6db6a75af2f50c2b0bbe', 1510643483, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(95, '', 'Anudeep', '', 'anu.709699@gmail.com', '+91', 9441709699, '1ab7628a376ae5448d0b51c2c6a0a22f', 1510645585, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(96, '', 'Suraj Kumar ', '', 'suraj.k.deaf@gmail.com', '+91', 9636696004, '54c3957c8dfa39414aba9942a45a5fd0', 1510652167, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(97, '', 'Santosh kumar', '', 's568481162@gmail.com', '+91', 7906404160, 'dee85bc9b156d25439f94edc47b38237', 1510654737, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(98, '', 'add', '', 'ada@gmail.com', '+91', 7696324223, '567c4b375398ad9f5c58cf216e32f1dc', 1510742022, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(99, '', 'ANSHUL', 'SACHAR', 'sacharanshul@gmail.com', '+91', 9891073250, 'eda0ce710362a99f9cec2f322f3268ff', 1510743428, 0.00, 0, 1, 0, 'DELHI', 'NEW DELHI', 'C-20 FF SOAMI NAGAR NORTH', '110017', '', '0', '203.122.57.210'),
(100, '', 'Mohammad ', 'Hafeez', 'hafeezmohd214@gmail.com', '+91', 9026354733, 'e6d1a31ddcbfa7690072f24cddfc697b', 1510758206, 0.00, 0, 1, 0, '', '', '', '', '', '0', '101.222.7.127'),
(101, '', 'Gaurav', 'Shukla', 'inder.chaudhry@outcomess.com', '+91', 9990809840, 'cb9899576c15ab350c31b3f5b6583829', 1510829351, 0.00, 0, 1, 0, 'UTTAR PRADESH', 'GHAZIABAD', 'E-104, RAJHANS APARTMENT, AHINSA KHAND, 1, INDIRAPURAM ', '201010', '', '0', '122.162.237.59'),
(102, '', 'Ankit ', '', 'paliwalankit909@gmail.com', '+91', 9782718432, '5527f3ad7f9f5dd5f75972532f49bb09', 1510844190, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(103, '', 'Anwar', 'Basha', 'anwarbasha523@gmail.com', '+91', 9676248272, '1e6a73da6a0dada399c099469dc9357d', 1510844890, 0.00, 0, 1, 0, '', '', '', '', '', '0', '42.111.200.186'),
(104, '', 'karthick', '', 'rskarthickmba@gmail.com', '+91', 9600113873, 'ca08b59f8308bc436a835346f85c31bf', 1510845158, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(105, '', 'DEEPAK ', '', 'deepakshkl9@gmail.com', '+91', 9871171326, 'a1c98d5b0a1cab55d0f9875290a5c8eb', 1510920837, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(106, '', 'Sargam', '', 'sargamc7@gmail.com', '+91', 9999276675, '8125d0787351dbb39ca011cc63e32a37', 1510922642, 0.00, 0, 1, 0, 'Uttar Pradesh', 'Lucknow', '86/97, Risaldar Park Near Budh Mandir,', '226018', '', '0', ''),
(107, '', 'ISHU', '', 'amit.bhardwaj35@gmail.com', '+91', 9417416353, '9ffdfe8133744092da540a53bba6112e', 1510923851, 0.00, 0, 1, 0, 'PUNJAB', 'HOSHIARPUR', 'VILLAGE NAGAR POST OFFICE TALWARA TEHSIL MUKERIAN DISTT HOSHIARPUR PUNJAB -144216', '144216', '', '0', ''),
(108, '', 'Lovepreet ', '', 'preetramgharia84@gmail.com ', '+91', 9779547573, '0241350ff2f6781ddad3e83a1f8cb134', 1510924205, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(109, '', 'Sandip', '', 'de_sandeep@yahoo.com', '+91', 9819646079, '75780f1e39a2f37924a0aa787ebe05de', 1510929936, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(110, '', 'MAHENDRA KUMAR  SUGREEV CHAUHAN', '', 'mahendrachauhan02196@gmail.com', '+91', 9721374810, 'fcd19a911db329580d08fa388ae08cdb', 1510930698, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(111, '', 'Yusuf', 'Saifi', 'yusufsaifi1399@gmail.com', '+91', 9084116790, '1849ab2bb7c8b49caf0070fabe348565', 1510931409, 0.00, 0, 1, 0, '', '', '', '', '', '0', '136.185.182.193'),
(112, '', 'Achint ', 'Arora', 'pgp12.achint@spjimr.org', '+91', 9930940943, 'e0f7c3f489602e74e8672a6a59a58f38', 1510986793, 0.00, 0, 1, 0, 'DELHI', 'NEW DELHI', 'B-504, DELHI STATE C.G.H.S, PLOT NO.1, SEC-19, DWARKA', '110075', '', '0', '103.62.94.38'),
(113, '', 'ANURAG', '', 'anuragdixit2000@yahoo.com', '+91', 9811424511, '74ffe4273a885a0da4e8f813d57bfdec', 1511012567, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(114, '', 'Munawar', '', 'mohammad.munawar@yahoo.com', '+91', 9059678532, '31ff5d52099974c0f173be3cde55a022', 1511084825, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(115, '', 'Sumedh ', '', 'sumedh.rege@gmail.com', '+91', 9923060041, '5ff3035798d8e08c7ee70075c22f6c6c', 1511085580, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(116, '', 'Aman', '', 'amankaushikkannu@gmail.com', '+91', 9606844890, '3fe19be0d8a252c398c764f0f6276f2b', 1511087554, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(117, '', 'Navdeep', '', 'nk587384@gmail.com ', '+91', 9056799984, 'e22ea97ec3f7f8ee9966b8568bad8765', 1511089125, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(118, '', 'bvnnv', '', 'vbn@sdsd.com', '+91', 8678965456, '943adf3039f463b770759d61f60e35c3', 1511089499, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(119, '', 'Pettarusp', '', 'brkaranjia@gmail.com', '+91', 9769036661, 'e4f6232bae13990bb3d493ed73c3410a', 1511089809, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(120, '', 'Mohammed Abdul', '', 'wallstreet78@gmail.com', '+91', 8978761033, 'e1aba94b381b7626f9ee67b7f15b82e2', 1511102459, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(121, '', 'Iman', 'Das', 'iman.das1@gmail.com', '+91', 9900302306, '9cf0e97598ea47e7dbda0ba9853731d5', 1511167527, 0.00, 0, 1, 0, 'Karnataka', 'bengaluru', '103 Block A anu apartments Opp Ujjwal School Hope Farm, whitefield ', '560066', '', '0', '122.167.117.166'),
(122, '', 'Anam', '', 'anamkhan0691@gmail.com', '+91', 7982226298, 'a47d0306dc64d094a86ae5d028f82074', 1511167749, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(123, '', 'Prakash ', '', 'praky.ap@gmail.com', '+91', 9884029486, '9b71f34690ea29b4bee8dddd40d31768', 1511169478, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(124, '', 'SAURABH ', '', 'saurabhv@hotmail.com', '+91', 9958145276, 'f716f3e305876906054c37fae3052dd0', 1511246203, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(125, '', 'Ryan Vijay Henry', 'Mathias', 'mathiashryan@gmail.com', '+91', 8149702227, '09e62046628a36da5bd710b6157c8c39', 1511318125, 0.00, 0, 1, 0, 'Maharashtra', 'Mumbai', 'Arun Niwas, Room no 8, Godavari Mhatre Road, \r\nOpposite Jai Ganga Apt, Dahisar - West \r\n', '400068', '', '0', '76.98.159.101'),
(126, '', 'asfal', '', 'afsalt09@gmail.com', '+91', 9037972517, '8baa84c0135062af508a9128ef6739b1', 1511360637, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(127, '', 'arun', '', 'arun.kumar.103@gmail.com', '+91', 9866070564, 'e047a52bdb7d0c9acdd7486955301754', 1511363361, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(128, '', 'Shuaibur', 'Rahman', 'optomubaid@gmail.com', '+91', 918052561566, 'f5d2f4e34c0caf1babab88e41eb304f3', 1511363582, 0.00, 616789800, 1, 0, '', '', '', '', '', '0', '112.79.154.34'),
(129, '', 'abc', '', 'lucky199141@gmail.com', '+91', 9899647594, '9264b6b474019f580668f3641b9a6718', 1511417058, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(130, '', 'HARSH', '', 'ajayshukla@eastindiaudyog.com', '+91', 8750056001, '679c9d938d482f2be43642ec277eab34', 1511418625, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(131, '', 'Prashant', 'Shah', 'prashant@ator.in', '+91', 9004844440, 'dfe503d8c11cf0ff2cda1062be48406f', 1511420351, 0.00, 0, 1, 0, '', '', '', '', '', '0', '103.48.103.191'),
(132, '', 'Kumar', 'Akshay', 'kumarakshay2003@gmail.com', '+91', 7755054958, '9069c4a1873c0de769b56a5d5ff19b44', 1511436109, 0.00, 0, 1, 0, 'Delhi', 'New Delhi', 'Flat G304, Saraswati Apartment, Madhu Vihar, IP Extension ', '110092', '', '0', '103.12.135.170'),
(133, '', 'Gaurav', 'Sherkhane', 'gaurav.sherkhane@gmail.com', '+91', 8108087499, '112921ca27101e222bcac9ae11e88965', 1511524020, 0.00, 0, 1, 0, 'Maharashtra', 'Thane', 'C208, Kalyan Nagari, Kalyan west', '421301', '', '0', '137.59.69.83'),
(134, '', 'Swati', '', 'swati@globaltechnology.ind.in', '+91', 9811740909, 'a8aa29e3a40fce1f7ed66500afee5a41', 1511525918, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(135, '', 'suchi', 'rai', 'suchiraishikha@gmail.com', '+91', 7507005008, '0682786abee2b214616e57664f3b2eef', 1511526866, 0.00, 0, 1, 0, '', '', '', '', '', '0', '182.70.102.158'),
(136, '', 'Mandeep', '', 'mssaini2007@yahoo.co.in', '+91', 9463516202, '69c4bb76d60058f8640ce06ca0ab132b', 1511610543, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(137, '', 'GAGANDEEP', '', 'gagandeep.chaudhary@gmail.com', '+91', 9999551252, '8c5c919f5009e2d6907b4bd101816e52', 1511610871, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(138, '', 'MANNUR ', '', 'vishwamannur@gmail.com', '+91', 9573323222, 'd963f158affa09a611f67114e476178e', 1511611470, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(139, '', 'Nalin ', 'Swami', 'swami.nalin01@gmail.com', '+91', 9167515049, '5797e2912e66f5c732bdd6d212c3b2ee', 1511613145, 0.00, 0, 1, 0, 'Maharashtra', 'Mumbai', '301 Ashajeevan Malwani no. 8 Malad west', '400095', '', '0', '115.97.56.115'),
(140, '', 'Dinesh', '', 'ddinesh0531@gmail.com', '+91', 9600945382, '6878cb903e479bb9612475e0c7b41170', 1511678310, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(141, '', 'Rita ', '', 'denzilmathias2003@yahoo.co.in', '+91', 9820829904, '8556098ee78aceba4e1868ddf34253f0', 1511699912, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(142, '', 'Ansari', '', 'dbaprof1234@gmail.com', '+91', 9000000025, '1d656f04198780df951b72ead33e1e8b', 1511762660, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(143, '', 'Ansari', 'Imteyaz', 'dbaprof123@gmail.com', '+91', 9000000025, 'bc321fc05ff190b8cf71c78af3d20334', 1511762714, 0.00, 0, 1, 0, '', '', '', '', '', '0', '124.123.13.248'),
(144, '', 'Gaurav', 'Kumar', 'keenra4u@gmail.com', '+91', 9654441545, 'b03b59f2e2b0ee8103791f594d28a134', 1511763625, 0.00, 0, 1, 0, 'Delhi', 'New Delhi', 'B-1, Bhoti Devi Apartment, Near Pandit Mohalla, Aya Nagar', '110047', '', '0', '125.63.95.202'),
(145, '', 'Meenu ', '', ' meenuv69@gmail.com', '+91', 8939917111, '97c344f3b21ecb8b74db8091be7fac6b', 1511764687, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(146, '', 'KAUSIK', '', 'kausikbhowmick@yahoo.com', '+91', 9822531743, 'c50eeeca067b9dc4cca5249a2276e44e', 1511780677, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(147, '', 'WASIM', 'SHAIKH', 'wasimnroad07@gmail.com', '+91', 9175240742, '774f0c1bf9a127effa81551d55ad67be', 1511780902, 0.00, 0, 1, 0, '', '', '', '', '', '0', '103.90.200.171'),
(148, '', 'VHGV;IUH', '', 'indarpur120@gmail.com', '+91', 7052001310, 'be31617d2fdd47c6dbbbebad9ca0379b', 1511782442, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(149, '', 'Mayank', '', 'tmayank@ford.com', '+91', 9920833916, 'eb6dd898b9b193ac3c057b97c03f0cc6', 1511849233, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(150, '', 'Rohan', '', 'therohangandhi@gmail.com', '+91', 9699894045, 'dbe3c01a3932deeb98459e66dee0e795', 1511849757, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(151, '', 'PARVEEN', '', 'rakesh111kumar11125@gmail.com', '+91', 9779272226, 'e78b64580ef7ee156e7e164c4b5d26f4', 1511867590, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(152, '', 'Jigar', '', 'jigarmuni@gmail.com', '+91', 9820530929, '0532e3c3af19d1d76e11f43fef4a72b5', 1511867611, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(153, '', 'DEVANG', '', 'palan.devang@gmail.com', '+91', 9322696267, '456cbf9225628418f2ccf26433c790a4', 1511872734, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(154, '', 'URVASHI ', 'PATIL', 'dabredivya@gmail.com', '+91', 9623652536, '4c37b4dea26aa5426c24583624d2f9b0', 1511935140, 0.00, 0, 1, 0, '', '', '', '', '', '0', '117.211.110.130'),
(155, '', 'Gangadhar Dnyaneshwar', '', 'gangadharbiradar15@gmail.com', '+91', 9738072999, '3034ea2e99198858576eb9ff5e754949', 1511935453, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(156, '', 'gfjh', '', 'gfhhjz@gmail.com', '+91', 9874561239, '5e8fad4267fe26d33e09d0c5f1c08fd7', 1511951774, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(157, '', 'MUHAMMAD', '', 'muhammadmukeem786@gmail.com', '+91', 7084708654, '175241b8bb62088b867f552b4528cdea', 1511968415, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(158, '', 'JIGNESH', '', 'jignesh@manubhai.net', '+91', 9820030835, 'a0c18798f34331417f2337bc59bf2981', 1511970072, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(159, '', 'JIGNESH', 'Sanghavi', 'jmsanghavi1@gmail.com', '+91', 9820133800, 'c23b9c26e2aa9db6220c046d18c04454', 1511970465, 0.00, 0, 1, 0, '', '', '', '', '', '0', '49.33.93.231'),
(160, '', 'Avanthika ', '', 'avanthika1112@gmail.com', '+91', 9500092982, 'b4c59073a99bb58c6b516584bd693516', 1512010202, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(161, '', 'Boris ', 'Kenneth', 'boris414.m@gmail.com', '+91', 9980181588, '5f6c3769e44913d3775e0780d5adf0db', 1512021472, 0.00, 0, 1, 0, 'Karnataka', 'Bangalore', '#1020/b 2nd Floor 17th D Cross, Indira Nagar\r\nEshwara Layout', '560025', '', '0', '123.201.152.126'),
(162, '', 'EISHVINDER', '', 'esratra@gmail.com', '+91', 9741498590, '687dd72eaea6c032c9095e5ebf958be2', 1512110133, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(163, '', 'Divagar', '', 'diva.bics@gmail.com', '+91', 9500660668, 'f207fbe05aca15115ce7944ca7617d88', 1512125311, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(164, '', 'Umar', '', 'omar.serco@gmail.com', '+91', 7838794078, '743ff1538940def1631c209a90481edf', 1512126119, 0.00, 0, 1, 0, 'JAMMU AND KASHMIR', 'SRINAGAR', 'URDU BAZAR RAFIQI KOCHA FATEH KADAL', '190002', '', '0', ''),
(165, '', 'Arihanth', 'Jain', 'arihant.jain18@gmail.com', '+91', 9841711544, '137873cdfe61fe9260d8afc1eea3d2bb', 1512197071, 0.00, 0, 1, 0, '', '', '', '', '', '0', '182.65.207.163'),
(166, '', 'Justin', 'Castelino', 'justin.castelino@timesgroup.com', '+91', 8291036580, 'd650e1ed8bf108ab1a5e5dec701830da', 1512197456, 0.00, 0, 1, 0, '', '', '', '', '', '0', '14.141.71.10'),
(167, '', 'Rafnas', '', 'rafnastprocketmail.com@gmail.com', '+91', 9945543570, '52079997471e6ef218a0bb273a0c2cda', 1512197579, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(168, '', 'RITESHKUMAR RADHESHYAM', '', 'mailtoalienterprises@yahoo.com', '+91', 8108502239, '7e2c553ee472dc780f97254fc119e8a5', 1512201306, 0.00, 0, 1, 0, 'Maharashtra', 'MUMBAI', 'P2/9, DEEP SADAN CHS, SUNDER NAGAR S V ROAD MALAD WEST', '400064', '', '0', ''),
(169, '', 'Gurpartap', '', 'singhgurpartap166@gmail.com', '+91', 8196857477, 'eb6c59f179ea460d4d4e9ddc8068eddf', 1512211255, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(170, '', 'aiman ', '', 'aiman16@gmail.com', '+91', 9820790496, '0dc4d26297f707b6214dc35ed34f4425', 1512544098, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(171, '', 'Amit', 'Taparia', 'amit.taparia1@gmail.com', '+91', 7303149322, 'e89485f340086bb4c43029e92eff4b39', 1512544271, 0.00, 0, 1, 0, '', '', '', '', '', '0', '182.19.88.109'),
(172, '', 'Mohammed', '', 'mohammedjaveed991@gmail.com', '+91', 7904708053, 'abd38bac07461ed2973b33552922193a', 1512545413, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(173, '', 'Amit Sudheer', 'Deshpande', 'amitsd@gmail.com', '+91', 9820527250, 'a60d49218fe9d7e7a881c37b250352a9', 1512545414, 0.00, 0, 1, 0, 'MAHARASHTRA', 'THANE', '1802, BEAUTY PALMS CHS, KOLBAD, KHOPAT', '400601', '', '0', '182.57.216.0'),
(174, '', 'Siva ', '', 'sivatry.shankar@gmail.com', '+91', 9789707388, '6528c5db0fba4c7ef4ca3b722510584d', 1512552262, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(175, '', 'Monisha', '', 'monisha.kanavi@gmail.com', '+91', 8880110065, '678a547effc563273fbc487d1717fa06', 1512627440, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(176, '', 'Narayana Rao', '', 'narayanarao.desai@gmail.com', '+91', 9849806442, '378f9df8bd19f5ca5f0fc65299dba664', 1512627638, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(177, '', 'Nadeem Ahmed', '', 'nadeemartech@yahoo.com', '+91', 9535696805, '3a4666d1677c6b11bbdc550cdb9343f7', 1512629485, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(178, '', 'Rohit ', '', 'seemarvishal@gmail.com', '+91', 9818393099, '895e9ab8a473689942b1779f1e0e202f', 1512629600, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(179, '', 'SAMESH', '', 'sameshagrawal.09@gmail.com', '+91', 9004497706, '001caddc87b5a8cbb9cf145456251f5b', 1512729679, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(180, '', 'Kiran', '', 'akbrahma@gmail.com', '+91', 9867007691, 'be5121cb6f99e3a587c8f978e330dd93', 1512729729, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(181, '', 'INTU KUMAR RAJBHAR ', 'HIRAMAN', 'hrsaiftravellinks@gmail.com', '+91', 9004228387, '4ed7cf8f039d1b972bcbf10081554db3', 1512729843, 0.00, 0, 1, 0, '', '', '', '', '', '0', '182.56.250.75'),
(182, '', 'Simran ', '', 'simranschauhan@outlook.com', '+91', 7347392149, '9d7d626829199a597d48f8d6881184c4', 1512730313, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(183, '', 'babu', '', 'ramshanshan291279@gmail.com', '+91', 9945573517, '94e5683a2c31ba6e75d481f437bcbec6', 1512977531, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(184, '', 'Syed Shahbaz ', '', 'mailsyedshahbaz@gmail.com', '+91', 7889445963, '13f2fe2978fa825c94f2c6ae095dd8c8', 1513143990, 0.00, 0, 1, 0, 'Jammu and Kashmir ', 'Srinagar', 'Sheikhara,Sonta bugh, Pulwama\r\nJammu and Kashmir ', '192301', '', '0', ''),
(185, '', 'KARTIK', '', 'kartikbhag@gmail.com', '+91', 7738871007, 'a3fcf98c45855330a14fdbae959667b7', 1513153774, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(186, '', 'Kahan', 'Taraporevala', 'kahantara11@gmail.com', '+91', 9619424392, '64db6c6c963ceb561b4b7e3fc5bdb974', 1513161360, 0.00, 0, 1, 0, 'Maharashtra', 'Mumbai', 'B3, Amalfi 15, L D Ruparel RD\r\nMalabar Hill', '400006', '', '0', '203.115.123.17'),
(187, '', 'gourav', '', 'gourav.katyal@gmail.com', '+91', 9871125557, '875c7456ec16364d4a425db904cc94c0', 1513161711, 0.00, 0, 1, 0, '', '', '', '', '', '0', ''),
(188, '', 'SIDDHARTH', '', 'sidarora@gmail.com', '+91', 9820053796, '72c30a54f4a298e1baa874d476af7044', 1513162363, 0.00, 0, 1, 0, 'Maharashtra', 'Mumbai', '602, Sharan Apartment,27th Road, Bandra (W) ', '400050', '', '0', ''),
(189, '', 'Prisca', '', 'prisca.p.charles@gmail.com', '+91', 9840979303, '61dadcfc74000688a6db98ee858f788e', 1513163485, 0.00, 0, 1, 0, '', '', '', '', '', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_price`
--

CREATE TABLE IF NOT EXISTS `tbl_user_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) DEFAULT NULL,
  `session_id` varchar(20) DEFAULT NULL,
  `hotal_id` varchar(20) DEFAULT NULL,
  `amount` varchar(20) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_values`
--

CREATE TABLE IF NOT EXISTS `tbl_values` (
  `vl_id` int(11) NOT NULL AUTO_INCREMENT,
  `vl_title` varchar(255) NOT NULL,
  `vl_value1` varchar(255) NOT NULL,
  `vl_value2` varchar(255) NOT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`vl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_values`
--

INSERT INTO `tbl_values` (`vl_id`, `vl_title`, `vl_value1`, `vl_value2`, `is_delete`) VALUES
(3, 'Email From', 'info@yourdubaivisa.com', '', 0),
(4, 'Email To', 'info@yourdubaivisa.com', '', 0),
(5, 'India Booking Code', 'YDV', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_visa_app`
--

CREATE TABLE IF NOT EXISTS `tbl_visa_app` (
  `v_id` int(11) NOT NULL AUTO_INCREMENT,
  `v_ref_no` varchar(100) NOT NULL,
  `v_uid` int(11) NOT NULL,
  `v_lid` int(11) NOT NULL,
  `v_aid` int(11) NOT NULL,
  `v_status` tinyint(4) NOT NULL DEFAULT '1',
  `v_doi` int(11) NOT NULL,
  `v_comment` text NOT NULL,
  `v_visa` varchar(255) NOT NULL,
  `v_ved` varchar(255) NOT NULL,
  `v_dstatus` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`v_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_visa_status`
--

CREATE TABLE IF NOT EXISTS `tbl_visa_status` (
  `vist_id` int(11) NOT NULL AUTO_INCREMENT,
  `vist_applicant_details_id` int(11) NOT NULL,
  `vist_reference_no` varchar(255) NOT NULL,
  `vist_comment` varchar(255) NOT NULL,
  `vist_status` int(11) NOT NULL,
  `vist_uploadfile` varchar(255) NOT NULL,
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`vist_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=151 ;

--
-- Dumping data for table `tbl_visa_status`
--

INSERT INTO `tbl_visa_status` (`vist_id`, `vist_applicant_details_id`, `vist_reference_no`, `vist_comment`, `vist_status`, `vist_uploadfile`, `is_delete`) VALUES
(1, 1, '4', '', 0, '', 0),
(2, 1, '4', '', 0, '', 0),
(3, 1, '4', '', 0, '', 0),
(4, 1, '4', '', 0, '', 0),
(5, 2, '5', '', 0, '', 0),
(6, 2, '6', '', 0, '', 0),
(7, 3, '21', '', 0, '', 0),
(8, 3, '21', '', 0, '', 0),
(9, 3, '21', 'Sample', 5, '', 0),
(10, 2, '22', '', 0, '', 0),
(11, 2, '23', '', 0, '', 0),
(12, 2, '24', '', 0, '', 0),
(13, 5, '27', '', 0, '', 0),
(14, 2, '28', '', 0, '', 0),
(15, 2, '28', '', 0, '', 0),
(16, 2, '28', '', 0, '', 0),
(17, 2, '28', '', 0, '', 0),
(18, 2, '29', '', 0, '', 0),
(19, 2, '30', '', 0, '', 0),
(20, 2, '31', '', 0, '', 0),
(21, 2, '32', '', 0, '', 0),
(22, 2, '33', '', 0, '', 0),
(23, 2, '33', '', 0, '', 0),
(24, 2, '33', '', 0, '', 0),
(25, 2, '35', '', 0, '', 0),
(26, 2, '36', '', 0, '', 0),
(27, 5, '37', '', 0, '', 0),
(28, 5, '38', '', 0, '', 0),
(29, 5, '39', '', 0, '', 0),
(30, 5, '40', '', 0, '', 0),
(31, 5, '40', '', 0, '', 0),
(32, 2, '41', '', 0, '', 0),
(33, 1, '42', '', 0, '', 0),
(34, 1, '43', '', 0, '', 0),
(35, 1, '44', '', 0, '', 0),
(36, 1, '44', '', 0, '', 0),
(37, 5, '45', '', 0, '', 0),
(38, 5, '46', '', 0, '', 0),
(39, 5, '46', '', 0, '', 0),
(40, 5, '47', '', 0, '', 0),
(41, 5, '47', '', 0, '', 0),
(42, 5, '49', '', 0, '', 0),
(43, 5, '50', '', 0, '', 0),
(44, 5, '', '', 0, '', 0),
(45, 5, '51', 'uk', 1, '', 0),
(46, 5, '52', '', 0, '', 0),
(47, 5, '54', '', 0, '', 0),
(48, 5, '55', '', 0, '', 0),
(49, 5, '56', '', 0, '', 0),
(50, 5, '56', '', 0, '', 0),
(51, 5, '57', '', 0, '', 0),
(52, 5, '58', '', 0, '', 0),
(53, 3, '59', '', 0, '', 0),
(54, 3, '60', '', 0, '', 0),
(55, 3, '61', '', 0, '', 0),
(56, 3, '62', '', 0, '', 0),
(57, 3, '63', '', 0, '', 0),
(58, 3, '63', '', 0, '', 0),
(59, 3, '64', '', 0, '', 0),
(60, 3, '65', '', 0, '', 0),
(61, 3, '66', '', 0, '', 0),
(62, 3, '67', '', 0, '', 0),
(63, 3, '68', '', 0, '', 0),
(64, 3, '69', '', 0, '', 0),
(65, 3, '70', '', 0, '', 0),
(66, 2, '71', '', 0, '', 0),
(67, 5, '72', '', 0, '', 0),
(68, 5, '74', '', 0, '', 0),
(69, 2, '71', 'jhk', 1, '', 0),
(70, 3, '90', '', 0, '', 0),
(71, 3, '91', '', 0, '', 0),
(72, 3, '91', '', 0, '', 0),
(73, 3, '91', '', 0, '', 0),
(74, 3, '91', '', 0, '', 0),
(75, 3, '91', '', 0, '', 0),
(76, 3, '92', '', 0, '', 0),
(77, 2, '93', '', 0, '', 0),
(78, 5, '94', '', 0, '', 0),
(79, 3, '95', '', 0, '', 0),
(80, 3, '96', '', 0, '', 0),
(81, 2, '100', '', 0, '', 0),
(82, 8, '108', '', 0, '', 0),
(83, 9, '108', '', 0, '', 0),
(84, 3, '111', '', 0, '', 0),
(85, 3, '111', '', 0, '', 0),
(86, 3, '111', '', 0, '', 0),
(87, 3, '111', '', 0, '', 0),
(88, 2, '112', '', 0, '', 0),
(89, 2, '113', '', 0, '', 0),
(90, 2, '137', '', 0, '', 0),
(91, 10, '141', '', 0, '', 0),
(92, 11, '141', '', 0, '', 0),
(93, 12, '142', '', 0, '', 0),
(94, 13, '142', '', 0, '', 0),
(95, 14, '142', '', 0, '', 0),
(96, 15, '142', '', 0, '', 0),
(97, 16, '142', '', 0, '', 0),
(98, 19, '158', '', 0, '', 0),
(99, 25, '169', '', 0, '', 0),
(100, 26, '168', '', 0, '', 0),
(101, 26, '168', '', 0, '', 0),
(102, 30, '191', '', 0, '', 0),
(103, 31, '191', '', 0, '', 0),
(104, 32, '191', '', 0, '', 0),
(105, 30, '191', '', 0, '', 0),
(106, 31, '191', '', 0, '', 0),
(107, 32, '191', '', 0, '', 0),
(108, 30, '191', '', 0, '', 0),
(109, 31, '191', '', 0, '', 0),
(110, 32, '191', '', 0, '', 0),
(111, 30, '191', '', 0, '', 0),
(112, 31, '191', '', 0, '', 0),
(113, 32, '191', '', 0, '', 0),
(114, 34, '204', '', 0, '', 0),
(115, 35, '204', '', 0, '', 0),
(116, 36, '204', '', 0, '', 0),
(117, 37, '204', '', 0, '', 0),
(118, 38, '204', '', 0, '', 0),
(119, 39, '204', '', 0, '', 0),
(120, 33, '211', '', 0, '', 0),
(121, 44, '222', '', 0, '', 0),
(122, 41, '220', '', 0, '', 0),
(123, 43, '220', '', 0, '', 0),
(124, 45, '225', '', 0, '', 0),
(125, 46, '225', '', 0, '', 0),
(126, 48, '225', '', 0, '', 0),
(127, 50, '233', '', 0, '', 0),
(128, 51, '233', '', 0, '', 0),
(129, 52, '233', '', 0, '', 0),
(130, 50, '238', '', 0, '', 0),
(131, 51, '238', '', 0, '', 0),
(132, 52, '238', '', 0, '', 0),
(133, 53, '254', '', 0, '', 0),
(134, 54, '254', '', 0, '', 0),
(135, 53, '254', '', 0, '', 0),
(136, 54, '254', '', 0, '', 0),
(137, 53, '254', '', 0, '', 0),
(138, 54, '254', '', 0, '', 0),
(139, 56, '263', '', 0, '', 0),
(140, 57, '263', '', 0, '', 0),
(141, 58, '262', '', 0, '', 0),
(142, 56, '264', '', 0, '', 0),
(143, 57, '264', '', 0, '', 0),
(144, 60, '268', '', 0, '', 0),
(145, 61, '268', '', 0, '', 0),
(146, 62, '275', '', 0, '', 0),
(147, 63, '275', '', 0, '', 0),
(148, 64, '275', '', 0, '', 0),
(149, 69, '298', '', 0, '', 0),
(150, 69, '299', '', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_visa_type`
--

CREATE TABLE IF NOT EXISTS `tbl_visa_type` (
  `vt_id` int(11) NOT NULL AUTO_INCREMENT,
  `vt_title` varchar(255) NOT NULL,
  `vt_duration` int(11) NOT NULL,
  `vt_price` decimal(10,2) NOT NULL,
  `vt_cost_price` decimal(10,2) NOT NULL,
  `vt_unite` varchar(255) NOT NULL,
  `vt_no_of_day` int(11) NOT NULL,
  `vt_special_price` decimal(10,2) NOT NULL,
  `vt_express_add_on` decimal(10,2) NOT NULL,
  `vt_short_description` varchar(255) NOT NULL,
  `vt_long_description` text NOT NULL,
  `vt_services_type` int(11) NOT NULL,
  `vt_documents` varchar(200) NOT NULL,
  `vt_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=inactive 1=active',
  `vt_oktb` tinyint(4) NOT NULL DEFAULT '0',
  `is_delete` tinyint(2) NOT NULL DEFAULT '0',
  `vt_document_required` text NOT NULL,
  `vt_order` int(11) NOT NULL DEFAULT '50',
  PRIMARY KEY (`vt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_visa_type`
--

INSERT INTO `tbl_visa_type` (`vt_id`, `vt_title`, `vt_duration`, `vt_price`, `vt_cost_price`, `vt_unite`, `vt_no_of_day`, `vt_special_price`, `vt_express_add_on`, `vt_short_description`, `vt_long_description`, `vt_services_type`, `vt_documents`, `vt_status`, `vt_oktb`, `is_delete`, `vt_document_required`, `vt_order`) VALUES
(1, '14 Days Tourist Visa', 14, 5699.00, 5250.00, 'DAYS', 14, 0.00, 2500.00, '<p>This visa permits stay upto a maximum period of 14 days from the date of entry into Dubai. The maximum validity of the visa is 58 days from the date of issue. This visa is suitable for tourists whose travel dates are flexible but within the validity pe', '<p>14 days Visa is a single-entry and single exit visa valid for 58 days from the date of issuance and duration of stay is limited to 14 days from the date of entry. This visa is non-extendable.</p>', 6, '1,2', 1, 0, 0, '<p><strong>Mandatory Documents-</strong></p>\r\n<ul>\r\n<li>Passport Front and Last Page</li>\r\n<li>Air Ticket Copy (Can Be provided after approval of Visa)</li>\r\n<li>Passport size photograph on white background with 80 % Face.</li>\r\n</ul>\r\n<p><span>Note:</span><span>&nbsp;Additional documents may be required by the UAE immigration authorities whom we will communicate to you through the email ID provided by you.</span></p>', 50),
(2, '30 Days Tourist Visa', 30, 6299.00, 5150.00, 'Days', 30, 0.00, 2400.00, '<p>30 days Visa is a single-entry and single exit visa valid for 58 days from the date of issuance and duration of stay is limited to 30 days from the date of entry.</p>', '<p>30 days Visa is a single-entry and single exit visa valid for 58 days from the date of issuance and duration of stay is limited to 30 days from the date of entry. This visa is non-extendable. With this visa, you may travel to Dubai, Abu Dhabi, Sharjah, Ajman, Fujairah, Ras Al Khaimah and Umm al-Quwain.This Visa is valid for 30 days availed by travellers having a longer stay in UAE. This type of Visa is preferred by travellers going to UAE for holidays, visit friends n family or for business trainings etc.</p>', 6, '1,2', 1, 0, 0, '<p><strong>Mandatory Documents-</strong></p>\r\n<ul>\r\n<li>Passport Front and Last Page</li>\r\n<li>Air Ticket Copy (Can Be provided after approval of Visa)</li>\r\n<li>Passport size photograph on white background with 80 % Face.</li>\r\n</ul>\r\n<h3></h3>\r\n<ul>\r\n</ul>', 50),
(3, '90 Days Dubai Tourist Visa', 90, 20000.00, 18000.00, 'DAYS', 90, 0.00, 2500.00, '<p>Regular Visit VISA let you enjoy your stay in UAE for 90 days.</p>', '<p class="col-md-10">90 Days Tourist Visa let you enjoy your stay in UAE for 90 days. This category of VISA is good for traveller who are going in a search of opportunities or planning to be your family in UAE or for medical aid.</p>', 6, '', 1, 0, 0, '<p><strong>Mandatory Documents-</strong></p>\r\n<ul>\r\n<li>Passport Front and Last Page</li>\r\n<li>Air Ticket Copy (Can Be provided after approval of Visa)</li>\r\n<li>Passport size photograph on white background with 80 % Face.</li>\r\n</ul>\r\n<p><strong>Additional Documents-&nbsp;</strong></p>\r\n<ul>\r\n<li>Passport      copies of friends/relatives residing in Dubai</li>\r\n<li>Residential      proof of friends/relatives residing in Dubai</li>\r\n<li>Invitation      letter issued by friends/relatives residing in Dubai</li>\r\n<li>Alternate      local contact detail of 2 friends/relatives residing in Dubai</li>\r\n</ul>\r\n<ul class="list_style">\r\n</ul>', 50),
(4, '96 Hours Dubai Transit visa', 96, 4799.00, 4200.00, 'Hrs', 4, 0.00, 1500.00, '<p><span>Transit Visa is a non extendable single entry visa valid for 14 days from the date of issue and the duration of stay is upto 96 hours from time of entry. Transit passengers stopping at Dubai International Airport for a minimum of 8 hours are elig', '<p><span>Transit Visa is a non extendable single entry visa valid for 14 days from the date of issue and the duration of stay is upto 96 hours from time of entry. Transit passengers stopping at Dubai International Airport for a minimum of 8 hours are eligible for a 96-hour transit visa.</span></p>', 6, '1', 1, 0, 0, '<p><strong>Mandatory Documents-</strong></p>\r\n<ul>\r\n<li>Passport Front and Last Page</li>\r\n<li>Air Ticket Copy (Can Be provided after approval of Visa)</li>\r\n<li>Passport size photograph on white background with 80 % Face.</li>\r\n</ul>\r\n<p><span>Note:</span><span>&nbsp;Additional documents may be required by the UAE immigration authorities whom we will communicate to you through the email ID provided by you.</span></p>\r\n<ul>\r\n</ul>', 50),
(5, '30 Days Multiple Entry Dubai Visa', 30, 18500.00, 16500.00, 'Days', 30, 0.00, 2500.00, '<p>It is Short Term Visit Multiple Entry Tourist visa for frequent travellers planning to vist UAE Multiple times for business, seminars &amp; exhibitions, or a vacation and transit</p>', '<p><span>It is Short Term Visit Multiple Entry Tourist visa for frequent travellers planning to vist UAE Multiple times for business, seminars &amp; exhibitions, or a vacation and transit</span></p>', 6, '', 1, 0, 0, '<p><strong>Mandatory Documents-</strong></p>\r\n<ul>\r\n<li>Passport Front and Last Page</li>\r\n<li>Air Ticket Copy (Can Be provided after approval of Visa)</li>\r\n<li>Passport size photograph on white background with 80 % Face.</li>\r\n</ul>\r\n<p><span>Note:</span><span>&nbsp;Additional documents may be required by the UAE immigration authorities whom we will communicate to you through the email ID provided by you.</span></p>', 50),
(6, '90 Days Dubai Visa Multiple Entry', 90, 37000.00, 32000.00, 'DAYS', 90, 0.00, 2500.00, '<p><br /><span>Visa is valid for 90 days availed by travellers having a longer stay and travelling multiple times to UAE. The validity of the visa is 58 days from the date of issue and the client can enter UAE multiple times and stay there for 90 Days fro', '<p><span>Visa is valid for 90 days availed by travellers having a longer stay and travelling multiple times to UAE. The validity of the visa is 58 days from the date of issue and the client can enter UAE multiple times and stay there for 90 Days from the first entry in UAE.</span></p>', 6, '', 1, 0, 0, '<p><strong>Mandatory Documents-</strong></p>\r\n<ul>\r\n<li>Passport Front and Last Page</li>\r\n<li>Air Ticket Copy (Can Be provided after approval of Visa)</li>\r\n<li>Passport size photograph on white background with 80 % Face.</li>\r\n</ul>\r\n<p><strong>Additional Documents-</strong></p>\r\n<ul>\r\n<li>Passport      copies of friends/relatives residing in Dubai</li>\r\n<li>Residential      proof of friends/relatives residing in Dubai</li>\r\n<li>Invitation      letter issued by friends/relatives residing in Dubai</li>\r\n<li>Alternate      local contact detail of 2 friends/relatives residing in Dubai</li>\r\n</ul>', 50),
(7, 'Visa on Arrival for US Visa Holders + Bronze Marhaba Service', 14, 4999.00, 5000.00, 'Days', 14, 0.00, 500.00, '', '', 5, '1,2', 0, 1, 0, '', 50),
(8, '14 Days On-Arrival Visa', 14, 4499.00, 3960.00, 'Days', 14, 0.00, 0.00, '', '', 6, '1,2,4', 1, 0, 0, '<ul class="visaq_list">\r\n<li><span>To qualify, a US visit visa or Green Card is required</span></li>\r\n<li><span>Approval within 4 working hours</span></li>\r\n<li><span>Fast track through the airport</span></li>\r\n<li><span>Pre-approval is available 24/7</span></li>\r\n</ul>', 50);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
