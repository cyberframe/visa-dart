<?php 
include("application.php");
include("includes/head_top.php"); 
?>

<?php 
$page = "faq";
$hd = "FAQ";
$newsrwhere=" is_delete='0' && faq_status=1";
$faq= $common_obj->fun_select("faq",$newsrwhere);
?>
<link rel="stylesheet" type="text/css" href="<?= SITE_URL.'css/accordian-css.css' ?>">
    <div class="topheadbar" id="headtopbg" style="background-image: url(imgs/visaraaivalsbg.jpg);">
        <?php include("includes/header.php"); ?>
        <div class="split"></div>
        <h1>FAQS</h1>
    </div>
    <!-- head div end -->  
    <div class="breadcum">
        <div class="wrapper">
            <div class="currently"><strong>You are viewing:</strong> &nbsp; <i class="icon fa-home"></i> &nbsp;/&nbsp; FAQS</div>
        </div>
    </div>
    <div class="ipage">
        <div class="container">
            <div class="clearfix"></div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php 
                    if(count($faq)>0){
                        $i=1;
                        foreach($faq as $myfaq){ 
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading<?php echo $i;?>">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i;?>" aria-expanded="true" aria-controls="collapse<?php echo $i;?>">
                                <?php echo(strip_tags($myfaq['faq_question']));?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse<?php echo $i;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $i;?>">
                        <div class="panel-body">
                                <?php echo $myfaq['faq_answer']?>
                        </div>
                    </div>
                </div>
                <?php $i++;  } } ?>
            </div>
        </div>
    </div>
    <!-- ipage content div end -->
    <?php include("includes/footer.php"); ?>
    </body>
</html>
<script type="text/javascript">
$(document).ready(function() {
    $(".toggle-accordion").on("click", function() {
        var accordionId = $(this).attr("accordion-id"),
        numPanelOpen = $(accordionId + ' .collapse.in').length;
        $(this).toggleClass("active");
        if (numPanelOpen == 0) {
            openAllPanels(accordionId);
        } else {
            closeAllPanels(accordionId);
        }
    })
    openAllPanels = function(aId) {
        console.log("setAllPanelOpen");
        $(aId + ' .panel-collapse:not(".in")').collapse('show');
    }
    closeAllPanels = function(aId) {
        console.log("setAllPanelclose");
        $(aId + ' .panel-collapse.in').collapse('hide');
    }   
});
</script>