<footer>
    <div class="wrapper" align="left">
    <div class="botlinks">
        <ul>
            <h3><i class="icon fa-arrow-circle-down red"></i> Visa</h3>
            <li><a href="<?= SITE_URL ?>page.php?id=22"><i class="icon fa-angle-right"></i> Visa on Arrivals</a></li>
            <li><a href="<?= SITE_URL; ?>visa_fees.php"><i class="icon fa-angle-right"></i> Visa Fees</a></li>
            <li><a href="#"><i class="icon fa-angle-right"></i> Visa Status</a></li>
            <li><a href="<?= SITE_URL; ?>page.php?id=3"><i class="icon fa-angle-right"></i> Docs Info</a></li>
        </ul>
        <ul>
            <h3><i class="icon fa-arrow-circle-down red"></i> The Company</h3>
            <li><a href="<?= SITE_URL ?>page.php?id=1"><i class="icon fa-angle-right"></i> About us</a></li>
            <li><a href="<?= SITE_URL; ?>faqs.php"><i class="icon fa-angle-right"></i> FAQ's</a></li>
            <li><a href="#"><i class="icon fa-angle-right"></i> Testimonials</a></li>
            <li><a href="#"><i class="icon fa-angle-right"></i> News & Events</a></li>
            <li><a href="<?= SITE_URL; ?>contact_us.php"><i class="icon fa-angle-right"></i> Contact us</a></li>
        </ul>
        <ul>
            <!-- <h3><i class="icon fa-arrow-circle-down red"></i> Get in Touch</h3> -->
            <li><a href="<?= SITE_URL; ?>apply.php"><i class="icon fa-user"></i> Apply Now</a></li>
            <li><a href="#"><i class="icon fa-download"></i> Request a Call Back</a></li>
            <li><a href="#"><i class="icon fa-comments"></i> Feedback/Suggestion</a></li>
        </ul>
        <ul>
            <h3><i class="icon fa-arrow-circle-down red"></i> Stay On</h3>
            <li><a href="#"><i class="icon fa-facebook"></i> Facebook</a></li>
            <li><a href="#"><i class="icon fa-twitter"></i> Twitter</a></li>
            <li><a href="#"><i class="icon fa-google-plus"></i> Google Plus</a></li>
            <li><a href="#"><i class="icon fa-linkedin"></i> Linked In</a></li>
        </ul>
        <ul class="text_right">
            <div class="rigtt_bot_content" align="right">
                <img src="imgs/logovd.png">
                <p><i class="icon fa-map-marker"></i> Ludhiana - 141010, Punjab, INDIA</p>
                <p><i class="icon fa-phone"></i> Call us: +91-888-282-282-2 <br>
                    <i class="icon fa-envelope"></i> Mail us: <a href="mailto:info@visadart.in">info@visadart.in</a>
                </p>
            </div>
            <div class="copyright">
                <p>&copy; 2017 www.visadart.in, All Right Reserved</p>
            </div>
        </ul>
        <div class="split"></div>
    </div>
</footer>