<!doctype html>
<html class="fsvs demo" lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Welcome to Visa Dart</title>
        <meta name="description" content="Visa Dart" />
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
        <link rel="icon" type="<?= SITE_URL ?>image/png" href="imgs/fav.png">
        <link rel="stylesheet" href="<?= SITE_URL ?>istyle.css">
        <link rel="stylesheet" href="<?= SITE_URL ?>icons.css">
        <link rel="stylesheet" href="<?= SITE_URL.'css/bootstrap.css' ?>">
        <script src="<?= SITE_URL ?>js/jquery-1.11.0.min.js"></script>
        <script src="<?= SITE_URL.'js/bootstrap.js' ?>"></script>
        <script language="javascript">
            $(window).scroll(function () {
               var yPos = -($(window).scrollTop() / 1);
               var bgpos = '50% ' + yPos + 'px';
               $('#scaffoldingbg').css('background-position', bgpos);
            });
        </script>
    </head>
    <body>