<header>
    <div class="logo"> 
        <img src="<?= SITE_URL; ?>imgs/logovd.png" alt="visa dart">
    </div>
    <!-- navigation start -->
    <div class="navigation">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li>
                <a href="#">Visva</a>
                <ul>
                    <li><a href="./page.php?id=22">Visa on Arrivals</a></li>
                    <li><a href="./visa_fees.php">Visa Fees</a></li>
                    <li><a href="#">Visa Status</a></li>
                    <li><a href="./page.php?id=3">Docs Info</a></li>
                </ul>
            </li>
            <li><a href="./faqs.php">Faq's</a></li>
            <li><a href="./page.php?id=1">About us</a></li>
            <li><a href="./contact_us.php">Contact us</a></li>
            <li><a href="./apply.php">Apply Now</a></li>
            <li><a href="./login.php">Login</a></li>
        </ul>
    </div>
    <!-- navigation end -->
    
    <!-- call us start -->
    <div class="callus">
        <p>Support <i class="icon fa-phone"></i> +91-888-282-282-2 &nbsp;|&nbsp; 
            <a href="http://facebook.com" target="_blank"><i class="icon fa-facebook"></i></a> 
            <a href="http://twitter.com" target="_blank"><i class="icon fa-twitter"></i></a>
        </p>
    </div>
    <!-- call us end -->
</header>