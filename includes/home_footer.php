<div class="slide">
    <div class="wrapper" align="left">
        <!-- left panel start -->
        <div class="leftpanel_home">
            <div class="botlinks">
                <ul>
                    <h3><i class="icon fa-arrow-circle-down red"></i> Visa</h3>
                    <li><a href="<?= SITE_URL ?>page.php?id=22"><i class="icon fa-angle-right"></i> Visa on Arrivals</a></li>
                    <li><a href="<?= SITE_URL; ?>visa_fees.php"><i class="icon fa-angle-right"></i> Visa Fees</a></li>
                    <li><a href="#"><i class="icon fa-angle-right"></i> Visa Status</a></li>
                    <li><a href="<?= SITE_URL; ?>page.php?id=3"><i class="icon fa-angle-right"></i> Docs Info</a></li>
                </ul>
                <ul>
                    <h3><i class="icon fa-arrow-circle-down red"></i> The Company</h3>
                    <li><a href="<?= SITE_URL ?>page.php?id=1"><i class="icon fa-angle-right"></i> About us</a></li>
                    <li><a href="<?= SITE_URL; ?>faqs.php"><i class="icon fa-angle-right"></i> FAQ's</a></li>
                    <li><a href="<?= SITE_URL ?>testimonial.php"><i class="icon fa-angle-right"></i> Testimonials</a></li>
                    <li><a href="<?= SITE_URL ?>newsevent.php"><i class="icon fa-angle-right"></i> News & Events</a></li>
                    <li><a href="<?= SITE_URL ?>contact_us.php"><i class="icon fa-angle-right"></i> Contact us</a></li>
                </ul>
                <ul>
                    <h3><i class="icon fa-arrow-circle-down red"></i> Get in Touch</h3>
                    <li><a href="<?= SITE_URL ?>apply.php"><i class="icon fa-user"></i> Apply Now</a></li>
                    <li><a href="<?= SITE_URL ?>contact_us.php"><i class="icon fa-download"></i> Request a Call Back</a></li>
                    <li><a href="<?= SITE_URL ?>feedback.php"><i class="icon fa-comments"></i> Feedback/Suggestion</a></li>
                </ul>
                <ul>
                    <h3><i class="icon fa-arrow-circle-down red"></i> Stay On</h3>
                    <li><a href="http://www.facebook.com" target="_blank"><i class="icon fa-facebook"></i> Facebook</a></li>
                    <li><a href="http://www.twitter.com" target="_blank"><i class="icon fa-twitter"></i> Twitter</a></li>
                    <li><a href="http://www.google-plus.com" target="_blank"><i class="icon fa-google-plus"></i> Google Plus</a></li>
                    <li><a href="http://www.linkedin.com" target="_blank"><i class="icon fa-linkedin"></i> Linked In</a></li>
                </ul>
            </div>
            <div class="split"></div>
            <div class="copyright">
                <p>&copy; 2015 www.visadart.com, All Right Reserved</p>
                <p><strong>Developed &amp; Hosted by:</strong> www.cyberframe.in</p>
            </div>
        </div>
        <!-- left panel end -->
        <!-- right panel start -->
        <div class="rightpanel_home">
            <div class="rigtt_bot_content" align="right">
                <h3><img src="imgs/logobot.png"></h3>
                <p><i class="icon fa-map-marker"></i> Ludhiana - 141010, Punjab, INDIA</p>
                <p><i class="icon fa-phone"></i> Call us: +91-888-282-282-2 <br>
                    <i class="icon fa-envelope"></i> Mail us: <a href="mailto:info@visadart.in">info@visadart.in</a>
                </p>
            </div>
        </div>
        <!-- right panel end -->
    </div>
    <div class="pattern_bot"></div>
</div>