<!doctype html>
<html class="fsvs demo" lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Welcome to Visa Dart</title>
        <meta name="description" content="Visa Dart" />
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta HTTP-EQUIV="Pragma" content="no-cache">
        <meta HTTP-EQUIV="Expires" content="-1" >
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Maven+Pro" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="icons.css">
        <link rel="icon" type="image/png" href="imgs/fav.png">
        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="js/jquery-migrate-1.2.1.min.js"></script>
        <script src="js/jquery.swipe-events.js"></script>
        <script src="js/prismjs.js"></script>
        <script src="js/fsvs.js"></script>
        <script src="js/main.js"></script>
        <script>
            var currentPage;
            $(document).ready(function() {
                                       
            currentPage = document.location.hash;
            
            animateBlock(currentPage);
                
            $(window).on('hashchange',function(){
                currentPage = document.location.hash;
                animateBlock(currentPage);               
                });
            
            
            });
            
            
            function animateBlock(currentPage)
            {
            if(currentPage == '#slide-1' || currentPage == '#slide-5')
            {
               $('#pack_thumb1').removeClass("pack_thumb1");
            
                
                $('#cast_thumb1').removeClass("cast_thumb1");
                $('#cast_thumb2').removeClass("cast_thumb2");
                $('#cast_thumb3').removeClass("cast_thumb3");
            
                $('#why_thumb1').removeClass("why_thumb1");
                $('#why_thumb2').removeClass("why_thumb2");
                $('#why_thumb3').removeClass("why_thumb3");
                
            
            }
            if(currentPage == '#slide-2')
            {
                $('#why_thumb1').addClass("why_thumb1");
                $('#why_thumb2').addClass("why_thumb2");
                $('#why_thumb3').addClass("why_thumb3");
                
                      $('#pack_thumb1').removeClass("pack_thumb1");
                      
                $('#visa_thumb1').removeClass("visa_thumb1");
                $('#visa_thumb2').removeClass("visa_thumb2");
                $('#visa_thumb3').removeClass("visa_thumb3");
            }
            
            if(currentPage == '#slide-3')
            {
                $('#visa_thumb1').addClass("visa_thumb1");
                $('#visa_thumb2').addClass("visa_thumb2");
                $('#visa_thumb3').addClass("visa_thumb3");
                
                   $('#pack_thumb1').removeClass("pack_thumb1");
                
                $('#why_thumb1').removeClass("why_thumb1");
                $('#why_thumb2').removeClass("why_thumb2");
                $('#why_thumb3').removeClass("why_thumb3");
                
                }
                
            if(currentPage == '#slide-4')
            {
            
                $('#pack_thumb1').addClass("pack_thumb1");
                
                      $('#why_thumb1').removeClass("why_thumb1");
                $('#why_thumb2').removeClass("why_thumb2");
                $('#why_thumb3').removeClass("why_thumb3");
                
                $('#visa_thumb1').removeClass("visa_thumb1");
                $('#visa_thumb2').removeClass("visa_thumb2");
                $('#visa_thumb3').removeClass("visa_thumb3");
                
                }
                
            if(currentPage == '#slide-5') 
            {
            $('.mouse').slideUp();
            }
            else {
            $('.mouse').slideDown();    
            }
                
                
            }
            
        </script>
        <script type="text/javascript">
            $(window).load(function() {
                $(".loader").fadeOut("slow");
            })
        </script>
    </head>