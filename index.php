<?php 
include("application.php");
include("includes/home_header.php"); 
?>
    <body>
        <div class="loader"></div>
        <div class="mouse">
            <div class="wheel"><img src="imgs/wheelicon.png"  alt="wheel"></div>
        </div>
        <div id="fsvs-body">
            <div class="slide" >
                <script src="js/device.min.js"></script> <!--OPTIONAL JQUERY PLUGIN-->
                <script src="js/jquery.mb.YTPlayer.js"></script>
                <script src="js/custom.js"></script>
                <section id="foot_video">
                    <a id="bgndVideo" class="player" data-property="{videoURL:'https://www.youtube.com/watch?v=0_7eRKz7x5k&feature=youtu.be',containment:'body',autoPlay:true, mute:true, startAt:0, opacity:1}"></a>
                    <div class="pattern"></div>
                    <div class="logo"><img src="<?= SITE_URL ?>imgs/logovd.png"></div>
                    <!-- navigation start -->
                    <div class="navigation">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li>
                                <a href="#">Visa</a>
                                <ul>
                                    <li><a href="./page.php?id=22">Visa on Arrivals</a></li>
                                    <li><a href="./visa_fees.php">Visa Fees</a></li>
                                    <li><a href="#">Visa Status</a></li>
                                    <li><a href="./page.php?id=3">Docs Info</a></li>
                                </ul>
                            </li>
                            <li><a href="./faqs.php">Faq's</a></li>
                            <li><a href="./page.php?id=1">About us</a></li>
                            <li><a href="./contact_us.php">Contact us</a></li>
                            <li><a href="./apply.php">Apply Now</a></li>
                            <li><a href="./login.php">Login</a></li>
                        </ul>
                    </div>
                    <div class="callus">
                        <p>Support <i class="icon fa-phone"></i> +91-888-282-282-2 &nbsp;|&nbsp; <i class="icon fa-facebook"></i> <i class="icon fa-twitter"></i></p>
                    </div>
                    <!-- call us end -->
                    <div class="split"></div>
                    <h2 class="saia"><img src="imgs/malayesia.png" alt="malayesia"></h2>
                    <div class="wrapper">
                        <p>Get your visa in your hands just in 24hrs</p>
                        <p><a href="<?= SITE_URL; ?>login.php" class="blinknew">Apply Now <i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a></p>
                    </div>
                </section>
            </div>
            <div class="slide">
                <h2 class="headprop_bot" style="text-align: center">Why Choose us?</h2>
                <div class="split"></div>
                <div class="wrapper" align="left">
                    <!-- singapore visa content start -->
                    <div class="home_content">
                        <p>Visa Dart is a leading service provider for Singapore Visa. With Visa Dart getting a Malaysia visa is extremely simple and hassle free process.</p>
                        <a href="#" class="blinknew">More...</a>
                    </div>
                    <!-- singapore visa content end -->
                    <!-- singapore visa  square thumbs start -->
                    <div class="thumbs_d">
                        <div id="why_thumb1">
                            <img src="imgs/singaporevisa.jpg" alt="casting">
                            Leading company for<br>Malaysia visa in India    
                        </div>
                        <div id="why_thumb2">
                            <img src="imgs/fast-processing.jpg" alt="casting">
                            Fast Processing &<br>Turnaround Time  
                        </div>
                        <div id="why_thumb3">
                            <img src="imgs/paymentoption.jpg" alt="casting">
                            Easy & Multiple <br>Payment Option  
                        </div>
                    </div>
                    <!-- singapore visa thumbs end -->
                </div>
            </div>
            <div class="slide">
                <h2 class="headprop_bot" style="text-align: center">Visa Services</h2>
                <div class="split"></div>
                <div class="wrapper" align="left">
                    <!-- visa types content start -->
                    <div class="home_content">
                        <h1>Get your visa in your hands just in 24hrs</h1>
                        <p>We are specialised to assist, submit and collect (where agents are allowed)<br> visa and consult for student and immigrant visas.</p>
                        <a href="<?= SITE_URL; ?>page.php?id=22" class="blinknew">Visa on Arrivals</a>
                        <a href="<?= SITE_URL; ?>visa_fees.php" class="blinknew">Visa Fees</a>
                        <a href="#" class="blinknew">Visa Status</a>
                        <a href="<?= SITE_URL; ?>page.php?id=3" class="blinknew">Docs Info</a>    
                    </div>
                    <!-- visa types content end -->
                    <!-- visa thumbs start -->
                    <div class="thumbs_d">
                        <div id="visa_thumb1">
                            <img src="imgs/malayesiasqpc1.jpg" alt="">
                        </div>
                        <div id="visa_thumb2">
                            <img src="imgs/malayesiasqpc2.jpg" alt="">
                        </div>
                        <div id="visa_thumb3">
                            <img src="imgs/malayesiasqpc3.jpg" alt="">
                        </div>
                    </div>
                    <!-- visa thumbs end -->
                </div>
            </div>
            <div class="slide">
                <div class="wrapper" align="left">
                    <!-- packages content start -->
                    <div id="pack_thumb1">
                        <h2>Holidays Packs</h2>
                        <div class="split"></div>
                        <div class="boxplan">
                            <div class="padprop">
                                <h3>Langkawi & Kuala Lumpur</h3>
                                <h4>4 Nights Langkawi - 2 Nights Kuala Lumpu <span><i class="icon fa-rupee"></i>35,450</span></h4>
                            </div>
                            <img src="imgs/malaysiapack.jpg">    
                            <div class="padpropnew">
                                <p>Langkawi and Kuala Lumpur are both parts of Malaysia, but these delight tourists in totally different ways. While the former immerses people...  <a href="#">Read More</a></p>
                            </div>
                        </div>
                        <a href="#" class="blink1new">More Packages...</a>
                    </div>
                    <!-- packages content end -->
                </div>
            </div>
            <?php include("includes/home_footer.php"); ?>
        </div>
    </body>
</html>

