<?php 
include("application.php");
include("includes/home_header.php"); 
$page = "login";
$hd = "Login";
@extract($_REQUEST);
if (isset($_POST['submit'])) {
    $msg = $login_obj->login();
}
?>
    <body>
    <div class="loader"></div>
    <div class="mouse">
        <div class="wheel"><img src="imgs/wheelicon.png"  alt="wheel"></div>
    </div>
    <div id="fsvs-body">
            <div class="slide" >
                <script src="js/device.min.js"></script> <!--OPTIONAL JQUERY PLUGIN-->
                <script src="js/jquery.mb.YTPlayer.js"></script>
                <script src="js/custom.js"></script>
                <section id="foot_video">
                    <a id="bgndVideo" class="player" data-property="{videoURL:'https://www.youtube.com/watch?v=0_7eRKz7x5k&feature=youtu.be',containment:'body',autoPlay:true, mute:true, startAt:0, opacity:1}"></a>
                    <div class="pattern"></div>
                    <div class="logo"><img src="<?= SITE_URL ?>imgs/logovd.png"></div>
                    <!-- navigation start -->
                    <div class="navigation">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li>
                                <a href="#">Visa</a>
                                <ul>
                                    <li><a href="<?= SITE_URL ?>page.php?id=22">Visa on Arrivals</a></li>
                                    <li><a href="<?= SITE_URL; ?>visa_fees.php">Visa Fees</a></li>
                                    <li><a href="#">Visa Status</a></li>
                                    <li><a href="<?= SITE_URL ?>page.php?id=3">Docs Info</a></li>
                                </ul>
                            </li>
                            <li><a href="<?= SITE_URL; ?>faqs.php">Faq's</a></li>
                            <li><a href="<?= SITE_URL ?>page.php?id=1">About us</a></li>
                            <li><a href="<?= SITE_URL; ?>contact_us.php">Contact us</a></li>
                            <li><a href="#">Apply Now</a></li>
                            <li><a href="<?= SITE_URL; ?>login.php">Login</a></li>
                        </ul>
                    </div>
                    <div class="callus">
                        <p>Support <i class="icon fa-phone"></i> +91-888-282-282-2 &nbsp;|&nbsp; <i class="icon fa-facebook"></i> <i class="icon fa-twitter"></i></p>
                    </div>
                    <!-- call us end -->
                    <div class="split"></div>
                    <!-- <h2 class="saia"><img src="imgs/malayesia.png" alt="malayesia"></h2> -->
                    <div class="wrapper">
                        <div class="ipage" style="background:#f2f2f2;width:80%;margin:auto;margin-top:10%;">
                            <h4 style="text-align:center;font-weight:600;padding-top:20px;">Login</h4>
                            <section class="login_sec">
                                <div class="row" style="width:100%;margin:auto;    padding-bottom: 5px;">
                                    <div class="col-md-offset-3 col-md-6">
                                        <div class="login_box form-horizontal">
                                            <?php if ($msg != '') { ?>
                                                <div class="alert label-success text-center"><?php echo $msg; ?></div>
                                            <?php } ?>
                                            <form style="width:50%;margin:auto;" method="post" name="data_form" action="" enctype="multipart/form-data">
                                                <div class="form-group">
                                                    <div class="col-md-12" style="margin-bottom:10px;">
                                                        <label style="text-align:left;font-size:small;">Email ID</label>
                                                        <input type="text" placeholder="Email Id" required name="u_emailid" class="form-control" style="border:0px;border-radius:0px;background:#fff;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label style="text-align:left;font-size:small;">Password</label>
                                                        <input type="password" required="required" placeholder="Password" required name="u_password" class="form-control" style="border:0px;border-radius:0px;background:#fff;">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <p style="text-align:left;color:red;font-size:12px;margin-bottom:0px;"><a href="<?php echo SITE_URL; ?>forgotPassword.php" style="color:red;">Forgot Password ?</a></p>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12" style="text-align:center;">
                                                        <button type="submit" name="submit" class="btn btn-warning" style="border-radius:0px;color:#fff;background:red;width:100px;height:40px;"><p style="padding-top:5px;font-size:15px;">Login</p></button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>
            </div>
            <?php include("includes/home_footer.php"); ?>
        </div>
    <!-- ipage content div end -->
    </body>
</html>