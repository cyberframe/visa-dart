<?php 
include("application.php");
include("includes/head_top.php"); 
?>
<?php 
$page = "page";
$id=$_REQUEST['id'];
$pagesql=" pag_id='".$id."'";
$page= $common_obj->fun_select("pages",$pagesql);
$hd =  $page[0]['pag_title'];
?>
<link rel="stylesheet" type="text/css" href="<?= SITE_URL.'css/accordian-css.css' ?>">
    <?php if($id == '3') { ?>
        <div class="topheadbar" id="headtopbg" style="background-image: url(imgs/visaraaivalsbg.jpg);">
    <?php }else{ ?>
        <div class="topheadbar" id="headtopbg" style="background-image: url('img/page_images/<?= $page[0]['page_image']; ?>');">
    <?php } ?>
        <?php include("includes/header.php"); ?>
        <div class="split"></div>
        <h1><?php echo $page[0]['pag_title'];?></h1>
    </div>
    <!-- head div end -->  
    <div class="breadcum">
        <div class="wrapper">
            <div class="currently"><strong>You are viewing:</strong> &nbsp; <i class="icon fa-home"></i> &nbsp;/&nbsp; <?php if($id == '3') { echo "Docs Info"; }else{ echo $page[0]['pag_title']; } ?></div>
        </div>
    </div>
    <div class="ipage">
        <?php if($id == '3') { ?>
            <div class="container">
                <div class="clearfix"></div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php 
                        $vtwhere=" is_delete='0' && vt_status=1";
                        $visat= $common_obj->fun_select("visa_type",$vtwhere);
                        if(count($visat)>0){
                            $i=1;
                            foreach($visat as $myvisat){ 
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading<?php echo $i;?>">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i;?>" aria-expanded="true" aria-controls="collapse<?php echo $i;?>">
                                    <?php 
                                        echo(strip_tags($myvisat['vt_title']));
                                    ?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse<?php echo $i;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $i;?>">
                            <div class="panel-body">
                                    <?php echo $myvisat['vt_document_required']; ?>
                            </div>
                        </div>
                    </div>
                    <?php $i++;  } } ?>
                </div>
            </div>
       <?php } else  { ?>
        <div class="wrapper">
            <?php echo $page[0]['pag_content'];?>
            <div class="split"></div>
        </div>
        <?php } ?>
    </div>
    <!-- ipage content div end -->
    <?php include("includes/footer.php"); ?>
    </body>
</html>
<script type="text/javascript">
$(document).ready(function() {
    $(".toggle-accordion").on("click", function() {
        var accordionId = $(this).attr("accordion-id"),
        numPanelOpen = $(accordionId + ' .collapse.in').length;
        $(this).toggleClass("active");
        if (numPanelOpen == 0) {
            openAllPanels(accordionId);
        } else {
            closeAllPanels(accordionId);
        }
    })
    openAllPanels = function(aId) {
        console.log("setAllPanelOpen");
        $(aId + ' .panel-collapse:not(".in")').collapse('show');
    }
    closeAllPanels = function(aId) {
        console.log("setAllPanelclose");
        $(aId + ' .panel-collapse.in').collapse('hide');
    }   
});
</script>