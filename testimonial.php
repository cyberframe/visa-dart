<?php 
include("application.php");
include("includes/head_top.php"); 
?>
<?php 
$page = "contact";
$hd = "Contact Us";
if(isset($_POST['btn']))
{
    if (strpos($_POST['email'], '}') !== false) 
    {
        $msg = "Invalid Email ID";
    }
    else
    {
                     
        $msg=$contactus_obj->fun_add();
       
    }
}
?>
    <div class="topheadbar" id="headtopbg" style="background-image: url(imgs/visaraaivalsbg.jpg);">
        <?php include("includes/header.php"); ?>
        <div class="split"></div>
        <h1>Testimonial</h1>
    </div>
    <div class="breadcum">
        <div class="wrapper">
            <div class="currently"><strong>You are viewing:</strong> &nbsp; <i class="icon fa-home"></i> &nbsp;/&nbsp; Testimonial</div>
        </div>
    </div>
    <div class="ipage">
        <section class="login_sec">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                    <form action="" id="data_form" method="post" enctype="multipart/form-data" class="form-horizontal form-box">
                        <div class="applicant_details form-horizontal">
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <p>( Please contact us for any of your query or Inquiry. )</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">Full Name <em>*</em>:</label>
                                    <input type="text"  name="name" id="name" value="<?php echo $_POST['name'];?>"  placeholder="Enter Full Name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">Email Id <em>*</em>:</label>
                                    <input type="email" name="email" id="email" value="<?php echo $_POST['email'];?>" required  placeholder="Enter Email Id" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">Mobile Number <em>*</em>:</label>
                                    <input type="tel" name="mobile" value="<?php echo $_POST['mobile'];?>" id="mobile"  placeholder="Enter Mobile Number" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">Message <em>*</em>:</label>
                                    <textarea required name="message"  id="message" rows="4" placeholder="Enter your message" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <button type="submit" name="btn" class="btn btn-default">Send</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                    <div class="col-md-4">
                        <p><b>Address:</b> <?php echo $contact_details['add_address'];?></p>
                        <br/>
                        <p><span><i class="fa fa-phone"></i></span> <?php echo $contact_details['add_phone'];?></p>
                        <p><span><i class="fa fa-envelope"></i></span> <?php echo $contact_details['add_email'];?></p>
                    </div>
                </div>
            </div>
        </section>
        <section class="location-map-area">
            <div class="container">
                <h3><span>Find us on Google Map</span></h3>
                <iframe  class="location-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8154.191783818107!2d76.78782692388636!3d30.76292091282837!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390fed0be66ec96b%3A0xa5ff67f9527319fe!2sChandigarh!5e0!3m2!1sen!2sin!4v1505625689238" frameborder="0" allowfullscreen></iframe>
            </div>
        </section>
    </div>
    <!-- ipage content div end -->
    <?php include("includes/footer.php"); ?>
    </body>
</html>