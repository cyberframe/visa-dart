<?php 
include("application.php");
include("includes/head_top.php"); 
?>
<?php 
$page = "page";
$hd = "Visa Type";
$cond = " vt_status=1 && is_delete=0";
$rows = $common_obj->fun_select("visa_type", $cond);
?>
<style>
.visa_fee_sec {
    background: #FEECF5;
    margin-bottom: 20px;
    box-shadow: 0 2px 5px 0px #aaa;
    padding: 20px 0;
}
</style>
<link rel="stylesheet" type="text/css" href="<?= SITE_URL.'css/accordian-css.css' ?>">
    <div class="topheadbar" id="headtopbg" style="background-image: url(imgs/visaraaivalsbg.jpg);">
        <?php include("includes/header.php"); ?>
        <div class="split"></div>
        <h1>VISA FEES</h1>
    </div>
    <!-- head div end -->  
    <div class="breadcum">
        <div class="wrapper">
            <div class="currently"><strong>You are viewing:</strong> &nbsp; <i class="icon fa-home"></i> &nbsp;/&nbsp; VISA FEES</div>
        </div>
    </div>
    <div class="ipage">
        <div class="container">
            <div class="clearfix"></div>
            <?php
                foreach ($rows as $row) {
                    $name_url = str_replace(" ", "_", $row['vt_title']);
                    $name_url = str_replace("-", "_", $name_url);
                    $name_url = str_replace("__", "_", $name_url);
                    $name_url = str_replace("__", "_", $name_url);
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="visa_fee_sec">
                        <div class="row">
                            <div class="col-sm-8 col-md-9">
                                <div class="list-wrapper">
                                    <div class="col-sm-12">
                                        <h3><?php echo $row['vt_title']; ?></h3>
                                        <p><?php echo substr($row['vt_long_description'], 0, 300); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-3">
                                <div class="visa_pricing">
                                    <h3><i class="contact-info"><i class="fa fa-rupee" ></i> <?php echo $row['vt_price']; ?></i></h3>
                                    <a href="<?php echo SITE_URL; ?>apply.php" class="btn btn-default">Apply Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <!-- ipage content div end -->
    <?php include("includes/footer.php"); ?>
    </body>
</html>